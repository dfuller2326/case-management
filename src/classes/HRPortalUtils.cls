/**
 * Class: HRPortalUtils
 * Purpose: Define all utility methods needed for HR portal here.
 */
public with sharing class HRPortalUtils {
	
  //variables to store the new hire days and lowest executive grade. Because these are settings values
  //that doesnt change for the life of a transaction, we use singleton design pattern here 
  integer newHireDays;
  integer lowestExecutiveGrade;
  Set<String> businessCountries;
  
  Set<String> integrationProfileIds;
  
   //Note: this is the static handle to the single class reference.
  //Its completely hidden to outside world. PRIVATE access modifier used  
  private static HRPortalUtils self;
   
  // NOTE: the constructor is marked PRIVATE
  // so that no Apex code outside this class can create the instance
  private HRPortalUtils() {
    // Do all the non DML initalization here
    // set the value of lowest executive grade and new hire days here
    List<String> lowestGrades=getPortalPropertyValues(BiogenConstants.LOWEST_EXECUTIVE_GRADE);
    lowestExecutiveGrade=Integer.valueOf(lowestGrades.get(0));

    List<String> hireDays=getPortalPropertyValues(BiogenConstants.NEW_HIRE_CONDITION);
    newHireDays=Integer.valueOf(hireDays[0]);
    
    businessCountries=new Set<String>();
    businessCountries.addAll(getPortalPropertyValues(BiogenConstants.BIOGEN_BUSINESS_COUNTRY));
    
    integrationProfileIds=new Set<String>();
    if(getPortalPropertySFIdMap(BiogenConstants.INTEGRATION_PROFILE)!=null){
       integrationProfileIds.addAll(getPortalPropertySFIdMap(BiogenConstants.INTEGRATION_PROFILE).values());
    }
  }
   
  /*
    This static method will be the only way to get the Single
    instance of this class.
  */
  public static HRPortalUtils getInstance() {
    // "self" is NOT NULL, that means we already did
    // required initalization, so just return.
    if (self != null) return self;
       
      // Create the single instance now.
      self = new HRPortalUtils();
       
    
       
      return self;
  } 
   
  // ..
  // ....
  // Rest of the code can be instance level methods that
  // provide access through the single handle
   
  public Integer getNewHireDays() {
     return newHireDays;
  }
 
  public Integer getLowestExecutiveGrade() {
     return lowestExecutiveGrade;
  }

  public Set<String> getBiogenBusinessCountries(){
    return businessCountries;
  }
  
  public Set<String> getIntegrationProfileIds(){
    return integrationProfileIds;
  }

  
  public static String constructErrorString(Database.Upsertresult ur, Database.Saveresult sr, Database.DeleteResult dr, Object obj)
    {
    	    String str = '';
        
        if(sr!=null){
	        for(Database.Error err : sr.getErrors()) {       
	            System.debug(err.getStatusCode() + ': ' + err.getMessage());
	            System.debug('Fields that affected this error: ' + err.getFields());
	            str+='\n\n*******************************************************\n\n';
	            str+= err.getStatusCode() + ': ' + err.getMessage();
	            str+= '\Fields that affected this error: ' + err.getFields();
	            str+= '\nFields that affected this error: ' + err.getFields();
	            str+= '\nObject that affected this error: ' + obj;
	        }
        }
        
        if(ur!=null){
	        for(Database.Error err : ur.getErrors()) {       
	          
	            System.debug(err.getStatusCode() + ': ' + err.getMessage());
	            System.debug('Fields that affected this error: ' + err.getFields());
	            str+='\n\n*******************************************************\n\n';
	            str+= err.getStatusCode() + ': ' + err.getMessage();
	            str+= '\nFields that affected this error: ' + err.getFields();
	            str+= '\nObject that affected this error: ' + obj;
	            
	        }
        }

       if(dr!=null){
	        for(Database.Error err : dr.getErrors()) {       
	          
	            System.debug(err.getStatusCode() + ': ' + err.getMessage());
	            System.debug('Fields that affected this error: ' + err.getFields());
	            str+='\n\n*******************************************************\n\n';
	            str+= err.getStatusCode() + ': ' + err.getMessage();
	            str+= '\nFields that affected this error: ' + err.getFields();
	            str+= '\nObject that affected this error: ' + obj;
	            
	        }
        }
        
        return str;
           
    }


 public static void emailError(String sub, String errorString)
    {
    	    Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();  
        String[] toAddresses = new String[] {getErrorEmailAddress()};  
        mail.setToAddresses(toAddresses);  
        mail.setReplyTo('donotreply@salesforce.com');  
        mail.setSenderDisplayName('Salesforce Exception');  
        mail.setSubject(sub+' Org: ' + UserInfo.getOrganizationName());  
        mail.setPlainTextBody(errorString);  
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
           
    }

     


  /**
	 * Method:getProfilesofInterest
	 * Purpose: get all the values of interested profile ids that needs to be worked by the trigger. Any other profile id is left 
	 * untouched by the trigger.
	 */
  

	public static Map<String,String> getProfilesofInterest(){
       Map<String,String> profileSFIdMap=new Map<String,String>();

       try{
         profileSFIdMap=getPortalPropertySFIdMap(BiogenConstants.INTERESTED_PROFILE);
         profileSFIdMap.putAll(getPortalPropertySFIdMap(BiogenConstants.INTEGRATION_PROFILE));
       }
       catch( PropertyUndefinedException undefinedEx){
 
       }
       catch(Exception e){
 
       } 
       return profileSFIdMap;
  }
  
   

  public static Map<String,String> getPermSetsMap(){
       Map<String,String> permSetSFIdMap=new Map<String,String>();

       try{
         permSetSFIdMap=getPortalPropertySFIdMap(BiogenConstants.PERMSET_ENTITY_TYPE);
       }
       catch( PropertyUndefinedException undefinedEx){
 
       }
       catch(Exception e){
 
       } 
       return permSetSFIdMap;
  }

    
  public static String getErrorEmailAddress(){
  	 List<String> errorEmailAddress=getPortalPropertyValues(BiogenConstants.ERROR_EMAIL);
  	 if(errorEmailAddress!=null && errorEmailAddress.size()==1){
  	 	return errorEmailAddress.get(0);
  	 }
  	 else{
  	 	return 'pramaswamy@salesforce.com';
  	 }
  }

  public static List<String> getPortalPropertyValues(String entityType){
      List<String> portalPropertyNames=new List<String>();

      for(HR_Portal_Property__c portalProperty:[  select Name 
                                                from HR_Portal_Property__c
                                                where Entity_Type__c =:entityType
                                               ]){
          if(portalProperty!=null){
             portalPropertyNames.add(portalProperty.Name);
          }
      }
      /*Added 'Executive Assistant to an Executive Officer' value to JOB_TITLE_FOR_EXEC here in code because of 
      System limitation regarding Custom settings name length*/
      if(entityType == 'JOB_TITLE_FOR_EXEC')
      	  portalPropertyNames.add('Executive Assistant to an Executive Officer');
      	  
      System.debug(entityType + ' : test : '+portalPropertyNames);
      return portalPropertyNames;
  }

  private static Map<String,String> getPortalPropertySFIdMap(String entityType){
      List<String> portalPropertyNames=getPortalPropertyValues(entityType);
      return getSalesforceIdsBulk(portalPropertyNames,entityType);
  }

   
  private static Map<String,String> getProfileIdsBulk(List<String> profileNames){
        
        Map<String,String> profileIdMap=new Map<String,String>();
        for(Profile profileInstance:[select Id, Name from Profile where Name=:profileNames]){
              profileIdMap.put(profileInstance.Name,profileInstance.Id);
        }
        return profileIdMap;
  }

  private static Map<String,String> getPermSetIdsBulk(List<String> permSetNames){
      
        Map<String,String> permSetIdMap=new Map<String,String>();
      
        if(permSetNames==null || permSetNames.size()==0){
          for(PermissionSet permSetInstance:[select Id, Name from PermissionSet]){
              permSetIdMap.put(permSetInstance.Name,permSetInstance.Id);
          }
        }
        else{
           for(PermissionSet permSetInstance:[select Id, Name from PermissionSet where Name=:permSetNames]){
              permSetIdMap.put(permSetInstance.Name,permSetInstance.Id);
           }
        }
        return permSetIdMap;
  }

  public static Set<String> constructPermSetNames(User userInstance){
      Set<String> biogenBusinessCountries=HRPortalUtils.getInstance().getBiogenBusinessCountries();
      Set<String> permSetNames=new Set<String>();
      //variable to store the permission set name
      String permSetName='';
      
      //Special case for Denmark, where visibility of articles need to be controlled by location as well
      if(userInstance.Country == 'Denmark' && userInstance.Location__c != null && userInstance.Location__c != ''){
      	permSetnames.add(constructLocationSpecificPermSetname(userInstance));	
      } 
      else
      	permSetNames.add(constructCountrySpecificPermSetName(userInstance));

      //construct the global perm set name
      //Update: 8/18/14. Manish. If user is from HR department, they should always get Global HRforHR Article visibility, even if their profile is different
      if (userInstance!=null && userInstance.Division__c!=null && userInstance.Division__c.contains(BiogenConstants.HUMAN_RESOURCES))
		permSetName=BiogenConstants.BIOGEN_GLOBAL + ' ' + BiogenConstants.HR4HR_PROFILE + ' ' + BiogenConstants.PERMSET_SUFFIX;
	  else
      	permSetName=BiogenConstants.BIOGEN_GLOBAL + ' ' + userInstance.Profile_Name__c + ' ' + BiogenConstants.PERMSET_SUFFIX;
      	
      permSetName=permSetName.replaceAll(' ','_');
      permSetNames.add(permSetName);

      return permSetNames;
  }
  
  public static string constructLocationSpecificPermSetName(User userInstance){
  	  //construct perm set name
      String permSetName=userInstance.Location__c+' ' + userInstance.Profile_Name__c + ' ' + BiogenConstants.PERMSET_SUFFIX;
      permSetName=permSetName.replaceAll('-','_');
      permSetName=permSetName.replaceAll(' ','_');
      
      return permSetName;
      
  }
  
  public static string constructCountrySpecificPermSetName(User userInstance){
  	  Set<String> biogenBusinessCountries=HRPortalUtils.getInstance().getBiogenBusinessCountries();
      String permSetCountry=(biogenBusinessCountries!=null && biogenBusinessCountries.contains(userInstance.country))?userInstance.country:BiogenConstants.DEFAULT_COUNTRY;
      //construct perm set name
      String permSetName=permSetCountry+' ' + userInstance.Profile_Name__c + ' ' + BiogenConstants.PERMSET_SUFFIX;
      permSetName=permSetName.replaceAll(' ','_');
      
      return permSetName;
      
  }

  public static Map<String,String> getSalesforceIdsBulk(List<String> SFPropertyNames, String entityType){
      if(  BiogenConstants.PROFILE_ENTITY_TYPE.equalsIgnoreCase(entityType) 
         || BiogenConstants.INTERESTED_PROFILE.equalsIgnoreCase(entityType)
         || BiogenConstants.INTEGRATION_PROFILE.equalsIgnoreCase(entityType)
         ){
         return getProfileIdsBulk(SFPropertyNames);
       }

       else if(BiogenConstants.PERMSET_ENTITY_TYPE.equalsIgnoreCase(entityType)){
          return getPermSetIdsBulk(SFPropertyNames);
       }

        else{
          throw new PropertyUndefinedException('No property value defined for ' + SFPropertyNames + ' of entity type '+ entityType);
       }
  }
  
  public static PermissionSetAssignment isPermSetExisting(Set<PermissionSetAssignment> permSetAssignSet,String assigneeId,String PermissionSetId){
  	if(permSetAssignSet!=null){
	    for(PermissionSetAssignment permSetAssign:permSetAssignSet){
	    	  System.debug('===permSetAssign.AssigneeId====' + permSetAssign.AssigneeId);
	    	  System.debug('===permSetAssign.PermissionSetId====' + permSetAssign.PermissionSetId);
	    	  System.debug('===assigneeId====' + assigneeId);
	    	  System.debug('===PermissionSetId====' + PermissionSetId);
	    	  
	    	  if (permSetAssign!=null && permSetAssign.AssigneeId==assigneeId && permSetAssign.PermissionSetId==PermissionSetId){
	    	  	 System.debug('===Inside if statement=====');
	    	  	 return permSetAssign;
	    	  }
	    } 
  	}
    return null;
  }
  
  public static Map<String,Set<PermissionSetAssignment>> getCurrentPermSetAssignments(Set<id> assigneeIds){
  	  Map<String,Set<PermissionSetAssignment>> assigneeMap=new Map<String,Set<PermissionSetAssignment>>();
  	  
  	  for(PermissionSetAssignment permSetAssign:[select   Id
  	                                                    , AssigneeId
  	                                                    , PermissionSetId
  	                                                    , PermissionSet.IsOwnedByProfile
  	                                             from PermissionSetAssignment
  	                                             where AssigneeId in :assigneeIds
  	                                             and PermissionSet.IsOwnedByProfile=false
  	                                            ]){
  	         if(assigneeMap.get(permSetAssign.AssigneeId)!=null){
  	         	assigneeMap.get(permSetAssign.AssigneeId).add(permSetAssign);
  	         }                                           	
  	         else{
  	         	assigneeMap.put(permSetAssign.AssigneeId,new Set<PermissionSetAssignment>{permSetAssign});
  	         }
  	  }
  	  return assigneeMap;
  }
	 
}