public with sharing class HRP_ArticleDetailController {
	public SObject article {get;set;}

	public HRP_ArticleDetailController() {
		String userLang = UserInfo.getLanguage();
		List<DescribeFieldResult> dfrList = new List<DescribeFieldResult>();
		String articleId = ApexPages.currentPage().getParameters().get('id');
		Schema.SObjectType sobjType = Id.valueOf(articleId).getSObjectType();
		Map<String, Schema.SObjectField> sobjFields = sobjType.getDescribe().fields.getMap();
		for(Schema.SObjectField sobjField : sobjFields.values()) {
			Schema.DescribeFieldResult dfr = sobjField.getDescribe();
			dfrList.add(dfr);
		}
		String sobjTypeNew = sobjType.getDescribe().getName().replaceAll('__ka', '__kav');
		article = sobjType.newSObject();
		String query = 'SELECT Id, Title, Body__c FROM ' + sobjTypeNew + ' WHERE KnowledgeArticleId = :articleId' + ' AND PublishStatus = \'Online\' AND Language = \'en_US\'' + ' LIMIT 1';
		try{
			article = (SObject)Database.query(query);
		}
		catch (System.QueryException ex) {
			System.debug(LoggingLevel.ERROR, 'Error retrieving searched article -- ' + ex.getMessage());
		}
	}

	// Wrapper class for ViewAll Response
	public class ArticleDetailResponse {
	    public HRP_ErrorInfo error {get; set;}
	}
}