/** 
* Class: DeleteOldVotes 
* Purpose: Use during the 5-star Article Rating enhancement for HRConnect Articles to delete all the older votes. Do not use it other time 
*/
global class DeleteOldVotes implements Database.Batchable<sObject>, Database.Stateful{
    List<Vote> lstVotes;
    string[] deletedVotes;
    global final String Query;
    
    global DeleteOldVotes(){
        Query = 'SELECT Id, ArticleNumber FROM KnowledgeArticle';
        lstVotes = new List<Vote>();
        String header = 'CreatedById, CreatedDate, Id, ParentId, ArticleNumber, Type';
        deletedVotes = new String[0];
        deletedVotes.add(header);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        for(Sobject s : scope){
            KnowledgeArticle ka = (KnowledgeArticle)s;
            List<Vote> votes = [SELECT CreatedById, CreatedDate, Id, ParentId, Type FROM Vote WHERE ParentId = :ka.Id];
            for(Vote v : votes){
                lstVotes.add(v);
                deletedVotes.add('"' + v.CreatedById + '","' + v.CreatedDate + '","' + v.Id + '","' + v.ParentId + '","' + ka.ArticleNumber + '","' + v.Type + '"');
            }
        }
        try{
            Database.delete(lstVotes, false);
        }catch(Exception e){
            System.debug('Exception: '+e.getMessage());
        }
        System.debug('In Execute: '+deletedVotes);
        System.debug('In Execute: '+lstVotes);
        
    }

    global void finish(Database.BatchableContext BC){
        System.debug('In Finish: '+deletedVotes);
        System.debug('In Finish: '+lstVotes);
        //Send an email to the User after your batch completes
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'manish.shrestha@biogen.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Apex Batch Job has completed.');
        mail.setHtmlBody('The Batch Apex job has Completed. <br/><br/>' + 
                         '<b>' + lstVotes.size() + '</b> votes were deleted from the System. <br/>' +
                         'Please find the attached document with all the votes that were deleted.'
                         );
        
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        Blob csvBlob = Blob.valueOf(String.join(deletedVotes, '\n'));
        string csvname= 'ArticleVotes_Deleted.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}