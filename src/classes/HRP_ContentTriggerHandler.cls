public with sharing class HRP_ContentTriggerHandler {
	public HRP_ContentTriggerHandler() {}
  public void OnBeforeInsert(List<SObject> newSObjs){
    updateFieldValueToDefault(newSObjs, HRP_Constants.HR_COUNTRIES_NAME, HRP_Constants.HR_DEFAULT_COUNTRY);
    updateFieldValueToDefault(newSObjs, HRP_Constants.HR_PROFILE_NAME, HRP_Constants.HR_DEFAULT_PROFILE);
  }

  public void OnBeforeUpdate(List<SObject> newSObjs, List<SObject> oldSObjs, Map<ID, SObject> oldSObjMap, Map<ID, SObject> newSObjMap){
    updateFieldValueToDefault(newSObjs, HRP_Constants.HR_COUNTRIES_NAME, HRP_Constants.HR_DEFAULT_COUNTRY);
    updateFieldValueToDefault(newSObjs, HRP_Constants.HR_PROFILE_NAME, HRP_Constants.HR_DEFAULT_PROFILE);
  }

  private void updateFieldValueToDefault(List<SObject> sObjs, String fieldName, String defaultValue){
    for(SObject sObj : sObjs){
      String fieldValue = String.valueOf(sObj.get(fieldName));
      if(String.isBlank(fieldValue)) continue;
      sObj.put(fieldName, fieldValue.containsIgnoreCase(defaultValue) ? defaultValue : fieldValue);
      system.debug('sObj==='+sObj);
    }
  }
}