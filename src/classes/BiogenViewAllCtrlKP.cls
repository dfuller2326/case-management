public with sharing class BiogenViewAllCtrlKP {
    public BiogenViewAllCtrlKP() {
        
    }

    @RemoteAction
    public static String getViewAllData(Integer offset) {
        // sample image src list
        List<String> images = new List<String> {'http://www.comitydesigns.com/wp/wp-content/uploads/2014/10/concur.png', 
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2010/12/credo-mobile.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2010/12/docusign.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2013/10/evault.png',
                                                'http://comitydesigns.com/wp/wp-content/uploads/2011/12/Fusion-io.jpg',
                                                'http://comitydesigns.com/wp/wp-content/uploads/2011/12/Keynote_Logo_R.png',
                                                'http://comitydesigns.com/wp/wp-content/uploads/2010/12/ribbit.png',
                                                'http://comitydesigns.com/wp/wp-content/uploads/2010/12/pros.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2014/10/ahm.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2010/12/arcsight.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2010/12/big-machines.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2014/10/box.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2010/12/canon.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2013/10/veeva1.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2010/12/vovici.png',
                                                'http://comitydesigns.com/wp/wp-content/uploads/2009/07/conviva-logo.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2013/10/crc-health-group.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2010/12/deloitte.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2010/12/demandtec.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2010/12/dubmenow.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2013/10/evault.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2013/10/evernote.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2010/12/foxt.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2010/12/harley-davidson.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2013/10/ncr.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2014/10/service-source.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2013/10/twitter.png',
                                                'http://www.comitydesigns.com/wp/wp-content/uploads/2010/12/nvidia.png'};
        
        ViewAllResponse respObj = new ViewAllResponse();
        try {
            respObj.bWrappers = new List<BiogenWrapper>();

            for(Integer i=0; i<offset; i++) {
                BiogenWrapper bWrap = new BiogenWrapper();
                bWrap.imgSrc = images[i];
                bWrap.title = 'Some Title ' + (i+1);
                bWrap.description = 'Some Description ' + (i+1);

                respObj.bWrappers.add(bWrap);
            }
        } catch(Exception ex) {
            respObj.error = new ErrorInfo('Exception-1', ex.getMessage());
        }

        return JSON.serialize(respObj);
    }

    private class ViewAllResponse {
        public ErrorInfo error {get; set;}
        public List<BiogenWrapper> bWrappers {get; set;}
    }

    private class BiogenWrapper {
        public String imgSrc {get; set;}
        public String title {get; set;}
        public String description {get; set;}
    }

    private class ErrorInfo {
        public String errCode {get;set;}
        public String errMsg {get;set;}
        public ErrorInfo(String ec, String em){
            errCode = (String.isNotBlank(ec)) ? ec : '';
            errMsg = (String.isNotBlank(em)) ? em : '';
        }
    }  
}