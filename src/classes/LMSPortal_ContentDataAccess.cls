public with sharing class LMSPortal_ContentDataAccess {
	
	public static List<LMS_Content__c> getLMSContent(Boolean isNewContent, String selectedDate, String businessUnit, String divisionVal){
		String queryString = 'SELECT Id, Description__c, Summary__c, Title__c, Banner_Document_Id__c FROM LMS_Content__c WHERE New_Content__c = ' + isNewContent;
		if(selectedDate != null){
			queryString += ' AND Start_Date__c = ' + selectedDate;
		}
		if(businessUnit != null){
			queryString += ' AND Business_Unit__c = ' + businessUnit;
		}
		if(divisionVal != null){
			queryString += ' AND Division__c = ' + divisionVal;
		}
		List<LMS_Content__c> lmsContentList = Database.query(queryString);
		//[ WHERE New_Content__c =: true];
		return lmsContentList;
	}

	public static List<LMS_Content__c> getLearningContentForIds(Set<String> learningContentIds)
	{
		List<LMS_Content__c> lmsContentList = new List<LMS_Content__c>();
		String queryString = 'SELECT Id, Description__c, Summary__c, Title__c, Banner_Document_Id__c, ' + 
								'Business_Unit__c, Category__c, Content_Type__c, Division__c, End_Date__c, ' + 
								'External_Media_URL__c, Functional_Competency__c, Leadership_Competency__c, ' +
								'Location__c, New_Content__c, Start_Date__c, Status__c, Sub_Category__c, ' +
								'Sub_Functional_Competencies__c, Target_Window__c ' +   
								'FROM LMS_Content__c';
		queryString += ' WHERE Id IN :learningContentIds';
		lmsContentList = Database.query(queryString);
		return lmsContentList;
	}
}