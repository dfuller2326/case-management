@isTest
private class UserTriggerUtilsTest {
     
	
	@isTest static void testManagerCases() {
		// Implement test code
		// create employee test users. Create 200 users and test the user related triggers.
		
		PrepTestDataUtils.initialize();
		PrepTestDataUtils.populateInterestedProfiles();
		Test.startTest(); 
		List<User> managerUsers=PrepTestDataUtils.createBulkUsers(BiogenConstants.MANAGER_PROFILE, 10);
		Test.stopTest();
		//query the user records to fetch the updated values after the trigger
		Map<Id,User> managerUserMap=PrepTestDataUtils.queryTestClassUsers();
        //validate profile assignment
        System.assertEquals(true,PrepTestDataUtils.validateProfileAssignment(managerUserMap.values(), BiogenConstants.MANAGER_PROFILE));
        //validate contact creation
        System.assertEquals(true,PrepTestDataUtils.validateContactCreation(managerUserMap.values()));
        //validate permission set assignment
        System.assertEquals(true,PrepTestDataUtils.validatePermSetAssignment(managerUserMap));
        //validate public group assignment
        System.assertEquals(true,PrepTestDataUtils.validatePublicGroupAssignment(managerUserMap.keySet()));
        
	}
	
	@isTest static void testHRUpdateCases() {
		// Implement test code
	   
	    PrepTestDataUtils.initialize();
		PrepTestDataUtils.populateInterestedProfiles();
		Test.startTest(); 
		List<User> employeeUsers=PrepTestDataUtils.createBulkUsers(BiogenConstants.EMPLOYEE_PROFILE, 10);
        //update the division to Human Resources
        for(User userInstance:employeeUsers){
        	userInstance.Grade__c=55;
        }

        update employeeUsers;

		Test.stopTest();
		//query the user records to fetch the updated values after the trigger
		Map<Id,User> hrUserMap=PrepTestDataUtils.queryTestClassUsers();
        //validate profile assignment
        System.assertEquals(true,PrepTestDataUtils.validateProfileAssignment(hrUserMap.values(), BiogenConstants.EXECUTIVE_PROFILE));
        //validate contact creation
        System.assertEquals(true,PrepTestDataUtils.validateContactCreation(hrUserMap.values()));
        //validate permission set assignment
        System.assertEquals(true,PrepTestDataUtils.validatePermSetAssignment(hrUserMap));
        //validate public group assignment
        System.assertEquals(true,PrepTestDataUtils.validatePublicGroupAssignment(hrUserMap.keySet()));
	}

	@isTest static void testHRCases() {
		// Implement test code
	   
	    PrepTestDataUtils.initialize();
		PrepTestDataUtils.populateInterestedProfiles();
		Test.startTest(); 
		List<User> hrUsers=PrepTestDataUtils.createBulkUsers(BiogenConstants.HR4HR_PROFILE, 10);
        
		Test.stopTest();
		//query the user records to fetch the updated values after the trigger
		Map<Id,User> hrUserMap=PrepTestDataUtils.queryTestClassUsers();
        //validate profile assignment
        System.assertEquals(true,PrepTestDataUtils.validateProfileAssignment(hrUserMap.values(), BiogenConstants.HR4HR_PROFILE));
        //validate contact creation
        System.assertEquals(true,PrepTestDataUtils.validateContactCreation(hrUserMap.values()));
        //validate permission set assignment
        System.assertEquals(true,PrepTestDataUtils.validatePermSetAssignment(hrUserMap));
        //validate public group assignment
        System.assertEquals(true,PrepTestDataUtils.validatePublicGroupAssignment(hrUserMap.keySet()));
	}

	@isTest static void testEmployeeCases() {
		// Implement test code
	   
	    PrepTestDataUtils.initialize();
		PrepTestDataUtils.populateInterestedProfiles();
		Test.startTest(); 
		List<User> hrUsers=PrepTestDataUtils.createBulkUsers(BiogenConstants.EMPLOYEE_PROFILE, 10);
        
		Test.stopTest();
		//query the user records to fetch the updated values after the trigger
		Map<Id,User> hrUserMap=PrepTestDataUtils.queryTestClassUsers();
        //validate profile assignment
        System.assertEquals(true,PrepTestDataUtils.validateProfileAssignment(hrUserMap.values(), BiogenConstants.EMPLOYEE_PROFILE));
        //validate contact creation
        System.assertEquals(true,PrepTestDataUtils.validateContactCreation(hrUserMap.values()));
        //validate permission set assignment
        System.assertEquals(true,PrepTestDataUtils.validatePermSetAssignment(hrUserMap));
        //validate public group assignment
        System.assertEquals(true,PrepTestDataUtils.validatePublicGroupAssignment(hrUserMap.keySet()));
	}

	@isTest static void testNullHireDate() {
		// Implement test code
		// although the users are created with new hire profile. They should be updated to regular once the hire date is set to null
	   
	    PrepTestDataUtils.initialize();
		PrepTestDataUtils.populateInterestedProfiles();
		Test.startTest(); 
		List<User> execUsers=PrepTestDataUtils.createBulkUsers(BiogenConstants.NEWLY_HIRED + BiogenConstants.EXECUTIVE_PROFILE, 10);

		List<User> nullHireDateUsers=new List<User>();
		//update the hire date to null for these users. that should set employee as the profile
		
        
		Test.stopTest();
		//query the user records to fetch the updated values after the trigger
		Map<Id,User> employeeUserMap=PrepTestDataUtils.queryTestClassUsers();
        //validate profile assignment
        System.assertEquals(true,PrepTestDataUtils.validateProfileAssignment(employeeUserMap.values(),BiogenConstants.NEWLY_HIRED + BiogenConstants.EXECUTIVE_PROFILE));
        //validate contact creation
        System.assertEquals(true,PrepTestDataUtils.validateContactCreation(employeeUserMap.values()));
        //validate permission set assignment
        System.assertEquals(true,PrepTestDataUtils.validatePermSetAssignment(employeeUserMap));
        //validate public group assignment
        System.assertEquals(true,PrepTestDataUtils.validatePublicGroupAssignment(employeeUserMap.keySet()));

        for(User userInstance:execUsers){
			userInstance.Hire_Date__c=null;
			nullHireDateUsers.add(userInstance);
		}
		update nullHireDateUsers;

		//query the user records to fetch the updated values after the trigger
		employeeUserMap=PrepTestDataUtils.queryTestClassUsers();
        //validate profile assignment
        System.assertEquals(true,PrepTestDataUtils.validateProfileAssignment(employeeUserMap.values(), BiogenConstants.EXECUTIVE_PROFILE));

	}

	@isTest static void testSpecificPermSetFetch() {
		// Implement test code
		// although the users are created with new hire profile. They should be updated to regular once the hire date is set to null
	   
	    PrepTestDataUtils.initialize();
		PrepTestDataUtils.populateInterestedProfiles();
		System.runAs(new User(Id = Userinfo.getUserId())){
			HR_Portal_Property__c profilePortalProperty=new HR_Portal_Property__c(Name='Spain_Employee_Article_Visibility', Entity_Type__c=BiogenConstants.PERMSET_ENTITY_TYPE);
			insert profilePortalProperty;
		}
		Test.startTest(); 
		
		Map<String,String> permSetMap=HRPortalUtils.getPermSetsMap();
        //omly the perm set corresponding to US employee should be fetched.
		System.assertEquals(1,permSetMap.size());

        for(String permSetName:permSetMap.keySet()){
        	System.assertEquals('Spain_Employee_Article_Visibility',permSetName);
        }

	}

	@isTest static void testInvalidEntityType() {
		// Implement test code
		// although the users are created with new hire profile. They should be updated to regular once the hire date is set to null
	   
	    PrepTestDataUtils.initialize();
		PrepTestDataUtils.populateInterestedProfiles();
		Test.startTest(); 
		try{
		  Map<String,String> permSetMap=HRPortalUtils.getSalesforceIdsBulk(new List<String> {'Spain_Employee_Article_Visibility'}, 'Test 123');
        }

        catch(Exception e){
           System.assertEquals('No property value defined for (Spain_Employee_Article_Visibility) of entity type Test 123', e.getMessage());
		}

	}        
	
	 
	
}