public with sharing class LMSPortal_Util {
	
	public static String constructSearchCriteria(String searchKeyword){
		String wClause = '';
		List<String> conditions = new List<String>();
		Map<String, List<String>> mpConditions = new Map<String, List<String>>();
		Map<String, Schema.SObjectField> lcFieldsMap = Schema.SObjectType.LMS_Content__c.fields.getMap();
		
		//loop throught LMS_Content__c fields to get the type
		for(String fld : lcFieldsMap.keySet()){
			Schema.SObjectField field = lcFieldsMap.get(fld);
			Schema.DisplayType fldType = field.getDescribe().getType();
			if(String.valueOf(fldType) == 'PICKLIST' || String.valueOf(fldType) == 'STRING'){
				conditions.add( field + ' LIKE ' + '\'%' + searchKeyword + '%\'');
			}
			if(String.valueOf(fldType) == 'TEXTAREA' && field.getDescribe().getLength() <= 255){
				conditions.add( field + ' LIKE ' + '\'%' + searchKeyword + '%\'');
			}
			if(String.valueOf(fldType) == 'MULTIPICKLIST'){
				Schema.DescribeFieldResult fieldResult = field.getDescribe();
				List<Schema.PicklistEntry> pleList = fieldResult.getPicklistValues();
				for(Schema.PicklistEntry f : pleList){
					List<String> mplistForKey = mpConditions.get(fld);
					if (mplistForKey == null) {
				        mplistForKey = new List<String>();
				        mpConditions.put(fld, mplistForKey);
				    }
				    if(f.getValue().toLowerCase().contains(searchKeyword.toLowerCase())){
				   		mplistForKey.add(f.getValue());
				    }
				}
			}
		}
		
		if(conditions != null && !conditions.isEmpty()) {
            wClause +=  conditions[0];
            for (Integer i = 1; i < conditions.size(); i++) {
                wClause += ' OR ' + conditions[i];
            }
        }
        
        if(mpConditions != null && !mpConditions.isEmpty()) {
            for (String mpconkey : mpConditions.keySet()) {
            	if(mpConditions.get(mpconkey) != null && !mpConditions.get(mpconkey).isEmpty()){
            		String includeFields = '';
            		for(String inc : mpConditions.get(mpconkey)){
            			includeFields += '\'' + inc + '\' , ';
            		}
            		includeFields = includeFields.substring(0, includeFields.length() - 2);
 					includeFields = ' ( ' + includeFields + ' ) ';
            		if(!wClause.contains('OR')){
		        		//wClause += ' OR ';
		        	}
		        	else{
                		wClause += ' OR ' + mpconkey + ' INCLUDES ' + includeFields;
		        	}
            	}
            }
        }
		wClause = ' (' + wClause + ') ';
		return wClause;
	}
	
	public static List<LMS_Content__c> searchWithFilters(String previewparamJson, String sKeyword, String sFields, Integer recordLimit){
		
		PreviewWrapper previewParam = getPreviewParam(previewParamJson); 
		String sClause = '';
		List<LMS_Content__c> lmsList = new List<LMS_Content__c>();
		
		// apply preview params
	        if(previewParam != null) {
	        	System.debug('PREVIEW PARAM NOT NULL');
	            Date effectiveDate = normalizeDate(previewParam.effectiveDate);
	            System.debug('EFFECTIVE DATE :: ' + effectiveDate);
	            Set<Id> learningContentIds = new Set<Id>();
	            if(String.isNotBlank(previewParam.businessUnit) && String.isNotBlank(previewParam.division) && String.isNotBlank(previewParam.location)) {
	                for(Learning_Content_Access__c lca: [SELECT Learning_Content__c FROM Learning_Content_Access__c 
	                                                     WHERE Business_Unit__c = :previewParam.businessUnit 
	                                                     AND Division__c = :previewParam.division
	                                                     AND Location__c INCLUDES (:previewParam.location)]) {
	                    if(String.isNotBlank(lca.Learning_Content__c)) {
	                        learningContentIds.add(lca.Learning_Content__c);
	                    }
	                }
	            }
	            system.debug('LEARNING CONTENT IDS :: ' + learningContentIds);

	            if(!learningContentIds.isEmpty() && effectiveDate != null) {
	                sClause += ' Id IN : learningContentIds AND Start_Date__c <= :effectiveDate and End_Date__c >= :effectiveDate AND';
	            }
	            sClause += constructSearchCriteria(sKeyword);
	            
	            String sLimit = (recordLimit == null || recordLimit == 0 || recordLimit >= 2000) ? '' : 
    			( ' LIMIT ' + String.valueOf(recordLimit));
	            String qString = 'SELECT  ' + sFields + ' FROM LMS_Content__c WHERE ' + sClause + ' ORDER BY Title__c ' + sLimit;
	            
	            lmsList = Database.query(qString);
	        }
		return lmsList;
	}
	/**
	* @description: Return the picklist values for the Category field on the Learning Content object
	* @return: A list of UrlEncodedCategories wrapper classes
	*/
	public static List<UrlEncodedCategories> getCategoryPicklistValues(){
		List<Schema.PicklistEntry> pList = new List<Schema.PicklistEntry>();
		List<UrlEncodedCategories> catList = new List<UrlEncodedCategories>();
		try{
			Schema.DescribeFieldResult dfr = LMS_Content__c.Category__c.getDescribe();
			pList = dfr.getPicklistValues();

			//Loop through picklist values and build URL parameter for each picklist value
			for(Schema.PicklistEntry pEntry : pList){
				String urlParam = EncodingUtil.urlEncode(pEntry.getValue(), 'UTF-8');
				catList.add(new UrlEncodedCategories(pEntry, urlParam));
			}

		} catch(Exception ex) {
			System.debug(LoggingLevel.ERROR, 'Error Retrieving Category Picklist Values :: ' + ex.getMessage());
		}

		return catList;
	}
	
	/**
    * @description method to get the preview options 
    * @param String: previewParamJson
    * @return PreviewWrapper: previewParams
    */
	public static PreviewWrapper getPreviewParam(String previewParamJson) {
        PreviewWrapper previewParams = null;
        
        if(String.isNotBlank(previewParamJson)) {
            previewParams = (PreviewWrapper) JSON.deserialize(previewParamJson, PreviewWrapper.class);
        } else {
            User usr = [Select Id, Business_Unit__c, Division__c, Location__c From User Where Id = :UserInfo.getUserId()];
            if(usr != null) {
                previewParams = new PreviewWrapper(DateTime.now().getTime(), usr.Business_Unit__c, usr.Division__c, usr.Location__c);
            }
        }

        return previewParams;
    }

    /**
    * @description method to convert the Javascript date string to Salesforce Datetime
    * @param String: Javascript date string
    * @return Datetime: value
    */
    private static Date normalizeDate(Long dateInMilliseconds) {
        Datetime dt = DateTime.newInstance(dateInMilliseconds);
        return dt.date();
    }
    
    // Wrapper class for Preview Options
	public class PreviewWrapper {
        public Long effectiveDate {get; set;}
        public String businessUnit {get; set;}
        public String division {get; set;} 
        public String location {get; set;} 
        public PreviewWrapper(Long dateInMilliseconds, String businessUnit, String division, String location) {
            this.effectiveDate = dateInMilliseconds;
            this.businessUnit = String.isNotBlank(businessUnit) ? businessUnit : '';
            this.division = String.isNotBlank(division) ? division : '';
            this.location = String.isNotBlank(location) ? location : '';
        }
    }

    // Wrapper class used by getCategoryPicklistValues() method to return each picklist entry with its Url encoded value
    public class UrlEncodedCategories {
    	public Schema.PicklistEntry pEntry {get;set;}
    	public String catEncoded {get;set;}

    	public UrlEncodedCategories(Schema.PicklistEntry pEntry, String catEncoded) {
    		this.pEntry = pEntry;
    		this.catEncoded = catEncoded;
    	}
    }

    /*
    * @description: method to get the Abbreviated Division Name from BU_Division__c
    * @param String: userDivision
    * @return String
    */
    public static String getAbbrDivisionName(String userDivision) {
    	String retVal = '';
    	if(String.isNotBlank(userDivision)) {
    		List<BU_Division__c> buDivList = [SELECT Division__c, Abbr_Division_Name__c
                                              FROM BU_Division__c
                                              WHERE Division__c = :userDivision
                                              LIMIT 1
                                            ];
            if(buDivList != null && !buDivList.isEmpty()) {
            	retVal = (String.isNotBlank(buDivList[0].Abbr_Division_Name__c)) ? buDivList[0].Abbr_Division_Name__c : '';
            } else {
            	retVal = userDivision;
            }
    	}

    	return retVal;
    }
}