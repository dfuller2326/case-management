public with sharing class CaseTriggerUtils { 

    public static void performCaseUpdates(Map<Id,Case> newCaseRecordMap, Map<Id,Case> oldCaseRecordMap){

      for(Case caseInstance:newCaseRecordMap.values()){
        Case prevCaseInstance = oldCaseRecordMap.get(caseInstance.Id);

        if (caseInstance.Group__c != prevCaseInstance.Group__c) {
          if (caseInstance.Number_of_Escalations__c != null) {
            caseInstance.Number_of_Escalations__c = caseInstance.Number_of_Escalations__c + 1;
          } else {
            caseInstance.Number_of_Escalations__c = 1;
          }
          // setting priority date to current timestamp for escalation datetime
          caseInstance.Priority_Date__c = Datetime.now();
          // changing the status back to Open
          caseInstance.Status = 'Open';
        }

        if (caseInstance.OwnerId != prevCaseInstance.OwnerId) {
          String caseRecordPrefix = String.valueOf(caseInstance.OwnerId).substring(0,3);
          if (caseRecordPrefix == '005') { //user
            if (caseInstance.Number_of_Assignee_Change__c != null) {
                caseInstance.Number_of_Assignee_Change__c = caseInstance.Number_of_Assignee_Change__c + 1;
              } else {
                caseInstance.Number_of_Assignee_Change__c = 1;
              }
          }
        }
      }      
    }

    public static void updateCaseDetails(List<Case> caseRecords) {

      List<String> empIds = new List<String>();
      Map<String, String> employeeIdLocaleMap = new Map<String, String>();


      // if case created by user, raise the escalations by 1
      // create the empIds list
      for(Case caseInstance : caseRecords){
        
        String caseRecordPrefix = String.valueOf(caseInstance.OwnerId).substring(0,3);
        if (caseRecordPrefix == '005') { //user
          if (caseInstance.Number_of_Escalations__c == 0) {
            caseInstance.Number_of_Escalations__c = 1;
          }
        }
        empIds.add(String.valueOf(caseInstance.Employee_ID__c));
      }

      // get the List of User Records
      if (empIds != null && empIds.size() > 0) {
        List<User> userRecordList = [select Id, Employee_ID__c, LanguageLocaleKey from User where
                              Employee_ID__c in :empIds];

        for (User userObj : userRecordList) {
          employeeIdLocaleMap.put(String.valueOf(userObj.Employee_ID__c), String.valueOf(userObj.LanguageLocaleKey));
        }

        for(Case caseInstance : caseRecords){
          if (employeeIdLocaleMap != null 
            && employeeIdLocaleMap.containsKey(String.valueOf(caseInstance.Employee_ID__c))) {
              caseInstance.Contact_Language__c = employeeIdLocaleMap.get(String.valueOf(caseInstance.Employee_ID__c));
          }  
        }
      }
    }

     
    public static void performCaseUpserts(List<Case> caseRecords){
        
       Map<Id,Id> caseOwnerMap=new Map<Id,Id>();
       Map<Id,Case> caseInstanceMap=new Map<Id,Case>();
       Map<Id,String> ownerEmployeeIdMap=new Map<id,String>();
       Map<String,Id> EmployeeContactMap=new Map<String,Id>();

       //check if it is a portal case
       for(Case caseInstance:caseRecords){

        if(  caseInstance.On_Behalf_Of2__c!=null 
         && (caseInstance.Portal_Case__c || caseInstance.ContactId!=null) 
         && caseInstance.ContactId!=caseInstance.On_Behalf_Of2__c){
            
            caseInstance.Initial_Case_Creator__c=caseInstance.ContactId;
            caseInstance.ContactId=caseInstance.On_Behalf_Of2__c;
        }
        else if(  caseInstance.Portal_Case__c 
               && caseInstance.On_Behalf_Of2__c==null
               && caseInstance.Id!=null
               && caseInstance.CreatedById!=null
               && caseInstance.ContactId==null
               ){
             caseOwnerMap.put(caseInstance.Id,caseInstance.CreatedById);
             caseInstanceMap.put(caseInstance.id,caseInstance);
        }

         
       } 

       for(User userInstance:[select Employee_ID__c from User where Id in :caseOwnerMap.values()]){
            ownerEmployeeIdMap.put(userInstance.Id,userInstance.Employee_ID__c);
       }

       for(Contact contactInstance:[select id,Employee_ID__c from Contact where Employee_ID__c in :ownerEmployeeIdMap.values()]){
            EmployeeContactMap.put(contactInstance.Employee_ID__c,contactInstance.Id);
       }

       for(Case caseInstance:caseInstanceMap.values()){
          
           caseInstance.ContactId=EmployeeContactMap.get(ownerEmployeeIdMap.get(caseInstance.CreatedById));
      }


      }

    public static void performCaseTeamUpserts(Map<Id,Case> caseRecordMap){
        
            Map<Id,Set<String>> parentIdCaseTeamMap=new Map<Id,Set<String>>();
            //make sure the case teams doesn't exist before for the entities.
        for(CaseTeamMember caseTeamInstance:[select  Id
                                            , ParentId
                                            , MemberId 
                                      from CaseTeamMember 
                                      where ParentId in :caseRecordMap.keySet()]){
                
                if(parentIdCaseTeamMap.get(caseTeamInstance.ParentId)!=null){
                         parentIdCaseTeamMap.get(caseTeamInstance.ParentId).add(caseTeamInstance.MemberId);
                }
                else{
                        parentIdCaseTeamMap.put(caseTeamInstance.ParentId,new Set<String>{caseTeamInstance.MemberId});
                }
        }
        
        List<CaseTeamMember> caseTeamMembers=new List<CaseTeamMember>();
          for(Case caseInstance:caseRecordMap.values()){
              //private case
              if(caseInstance.Private__c){
                  if(caseInstance.ContactId != null && (parentIdCaseTeamMap==null || parentIdCaseTeamMap.get(caseInstance.Id)==null || !parentIdCaseTeamMap.get(caseInstance.Id).contains(caseInstance.ContactId))){
                     CaseTeamMember caseTeamMemberInstance=new CaseTeamMember(ParentId=caseInstance.Id,MemberId=caseInstance.ContactId, TeamRole=new CaseTeamRole(Name='HR for HR'));
                     caseTeamMembers.add(caseTeamMemberInstance);
                  }
                  if(String.valueOf(caseInstance.OwnerId).startsWith('005') //exclude queues
                      && (parentIdCaseTeamMap==null || parentIdCaseTeamMap.get(caseInstance.Id)==null || !parentIdCaseTeamMap.get(caseInstance.Id).contains(caseInstance.OwnerId))){
                     CaseTeamMember caseTeamMemberInstance=new CaseTeamMember(ParentId=caseInstance.Id,MemberId=caseInstance.OwnerId,TeamRole=new CaseTeamRole(Name='HR for HR'));
                     caseTeamMembers.add(caseTeamMemberInstance);
                  }
             }
          }
          try{
          List<Database.Upsertresult> ur=Database.upsert(caseTeamMembers,false);
          String errorString='';
           for (Integer i=0;i<ur.size();i++) {
                if (!ur[i].isSuccess()) {   
                    errorString+=HRPortalUtils.constructErrorString(ur[i],null,null,caseTeamMembers[i]);  
                }  
            }
            if(errorString!=null && errorString.length()>0){
                  HRPortalUtils.emailError('Case Team Insert Failed', errorString); 
            }
          }
          catch(Exception e){
            throw e;
          }
          
    }


}