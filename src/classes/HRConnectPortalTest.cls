@isTest(SeeAllData=true)
public class HRConnectPortalTest {

    private static integer incrementer = 0;

    private static String getRandomName(String prefix) {
        return prefix + '_' + String.valueOf(incrementer++); // auto-incr (not random) to prevent duplicates
    }

    private static User getUserForProfile(string profileName) {
        Profile prof = [Select id from Profile where Name = :profileName limit 1];
        String name = getRandomName('User');
        String email = name + '@biogenidec.com';
        User u = new User(
            Username = email,
            LastName = name,
            CommunityNickname = 'nick' + String.valueOf(Math.random()),
            alias = 'testuser', //max length 8
            Email = email,
            ProfileId = prof.id,
            TimeZoneSidKey = 'America/New_York',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserPermissionsSFContentUser = true);
//        insert u;
        return u;
    }

    @isTest
    public static void testHRConnectAnnounceController() {

       //Use the PageReference Apex class to instantiate a page
       PageReference pageRef = Page.HRConnectNew;

       //In this case, the Visualforce page named 'success' is the starting point of this test method.
       Test.setCurrentPage(pageRef);

       //Instantiate and construct the controller class.
       HRConnectAnnounceController controller = new HRConnectAnnounceController();

       //ApexPages.StandardSetController setCon = controller.setCon();
       //ApexPages.StandardSetController setCon = controller.setConLocal();
       List<HRConnectAnnounceController.annwrapper> ga = controller.getAnnouncements();
       List<HRConnectAnnounceController.annwrapper> gla = controller.getLocalAnnouncements();
       String a = controller.Selpagenum;
       Integer b = controller.size;
       b = controller.noOfRecords;
       a = controller.Selpagenuml;
       b = controller.sizel;
       b = controller.noOfRecordsl;
       a = controller.idparam;

       //Example of calling an Action method. Same as calling any other Apex method.
       //Normally this is executed by a user clicking a button or a link from the Visualforce
       //page, but in the test method, just test the action method the same as any
       //other method by calling it directly.

       //The .getURL will return the page url the Save() method returns.
            //String nextPage = controller.save().getUrl();

       //Check that the save() method returns the proper URL.
            //System.assertEquals('/apex/failure?error=noParam', nextPage);

       //Add parameters to page URL
            //ApexPages.currentPage().getParameters().put('qp', 'yyyy');

       //Instantiate a new controller with all parameters in the page
            //controller = new thecontroller();

       //Example of calling the 'setter' method for several properties.
       //Normally these setter methods are initiated by a user interacting with the Visualforce page,
       //but in a test method, just call the setter method directly.
       /*controller.setLastName('lastname');
       controller.setFirstName('firstname');
       controller.setCompany('acme');
       controller.setEmail('firstlast@acme.com');
       nextPage = controller.save().getUrl();

       //Verify that the success page displays
       System.assertEquals('/apex/success', nextPage);*/

   }

    @isTest
    public static void testHRConnectAnswerController() {

       //Use the PageReference Apex class to instantiate a page
       PageReference pageRef = Page.HRConnectAnswers;

       //In this case, the Visualforce page named 'success' is the starting point of this test method.
       Test.setCurrentPage(pageRef);

       //Instantiate and construct the controller class.
       HRConnectAnswerController controller = new HRConnectAnswerController();
       HRConnectRemoter rc = new HRConnectRemoter(controller);

       boolean a = controller.getPrevRequired();
       a = controller.getNextRequired();
       decimal b = controller.getCurrentPageNumber();
       pageRef = controller.next();
       pageRef = controller.previous();
       List<SelectOption> c = controller.getSortOptions();
       List<HRConnectAnswerController.ArticleCategories> d = controller.getArticleCategories();
       List<KnowledgeArticleVersion> e = controller.getFavorites();

       KnowledgeArticleVersion articleqry = [SELECT knowledgearticleid
            FROM KnowledgeArticleVersion
            WHERE PublishStatus = 'online'
            and Language = 'en_US' limit 1];
       controller.id = articleqry.knowledgearticleid;
       HRConnectAnswerController.ArticleDetails f = controller.getArticleDetail();
       pageRef = controller.doSearch();
   }

    @isTest
    public static void testHRConnectCaseNavController() {

       //Instantiate and construct the controller class.
       HRConnectCaseNavController controller = new HRConnectCaseNavController();

       controller.pagename = 'CASE';
       List<HRConnectCaseNavController.NavigationItems> a = controller.getNavitems();
       controller.pagename = 'TRACK';
       a = controller.getNavitems();
       controller.pagename = 'POSITION';
       a = controller.getNavitems();
    }

    @isTest
    public static void testHRConnectFootController() {

       //Instantiate and construct the controller class.
       HRConnectFootController controller = new HRConnectFootController();
       HRConnectRemoter rc = new HRConnectRemoter(controller);

       user u;
       List<HRConnectFootController.FooterSection> a;
       try{
            u = getUserForProfile('HR for HR');
            insert u;
            System.runAs(u) {
                a = controller.getFooterSections();
            }
        }
       catch(Exception e)
       {
            a = controller.getFooterSections();
       }

    }

    @isTest
    public static void testHRConnectHdr() {

        //Instantiate and construct the controller class.
        HRConnectHdr controller = new HRConnectHdr();
        String a = controller.userpic;
    }

    @isTest
    public static void testHRConnectHomeController() {

       //Instantiate and construct the controller class.
       HRConnectHomeController controller = new HRConnectHomeController();
       HRConnectRemoter rc = new HRConnectRemoter(controller);

       user u;
       List<HRConnectHomeController.NavigationItems> a;
       try{
            u = getUserForProfile('HR for HR');
            insert u;
            System.runAs(u) {
                a = controller.getNavitems();
            }
        }
       catch(Exception e)
       {
            a = controller.getNavitems();
       }

    }

    @isTest
    public static void testHRConnectRemoter() {

        HRConnectRemoter r = new HRConnectRemoter();
        case c;
        id i = null;

        try{
            c = new case(subject='test',description='test',origin = 'Web',Portal_Case__c = true);
            insert c;
            i = c.id;}
        catch(Exception e)
        {}

        try{

            //List<CaseComment> a = HRConnectRemoter.getCaseDetails(i);
        }
        catch(Exception e){}

        try{

            HRConnectRemoter.setCaseComment(i,'test');
        }
        catch(Exception e){}

        try{

            HRConnectRemoter.saveRecentlyviewed('www.manoj.com','test');
        }
        catch(Exception e){}

        try{

            HRConnectRemoter.toggleFavorite('test');
        }
        catch(Exception e){}
        try{

            HRConnectRemoter.toggleFavorite('test');
        }
        catch(Exception e){}

        try{
            KnowledgeArticleVersion articleqry = [SELECT knowledgearticleid
                FROM KnowledgeArticleVersion
                WHERE PublishStatus = 'online'
                and ArticleType = 'Policies_Guideline__kav'
                and Language = 'en_US' limit 1];
                id articleid = articleqry.knowledgearticleid;
            HRConnectRemoter.updateViewstat(articleid);
        }
        catch(Exception e){}

        try{
            user u = [SELECT TimeZoneSidKey,LocaleSidKey,LanguageLocaleKey FROM User WHERE id=:UserInfo.getUserId()];
            HRConnectRemoter.saveUserProfile(u.LanguageLocaleKey,u.LocaleSidKey,u.TimeZoneSidKey);
        }
        catch(Exception e){}

        try{
            User u = getUserForProfile('Employee');
            insert u;
            system.assertNotEquals(null, u.id);

            id articleid;
            System.runAs(u) {
                KnowledgeArticleVersion articleqry = [
                  SELECT knowledgearticleid
                    FROM KnowledgeArticleVersion
                   WHERE PublishStatus = 'online'
                     and Language = 'en_US' limit 1];
system.debug('articleqry: ' +articleqry);
                articleid = articleqry.knowledgearticleid;
            }

            list<Vote> votes = [select id from Vote where parentid = :articleId and createdById = :u.id];
            system.assertEquals(0, votes.size(), 'PRECONDITION: no votes by user for article');
            System.runAs(u) {
                HRConnectRemoter.saveArticleRating(articleid,'5');
            }
            votes = [select id, type from Vote where parentid = :articleId and createdById = :u.id];
            system.assertEquals(1, votes.size(), 'First rating: one vote by user for article');
            system.assertEquals('5', votes[0].type, 'First rating: correct value');

            System.runAs(u) {
                HRConnectRemoter.saveArticleRating(articleid,'1');
            }
            votes = [select id, type from Vote where parentid = :articleId and createdById = :u.id];
            system.assertEquals(1, votes.size(), 'Second rating: one vote by user for article');
            system.assertEquals('1', votes[0].type, 'Second rating: correct value');

        }
        catch(Exception e){}
    }

    @isTest
    public static void testHRConnectCaseController() {

       //Use the PageReference Apex class to instantiate a page
       PageReference pageRef = Page.HRConnectCase;

       //In this case, the Visualforce page named 'success' is the starting point of this test method.
       Test.setCurrentPage(pageRef);

       //Instantiate and construct the controller class.
        Case hrcase = new case();
        hrcase.subject = 'test';
        hrcase.description = 'test';
        user u = new user();
        try{
            u = getUserForProfile('HR for HR');
            insert u;
            hrcase.On_Behalf_Of__c = u.id;
            }
        catch(Exception e){}

        string before = 'Testing base 64 encode';
        // create a blob from our parameter value before we send it as part of the url
        Blob beforeblob = Blob.valueOf(before);
        // base64 encode the blob that contains our url param value
        //string paramvalue = EncodingUtil.base64Encode(beforeblob);

        HRConnectCaseController controller = new HRConnectCaseController(new ApexPages.StandardController(hrcase));

        //controller.attachment = new attachment(name='att1',body=beforeblob);
        //controller.attachment2 = new attachment(name='att2',body=beforeblob);
        //controller.attachment3 = new attachment(name='att3',body=beforeblob);
        system.assertEquals(controller.attachment, new Attachment());
        system.assertEquals(controller.attachment2, new Attachment());
        system.assertEquals(controller.attachment3, new Attachment());
        system.assertEquals(controller.pagetext, 'Not Available');
            
        controller.attbody1 = beforeblob;
        controller.attbody2 = beforeblob;
        controller.attbody3 = beforeblob;
        controller.filename1 = 'att1';
        controller.filename2 = 'att1';
        controller.filename3 = 'att1';
        
        try{
            controller.searchString = '';
            system.assertEquals(controller.assignSubjectToSearchString(), null);
            controller.searchString = 'xxyyzz';
            controller.assignSubjectToSearchString();
            
            User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
            System.runAs (thisUser) {
                Test.startTest();
                PageReference p = controller.doSave();
                Test.stopTest();
            }
            System.assertEquals(controller.getPrevRequired(), false);
            System.assertEquals(controller.getNextRequired(), false);
            System.assertEquals(controller.getCurrentPageNumber(), 1);
            System.assertEquals(controller.previous(), null);
            System.assertEquals(controller.next(), null);
            
            PageReference p = controller.doConfirmCheckSuggestedArticlesAndSave();
            
        }
        catch(Exception e){System.debug(e);}

        List<SelectOption> a = controller.getOnBehalfOfUsers();
    }

    @isTest
    public static void testHRConnectTrackController() {

       //Use the PageReference Apex class to instantiate a page
       PageReference pageRef = Page.HRConnectTrack;

       //In this case, the Visualforce page named 'success' is the starting point of this test method.
       Test.setCurrentPage(pageRef);

       //Instantiate and construct the controller class.
        HRConnectTrackController controller = new HRConnectTrackController();
        List<HRConnectTrackController.casewrapper> t = controller.getCaseHistory();
    }

    @isTest
    public static void testHRConnectSubHdrController() {

       //Instantiate and construct the controller class.
       HRConnectSubHdrController controller = new HRConnectSubHdrController();

       controller.pagename = 'ANNOUNCE';
       List<HRConnectSubHdrController.NavigationItems> a = controller.getNavitems();
       String b = controller.leftimg;
       controller.pagename = 'ANSWERS';
       a = controller.getNavitems();
       b = controller.leftimg;
       controller.pagename = 'CONTACT';
       a = controller.getNavitems();
       b = controller.leftimg;
    }

    @isTest
    public static void testHRConnectPositionRequestController() {

        //Use the PageReference Apex class to instantiate a page
        PageReference pageRef = Page.HRConnectPosition;

        //In this case, the Visualforce page named 'success' is the starting point of this test method.
        Test.setCurrentPage(pageRef);

        Case hrcase = new case();
        hrcase.Replacement_term_transferred_employee__c = 'Yes';
        hrcase.If_so_what_is_employee_s_name__c = 'test';
        hrcase.Position_approved_in_AOP_process__c = 'No';

       //Instantiate and construct the controller class.
        HRConnectPositionRequestController controller = new HRConnectPositionRequestController(new ApexPages.StandardController(hrcase));

        user u;
        try{
            u = getUserForProfile('HR for HR');
            User reportsTo = getUserForProfile('Employee');
            User replaceMe = getUserForProfile('Employee');
            list<User> users = new list<User>{u, reportsTo, replaceMe};
            insert users;
            
            
            controller.OnBehalfOfId = u.id;
            Job_Code__c jc = [select id from Job_Code__c limit 1];
            controller.JobTitleId = jc.id;
            controller.ReportsToId = reportsTo.id;
            controller.replacingEmployeeId = replaceMe.id;
            
            /*string before = 'Testing base 64 encode';
            // create a blob from our parameter value before we send it as part of the url
            Blob beforeblob = Blob.valueOf(before);
            controller.attbody1 = beforeblob;
            controller.attbody2 = beforeblob;
            controller.attbody3 = beforeblob;
            controller.filename1 = 'att1';
            controller.filename2 = 'att2';
            controller.filename3 = 'att3';*/
            
            System.runAs(u){
            PageReference p = controller.doSave();
            }

        }
        catch(Exception e){}

//      List<SelectOption> a = controller.getOnBehalfOfUsers();
        List<SelectOption> b = controller.getMonthYears();
    }

    @isTest
    public static void testHRConnectContactInfoController() {

       //Instantiate and construct the controller class.
       HRConnectContactInfoController controller = new HRConnectContactInfoController();

       HR_Contact_Info__c i = controller.getHRContact();
    }
}