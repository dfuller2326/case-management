public class HRConnectContentPreviewController {
    
    private Id fileId;
    public HRConnectContentPreviewController () {
        fileId = System.currentPageReference().getParameters().get('fileId');
    }
    
    public String getContentVersions() {
        List<ContentDocument> contentDocs = [select LatestPublishedVersionId, Owner.Name from ContentDocument WHERE id = :fileId];
        if(contentDocs.size() > 0 )        
            return contentDocs[0].LatestPublishedVersionId;        
        else   
            return null;
    }
}