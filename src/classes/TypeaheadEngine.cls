global without sharing class TypeaheadEngine {

    public TypeaheadEngine(ApexPages.StandardController controller) {

    }

    @RemoteAction
    global static list<User> findUsers(String searchtext) {
        string searchterm = searchtext + '%';
        list<User> users = [select name, email from User where firstname like :searchterm or lastname like :searchterm or name like :searchterm];
        return users;
    }

    @RemoteAction
    global static list<Job_Code__c> findJobTitles(String searchtext) {
        string searchterm = '%' + searchtext + '%';
        list<Job_Code__c> jobCodes = [SELECT name FROM Job_Code__c where name like :searchterm order by name asc];
        return jobCodes;
    }

}