public with sharing class LMSPortal_TestHelper {
    // map that holds a mapping of Profile Name to Profile Id
    private static Map<String, Id> ProfileName2Id = new Map<String, Id>();

    // default profile name to use if one is not provided
    private static final String DEFAULT_PROFILE_NAME = 'System Administrator';

    // helper method for creating a user object
    private static User createUserObject(String profileName) {
        Long rand = Crypto.getRandomLong();
        rand = (rand < 0 ? rand * -1 : rand);

        // check if we have retrieved this profile before
        if (String.isNotBlank(profileName) && !ProfileName2Id.containsKey(profileName)) {
            Profile p = [SELECT Id FROM Profile WHERE Name = :profileName LIMIT 1];
            ProfileName2Id.put(profileName, p.Id);
        }

        // required fields: Username, Last Name, E-mail, Alias, Nickname, Time Zone, Locale, Email Encoding, Profile, Language
        User u = new User(
            Email = 'testuser@biogenidec.com',
            Username = 'testuser@test.sfdc.' + rand,
            LastName = 'user' + rand,
            Alias = 'testUsr',
            TimeZoneSidKey = 'America/Denver',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'ISO-8859-1',
            ProfileId = ProfileName2Id.get(profileName),
            LanguageLocaleKey = 'en_US',
            Employee_ID__c = '1234',
            Business_Unit__c = '1000-0020 Finance',
            Division__c = '1111-0025 Finance',
            Location__c = 'Campus-Cambridge'
        );

        return u;
    }

    // helper method for creating user record
    public static User createUser() {
        return createUser(DEFAULT_PROFILE_NAME);
    }

    // helper method for creating user record
    public static User createUser(String profileName) {
        User u = createUserObject(profileName);
        try {
            insert u;
        } catch(Exception ex) {
            System.Assert(ex == null, 'Creating test User produced errors!\n' + ex.getMessage());
        }

        // retrieve the user so I have the Id
        u = [SELECT Id, LastName, Business_Unit__c, Division__c, Location__c FROM User WHERE Email = : u.Email LIMIT 1];
        return u;
    }

    public static List<Learning_Content_Filters__c> createContentFilterCS(){
        List<Learning_Content_Filters__c> filterList = new List<Learning_Content_Filters__c>();
        for(Integer i = 0; i < 5; i++){
            Learning_Content_Filters__c contentFilter = new Learning_Content_Filters__c();
            contentFilter.Rank__c = i + 1;
            if(i == 0){
                contentFilter.Name = 'Category';
                contentFilter.API_Name__c = 'Category__c';
            } else if(i == 1){
                contentFilter.Name = 'Content Type';
                contentFilter.API_Name__c = 'Content_Type__c';
            } else if(i == 2){
                contentFilter.Name = 'Leadership Competency';
                contentFilter.API_Name__c = 'Leadership_Competency__c';
            } else if(i == 3){
                contentFilter.Name = 'Functional Competency';
                contentFilter.API_Name__c = 'Functional_Competency__c';
            } else if(i == 4){
                contentFilter.Name = 'Additional Filters';
                contentFilter.API_Name__c = 'Additional_Filters__c';
            }
            filterList.add(contentFilter);
        }
        return filterList;
    }

    public static FunctionalCompetencyConfig__c createFunctionalCompetencies() {
        FunctionalCompetencyConfig__c funcComp = new FunctionalCompetencyConfig__c(
            Name = 'FUNC_COMP_0001',
            Division__c = '000 MA Talent Acquisition',
            Functional_Area__c = 'Market Access',
            Functional_Competency__c = 'Business Knowledge'
        );
        return funcComp;
    }
}