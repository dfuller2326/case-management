public with sharing class LMSPortal_FooterController {
	
	public List<LMS_Link__c> helpfulLinks {get; set;}
	public Integer listSize {get; set;}
	public LMSPortal_FooterController(){
		helpfulLinks = new List<LMS_Link__c>();
		getHelpfulLinks(null);
	}
	
	public void getHelpfulLinks(String previewParamJson){
		String blankstr = '';
		if(Test.isRunningTest()) {
			blankstr = previewParamJson;
		}
		LMSPortal_Util.PreviewWrapper pWrapper = LMSPortal_Util.getPreviewParam(blankstr);
		System.debug('PREVIEW WRAPPER OUTPUT :: ' + pWrapper);

		if(String.isNotBlank(pWrapper.businessUnit) && String.isNotBlank(pWrapper.division) && String.isNotBlank(pWrapper.location)) {

			//Query Learning Content Access Object with BU and DIV from Preview Wrapper
			Set<Id> learningLinkIds = new Set<Id>();
			List<String> locList = new List<String>();
			locList.add(pWrapper.location);
		    for(Learning_Content_Access__c lca: [SELECT Learning_Link__c FROM Learning_Content_Access__c 
		                                         WHERE Business_Unit__c = :pWrapper.businessUnit 
		                                         AND Division__c = :pWrapper.division
		                                         AND Location__c INCLUDES (: locList[0])]) {
		        if(String.isNotBlank(lca.Learning_Link__c)) {
		            learningLinkIds.add(lca.Learning_Link__c);
		        }
		    }

		    if(!learningLinkIds.isEmpty()) {
			    helpfulLinks = [SELECT External_Media_URL__c,Label__c,Status__c,Target_Window__c,Type__c FROM LMS_Link__c WHERE Type__c =: LMSPortal_Constants.LMS_LINK_TYPE_QUICKLNK AND Status__c =: LMSPortal_Constants.LMS_LINK_STATUS_ACTV AND Id IN :learningLinkIds ORDER By Rank__c limit 8];
			    listSize = helpfulLinks.size();
			}
		}

	}
	
}