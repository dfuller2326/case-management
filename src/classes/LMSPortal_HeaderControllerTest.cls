@isTest
private class LMSPortal_HeaderControllerTest {

    @testSetup static void heardControllerTestSetup(){
        //Creating record to resolve Query Exception in Class.UserTriggerUtils.performContactUpserts
        Account a = new Account(Name = 'Biogen');
        INSERT a;
    }
    
    @isTest static void testConstructor() {
        LMSPortal_HeaderController ctrl = new LMSPortal_HeaderController();
        System.assert(LMSPortal_HeaderController.displayDate != null);
        System.assert(ctrl.previewCustPerm != null);
        System.assert(ctrl.divisionoptions != null);
    }

    //Standard user with LMS Portal App permission set
    @isTest static void testDisplayPreviewStandardUser(){

        //Get context user
        User u = LMSPortal_TestHelper.createUser(LMSPortal_Constants.BIOGEN_EMPLOYEE_PROFILE);
        
        //Query Permission Set
        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = :LMSPortal_Constants.LEARNING_PORTAL_ADMIN_PS LIMIT 1];
        System.debug('PERMISSION SET :: ' + ps);
        if(ps == null) {
            ps = new PermissionSet();
            ps.Name = LMSPortal_Constants.LEARNING_PORTAL_ADMIN_PS;
            ps.Label = LMSPortal_Constants.LEARNING_PORTAL_ADMIN_PS;
            insert ps;
            System.assertNotEquals(null, ps.Id);
        }
        
        List<PermissionSetAssignment> psaList = [SELECT PermissionSetId 
                                                FROM PermissionSetAssignment 
                                                WHERE AssigneeId = :UserInfo.getUserId() 
                                                AND PermissionSetId = :ps.Id  LIMIT 1];
        
        PermissionSetAssignment psa = null;
        if (psaList != null && !psaList.isEmpty()) {
            psa = psaList[0];
        } else {
            //Creat permission set assignment for context user
            psa = new PermissionSetAssignment();
            psa.AssigneeId = u.Id;
            psa.PermissionSetId = ps.Id;
            insert psa;
            System.assertNotEquals(null, psa.Id);
        }
        //Run as Context User
        System.runAs(u) {
            //Check if user has access to Preview Button
            LMSPortal_HeaderController ctrl = new LMSPortal_HeaderController();
            //Set showPreview value to false
            ctrl.showPreview = false;
            System.assertEquals(false, ctrl.previewCustPerm);
        }
    }

    //Standard user WITHOUT LMS Portal App permission set
    @isTest static void testPreviewStandardUserNoPerm(){

        //Get context user
        User u = LMSPortal_TestHelper.createUser(LMSPortal_Constants.BIOGEN_EMPLOYEE_PROFILE);

        //Run as Context User
        System.runAs(u) {
            //Check if user has access to Preview Button
            LMSPortal_HeaderController ctrl = new LMSPortal_HeaderController();
            //Set showPreview value to true
            ctrl.showPreview = true;
            System.assert(ctrl.previewCustPerm == false);
        }
    }

    @isTest static void testBUList(){
        LMSPortal_HeaderController ctrl = new LMSPortal_HeaderController();
        List<SelectOption> oList = ctrl.getBUList();

        for(SelectOption op : oList){
            System.assert(op.getLabel() != null);
            System.assert(op.getValue() != null);
        }

        System.assert(!oList.isEmpty());
        System.assert(oList != null);
    }

    @isTest static void testChange(){
        LMSPortal_HeaderController ctrl = new LMSPortal_HeaderController();

        ctrl.businessUnitVal = '000 MA Talent Acquisition';
        ctrl.change();

        System.assert(ctrl.divisionoptions != null);
        for(SelectOption op : ctrl.divisionoptions){
            System.assert(op.getLabel() != null);
            System.assert(op.getValue() != null);
        }
    }

    @isTest static void testGetDivisionsList(){
        LMSPortal_HeaderController ctrl = new LMSPortal_HeaderController();
        ctrl.getDivisionsList();
        System.assert(ctrl.divisionoptions != null);
    }

    @isTest static void testGetCategoryPiclistValues(){
        LMSPortal_HeaderController ctrl = new LMSPortal_HeaderController();
        if(ctrl.catList != null && !ctrl.catList.isEmpty()) {
            /* Assert Statements */
            for(LMSPortal_Util.UrlEncodedCategories cat : ctrl.catList){
                System.assertNotEquals(cat.pEntry, null);
                System.assertNotEquals(cat.catEncoded, null);
            }
        }
    }

    @isTest static void testGetLocationList(){
        LMSPortal_HeaderController ctrl = new LMSPortal_HeaderController();
        List<SelectOption> options = ctrl.getLocationList();
        System.assertNotEquals(null, options);
    }
}