public without sharing class HRConnectContactInfoController {
	private static string chatDeployName = 'Biogen_HR_Connect_Chat';
	private static string chatExecName = 'Executive_Chat';
	private static string chatGeneralName = 'General_Chat';
	private static string caseGeneralRecordTypeName = 'General';

	public boolean exec {get;set;}
	string usercountry = HRConnectCommon.usercountry;
	public string chatDeploymentId { get; set; }
	public string chatExecButtonId { get; set; }
	public string chatGeneralButtonId { get; set; }
	public string orgId { get; set; }
    public string caseGeneralRecordTypeId { get; set; }
    public string phone {get; set;}
    public string supportHours {get; set;}
    public boolean chatEnabled {get; set;}

    public string getChatServer() {
    	HRConnect_Portal_Config__c cs = HRConnect_Portal_Config__c.getValues('PORTAL_CONFIG_LIST');
    	return cs.Chat_Server__c;
    }


	public HRConnectContactInfoController()
	{
		if (HRConnectCommon.userprofilename.contains('Exec')) {
			exec = true;
		}
		else {
			exec = false;
		}

		//values for chat
		orgId = UserInfo.getOrganizationId().left(15); //liveagent init js requires 15-digit id
		list<LiveChatDeployment> deploys = [select id from LiveChatDeployment where DeveloperName = :chatDeployName];
		if (deploys.size()>0) {
			chatDeploymentId = ((String)deploys[0].id).left(15); //liveagent init js requires 15-digit id
		}
		list<LiveChatButton> buttons = [select id, DeveloperName from LiveChatButton where DeveloperName in (:chatExecName, :chatGeneralName)];
		system.debug(buttons);
		if(buttons.size()==2) {
			for (LiveChatButton button : buttons) {
				if (button.DeveloperName == chatExecName) {
					chatExecButtonId = ((String)button.id).left(15);
				}
				else {
					chatGeneralButtonId = ((String)button.id).left(15);
				}
			}
		}
		map<string, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosByName();
		if(rtMap.containsKey(caseGeneralRecordTypeName)) {
			caseGeneralRecordTypeId = rtMap.get(caseGeneralRecordTypeName).getRecordTypeId();
		}

        HR_Contact_Info__c info = getHRContact();
        chatEnabled = false;
        if (info!=null) {
        	phone = info.name;
        	supportHours = info.Support_Hours__c;
        	chatEnabled = info.Chat_Enabled__c
        	           && getChatServer() != null
        	           && orgId!=null
        	           && chatDeploymentId!=null
        	           && ( (exec && chatExecButtonId!=null) || (!exec && chatGeneralButtonId!=null));
        }

		//ShowChat = true;
	}

    //extension controller to allow this to be used from Answers page
    public HRConnectContactInfoController(HRConnectAnswerController controller) {
    	this();
    }

	public HR_Contact_Info__c getHRContact()
	{
			List<HR_Contact_Info__c> c = new List<HR_Contact_Info__c>();

			//exec contact
			c = [Select Name, Support_Hours__c ,Chat_Enabled__c
							From HR_Contact_Info__c
							where Employee_Location__c includes(:usercountry)
							and Employee_Role__c includes(:HRConnectCommon.userprofilename)];	//:HRConnectCommon.userprofilename
				system.debug('Exec: '+c.size());

				//US non-exec contact
				if (c.size() == 0 || c == null){
							c = [Select Name, Support_Hours__c ,Chat_Enabled__c
							From HR_Contact_Info__c
							where Employee_Location__c includes(:usercountry)
							and Employee_Role__c includes('All') limit 1];
							system.debug('Emp: '+c.size());
				}

				//Rest of world
				if (c.size() == 0 || c == null){
							c = [Select Name, Support_Hours__c ,Chat_Enabled__c
							From HR_Contact_Info__c
							where Employee_Location__c includes('Global')
							and Employee_Role__c includes('All') limit 1];
							system.debug('Global: '+c.size());
				}
				system.debug(c[0].name);
			return c[0];
	}
}