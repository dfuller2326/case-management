public with sharing class LMSPortal_HeaderController {
	
    public User currentUser {get; set;}
    public String businessUnitVal {get; set;}
    public String divisionVal {get; set;}
    public String locationVal {get; set;}
    public List<SelectOption> divisionoptions {get; set;}
	public Boolean previewCustPerm {get; set;}
	public static String displayDate {get; set;}
	public static String askHRLink {get; set;}
	
	public List<LMSPortal_Util.UrlEncodedCategories> catList {
		get {
			return LMSPortal_Util.getCategoryPicklistValues();
		}
	}
	
	public Boolean showPreview {
		get; 
		set {
			if(value != null) {
				showPreview = value;
				displayPreview();	
			}
		}
	}
	
	public LMSPortal_HeaderController(){
		displayDate = '';
		previewCustPerm = false;
		divisionoptions = new List<SelectOption>();
		// get current user info and division display name
		try{
            currentUser = [SELECT Id, FirstName, SmallPhotoUrl, Division__c FROM User WHERE Id = :UserInfo.getUserId()];
            System.debug('User Division :: ' + currentUser.Division__c);
            // Create Pattern Object
            // Pattern fAreaPattern = Pattern.compile('([^\\s]+)');

            // Create Matcher Object
            if(currentUser != null && String.isNotBlank(currentUser.Division__c)) {
                List<BU_Division__c> buDivList = [Select Division__c, Division_Name__c 
                                                    From BU_Division__c 
                                                    Where Division__c = :currentUser.Division__c];
                if(buDivList != null && !buDivList.isEmpty()) {
                    divisionVal = (String.isNotBlank(buDivList[0].Division_Name__c)) ? buDivList[0].Division_Name__c : currentUser.Division__c;
                } 
            } else {
            	divisionVal = '';
            }
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR, 'ERROR RETRIEVING CURRENT USER :: ' + ex.getMessage());
        }
		//Set Ask HR Link
        askHRLink = LMSPortal_Constants.ASKHR_LINK;
	}
	
	public void displayPreview(){
		//display preview button logic
		Set<Id> PersmissionSetIds = new Set<Id>();
	    for(PermissionSetAssignment p : [SELECT PermissionSetId FROM PermissionSetAssignment where AssigneeId=:userinfo.getUserId()]){
	    	System.debug('PERMISSION SET ID :: ' + p.Id);
	      	PersmissionSetIds.add(p.PermissionSetId);
	    }

		Set<Id> customPermissionId = new Set<Id>();
	    for(SetupEntityAccess ea: [SELECT SetupEntityId FROM SetupEntityAccess WHERE SetupEntityType =: LMSPortal_Constants.CUSTOM_PERMISSION and ParentId in:PersmissionSetIds]){
	    	System.debug('SETUP ENTITY ID :: ' + ea.SetupEntityId);
	    	customPermissionId.add(ea.SetupEntityId);
	    }	       
		
		//Query the custom permissions that the user has access to and set the appropriate boolean attributes 
	    for(CustomPermission cp:[select MasterLabel from CustomPermission where id in:customPermissionId]) {
	      	if(cp.MasterLabel == LMSPortal_Constants.CUSTOM_PERMISSION_STR){ 
	      		previewCustPerm = (showPreview) ? true : false;
	      	}
	    }
	}
	
	public List<SelectOption> getBUList() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('', 'Please Select'));
		//get the available business unit list
	    
	    List<BU_Division__c> buDivList = BU_Division__c.getall().values();
	    Set<String> uniqueBuDivList = new Set<String>();
        for(BU_Division__c bd: buDivList) {
            if(!uniqueBuDivList.contains(bd.Business_Unit__c)) {
                uniqueBuDivList.add(bd.Business_Unit__c);
            }
        }
		if(uniqueBuDivList != null && !uniqueBuDivList.isEmpty()){       
		   	for(String bu : uniqueBuDivList) {
		    	options.add(new SelectOption(bu, bu));
		    }
		}
        return options;		
	}
	
	public List<SelectOption> getLocationList() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('', 'Please Select'));
		//get the available business unit list
		Schema.DescribeFieldResult fieldResultbu = Learning_Content_Access__c.Location__c.getDescribe();
		List<Schema.PicklistEntry> plebu = fieldResultbu.getPicklistValues();        
	   	for(Schema.PicklistEntry f : plebu) {
	    	options.add(new SelectOption(f.getLabel(), f.getLabel()));
	    }
        return options;		
	}
	
	public void change() {
		divisionoptions = new List<SelectOption>();
		//get the available division list
		List<BU_Division__c> buDivList= [SELECT Business_Unit__c,Division__c,Id,Name FROM BU_Division__c WHERE Business_Unit__c = : businessUnitVal];
		
		for(BU_Division__c bud : buDivList){
			if(bud.Division__c != null){
				divisionoptions.add(new SelectOption(bud.Division__c, bud.Division__c));
			}
		} 		
	}
	
	public List<SelectOption> getDivisionsList() {
        return divisionoptions;
    }
}