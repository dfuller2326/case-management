public with sharing class LMSPortal_AllVideosViewAllController {
	
	public List<String> filterList {get;set;}
	public String sortBy {get;set;}
	public Integer count {get;set;}

	public LMSPortal_AllVideosViewAllController() {
		count = 0;
	}

	public List<BannerWrapperClass> getLearningResourcesWrapper(){
		List<BannerWrapperClass> wrapperList = new List<BannerWrapperClass>();
		Map<Id, LMS_Content__c> lmsMap = new Map<Id, LMS_Content__c>();
		Set<Id> documentIdSet = new Set<Id>();
		List<LMS_Content__c> lmsContentList = [SELECT Id, Description__c, Summary__c, Title__c, Banner_Document_Id__c FROM LMS_Content__c];

		//Create Map of Banner Document Id to LMS Content Record
		for(LMS_Content__c content : lmsContentList){
			documentIdSet.add(content.Banner_Document_Id__c);
			if(!lmsMap.containsKey(content.Banner_Document_Id__c)){
				lmsMap.put(content.Banner_Document_Id__c, content);
			}
		}
		System.debug('FINAL DOCUMENT ID SET :: ' + documentIdSet);

		//Use Map to get Document Record
		List<Document> docList = [SELECT Id, Body, ContentType, Url FROM Document WHERE Id IN :documentIdSet];
		for(Document d : docList){
			count++;
			System.debug('DOCUMENT BODY :: ' + d.Body);
			wrapperList.add(new BannerWrapperClass(count, d.Id, lmsMap.get(d.Id)));
		}
		System.debug('DOCUMENT LIST FOUND :: ' + docList);
		System.debug('FINAL WRAPPER LIST :: ' + wrapperList);
		return wrapperList;
	}

	public class BannerWrapperClass{

		public String documentUrl {get;set;}
		public LMS_Content__c contentRecord {get;set;}
		public String bannerUrl {get;set;}
		public Integer count {get;set;}

		public BannerWrapperClass(Integer count, String documentId, LMS_Content__c contentRecord){
			String imageUrlConst = '/servlet/servlet.FileDownload?file=';
			this.bannerUrl = imageUrlConst + documentId;
			this.documentUrl = documentUrl;
			this.contentRecord = contentRecord;
			this.count = count;
		}

	}
}