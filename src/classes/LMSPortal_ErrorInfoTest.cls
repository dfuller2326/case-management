@isTest
private class LMSPortal_ErrorInfoTest {
	
	@isTest static void testConstructor() {
		 LMSPortal_ErrorInfo cstr = new LMSPortal_ErrorInfo('Error Code 123', 'Test Error Message');
		 System.assertEquals(cstr.errCode, 'Error Code 123');
		 System.assertEquals(cstr.errMsg, 'Test Error Message');	
	}

	@isTest static void testConstructorBlankParams() {
		 LMSPortal_ErrorInfo cstr = new LMSPortal_ErrorInfo('', '');
		 System.assertEquals(cstr.errCode, '');
		 System.assertEquals(cstr.errMsg, '');	
	}
}