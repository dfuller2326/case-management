@isTest(SeeAllData=false)
private class HRP_ArticleRedirectControllerTest {
  private static List<Procedure__kav> procArticles = new List<Procedure__kav>();

  private static void setupProcedureArticles(){
    for(Integer i = 0; i < 5; i++){
      Procedure__kav article = HRP_TestDataFactory.buildProcedureArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      procArticles.add(article);
    }
    insert procArticles;

    procArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId, ArticleNumber FROM Procedure__kav WHERE Language = 'en_US'
      AND PublishStatus = 'Draft'];

    for(Procedure__kav article : procArticles){
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }
  }

  private static testMethod void redirectToArticle_Positive_ArticleNumber() {

    setupProcedureArticles();

    PageReference actualPgRef;
    PageReference expectedPgRef = new PageReference(HRP_Constants.PROCEDURE_URL + '?id=' + procArticles[0].KnowledgeArticleId);

    Test.startTest();
      PageReference pgRef = Page.HRP_ArticleRedirect;
      pgRef.getParameters().put('anum', procArticles[0].ArticleNumber);
      Test.setCurrentPage(pgRef);

      HRP_ArticleRedirectController articleRedrectCtrl = new HRP_ArticleRedirectController();
      actualPgRef = articleRedrectCtrl.redirectToArticle();
    Test.stopTest();

    System.assertEquals(expectedPgRef.getUrl(), actualPgRef.getUrl());
  }

  private static testMethod void redirectToArticle_Positive_ArticleId() {

    setupProcedureArticles();

    PageReference actualPgRef;
    PageReference expectedPgRef = new PageReference(HRP_Constants.PROCEDURE_URL + '?id=' + procArticles[0].KnowledgeArticleId);

    Test.startTest();
      PageReference pgRef = Page.HRP_ArticleRedirect;
      pgRef.getParameters().put('aid', procArticles[0].KnowledgeArticleId);
      Test.setCurrentPage(pgRef);

      HRP_ArticleRedirectController articleRedrectCtrl = new HRP_ArticleRedirectController();
      actualPgRef = articleRedrectCtrl.redirectToArticle();
    Test.stopTest();

    System.assertEquals(expectedPgRef.getUrl(), actualPgRef.getUrl());
  }
}