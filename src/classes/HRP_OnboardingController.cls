/**
* @description an controller class for Apex Component: Onboarding in HR Connect Portal 
* @created on 12/04/2015
*/
global with sharing class HRP_OnboardingController {
	// constructor
	public HRP_OnboardingController() {}

	/**
	* @description remote action method to get the Onboarding Content(s)
	* @return String: json response
	*/
	@RemoteAction
	global static String getOnboardingContent() {
		system.debug('Inside HRP_OnboardingController:getOnboardingContent()');
		OnboardingResponse respObj = new OnboardingResponse();
		try {
			// Query context User and User Profile
			User usr = [SELECT Id, Country, ProfileId FROM User WHERE Id = :UserInfo.getUserId()];
			if(usr != null && String.isNotBlank(usr.Country)) {
				system.debug('HRP_OnboardingController: User Country:: ' + usr.Country);
				Profile usrProfile = [SELECT Id, Name FROM Profile WHERE Id = :usr.ProfileId];
				
				// Retrieve Home Page Onboarding Content
		  		respObj.onboardingContent = HRP_Content.retrieveHomePageJobOnBoarding(usr.Country, usrProfile.Name);
			}
		} catch(Exception ex) {
		  	respObj.error = new HRP_ErrorInfo(System.Label.Home_Page_Error_Heading, ex.getMessage());
		  	system.debug(LoggingLevel.ERROR, 'Error in retrieving Onboarding Content:: ' + ex.getMessage());
		}
		
		system.debug('HRP_OnboardingController:getOnboardingContent()::Response :: ' + JSON.serialize(respObj));
		system.debug('HRP_OnboardingController:getOnboardingContent() returning');
		
		return JSON.serialize(respObj);
	}

	public class OnboardingResponse {
		public List<HRP_ContentWrapper> onboardingContent {get; set;}
		public HRP_ErrorInfo error {get; set;}
		public OnboardingResponse() {}
	}
}