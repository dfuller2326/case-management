public class HRConnectSubHdrController {
	public String pagename {get;set;}
	public String lefttitle {get;set;}
	public String leftimg {get
		{
			String img;
			if (pagename == 'ANNOUNCE')
			{
				img = 'whats_new_whats_new.png';
				lefttitle = 'What\'s New?';
			}
			else if(pagename == 'ANSWERS')
			{
				img = 'main_nav_find_search_hr.png';
				lefttitle = 'Search HR';				
			}
			else if(pagename == 'CONTACT')
			{
				img = 'main_nav_ask_HR.png';
				lefttitle = 'Ask HR';				
			}			
			return img;
		}
		set;}
	//inner class to hold navigation items
	public class NavigationItems
	{
		public String imagename {get;set;}
		public String coltitle {get;set;}
		public String bsclass {get;set;}
		public String loc {get;set;}
		
		NavigationItems(String img, String title, String bsc, String loc)
		{
			imagename = img;
			coltitle = title;
			bsclass = bsc;
			this.loc = loc;
		}		
	}
	List<NavigationItems> navitems;

	public List<NavigationItems> getNavitems()
	{
		string userprofile = HRConnectCommon.userprofilename;
		Integer bscols = 2;
		String bsclass;
		String bsclassos = '';
		Boolean offset = false;
		String baseurl = 'https://'+ApexPages.currentPage().getHeaders().get('Host')+'/apex/';
		String welcomeurl = '#';

		HRConnect_Portal_Config__c cs = HRConnect_Portal_Config__c.getValues('PORTAL_CONFIG_LIST');
		if (cs.New_Hire_Welcome_Link__c != null && cs.New_Hire_Welcome_Link__c != '')
			welcomeurl = cs.New_Hire_Welcome_Link__c;		
		
		if (userprofile.contains('New'))
				bscols++;	
		
		if (bscols == 4)
		{
			bsclass = 'col-md-3';
			//bsclassos = 'col-md-offset-1';
		}
		else if (bscols == 3)
		{
			bsclass = 'col-md-3';
			bsclassos = 'col-md-offset-3';
		}
		else
		{
			bsclass = 'col-md-3';
			bsclassos = 'col-md-offset-6';
		}
			
		if(navitems == null) {
            navitems = new List<NavigationItems>();
            if (userprofile.contains('New'))
            {
            	navitems.add(new NavigationItems('main_nav_small_welcome.png','Welcome',bsclass+' '+bsclassos,welcomeurl));
            	offset = true;
            }
            if (pagename != 'ANNOUNCE')
            	if (offset)
            		navitems.add(new NavigationItems('whats_new_small_whats_new.png','What\'s New?',bsclass,baseurl+'HRConnectNew'));
            	else
            	{
            		navitems.add(new NavigationItems('whats_new_small_whats_new.png','What\'s New?',bsclass+' '+bsclassos,baseurl+'HRConnectNew'));
            		offset = true;
            	}
            
            if (pagename != 'ANSWERS')
            	if (offset)
            		navitems.add(new NavigationItems('main_nav_small_find_answers.png','Search HR',bsclass,baseurl+'HRConnectAnswers'));
            	else
            	{
            		navitems.add(new NavigationItems('main_nav_small_find_answers.png','Search HR',bsclass+' '+bsclassos,baseurl+'HRConnectAnswers'));
            		offset = true;
            	}
            
            if (pagename != 'CONTACT')
            	if (offset)
            		navitems.add(new NavigationItems('main_nav_small_ask_HR.png','Ask HR',bsclass,baseurl+'HRConnectCase'));  
            	else
            	{
            		navitems.add(new NavigationItems('main_nav_small_ask_HR.png','Ask HR',bsclass+' '+bsclassos,baseurl+'HRConnectCase'));
            		offset = true;
            	}          	
            
            //if (pagename != 'HR' && bscols == 4)
            //	navitems.add(new NavigationItems('announce_sm.PNG','HR for HR',bsclass));
            //if (pagename != 'RESOURCES')
            //	navitems.add(new NavigationItems('announce_sm.PNG','Resources',bsclass,''));
        }
        return navitems;
	}		
}