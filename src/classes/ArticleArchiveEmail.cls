/******
Class Name: ArticleArchiveEmail
Created By: Manish Shrestha
Created Date: 07/08/2014
Description:This Apex class implements the Messaging.InboundEmailHandler interface and is used by ArticleArchiveEmailService Email Service.
            
            Email comes in the following format(Example):
            ID:ka1J000000004kr
            Archive Date:7/9/2014
            Publish Status:Draft
            Article Type:FAQ

*****/
global class ArticleArchiveEmail implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, 
                                                         Messaging.Inboundenvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        String id;
        String archiveDate;
        String publishStatus;
        String articleType; 
        String articleId;
        try{
            String[] splitString = email.plainTextBody.split('\n');
            
            if(!splitString[0].endsWith(':'))
                id = splitString[0].split(':')[1];
            if(!splitString[1].endsWith(':'))
                archiveDate = splitString[1].split(':')[1];
            if(!splitString[2].endsWith(':'))
                publishStatus = splitString[2].split(':')[1];
            if(!splitString[3].endsWith(':'))
                articleType = splitString[3].split(':')[1];
            
            articleType = articleType.replaceAll(' ','_');
            articleType = articleType.replaceAll('_&_','_');
            System.debug('Desc:'+ id + ':' + archiveDate + ':' + publishStatus + ':' + articleType);
            
            List<sObject> kav = Database.query('SELECT KnowledgeArticleId FROM ' + articleType + '__kav WHERE Id = :id');            
            if(kav.size() > 0)
                articleId = (String) kav.get(0).get('KnowledgeArticleId');
            
            if(archiveDate == null || archiveDate == ''){
                //Blank Archive Date means, archiving has been removed from previously archived document, or has never been scheduled to archive. So Do Nothing
            }
            else{
                //Else Archive the article accordingly
                String[] dates = archiveDate.split('/');
                Datetime archiveDatetime = Datetime.valueOf(dates[2] + '-' + dates[0] + '-' + dates[1] + ' 23:00:00');
            
                System.debug('Article:'+ articleId + ':' + archiveDatetime);
    
                if(!publishStatus.equals('Published'))
                    KbManagement.PublishingService.publishArticle(articleId, false);
                
                //Archive the Article now
                KbManagement.PublishingService.archiveOnlineArticle(articleId, archiveDatetime);           

            }
            result.success = true;
        } catch (Exception e) {
            result.success = false;
            result.message = 'Oops, Archiving request failed for ' + articleId + ' because of following reason: '+e;
            System.debug('Failed: '+e);

        }
        return result;                                         
    }
}