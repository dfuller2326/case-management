public with sharing class CloseInvestigationController {
    
    public List<Investigated_Employee__c> investigatedEmployees;
    public Case thisCase;
    
    public CloseInvestigationController(){
        String caseId = ApexPages.currentPage().getParameters().get('CaseId');
        thisCase = [SELECT Id, CaseNumber, Contact.Name
                    FROM Case
                    WHERE Id = :caseId];
    }
    
    public Case getCase(){
        return thisCase;
    }
    
    public Boolean getHasEditAccess(){
        List<UserRecordAccess> access = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :thisCase.Id];
        if(access != null && access.size()>0)
            return access.get(0).HasEditAccess;
        return false;
    }
    
    public void ieSave(){
        try{
            update investigatedEmployees;
        }catch (Exception e){
            String msg=e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg)); 
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Changes Saved Successfully'));
    }
    
    public List<Investigated_Employee__c> getInvestigatedEmployees()
    {
       if ( (null!=thisCase.id) && (investigatedEmployees == null) )
       {
           investigatedEmployees = 
                   [SELECT Id, Type__c, Type_Detail__c, Sub_Type__c, Sub_Type_Detail__c,
                           Investigation_Status__c, Investigation_Closed_Reason__c,
                           Investigation_Closed_Comment__c, Employee__r.Name
                    FROM Investigated_Employee__c
                    WHERE Case__r.Id = : getCase().ID
                    ORDER BY CreatedDate];
       }
                           
       return investigatedEmployees;
    }
}