/**
* @description controller class for Admin page
*
*/
public with sharing class LMSPortal_ContentInlineController {
    
    //variables for business units picklist
    public Map<String, Boolean> businessUnitMap{get;set;}
    public List<String> leftselectedBU{get;set;}
    public List<String> rightselectedBU{get;set;}
    
    //variables for business units picklist
    public Map<String, Boolean> divisionMap{get;set;}
    public List<String> leftselectedDiv{get;set;}
    public List<String> rightselectedDiv{get;set;}
    
    //variables for locations picklist
    public String divDropdownSelected {get; set;}
    public Map<String, Map<String, Boolean>> locDivMap{get;set;}
    public List<String> leftselectedLoc{get;set;}
    public List<String> rightselectedLoc{get;set;}
    
    public Id recordId {get;set;}
    public boolean isChecked {get;set;}
    
    //variable for page refresh
    public String urlVal {get;set;}
    public Boolean succMsg {get;set;}
    
    //variables for image uploads
    public transient Blob bannerfilebody{get;set;}
    public String bannerfilename{get;set;}  
    public transient Blob thumbfilebody{get;set;}
    public String thumbfilename{get;set;}
    
    //attribute determining  display of image upload section
    public Boolean showDocument {
        get; 
        set {
            if(value != null)
            {
                showDocument = value;               
            }
        }
    }
    
    //Constructor
    public LMSPortal_ContentInlineController(){
        succMsg = false;
        isChecked = false;
        recordId= ApexPages.currentPage().getParameters().get('Id');
        urlVal = URL.getSalesforceBaseUrl().toExternalForm() + '/' + recordId;
        
        leftselectedBU = new List<String>();
        rightselectedBU = new List<String>();
        businessUnitMap = new Map<String, Boolean>();
        getBUPicklist();
        
        leftselectedDiv = new List<String>();
        rightselectedDiv = new List<String>();
        divisionMap = new Map<String, Boolean>();
        getDivPicklist(true);
        
        divDropdownSelected = getSelDivInDropdown();
        leftselectedLoc = new List<String>();
        rightselectedLoc = new List<String>();
        locDivMap = new Map<String, Map<String, Boolean>>();
        getLocPicklist(true);

    }

    /**
    * @description method to get the pickliast values for the business unit
    */
    public void getBUPicklist(){    
        SYSTEM.DEBUG('Entered getBUPicklist :: ');
            
        //complete list of business units
        Set<String> busList = getBusinessUnits();
        
        //get the already chosen business units
        List<Learning_Content_Access__c> contentAccessList = new List<Learning_Content_Access__c>();
        if(recordId.getSobjectType() == Schema.LMS_Content__c.SObjectType){
            contentAccessList = [Select Business_Unit__c From Learning_Content_Access__c Where Learning_Content__c =: recordId];
        }
        else if(recordId.getSobjectType() == Schema.LMS_Link__c.SObjectType){
            contentAccessList = [Select Business_Unit__c From Learning_Content_Access__c Where Learning_Link__c =: recordId];
        }
        
        Set<String> selectedBU = new Set<String>();
        if(contentAccessList != null && !contentAccessList.isEmpty()){
            for(Learning_Content_Access__c ca: contentAccessList){
                selectedBU.add(ca.Business_Unit__c);
            }
        }
        
        //update the business unit map for selected and available
        for(String bu : busList){
            if(selectedBU.contains(bu)){
                businessUnitMap.put(bu, true);
            }else{
                businessUnitMap.put(bu, false);
            }
        }
        
    }
    
    /**
    * @description method to get the pickliast values for the divisions
    * @param Boolean: isInvoked
    */
    public void getDivPicklist(Boolean onload){
        SYSTEM.DEBUG('Entered getDivPicklist :: ');
        
        //get the list of selected business units
        Set<String> tempBUSelList = new Set<String>();
        if(businessUnitMap != null && !businessUnitMap.isEmpty()){
            for(String bu : businessUnitMap.keySet()){  
                if(businessUnitMap.get(bu) == true){
                    tempBUSelList.add(bu);
                }
            }
        }
        SYSTEM.DEBUG('list of selected business units :: ' + tempBUSelList);
        
        //loop through all available divisions and pick the ones associated to selected business units
        Map<String, String> buDivMap = getDivisionBusniessUnitMap();
        Set<String> availDivs = new Set<String>();
        if(buDivMap != null && !buDivMap.isEmpty()){
            for(String div : buDivMap.keySet()){
                if(tempBUSelList.contains(buDivMap.get(div))){
                    availDivs.add(div);
                }
            }
        }
        SYSTEM.DEBUG('available divs :: ' + availDivs);
        
        if(onload){
            //get the already chosen division list      
            List<Learning_Content_Access__c> contentAccessList = new List<Learning_Content_Access__c>();
            if(recordId.getSobjectType() == Schema.LMS_Content__c.SObjectType){
                contentAccessList = [Select Division__c From Learning_Content_Access__c Where Learning_Content__c =: recordId];
            }
            else if(recordId.getSobjectType() == Schema.LMS_Link__c.SObjectType){
                contentAccessList = [Select Division__c From Learning_Content_Access__c Where Learning_Link__c =: recordId];
            }
            Set<String> selDivs = new Set<String>();
            if(contentAccessList != null && !contentAccessList.isEmpty()){
                for(Learning_Content_Access__c ca: contentAccessList){
                    selDivs.add(ca.Division__c);
                }
            }
            
            //update the division map for selected and available
            if(availDivs != null && !availDivs.isEmpty()){
                for(String div : availDivs){
                    if(selDivs.contains(div)){
                        divisionMap.put(div, true);
                    }else{
                        divisionMap.put(div, false);
                    }
                }   
            }       
        }
        else{
            String firstSelDiv = getSelDivInDropdown();
            if(divDropdownSelected != null || firstSelDiv != null){
                if(divDropdownSelected == null){
                    divDropdownSelected = firstSelDiv;
                }
            }
            
            // remove/add division if related business unit is unselected/selected
            Set<String> alldivsFromMap = new Set<String>();
            if(divisionMap != null && !divisionMap.isEmpty()){
                for(String div : divisionMap.keySet()){
                    alldivsFromMap.add(div);
                }
            }
            if(availDivs != null && !availDivs.isEmpty()){
                for(String avail : availDivs){
                    if(!alldivsFromMap.contains(avail)){
                        divisionMap.put(avail, false);
                    }                   
                }
            }
            if(divisionMap != null && !divisionMap.isEmpty()){
                for(String div : divisionMap.keySet()){
                    if(!availDivs.contains(div)){
                        divisionMap.remove(div);
                    }
                }
            }
        }
    }
    
    /**
    * @description method to get the pickliast values for the locations
    * @param Boolean: isInvoked
    */
    public void getLocPicklist(Boolean onload){
        SYSTEM.DEBUG('Entered getBUPicklist :: ');
            
        //complete list of locations 
        Set<String> allLocList = getAvailableLocs();    
        
        if(onload){
            //get the already chosen location list
            List<Learning_Content_Access__c> contentAccessList = new List<Learning_Content_Access__c>();
            if(recordId.getSobjectType() == Schema.LMS_Content__c.SObjectType){
                contentAccessList = [Select Division__c, Location__c From Learning_Content_Access__c Where Learning_Content__c =: recordId];
            }
            else if(recordId.getSobjectType() == Schema.LMS_Link__c.SObjectType){
                contentAccessList = [Select Division__c, Location__c From Learning_Content_Access__c Where Learning_Link__c =: recordId];
            }
            
            Map<String, Boolean> locMap;
            if(contentAccessList != null && !contentAccessList.isEmpty()){
                for(Learning_Content_Access__c ca: contentAccessList){
                    locMap = new Map<String, Boolean>();
                    if(ca.Location__c != null){
                        String locStr = ca.Location__c;
                        if(locStr.contains(';')){
                            List<String> splitList = locStr.split(';');
                            Set<String> locSets = new Set<String>();
                            locSets.addAll(splitList);
                            for(String loc : allLocList){
                                if(locSets.contains(loc)){
                                    locMap.put(loc, true); 
                                }
                                else{
                                    locMap.put(loc, false);
                                }
                            }
                        }
                        else{
                            for(String loc: allLocList){
                                if(loc == ca.Location__c){
                                    locMap.put(loc, true);
                                }else{
                                    locMap.put(loc, false);
                                }
                        }
                        }
                    }else{
                        for(String loc: allLocList){
                            locMap.put(loc, false);
                        }
                    }
                    
                    locDivMap.put(ca.Division__c, locMap);
                }
            }
        }else{
            // get list of division from right division panel
            Set<String> selectedDivs = new Set<String>();
            if(divisionMap != null && !divisionMap.isEmpty()){
                for(String div : divisionMap.keySet()){
                    if(divisionMap.get(div)){
                        selectedDivs.add(div);
                    }
                }
            }
            
            //get all division from locDivMap
            Set<String> alldivsFromMap = new Set<String>();
            Set<String> tempDivs = new Set<String>();
            if(divisionMap != null && !divisionMap.isEmpty()){
                for(String div : divisionMap.keySet()){
                    tempDivs.add(div);
                }
            }
            if(locDivMap != null && !locDivMap.isEmpty()){
                for(String div : locDivMap.keySet()){
                    if(divisionMap != null && !divisionMap.isEmpty()){
                        if(tempDivs.contains(div)){
                            if(divisionMap.get(div)){
                                alldivsFromMap.add(div);
                            }
                        }
                    }
                }
            }
            
            //add new selected divisions to locDivMap
            if(selectedDivs != null && !selectedDivs.isEmpty()){
                for(String sel : selectedDivs){
                    if(!alldivsFromMap.contains(sel)){
                        Map<String, Boolean> tempMap = new Map<String, Boolean>();
                        for(String loc: allLocList){
                            tempMap.put(loc, false);
                        }
                        locDivMap.put(sel, tempMap);
                    }                   
                }
            }
            
            //remove unselected divisions from locDivMap
            if(locDivMap != null && !locDivMap.isEmpty()){
                for(String div : locDivMap.keySet()){
                    if(!selectedDivs.contains(div)){
                        locDivMap.remove(div);
                    }
                }
            }
        }
    
    }
    
    /**
    * @description method to move the selected business unit from available list to selected list
    * @return PageReference: null
    */ 
    public PageReference selectclickBU(){
        onClick(leftselectedBU, businessUnitMap, true, false);
        getDivPicklist(false);
        getLocPicklist(false);
        isChecked = false;
        return null;
    }
    
    /**
    * @description method to remove the business unit from selcted list
    * @return PageReference: null
    */  
    public PageReference unselectclickBU(){
        onClick(rightselectedBU, businessUnitMap, false, false);
        getDivPicklist(false);
        getLocPicklist(false);
        isChecked = false;
        return null;
    }
    
    /**
    * @description method to retrive the list of business units which are to be removed from already selcted list
    * @return List<SelectOption>: list of business units which will be removed from already selected list
    */ 
    public List<SelectOption> getUnselectedBUValues(){
        return getpicklistValues(businessUnitMap, false);
    }
   
    /**
    * @description method to retrive the list of business units to add to selcted list
    * @return List<SelectOption>: list of business units which are to be added
    */  
    public List<SelectOption> getSelectedBUValues(){
        return getpicklistValues(businessUnitMap, true);
    }
    
    /**
    * @description method to move the selected divisions from available list to selected list
    * @return PageReference: null
    */
    public PageReference selectclickDiv(){
        onClick(leftselectedDiv, divisionMap, true, false);
        getLocPicklist(false);
        isChecked = false;
        return null;
    }
     
    /**
    * @description method to remove the divisions from selcted list
    * @return PageReference: null
    */  
    public PageReference unselectclickDiv(){
        onClick(rightselectedDiv, divisionMap, false, false);
        getLocPicklist(false);
        isChecked = false;
        return null;
    }
    
    /**
    * @description method to retrive the list of divisions which are to be removed from already selcted list
    * @return List<SelectOption>: list of divisions which will be removed from already selected list
    */ 
    public List<SelectOption> getUnselectedDivValues(){
        return getpicklistValues(divisionMap, false);
    }
 
    /**
    * @description method to retrive the list of divisions to add to selected list
    * @return List<SelectOption>: list of divisions which are to be added
    */  
    public List<SelectOption> getSelectedDivValues(){
        return getpicklistValues(divisionMap, true);
    }
    
    /**
    * @description method to move the selected locations from available list to selected list
    * @return PageReference: null
    */
    public PageReference selectclickLoc(){
        String firstSelDiv = getSelDivInDropdown();
        if(divDropdownSelected != null || firstSelDiv != null){
            if(divDropdownSelected == null){
                divDropdownSelected = firstSelDiv;
            }
        }
        onClick(leftselectedLoc, locDivmap.get(divDropdownSelected), false, true);
        getLocPicklist(false);
        isChecked = false;
        return null;
    }
     
    /**
    * @description method to remove the locations from selcted list
    * @return PageReference: null
    */  
    public PageReference unselectclickLoc(){
        String firstSelDiv = getSelDivInDropdown();
        if(divDropdownSelected != null || firstSelDiv != null){
            if(divDropdownSelected == null){
                divDropdownSelected = firstSelDiv;
            }
        }
        onClick(rightselectedLoc, locDivmap.get(divDropdownSelected), true, true);
        getLocPicklist(false);
        isChecked = false;
        return null;
    }
    
    /**
    * @description method to reset the locations panel when division is changed from dropdown
    */
    public void resetLocPicklist(){
        getLocPicklist(false);
    }
    
    /**
    * @description method to reset the locations panel when select all is checked
    */
    public void selectAllLocations(){
        Map<String, Boolean> locMap;
        Set<string> allLocs = getAvailableLocs();       
        if(divisionMap != null && !divisionMap.isEmpty()){
            if(isChecked){
                for(String div : divisionMap.keySet()){
                    locMap = new Map<String, Boolean>();
                    locDivMap.remove(div);
                    for(String loc : allLocs){
                        locMap.put(loc, true);
                    }
                    locDivMap.put(div, locMap);
                }
            }else{
                for(String div : divisionMap.keySet()){
                    locMap = new Map<String, Boolean>();
                    locDivMap.remove(div);
                    for(String loc : allLocs){
                        locMap.put(loc, false);
                    }
                    locDivMap.put(div, locMap);
                }
            }
        }
    }
    
    /**
    * @description method to retrive the list of locations which are to be removed from already selcted list
    * @return List<SelectOption>: list of locations which will be removed from already selected list
    */ 
    public List<SelectOption> getUnselectedLocValues(){
        List<SelectOption> unselLoc = new List<SelectOption>();
        String firstSelDiv = getSelDivInDropdown();
        if(divDropdownSelected != null || firstSelDiv != null){
            if(divDropdownSelected == null){
                divDropdownSelected = firstSelDiv;
            }
            unselLoc.addAll(getpicklistValues(locDivmap.get(divDropdownSelected), false));
        }
        return unselLoc;
    }
 
    /**
    * @description method to retrive the list of locations to add to selcted list
    * @return List<SelectOption>: list of locations which are to be added
    */  
    public List<SelectOption> getSelectedLocValues(){
        List<SelectOption> selLocMap = new List<SelectOption>();
        String firstSelDiv = getSelDivInDropdown();
        if(divDropdownSelected != null || firstSelDiv != null){
            if(divDropdownSelected == null){
                divDropdownSelected = firstSelDiv;
            }
            selLocMap.addAll(getpicklistValues(locDivmap.get(divDropdownSelected), true));
        }
        return selLocMap;
    }
    
    /**
    * @description method to save recors to the learning content access object
    */
    public PageReference saveContentAccess(){
        List<Learning_Content_Access__c> lcaList = new List<Learning_Content_Access__c>();
        Learning_Content_Access__c templca;
        system.debug('ENTERED SAVECONTENTACEESS METHOD');
        List<Learning_Content_Access__c> existingLCAList = new List<Learning_Content_Access__c>();
        if(recordId.getSobjectType() == Schema.LMS_Content__c.SObjectType){
            existingLCAList = [SELECT Id, Business_Unit__c,Division__c, Location__c, Learning_Content__c From Learning_Content_Access__c Where Learning_Content__c =: recordId];    
        }
        else if(recordId.getSobjectType() == Schema.LMS_Link__c.SObjectType){
            existingLCAList = [SELECT Id, Business_Unit__c,Division__c, Location__c, Learning_Link__c From Learning_Content_Access__c Where Learning_Link__c =: recordId];
        }
        
        //update the banner ids to the learning content object
        LMS_Content__c docObj = new LMS_Content__c();
        if(showDocument){
            docObj = uploadDocs();
        }
        
        //get the existing divisions list
        Set<String> existingDivList = new Set<String>();
        Boolean docuploaded = true;
        
        //get the selected divisions list
        Set<String> selectedDivs = new Set<String>();
        if(divisionMap != null && !divisionMap.isEmpty()){
            for(String div : divisionMap.keySet()){
                if(divisionMap.get(div)){
                    selectedDivs.add(div);
                }
            }
        }
        
        if(!selectedDivs.isEmpty() && selectedDivs != null)
        {
            for(String div : selectedDivs)
            {               
                templca = new Learning_Content_Access__c();
                templca.Division__c = div;
                /*
                    * Added logic to check the SObject token for the recordId and insert into the appropriate field depending on if the save occurs on the Learning Content object
                    or the Learning Link object
                */
                
                if(recordId.getSobjectType() == Schema.LMS_Link__c.SObjectType){
                    templca.Learning_Link__c = recordId;
                }
                else if(recordId.getSobjectType() == Schema.LMS_Content__c.SObjectType){
                    templca.Learning_Content__c = recordId;
                }
                
                Map<String, String> buDivMap = getDivisionBusniessUnitMap();
                templca.Business_Unit__c = buDivMap.get(div);
                
                String locStr = '';
                Integer selLocCount = 0;
                Map<String, Boolean> temploc = locDivMap.get(div);
                if(temploc != null && !temploc.isEmpty()){
                    for(String loc : temploc.keySet()){
                        if(tempLoc.get(loc)){
                            locStr += loc.trim() + '; ';
                            selLocCount++;
                        }
                    }
                }
                if(selLocCount < 1){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.ERROR,' Please associate atleast one Location per Divison');
                    ApexPages.addMessage(msg);
                    return null;
                }
                templca.Location__c = locStr.substring(0, locStr.length()-2);
                lcaList.add(templca);
                
                if(!existingLCAList.isEmpty() && existingLCAList != null){
                    for(Learning_Content_Access__c ex : existingLCAList){
                        existingDivList.add(ex.Division__c);
                        if(ex.Division__c == div){
                            templca.Id = ex.Id;
                        }
                    }
                }
                lcaList.add(templca);                               
            }
        }else{
            system.debug('in else ::' + existingDivList.size());
            if(existingLCAList.size() == 0 || existingDivList.size() == 0){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.ERROR,' Please select Business Unit and division');
                ApexPages.addMessage(msg);
                return null;
            }
        }
        
        Integer failUpsertCount = 0;
        if(!lcaList.isEmpty() && lcaList != null){
            try{
                if(showDocument){
                    if(docObj == null){
                        List<LMS_Content__c> templcList = [SELECT Id, Banner_Document_Id__c, Thumbnail_Document_Id__c, New_Content__c FROM LMS_Content__c WHERE Id =: recordId];
                        if(templcList[0].New_Content__c){
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.ERROR, 'Upload Banner/Thumbnail Image');
                            ApexPages.addMessage(msg);
                            return null;
                        }
                        else{                           
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.ERROR, 'Upload Thumbnail Image');
                            ApexPages.addMessage(msg);
                            return null;
                        }
                        return null;
                    }else{
                        Database.SaveResult updateResult = Database.Update(docObj, false);
                        system.debug('UPDATERESULT FOR DOCUMENT UPLOAD: ' + updateResult);
                    
                        if (!updateResult.isSuccess()) {
                            docuploaded = false;
                        }
                    }
                }              
                Set<Learning_Content_Access__c> LCSet = new Set<Learning_Content_Access__c>();
                LCSet.addAll(lcaList);
                lcaList.clear();
                lcaList.addAll(LCSet);

                List<Database.UpsertResult> upsertResults = Database.upsert(lcaList, true);
                for(Database.UpsertResult ur : upsertResults){
                    if (!ur.isSuccess()) {
                        failUpsertCount++;
                    }
                
                }
            }catch(Exception ex){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.ERROR, ex.getMessage());
                ApexPages.addMessage(msg);
                return null;
            }
        }
        
        //delete records for existing rows and not selected
        //Delete the current Division from the Picklist if it is not found in the Learning Content Access object
        
        List<Learning_Content_Access__c> delList = new List<Learning_Content_Access__c>();
        if(!existingLCAList.isEmpty() && existingLCAList != null){
            for(Learning_Content_Access__c ex : existingLCAList){
                if(!selectedDivs.contains(ex.Division__c)){
                    delList.add(ex);
                }
            }
        }
        
        Integer failDeleteCount = 0;
        if(failUpsertCount < 1){            
            List<Database.DeleteResult> deleteResults = Database.delete(delList);
            for(Database.DeleteResult dr : deleteResults){
                if (!dr.isSuccess()) {
                    failDeleteCount++;
                }
            }
        }       
        
        // if no soql errors return true
        if(failUpsertCount <1 && docuploaded){
            system.debug('ALL RECORDS UPDATED SUCCESSFULLY');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.INFO, Label.Save_Successful_Msg);
            ApexPages.addMessage(msg);
            succMsg = true;
        }
        return null;    
        
    }
    
    /**
    * @description method to upload new documents
    * @return Boolean: true/false
    */
    public LMS_Content__c uploadDocs()
    {       
        List<LMS_Content__c> templcList = [SELECT Id, Banner_Document_Id__c, Thumbnail_Document_Id__c, New_Content__c FROM LMS_Content__c WHERE Id =: recordId];
        system.debug('ENTERED UPLOADDOCS METHOD');
        Folder lmsDocFolder = new Folder();
        if(!Test.isRunningTest()) {
            lmsDocFolder = [SELECT Id FROM Folder WHERE DeveloperName = :LMSPORTAL_Constants.LMSPORTAL_DOC_FOLDER LIMIT 1];
        }
        
        system.debug('BANNER DOC AND THUMBNAIL DOC :' + bannerfilename + ' : ' + thumbfilename);
        if(templcList[0].Thumbnail_Document_Id__c == null && thumbfilename == null){
            return null;
        }
        if(templcList[0].Banner_Document_Id__c == null && bannerfilename == null && templcList[0].New_Content__c){
            return null;
        }
        
        if(bannerfilename != null){
            Document doc = new Document();
            if(!Test.isRunningTest()) {
                doc.FolderId = lmsDocFolder.Id;
            } else {
                doc.FolderId = UserInfo.getUserId();
            }
            
            doc.Name=bannerfilename;
            doc.body=bannerfilebody;
            insert doc;
            templcList[0].Banner_Document_Id__c = doc.Id; 
        }
        if(thumbfilename != null){
            Document doc1 = new Document();
            if(!Test.isRunningTest()) {
                doc1.FolderId = lmsDocFolder.Id;
            } else {
                doc1.FolderId = UserInfo.getUserId();
            }
            doc1.Name=thumbfilename;
            doc1.body=thumbfilebody;
            insert doc1; 
            templcList[0].Thumbnail_Document_Id__c = doc1.Id;
        }       
        return templcList[0];
    }
    
    /**
    * @description method to get the picklist values
    * @param Set<String>: panelValues
    * @return List<SelectOption>: options
    */
    public List<SelectOption> getpicklistValues(Map<String, Boolean> picklist, Boolean flag){
        List<SelectOption> options = new List<SelectOption>();
        if(picklist != null && !picklist.isEmpty()) {
            List<String> tempList = new List<String>();
            for(String pl : picklist.keySet()){
                if(flag == picklist.get(pl)){
                    tempList.add(pl);
                }
            }
            tempList.sort();
            if(tempList != null && !tempList.isEmpty()){
                for(String s : tempList) {
                    options.add(new SelectOption(s,s));
                }
            }
        }
    	List<String> selDivs = new List<String>();
    	if(divisionMap != null && !divisionMap.isEmpty()){
    		for(String div : divisionMap.keySet()){
    			if(divisionMap.get(div)){
    				selDivs.add(div);	
    			}
    		}
    	}
    	selDivs.sort();
    	Set<String> tempList = new Set<String>();
    	tempList.addAll(selDivs);
    	if(!tempList.contains(divDropdownSelected)){
    		for(String div : tempList){
    			divDropdownSelected = div;
    			break;	
    		}
    	}
        return options;
    }
    
    /**
    * @description All Business Units
    * @return String: availLocs
    */
    public Set<String> getBusinessUnits(){
        //get the available business unit list
        List<BU_Division__c> buDivList = BU_Division__c.getall().values();
        Set<String> uniqueBuDivList = new Set<String>();
        if(buDivList != null && !buDivList.isEmpty()){
            for(BU_Division__c bd: buDivList) {
                if(!uniqueBuDivList.contains(bd.Business_Unit__c)) {
                    uniqueBuDivList.add(bd.Business_Unit__c);
                }
            }
        }
        return uniqueBuDivList;
    }
    
    /**
    * @description method to get business unit from division
    * @param String: div
    * @return String: buName
    */
    public Map<String, String> getDivisionBusniessUnitMap(){
        Map<String, String> budMap = new Map<String, String>();
        List<BU_Division__c> buDivList= [SELECT Business_Unit__c, Division__c FROM BU_Division__c ];
        if(buDivList != null && !buDivList.isEmpty()){
            for(BU_Division__c bud : buDivList){
                budMap.put(bud.Division__c, bud.Business_Unit__c);
            }
        }
        return budMap;
    }
    
    /**
    * @description All Locations
    * @return String: availLocs
    */
    private Set<String> getAvailableLocs(){
        //get the available location list
        Set<String> availLocs = new Set<String>();
        Schema.DescribeFieldResult fieldResultbu = Learning_Content_Access__c.Location__c.getDescribe();
        List<Schema.PicklistEntry> plebu = fieldResultbu.getPicklistValues(); 
        if(plebu != null && !plebu.isEmpty()){       
            for(Schema.PicklistEntry f : plebu)
            {
                availLocs.add(f.getLabel());
            }
        }
        return availLocs;
    }
    
    public void onClick(List<String> panelList, Map<String, Boolean> panelMap, Boolean flag, Boolean isLocation){
        Set<String> items = new Set<String>();
        items.addAll(panelList);
        if(isLocation){
            String firstSelDiv = getSelDivInDropdown();
            if(divDropdownSelected != null || firstSelDiv != null){
                if(divDropdownSelected == null){
                    divDropdownSelected = firstSelDiv;
                }
            }
            Map<String, Boolean> newLocMap = new Map<String, Boolean>();
            if(locDivMap.get(divDropdownSelected).keySet() != null && !locDivMap.get(divDropdownSelected).keySet().isEmpty()){
                for(String loc : locDivMap.get(divDropdownSelected).keySet()){
                    if(items.contains(loc)){
                        newLocMap.put(loc, !flag);
                    }else{
                        newLocMap.put(loc, locDivMap.get(divDropdownSelected).get(loc));
                    }
                }
            }
            locDivMap.remove(divDropdownSelected);
            locDivMap.put(divDropdownSelected, newLocMap);
        }else{
            if(panelMap.keySet() != null && !panelMap.keySet().isEmpty()){
                for(String i : panelMap.keySet()){
                    if(items.contains(i)){
                        panelMap.remove(i);
                        panelMap.put(i, flag);
                    }
                }
            }
        }
    }
    
    public String getSelDivInDropdown(){
        String firstSelDiv = null;
        List<String> temp = new List<String>();
        if(divisionMap != null && !divisionMap.isEmpty()){
            for(String div : divisionMap.keySet()){
                if(divisionMap.get(div)){
                	temp.add(div);
                }
            }
        }
        temp.sort();
        if(temp != null && !temp.isEmpty()){
        	for(String str : temp){
                firstSelDiv = str;
                break;        	
        	}
        }
        return firstSelDiv;
    }
}