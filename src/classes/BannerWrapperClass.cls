public with sharing class BannerWrapperClass {
	
	public LMS_Content__c contentRecord {get;set;}
	public String contentBannerUrl {get;set;}
	public String contentTitle {get;set;}
	public String contentSummary {get;set;}
	public Integer count {get;set;}
	public String imageUrlConst = LMSPortal_Constants.DOCUMENT_URL;

	public BannerWrapperClass(Integer count, String documentId, LMS_Content__c contentRecord){
		this.contentRecord = contentRecord;
		this.contentBannerUrl = imageUrlConst + documentId;
		this.contentTitle = contentRecord.Title__c;
		this.contentSummary = contentRecord.Summary__c;
		this.count = count;
		//This is a test change in Banner Wrapper class
	}

}