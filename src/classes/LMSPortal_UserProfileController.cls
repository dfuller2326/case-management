public with sharing class LMSPortal_UserProfileController {
	public User userDetails {get;set;}
	
	public LMSPortal_UserProfileController(){
		userDetails = new User();
		getUserInfo();
	}
	
	public User getUserInfo(){
		try{
			List<User> userList = [Select Name, Division__c, Business_Unit__c From User where Id = : UserInfo.getUserId()];
			userDetails = userList[0];
			return userDetails;
		}
		catch(Exception ex){
			system.debug('Exception in getting user detials: ' + ex.getmessage());
		}
		return null;
	}

}