@isTest
private class ArticleArchiveEmail_Test{    

    @isTest
    static void testArticleArchiveEmail(){
        // Create a new email and envelope object       
        Messaging.InboundEmail email  = new Messaging.InboundEmail();       
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();             
        
        // Set up your data if you need to             
    	// Create a test article
    	FAQ__kav article = new FAQ__kav();
        article.title = 'Test article';
        article.body__c = 'This is a test article.';
        article.UrlName = 'test';
        insert article;
        
        // Create the email body       
        email.plainTextBody = 'ID:'+article.Id+'\nArchive Date:'+Datetime.now().format('MM/dd/YYYY')+'\nPublish Status:Draft\nArticle Type:FAQ';       
        email.fromAddress = 'test@test.com';    
        email.ccAddresses = new String[] {'articlearchiveemailservice@8yp60u2e15qigdpicvtf2rzuki57vh38zqxzvo5y03933my3s.j-3kscima0.cs10.apex.sandbox.salesforce.com'};
        email.subject = 'Archive FAQ Article';
        
        ArticleArchiveEmail aae = new ArticleArchiveEmail();             
        
        Messaging.InboundEmailResult result = aae.handleInboundEmail(email, env);       
        
        
        //System.assert (result.success, true); 
        //
      	//Do negative test, try to archive the same article again
      	// Create the email body       
        email.plainTextBody = 'ID:'+article.Id+'\nArchive Date:'+Datetime.now().format('MM/dd/YYYY')+'\nPublish Status:Draft\nArticle Type:FAQ';       
        email.fromAddress = 'test@test.com';    
        email.ccAddresses = new String[] {'articlearchiveemailservice@8yp60u2e15qigdpicvtf2rzuki57vh38zqxzvo5y03933my3s.j-3kscima0.cs10.apex.sandbox.salesforce.com'};
        email.subject = 'Archive FAQ Article';
        
        
        result = aae.handleInboundEmail(email, env);       
        
    }
}