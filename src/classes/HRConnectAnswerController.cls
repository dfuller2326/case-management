public with sharing class HRConnectAnswerController {
public String articlebody {get; set;}
public String articletitle {get; set;}
public String articleId {get; set;}
//private ApexPages.StandardController controller;
public String ChildCategory {get; set;}
public String pagetext {get; set;}

//Page Size
public Integer PAGE_NUMBER {get; set;}
//Search String used in ArticleList tag
public String searchstring { get; set; }
public String id { get; set; }
//public String cs { get; set; }
//public String feedback { get; set; }
static String userlang = HRConnectCommon.userlang;

//public HRConnectext(ApexPages.StandardController stdController) {
public HRConnectAnswerController() {
	//String qryString = 'SELECT Id, title, UrlName, LastPublishedDate,LastModifiedById FROM KnowledgeArticleVersion WHERE (PublishStatus = \'online\' and Language = \'en_US\')';
	//List<KnowledgeArticleVersion> articleList= Database.query(qryString);
	//maxSize = articleList.size() ;

	HRConnect_Portal_Config__c cs = HRConnect_Portal_Config__c.getValues('PORTAL_CONFIG_LIST');
	if (cs.Answers_Page_Size__c != null && cs.Answers_Page_Size__c > 0)	
		PAGE_NUMBER = (integer)cs.Answers_Page_Size__c;
	else
		PAGE_NUMBER = 10;
				
	searchstring = ApexPages.currentPage().getParameters().get('ss');
	id = ApexPages.currentPage().getParameters().get('id');

	//this.hrcase = (Case)stdController.getRecord();
	//this.controller = stdController;
	SortSearch = 'mostViewed';	//'lastUpdated';
	//if (CategoryStr == null)
	CategoryStr = 'Content_Category:';
	ShowFavs = 'false';
	currentPage = 1;
	system.debug('In HRConnectext, CategoryStr: '+CategoryStr);
}

//Keeps track of current page & max size of article list
public Integer currentPage {get;set;}
public Integer maxSize {get;set;}
// Returns whether we need to see previous button or not
public boolean getPrevRequired() {
return currentPage > 1;
}
// Returns whether we need to see next button or not
public boolean getNextRequired() {
return currentPage * PAGE_NUMBER < maxSize;
}
//Returns current page number
public Decimal getCurrentPageNumber() {
return this.currentPage;
}
//action for next click
public PageReference next() {
if(maxSize > this.currentPage * PAGE_NUMBER) {
this.currentPage = this.currentPage + 1;
}
return null;
}
//action for previous click
public PageReference previous() {
if(this.currentPage > 1)
this.currentPage = this.currentPage - 1;
return null;
}
//execute SOSL to find result count
public String Requery  {
	get {
	system.debug('currentPage: '+currentPage);
	String qryString;
if(this.currentPage == 1)
{
	if(searchString != null && searchString.length() >0 ) {
		try
		{
			if (searchString.length()==1)
			{
				maxSize = 0;
			}
			else
			{
				//String sstemp = searchString.replaceAll('\\','');
				String searchquery = 'FIND \'' + String.escapeSingleQuotes(searchString) + '\' IN ALL FIELDS RETURNING	KnowledgeArticleVersion(Id, title, UrlName, LastPublishedDate,LastModifiedById where PublishStatus =\'online\' and Language = \''+userlang+'\')';
				//WITH DATA CATEGORY Content_Category__c ABOVE_OR_BELOW all__c
				List<List<SObject>>searchList = search.query(searchquery);
				List<KnowledgeArticleVersion> articleList = (List<KnowledgeArticleVersion>)searchList[0];
				maxSize = articleList.size() ;
			}
		}
		catch(exception e)
		{
			system.debug(e.getMessage());
			maxSize = 0;
		}
	}
	else {
			if (CategoryStr=='Content_Category:' || CategoryStr == null)				
				qryString = 'SELECT Id, title, UrlName, LastPublishedDate,LastModifiedById FROM KnowledgeArticleVersion WHERE (PublishStatus = \'online\' and Language = \''+userlang+'\')';
				//WITH DATA CATEGORY Content_Category__c ABOVE_OR_BELOW all__c
			else
				qryString = 'SELECT Id, title, UrlName, LastPublishedDate,LastModifiedById FROM KnowledgeArticleVersion WHERE (PublishStatus = \'online\' and Language = \''+userlang+'\') WITH DATA CATEGORY Content_Category__c BELOW '+CategoryStr.replace('Content_Category:','')+'__c';
				
			system.debug('qryString: '+qryString);
			List<KnowledgeArticleVersion> articleList= Database.query(qryString);
			maxSize = articleList.size() ;
			system.debug('maxSize: '+maxSize);
	}
}
	decimal m = maxSize;
	decimal totalpages = m.divide((decimal)PAGE_NUMBER,0,system.RoundingMode.CEILING);
	//integer totalpages = (integer)tp.setScale(0,system.RoundingMode.UP);//(integer)tp.round(system.roundingMode.CEILING);
	if (totalpages > 0)
		pagetext = '('+string.valueOf(currentPage)+' of '+string.valueOf(totalpages)+')';
	else
		pagetext = '(No matching results found)';	
	//pagetext = string.valueOf(tp)+' '+string.valueOf(tp.setScale(0,system.RoundingMode.UP));
		
return null;
}
	set{}
}
/*public PageReference displayDetails() {
    KnowledgeArticleVersion articleqry = [SELECT title,summary FROM KnowledgeArticleVersion WHERE PublishStatus = 'online' and Language = 'en_US' and knowledgearticleid=:articleId];
        articlebody = articleqry.summary;
        articletitle = articleqry.title;
        return null;
    }
public PageReference displayTitle() {
    //Id articleId = Id.valueOf(this.articleId);
    KnowledgeArticleVersion articleqry = [SELECT title FROM KnowledgeArticleVersion WHERE PublishStatus = 'online' and Language = 'en_US' and knowledgearticleid=:articleId];
        articletitle = articleqry.title;
        return null;
    }*/
    
//public List<SelectOption> getHelpful() {
//        List<SelectOption> options = new List<SelectOption>(); 
//        options.add(new SelectOption('Y','Yes')); 
//        options.add(new SelectOption('N','No')); 
//    return options; 
//    }

/*public PageReference doSave() {

        //this.controller.save();
        //return newPage;

        Case hrcase;      
        PageReference ret = controller.save();
        hrcase = (Case)controller.getRecord();        
        system.assertNotEquals(null, hrcase.Id); // BOOM!
        ret = New PageReference('/apex/HRConnect');
        //newPage.getParameters().put('id',ApexPages.currentPage().getParameters().get('id'));
        ret.setRedirect(true);        
        return ret;
    } */
       
	public List<SelectOption> getSortOptions() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('lastUpdated','Published Date'));
            options.add(new SelectOption('mostViewed','Most Viewed'));
            options.add(new SelectOption('title','Title'));
            return options;
        }
            
	public String SortSearch {get;set;}
	public String CategoryStr {get;
		set
		{
			system.debug('In CategoryStr, value: '+value);
			if (value.contains('Content_Category:'))
				CategoryStr = value;
			else
				CategoryStr = 'Content_Category:'+value;
		}
	}
/*	public PageReference dummy() {
		system.debug('Dummy: ChildCategory'+ChildCategory);
		//system.debug('Dummy: cn'+ApexPages.currentPage().getParameters().get('cn'));
		CategoryStr = 'My_Career:'+ChildCategory;
        return null;
    }*/
	public class ArticleCategories
	{
		public String catname {get;set;}
		public String catcode {get;set;}
		public String id {get;set;}
		public String imgname {get;set;}
		
		ArticleCategories(String cname, String ccode, String id,String img)
		{
			catname = cname;
			catcode = ccode;
			this.id = id;
			imgname = img;
		}		
	}    
	List<ArticleCategories> cats;

	public List<ArticleCategories> getArticleCategories()
	{	
		if(cats == null) {
			string userprofile = HRConnectCommon.userprofilename;
			boolean ismgr = HRConnectCommon.ismanager;
			
            cats = new List<ArticleCategories>();
            cats.add(new ArticleCategories('My Life','My_Life','lnid1','mylife.png'));            
            cats.add(new ArticleCategories('My Career','My_Career','lnid2','mycareer.png'));
            cats.add(new ArticleCategories('My Health','My_Health','lnid3','myhealth.png'));
            cats.add(new ArticleCategories('My Money','My_Money','lnid4','mymoney.png'));
            
            if (userprofile.contains('Manager') || userprofile.contains('Exec') || ismgr)
            	cats.add(new ArticleCategories('My Team','My_Team','lnid5','myteam.png'));
            
            cats.add(new ArticleCategories('My Actions','My_Actions','lnid6','myprofile.png'));
            
            cats.add(new ArticleCategories('My Favorites','My_Favs','lnid7','myfavorites.png'));
		}
		return cats;
	}
	public List<KnowledgeArticleVersion> getFavorites()
	{
		List<id> ids = new List<id>();
		List<KnowledgeArticleVersion> favs;
		/*String sortby;
		if (SortSearch=='lastUpdated')
			sortby = 'LastPublishedDate';
		else if (SortSearch=='mostViewed')
			sortby = 'ArticleType';
		else
			sortby = 'title';*/
			
		for (Favorite_Articles__c f : [select article_id__c from Favorite_Articles__c where ownerid=:UserInfo.getUserId()])
		{
			ids.add(f.article_id__c);
		}
		
		if (SortSearch=='lastUpdated')
			favs = [Select k.Title, k.Summary, k.LastPublishedDate, k.knowledgearticleid, k.ArticleType From KnowledgeArticleVersion k
												WHERE (PublishStatus = 'online' and Language = :userlang)
												and knowledgearticleid in :ids order by LastPublishedDate];
		else if (SortSearch=='mostViewed')
			favs = [Select k.Title, k.Summary, k.LastPublishedDate, k.knowledgearticleid, k.ArticleType From KnowledgeArticleVersion k
												WHERE (PublishStatus = 'online' and Language = :userlang)
												and knowledgearticleid in :ids order by createddate];	
		else	
			favs = [Select k.Title, k.Summary, k.LastPublishedDate, k.knowledgearticleid, k.ArticleType From KnowledgeArticleVersion k
												WHERE (PublishStatus = 'online' and Language = :userlang)
												and knowledgearticleid in :ids order by title];																						
		return favs;
	}
	public string ShowFavs {get;set;}

	public class ArticleDetails
	{
		public string body {get;set;}
		public string title {get;set;}
		public integer isFavorite {get;set;}
		public string id {get;set;}
		
		ArticleDetails(string body,integer fav,string t,string id)
		{
			this.body = body;
			isFavorite = fav;
			title = t;
			this.id = id;
		}		
	}    
	
	public ArticleDetails getArticleDetail() {
	
		system.debug('Inside HRConnectAnswerController->getArticleDetail');
		system.debug('param id: '+id);

        ArticleDetails ad;
        KnowledgeArticleVersion articleqry;
        		
		try {
        	articleqry = [SELECT Id, Summary, Title, ArticleType
	        	FROM KnowledgeArticleVersion 
	        	WHERE PublishStatus = 'online' 
	        	and Language = :userlang 
	        	and knowledgearticleid=:id][0];	//UPDATE VIEWSTAT
		}
		catch(Exception e)
		{
			ad = new ArticleDetails('Article not found.',0,'Error!',id);
			return ad;
		}
        	
        String arttype = articleqry.ArticleType;

        system.debug('summary: '+articleqry.id); 
        
        List<Favorite_Articles__c> fa = [select id from Favorite_Articles__c where name=:UserInfo.getUserId() and article_id__c = :id limit 1];         
        
        sObject articletype = Database.query('SELECT Body__c FROM '+arttype+' WHERE PublishStatus = \'online\' and Language = \''+userlang+'\' and id=\''+articleqry.id+'\'');

        Policies_Guideline__kav articlebody1 = new Policies_Guideline__kav();
        FAQ__kav articlebody2 = new FAQ__kav();  
        Inet_Announcement__kav articlebody3 = new Inet_Announcement__kav();
        Leave_Administration__kav articlebody4 = new Leave_Administration__kav();
        Newsletter__kav articlebody5 = new Newsletter__kav();
        Procedure__kav articlebody6 = new Procedure__kav();
        Training__kav articlebody7 = new Training__kav();        
        
        /*if (arttype == 'Policies_Guideline__kav')
        {
       		articlebody = 
       			[SELECT Body__c 
       			FROM Policies_Guideline__kav p
				WHERE PublishStatus = 'online' and Language = 'en_US' and Id=:articleqry.id][0];			
        	//ArticleDetails ad = new ArticleDetails(articlebody.Body__c,fa.size());
        }*/
       
        if (arttype == 'Policies_Guideline__kav')
        {
			articlebody1 = (Policies_Guideline__kav)articletype;
			ad = new ArticleDetails(articlebody1.Body__c,fa.size(),articleqry.title,id);
        }
        else if (arttype == 'FAQ__kav')
        {
        	articlebody2 = (FAQ__kav)articletype;
        	ad = new ArticleDetails(articlebody2.Body__c,fa.size(),articleqry.title,id);
        }
        else if (arttype == 'Inet_Announcement__kav')
        {
        	articlebody3 = (Inet_Announcement__kav)articletype;
        	ad = new ArticleDetails(articlebody3.Body__c,fa.size(),articleqry.title,id);
        }
        else if (arttype == 'Leave_Administration__kav')
        {
        	articlebody4 = (Leave_Administration__kav)articletype;
        	ad = new ArticleDetails(articlebody4.Body__c,fa.size(),articleqry.title,id);
        }
        else if (arttype == 'Newsletter__kav')
        {
        	articlebody5 = (Newsletter__kav)articletype;
        	ad = new ArticleDetails(articlebody5.Body__c,fa.size(),articleqry.title,id);
        }
        else if (arttype == 'Procedure__kav')
        {
        	articlebody6 = (Procedure__kav)articletype;
        	ad = new ArticleDetails(articlebody6.Body__c,fa.size(),articleqry.title,id);
        }
        else if (arttype == 'Training__kav')
        {
        	articlebody7 = (Training__kav)articletype;
        	ad = new ArticleDetails(articlebody7.Body__c,fa.size(),articleqry.title,id);
        }       
        
        // update article viewstat and recently viewed object
        //HRConnectCommon.updateViewstat(id); 
                                    
        return ad;
    }	
	public PageReference doSearch() {
        CategoryStr = 'Content_Category:';
        id = null;
        ShowFavs = 'false';
        currentPage = 1;
        //ApexPages.currentPage().getParameters().put('id',null);
        return null;
        
        //PageReference ret = New PageReference('/apex/HRConnectAnswers');
        //ret.getParameters().put('id', null);
        //ret.setRedirect(false);        
        //return ret;        
    }    
	public PageReference doCatSearch() {
        id = null;
        currentPage = 1;
        system.debug('CategoryStr:'+CategoryStr);
        //system.debug('cs:'+cs);
        //CategoryStr = cs;
        return null;
	}    
}