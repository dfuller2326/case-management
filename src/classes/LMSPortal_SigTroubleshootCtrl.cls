public class LMSPortal_SigTroubleshootCtrl {
	public String url {get;set;}
	public String relativePath {get;set;}
	public String version {get;set;}
	public String ipaddress {get;set;}
	public Boolean sigQueryString {get;set;}
	public DateTime expiration {get;set;}
	public String signature {get;set;}
	public String finalURL {get;set;}
	public String urlBasePath {get;set;}
	public LMSPortal_SigTroubleshootCtrl() {
	}

	public void runFormValues(){
		String tempUrl = url;
		expiration = System.now() + 1;
		MPX_Platform_Secret__c mpx = MPX_Platform_Secret__c.getOrgDefaults();
		SignatureFactory sigCls = new SignatureFactory();
		signature = sigCls.calculateSignature(url, 0, null, mpx.Secret_Name__c, mpx.Secret_Value__c, expiration, false, relativePath);
		System.debug('SIGNATURE :: ' + signature);
		urlBasePath = mpx.URL_Base_Path__c;
		String secondHalfUrl = tempUrl.replace(urlBasePath, '');
		Integer index = secondHalfUrl.indexOf('?');

		if (index > 0) {
		     tempUrl += LMSPortal_Constants.SIG_AND_STR + signature;
		} else {
		    tempUrl += LMSPortal_Constants.SIG_QUERY_STR + signature;
		}

		finalURL = tempUrl;
	}
}