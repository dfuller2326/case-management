/**
* @description controller class for View All page
*
*/
global with sharing class LMSPortal_AllLearningContentController {

    // record limit to be used for offset clause
    public Integer recordLimit {get; set;}
    
    // query string
    private static String queryStr = 'SELECT Id, Name, Content_Type__c, Description__c, Summary__c, Title__c, Banner_Document_Id__c, Thumbnail_Document_Id__c, Category__c, Functional_Competency__c, Duration__c, Calculated_Duration__c, Duration_Unit__c FROM LMS_Content__c';
    private static String countQueryStr = 'SELECT count() FROM LMS_Content__c';
    
    //Constructor
    public LMSPortal_AllLearningContentController() {
        this.recordLimit = 24;
    } 

    /**
    * @description remote action method to get the Learning Content for View All page
    * @param Integer: offset
    * @param Integer: reclimit
    * @param String: filterParamJson
    * @param String: sortParam
    * @param String: searchParam
    * @param String: previewParamJson
    * @param String: category
    * @param Boolean: showNewContent
    * @param Boolean: showHotTopic
    * @return ContentWrapper
    */
    @RemoteAction
    global static String getViewAllData(Integer offset, Integer recordLimit, String filterParamJson, 
                                            String sortParam, String searchParam, String previewParamJson,
                                            String category, Boolean showNewContent, Boolean showHotTopic, String additionalFilter) { 

        system.debug(LMSPortal_Constants.GETVIEWALL_ENTER_MSG);
        ViewAllResponse respObj = new ViewAllResponse(); 
        try {             
            // get learning contents
            respObj.content = getLearningContent(offset, recordLimit, filterParamJson, sortParam, searchParam, previewParamJson, 
                                                    category, showNewContent, showHotTopic, additionalFilter);       
            
            // get sidebar info
            respObj.filters = getFilters(category, additionalFilter);

        } catch(Exception ex) {
            respObj.error = new LMSPortal_ErrorInfo(System.Label.All_Learning_Content_Error + ': ', ex.getMessage());
            system.debug(LoggingLevel.ERROR, LMSPortal_Constants.VIEWALL_EX_MSG + ex.getMessage());
        }

        system.debug(LMSPortal_Constants.VIEWALL_RESOBJ_DEBUG_HEAD + respObj);
        system.debug(LMSPortal_Constants.GETVIEWALL_RETURN_MSG);
        
        return JSON.serialize(respObj);
    }

    /**
    * @description method to get the Learning Content after applying all the business unit, division, filter(s)
    * @param Integer: offset
    * @param Integer: reclimit
    * @param String: filterParamJson
    * @param String: sortParam
    * @param String: searchParam
    * @param String: previewParamJson
    * @param String: category
    * @param Boolean: showNewContent
    * @param Boolean: showHotTopic
    * @return ContentWrapper: cWrapper
    */
    private static ContentWrapper getLearningContent(Integer offset, Integer reclimit, String filterParamJson, 
                                                        String sortParam, String searchParam, String previewParamJson,
                                                        String category, Boolean showNewContent, Boolean showHotTopic, String additionalFilter) {

        system.debug(LMSPortal_Constants.GETLEARNINGCONTENT_ENTER_MSG);
        ContentWrapper cWrapper = new ContentWrapper(new List<LMS_Content__c>(), 0);

        // retriev business unit, division form User Info or Preview Parameter
        PreviewWrapper previewParams = getPreviewParam(previewParamJson);
        if(previewParams != null) {
            List<String> conditions = new List<String>();
            Date effectiveDate = normalizeDate(previewParams.effectiveDate);
            Set<Id> learningContentIds = new Set<Id>();
            List<String> locList = new List<String>();
            locList.add(previewParams.location);
            String abbrDivisionName = LMSPortal_Util.getAbbrDivisionName(previewParams.division);
            if(String.isNotBlank(previewParams.businessUnit) && String.isNotBlank(previewParams.division) && String.isNotBlank(previewParams.location)) {
                for(Learning_Content_Access__c lca: [SELECT Learning_Content__c FROM Learning_Content_Access__c 
                                                     WHERE Business_Unit__c = :previewParams.businessUnit 
                                                     AND Division__c = :previewParams.division
                                                     AND Location__c INCLUDES (:locList[0]) ]) {
                    if(String.isNotBlank(lca.Learning_Content__c)) {
                        learningContentIds.add(lca.Learning_Content__c);
                    }
                }
            }

            if(!learningContentIds.isEmpty() && effectiveDate != null) {
                
                Date newReldate = effectiveDate.addDays(-30);

                // add search keyword to query string                                        
                if(String.isNotBlank(searchParam)) {
                    String searchKeyword = String.escapeSingleQuotes(searchParam.trim());
                    
                    //get search criteria
                    String searchCriteria = LMSPortal_Util.constructSearchCriteria(searchKeyword);
                    conditions.add( searchCriteria );
                }
                
                conditions.add('Id IN : learningContentIds AND Start_Date__c <= :effectiveDate and End_Date__c >= :effectiveDate');

                // filter condition for Category, Sub Category, Content Type, Leadership Competency
                if(String.isNotBlank(filterParamJson)) {
                    System.debug('Parameter Filters are Passed :: ' + filterParamJson); 
                    List<ContentFilterWrapper> filterParams = (List<ContentFilterWrapper>) JSON.deserialize(filterParamJson, List<ContentFilterWrapper>.class);
                        
                    if(filterParams != null && !filterParams.isEmpty()) {
                        List<String> categories = new List<String>();
                        List<String> contentTypes = new List<String>();
                        String functionalCompetency = '';
                        String leadershipCompetency = '';
                        
                        for(ContentFilterWrapper fp : filterParams) {
                            Boolean isChecked = false;
                            List<String> filters = new List<String>();
                            
                            if(fp.filterValues != null && !(fp.filterValues).isEmpty()) {
                                for(FilterValue fv : fp.filterValues) {
                                    if(fv.isChecked) {
                                        isChecked = true;
                                        filters.add(fv.value);
                                        if(fp.fieldApiName == LMSPortal_Constants.FUNCTIONAL_COMPETENCY_API) {
                                            functionalCompetency += '\'' + fv.value + '\'' + ',';
                                        } else if(fp.fieldApiName == LMSPortal_Constants.LEADERSHIP_COMPETENCY_API) {
                                            leadershipCompetency += '\'' + fv.value + '\'' + ',';
                                        }
                                    }
                                }
                            }

                            if(isChecked) {
                                if(fp.fieldApiName == LMSPortal_Constants.CATEGORY_API) {
                                    categories.addAll(filters);
                                    conditions.add('Category__c IN : categories');
                                } else if(fp.fieldApiName == LMSPortal_Constants.CONTENT_TYPE_API) {
                                    contentTypes.addAll(filters);
                                    conditions.add('Content_Type__c IN : contentTypes');
                                } else if(fp.fieldApiName == LMSPortal_Constants.LEADERSHIP_COMPETENCY_API) {
                                    //  remove last additional comma from string 
                                    leadershipCompetency = leadershipCompetency.subString(0, leadershipCompetency.length() - 1);
                                    conditions.add('Leadership_Competency__c INCLUDES (' + leadershipCompetency + ')');
                                } else if(fp.fieldApiName == LMSPortal_Constants.FUNCTIONAL_COMPETENCY_API) {
                                    //  remove last additional comma from string 
                                    functionalCompetency = functionalCompetency.subString(0, functionalCompetency.length() - 1);
                                    conditions.add('Sub_Functional_Competencies__c INCLUDES (' + functionalCompetency + ')');
                                } else if(fp.fieldApiName == 'Filter_Values__c') {
                                    for(String afilter: filters) {
                                        if(afilter == LMSPortal_Constants.FUNCTIONAL_LEARNING_STR) {
                                            conditions.add('Functional_Learning__c INCLUDES (: abbrDivisionName)');
                                        }
                                        if(afilter == LMSPortal_Constants.POPULAR_STR){
                                            conditions.add('Hot_Topic__c = true');
                                        }
                                        if(afilter == LMSPortal_Constants.NEW_RELEASES_STR){
                                            conditions.add('Start_Date__c >= :newReldate');
                                        }
                                        if(afilter == LMSPortal_Constants.QUICK_LEARNING_STR){
                                            conditions.add('Calculated_Duration__c < 10');
                                        }
                                        if(afilter == 'New Content'){
                                            conditions.add('New_Content__c = true');
                                        }
                                    }
                                } 
                            }
                        }
                    }
                } else {
                    System.debug('No json parameters');
                    // if url param containes selected category, sub category value
                    if(String.isNotBlank(additionalFilter)) {
                        if(additionalFilter.trim() == LMSPortal_Constants.FUNCTIONAL_LEARNING_STR) {
                            conditions.add('Functional_Learning__c INCLUDES (: abbrDivisionName)');
                        } else if(additionalFilter.trim() == LMSPortal_Constants.POPULAR_STR) {
                            conditions.add('Hot_Topic__c = true');
                        } else if(additionalFilter.trim() == LMSPortal_Constants.NEW_RELEASES_STR) {
                            conditions.add('Start_Date__c >= :newReldate');
                        } else if(additionalFilter.trim() == LMSPortal_Constants.QUICK_LEARNING_STR) {
                            conditions.add('Calculated_Duration__c < 10');
                        }
                    }
                    if(String.isNotBlank(category)) {
                        conditions.add('Category__c = :category');
                    }
                }
                

                // add more condtions to the queryStr 
                if(conditions != null && !conditions.isEmpty()) {
                    queryStr += ' WHERE ' + conditions[0];
                    countQueryStr += ' WHERE ' + conditions[0];
                    for (Integer i = 1; i < conditions.size(); i++) {
                        queryStr += ' AND ' + conditions[i];
                        countQueryStr += ' AND ' + conditions[i];
                    }
                }

                system.debug('Query Str:: ' + queryStr);

                // apply sort to the query string; default is LastModifiedDate
                String sortBy = (String.isNotBlank(sortParam)) ? sortParam : 'LastModifiedDate DESC';
                cWrapper = new ContentWrapper(Database.query(queryStr + ' ORDER BY ' + sortBy + ' LIMIT ' + reclimit.format() + ' OFFSET ' + offset.format()), 
                                    Database.countQuery(countQueryStr));
            } else {
                system.debug(LoggingLevel.Error, LMSPortal_Constants.BU_DIV_EX_MSG);
            }
        }
        system.debug(LMSPortal_Constants.GETLEARNINGCONTENT_RETURN_MSG);
        return cWrapper;
    } 

    /**
    * @description method to get the filters for View All page
    * @param String: category
    * @return List<ContentFilterWrapper>: contentWrapperList
    */
    private static List<ContentFilterWrapper> getFilters(String category, String additionalFilter) {
        system.debug(LMSPortal_Constants.GETFILTERS_ENTER_MSG);

        //Create new Wrapper list
        List<ContentFilterWrapper> contentWrapperList = new List<ContentFilterWrapper>();
        Set<String> picklistValueSet = new Set<String>();

        //Get field describe for Learning Content object
        Map<String,Schema.SObjectField> fieldMap = Schema.SObjectType.LMS_Content__c.fields.getMap();
        
        // Get possible filters from custom settings
        Map<String, Learning_Content_Filters__c> csMap = Learning_Content_Filters__c.getAll();
        
        if(csMap != null && !csMap.isEmpty()) {
            //Loop through Custom Settings values and use those to map to Custom Fields on Learning Content for Getting Describe Information
            for(String mapKey : csMap.keySet()) {
                if(csMap.get(mapKey).API_Name__c == LMSPortal_Constants.ADDITIONAL_FILTERS_API) {
                    List<FilterValue> filterValues = new List<FilterValue>();
                    Integer index = 0;
    
                    Map<String,Schema.SObjectField> fieldMapAF = Schema.SObjectType.Additional_Filters__c.fields.getMap();
                    
                    List<Additional_Filters__c> addFilters = [SELECT Filter_Values__c FROM Additional_Filters__c WHERE Name = : LMSPortal_Constants.ADDITIONAL_FILTERS];
                    String addFiltersStr = addFilters[0].Filter_Values__c;
                    List<String> afPicklistVals = addFiltersStr.split(',');
                    for(String afp :afPicklistVals ) {
                        filterValues.add(new FilterValue('', afp, afp, additionalFilter, index));
                        index++;
                    }
                    if(!filterValues.isEmpty()) {
                        Integer rank = csMap.get(mapKey).Rank__c == null ? 0 : csMap.get(mapKey).Rank__c.intValue();
                        for(Schema.SObjectField filter : fieldMapAF.values()){
                            Schema.DescribeFieldResult dfr = filter.getDescribe();
                            if(dfr.getName() == 'Filter_Values__c'){
                                contentWrapperList.add(new ContentFilterWrapper(rank, dfr, filterValues));
                            }
                        }
                    }
                } else if(csMap.get(mapKey).API_Name__c == LMSPortal_Constants.FUNCTIONAL_COMPETENCY_API) {
                    List<FilterValue> filterValues = new List<FilterValue>();
                    Integer index = 0;

                    User usr = [Select Id, Business_Unit__c, Division__c From User Where Id = :UserInfo.getUserId()];
                    if(usr != null && String.isNotBlank(usr.Division__c)) {
                        List<FunctionalCompetencyConfig__c> funcCompetencyList = [Select Division__c, Functional_Area__c, Functional_Competency__c
                                                                                  From FunctionalCompetencyConfig__c
                                                                                  Where Division__c = :usr.Division__c
                                                                                ];
                        if(funcCompetencyList != null && !funcCompetencyList.isEmpty()) {
                            Map<String, Schema.SObjectField> fieldMapFC = Schema.SObjectType.FunctionalCompetencyConfig__c.fields.getMap();
                            for(FunctionalCompetencyConfig__c fc : funcCompetencyList ) {
                                filterValues.add(new FilterValue(fc.Functional_Area__c, fc.Functional_Competency__c, fc.Functional_Competency__c, '', index));
                                index++;
                            }

                            if(!filterValues.isEmpty()) {
                                Integer rank = csMap.get(mapKey).Rank__c == null ? 0 : csMap.get(mapKey).Rank__c.intValue();
                                for(Schema.SObjectField filter : fieldMapFC.values()){
                                    Schema.DescribeFieldResult dfr = filter.getDescribe();
                                    if(dfr.getName() == LMSPortal_Constants.FUNCTIONAL_COMPETENCY_API){
                                        contentWrapperList.add(new ContentFilterWrapper(rank, dfr, filterValues));
                                    }
                                }
                            }
                        }
                    }
                } else {
                    for(Schema.SObjectField filter : fieldMap.values()) {
                        
                        Schema.DescribeFieldResult dfr = filter.getDescribe();

                        if(csMap.get(mapKey).API_Name__c == dfr.getName()) {    
                            //Build Content Wrapper - Field Describe Result, PicklistValue Schema List for filter panel output to the page
                            List<FilterValue> filterValues = new List<FilterValue>();
                            Integer index = 0;

                            for(PicklistEntry ple : dfr.getPicklistValues()) {
                                filterValues.add(new FilterValue('', ple.label, ple.value, category, index));
                                index++;
                            }

                            if(!filterValues.isEmpty()) {
                                Integer rank = csMap.get(mapKey).Rank__c == null? 0 : csMap.get(mapKey).Rank__c.intValue();
                                contentWrapperList.add(new ContentFilterWrapper(rank, dfr, filterValues));
                            }
                            break;
                        }
                    }
                }    
            }
        } else {
            system.debug(LoggingLevel.Error, 'Custom Settings::Learning_Content_Filters__c is empty');
        }

        if(!contentWrapperList.isEmpty()) {
            contentWrapperList.sort();
        }

        system.debug('Filter Values:: ' + contentWrapperList);
        
        system.debug('LMSPortal_AllLearningContentController::getFilters() returning');
        
        return contentWrapperList;
    }

    /**
    * @description method to get the Business Unit & Division form User record or the Preview parameters
    * @param String: previewParamJson
    * @return PreviewWrapper: previewParams
    */
    private static PreviewWrapper getPreviewParam(String previewParamJson) {

        system.debug('LMSPortal_AllLearningContentController::getPreviewParam() entered');

        PreviewWrapper previewParams = null;
        
        if(String.isNotBlank(previewParamJson)) {
            previewParams = (PreviewWrapper) JSON.deserialize(previewParamJson, PreviewWrapper.class);
        } else {
            User usr = [Select Id, Business_Unit__c, Division__c, Location__c From User Where Id = :UserInfo.getUserId()];
            if(usr != null) {
                previewParams = new PreviewWrapper(DateTime.now().getTime(), usr.Business_Unit__c, usr.Division__c, usr.Location__c);
            }
        }

        system.debug('Business Unit + Division:: ' + previewParams);
        system.debug('LMSPortal_AllLearningContentController::getPreviewParam() returning');

        return previewParams;
    }

    /**
    * @description method to convert the Javascript date string to Salesforce Datetime
    * @param String: Javascript date string in milliseconds
    * @return Datetime: value
    */
    private static Date normalizeDate(Long dateInMilliseconds) {
        Datetime dt = DateTime.newInstance(dateInMilliseconds);
        return dt.date();
    }

    // Wrapper class for ViewAll Response
    public class ViewAllResponse {
        public LMSPortal_ErrorInfo error {get; set;}
        public ContentWrapper content {get; set;}
        public List<ContentFilterWrapper> filters {get; set;}
    }

    // Wrapper class for Content
    public class ContentWrapper {
        public List<SObject> records { get; set; }
        public Integer totalRecords {get; set;}
        public ContentWrapper(List<SObject> records, Integer count) {
            this.records = records;
            this.totalRecords = count;
        }
    }

    // Wrapper class for Content Filter Categories
    public class ContentFilterWrapper implements Comparable {
        public String fieldLabel {get; set;}
        public String fieldApiName {get; set;}
        public Integer fieldRank {get; set;}
        public List<FilterValue> filterValues {get; set;}
        
        public ContentFilterWrapper(Integer rank, Schema.DescribeFieldResult dfr, List<FilterValue> fValues){
            this.fieldRank = rank;
            this.fieldLabel = dfr.getLabel();
            this.fieldApiName = dfr.getName();
            this.filterValues = fValues;
        }
        
        // Compare objects based on rank
        public Integer compareTo(Object compareTo) {
            // Cast argument to ContentFilterWrapper
            ContentFilterWrapper compareToObj = (ContentFilterWrapper)compareTo;
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (this.FieldRank > compareToObj.FieldRank) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (this.FieldRank < compareToObj.FieldRank) {
                // Set return value to a negative value.
                returnValue = -1;
            }
            
            return returnValue;       
        }
    }

    // Wrapper class for Content Filter Categories values
    public class FilterValue {
        public String category {get; set;}
        public String label {get; set;}
        public String value {get; set;}
        public Boolean isChecked {get; set;}
        public Integer index {get; set;}

        public FilterValue(String categoryName, String lbl, String val, String catParam, Integer index) {
            this.category = String.isNotBlank(categoryName) ? categoryName : '';
            
            this.label = (String.isNotBlank(lbl)) ? lbl : '';
            this.value = (String.isNotBlank(val)) ? val : '';
            if(String.isNotBlank(catParam) && catParam == this.value){
                this.isChecked = true;   
            } else {
                this.isChecked = false;
            }
            this.index = index;
        }
    }

    // Wrapper class for Business Unit, Division and Future Date
    private class PreviewWrapper {
        public Long effectiveDate {get; set;}
        public String businessUnit {get; set;}
        public String division {get; set;}
        public String location {get; set;} 
        public PreviewWrapper(Long dateInMilliseconds, String businessUnit, String division, String location) {
            this.effectiveDate = dateInMilliseconds;
            this.businessUnit = String.isNotBlank(businessUnit) ? businessUnit : '';
            this.division = String.isNotBlank(division) ? division : '';
            this.location = String.isNotBlank(location) ? location : '';
        }
    } 
}