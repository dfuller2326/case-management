@isTest
private class TestSignatureFactory{
    @isTest static void testSignatureFactory(){
        String url= 'http://link.theplatform.com/s/Zf-xWC/media/OAAz8uMMXbIr';
        Integer version = 0;
        String ipAddress = null;
        //MPX_Platform_Secret__c mc = MPX_Platform_Secret__c.getOrgDefaults();
        String secretName = 'SomeSecretName';
        String secretValue = 'SomeSecretvalue';
        Datetime tokenExpiration = Datetime.newInstance(2020, 9, 23);
        boolean signQuery = true;
        String releasePid = 'Zf-xWC/media/OAAz8uMMXbIr'; 
        
        String sig = (new SignatureFactory()).calculateSignature(url, version, ipAddress, secretName, secretValue, tokenExpiration, signQuery,  releasePid);
        System.assertEquals('105f6ac84083f9a8b3042bffea395b68022761a6290aa3d6ea536f6d655365637265744e616d65', sig, 'Compare signature');
    }

    @isTest static void testSignatureFactoryIPAddress(){
        String url= 'http://link.theplatform.com/s/Zf-xWC/media/OAAz8uMMXbIr';
        Integer version = 0;
        String ipAddress = '8.8.8.8';
        //MPX_Platform_Secret__c mc = MPX_Platform_Secret__c.getOrgDefaults();
        String secretName = 'SomeSecretName';
        String secretValue = 'SomeSecretvalue';
        Datetime tokenExpiration = Datetime.newInstance(2020, 9, 23);
        boolean signQuery = true;
        String releasePid = 'Zf-xWC/media/OAAz8uMMXbIr'; 
        
        String sig = (new SignatureFactory()).calculateSignature(url, version, ipAddress, secretName, secretValue, tokenExpiration, signQuery,  releasePid);
        System.assertEquals('205f6ac8404a5bd9571872dfc505568533683a7069f8ca610b536f6d655365637265744e616d65', sig, 'Compare signature');
    }
}