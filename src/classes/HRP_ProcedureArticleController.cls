public with sharing class HRP_ProcedureArticleController {
  public transient SObject article {get; set;}
	public transient SObject articleStats {get; set;}

  public HRP_ProcedureArticleController(ApexPages.StandardController stdController) {
		Id articleId = stdController.getId();
    String lang = String.valueOf(UserInfo.getLanguage());
    try {
      article = HRP_ArticleUtils.retrieveArticleTypeVersion(lang, articleId);
      articleStats = HRP_ArticleUtils.retrieveArticleStats(articleId);
    } catch(Exception e) {
      System.debug(e.getMessage());
    }
	}
}