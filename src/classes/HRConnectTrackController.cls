public without sharing class HRConnectTrackController {
    public String Selpagenum {get; set;}
    public Integer size{get;set;}
    public Integer noOfRecords{get; set;}
    public String pagetext {get; set;}
    
    Public class casewrapper {
        public String createdate {get;set;}
        public String moddate {get;set;}
        public case cs {get;set;}
        public casewrapper(case c){
            this.createdate=c.CreatedDate.format('M/d/yy'); //MMM d, h:mm a
            this.moddate=c.LastModifiedDate.format('h:mma').toLowerCase();
            this.cs = c;
        }       
    }
    public ApexPages.StandardSetController setCon 
    {         
        get{             
            if(setCon == null){      
                /*if (Selpagenum == null)
                    Selpagenum = '2';           
                size = integer.valueof(Selpagenum);  
                */
                HRConnect_Portal_Config__c cs = HRConnect_Portal_Config__c.getValues('PORTAL_CONFIG_LIST');
                if (cs.Request_Per_Page__c != null && cs.Request_Per_Page__c > 0)   
                    size = (integer)cs.Request_Per_Page__c;
                else
                    size = 10;
                    
                User u = [select Employee_ID__c from user where id=:UserInfo.getUserId()];
                String contactid = '';
                try {
                    List<Contact> c = [select id from contact where Employee_ID__c = :u.Employee_ID__c]; 
                    contactid = c[0].id;
                }
                catch (Exception e) {
                }     
                                     
                //string queryString = 'Select c.Subject, c.Case_Status__c, c.LastModifiedDate, c.Description, c.CreatedDate From Case c where c.CreatedById=\''+UserInfo.getUserId()+'\' order by Case_Status__c';   //LastModifiedDate desc              
                
    //          if (contactid != '')
                    string queryString = 'Select c.Id, c.Subject, c.Case_Status__c, c.LastModifiedDate, c.Description, c.CreatedDate From Case c where c.ContactId=\''+contactid+'\' and c.ContactId != null and RecordTypeId in (Select Id  From RecordType  Where SobjectType = \'Case\' and DeveloperName in(\'General\',\'Position_Request_Form\')) order by Case_Status__c, LastModifiedDate desc';
    
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));                 
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();                  
                /*Decimal totNum = noOfRecords;          
                Decimal totalpages = totNum.divide((decimal)size,0,system.RoundingMode.CEILING);
                System.debug('noOfRecords:'+noOfRecords+'totalNum:'+totNum+'totalpages:'+totalpages+'pagenumber:'+pageNumber);
                if (totalpages > 0)
                    pagetext = '('+string.valueOf(pageNumber)+' of '+string.valueOf(totalpages)+')';
                else
                    pagetext = '(No matching results found)';*/
            }                 
            return setCon;         
        }
        set;     
    }    
    
    public PageReference next() {
        setCon.next();
        return null;
    }

    public PageReference previous() {
        setCon.previous();
        return null;
    }
    
    public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }
  
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }     
    public string caseId {get;set;}
    public transient string filename {get;set;}
    public transient Blob filebody{get;set;}
    public PageReference submitFile(){
        Attachment attachment = new Attachment();
        
        if (filename != null && filename != '' && filebody != null)
        {
            attachment.body = filebody;
            attachment.name = filename;
            attachment.OwnerId = UserInfo.getUserId();
            attachment.ParentId = caseId;
            
            insert attachment;
        }
        else{
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please first select a file to upload.'));
        }
        return null;
    }
    
    public PageReference redirectAskHR(){
        PageReference reRend = new PageReference('/apex/HRConnectCase?caseId='+caseId);
        reRend.setRedirect(true);
        return reRend;
    }
    
    Public List<casewrapper> getCaseHistory()
    {         
    List<casewrapper> caseList = new List<casewrapper>();         
    for(Case a : (List<Case>)setCon.getRecords()) 
    {               
        caseList.add(new casewrapper(a));
    }      
    return caseList;     
    }    

public static List<CaseComment> getCaseDetails(String id) {

        system.debug('Inside HRConnectTrackController->getCaseDetails');
        List <CaseComment> cc = [Select c.CommentBody,c.LastModifiedDate,c.LastModifiedById From CaseComment c where c.ParentId=:id and ispublished=true order by c.LastModifiedDate desc];
        system.debug('Comment count: '+cc.size());
        return cc;
    }

public static void setCaseComment(String id,String cc) {

        system.debug('Inside HRConnectTrackController->setCaseComment');
        CaseComment newcomment = new CaseComment();
        newcomment.ParentId = id;
        newcomment.CommentBody = cc;
        newcomment.ispublished = true;
        insert newcomment;
    }    
}