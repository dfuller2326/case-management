@isTest 
private class CloseInvestigationController_Test {
    static testMethod void validateCloseInvestigationController() {

        RecordType recordTypeInstance=[select Id from RecordType where sObjectType='Case' and Name='HR Investigations' limit 1];
        
        // insert test account      
        Account accountInstance = new Account(name = 'Biogen', AccountNumber='123456'); 
        insert accountInstance;
        
        //create contacts
        Contact contactInstance1=new Contact(FirstName='Test1', LastName='Case10');
        insert contactInstance1;
        Contact contactInstance2=new Contact(FirstName='Test2', LastName='Case20');
        insert contactInstance2;
        Contact contactInstance3=new Contact(FirstName='Test3', LastName='Case30');
        insert contactInstance3;
        
        //create case
        Case testCase = new Case( AccountId=accountInstance.id
                                , BusinessHours=new BusinessHours(Name ='United States')
                                , Origin='Email'
                                , Status= 'Open'
                                , ContactId=contactInstance1.Id
                                , RecordTypeId=recordTypeInstance.id);
        
        insert testCase;
        Investigated_Employee__c emp1 = new Investigated_Employee__c( Case__c = testCase.Id
                                                                    , Employee__c = contactInstance2.Id);
        Investigated_Employee__c emp2 = new Investigated_Employee__c( Case__c = testCase.Id
                                                                    , Employee__c = contactInstance3.Id);
        insert emp1;
        insert emp2;
    
    
        PageReference pg = Page.Close_Investigation;
        Test.setCurrentPage(pg);
        pg.getParameters().put('CaseId', testCase.Id);
        CloseInvestigationController controller=new CloseInvestigationController();
        
        Case zCase = controller.getCase();
        System.assertEquals(testCase.Id, zCase.Id);
        List<Investigated_Employee__c> zAddEmp = controller.getInvestigatedEmployees();
        System.assertEquals(2, zAddEmp.size());
        controller.ieSave();
        Boolean access = controller.getHasEditAccess();
        System.assertEquals(access, true);
        
        //test Edit access of this user for a negative test
        controller.getCase().id = null;
        access = controller.getHasEditAccess();
        System.assertEquals(access, false);
        
        //testing exception
        zAddEmp.get(0).Id = null;
        try{
            controller.ieSave();
        } catch(Exception e){
            //System.AssertEquals(e.getLineNumber(),0); 
        }    
    }
    
    private static User getUserForProfile(string profileName) {        
        Profile prof = [Select id from Profile where Name = :profileName limit 1];        
        String name = 'name' + String.valueOf(Math.random());        
        String email = name + '@biogen.com';        
        User u = new User(  Username = email,            
                            LastName = name,            
                            CommunityNickname = 'nick' + name,            
                            alias = 'testuser',          
                            Email = email,            
                            ProfileId = prof.id,            
                            TimeZoneSidKey = 'America/New_York',            
                            EmailEncodingKey='UTF-8',            
                            LanguageLocaleKey = 'en_US',            
                            LocaleSidKey = 'en_US',            
                            UserPermissionsSFContentUser = true);
        insert u;        
        return u;    
    }
}