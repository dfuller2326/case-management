@isTest
public with sharing class LMSPortal_UtilTest {
	
	static testMethod void myUnitTest() {
    	LMSPortal_Util controller = new LMSPortal_Util();
        Datetime dt = Datetime.now();
    	Long l = dt.getTime();
    	PreviewWrapper preW = new PreviewWrapper(l, '000 MA Talent Acquisition', '000 MA Talent Acquisition', 'Test Campus');
    	String JSONString = JSON.serialize(preW);
    	
		List<LMS_Content__c> lmsList = createLMSContentData(8);
		List<Learning_Content_Access__c> lcList = createLMSLCAccessData(lmsList);
        test.StartTest();
        List<LMS_Content__c> lcListSearch = LMSPortal_Util.searchWithFilters(JSONString, 'bus', 'Title__c', 5);
        system.assertNotEquals(lcListSearch, null);
        
        LMSPortal_Util.getCategoryPicklistValues();
        test.stopTest();
    }
    
    public static List<LMS_Content__c> createLMSContentData(Integer recordCount) {
    	
    	List<LMS_Content__c> tmpList = new List<LMS_Content__c>();
    	if(recordCount == null)
    	{
    		recordCount = 1;
    	}
    	for(Integer i=1; i <= recordCount; i++)
    	{
    		LMS_Content__c lms = new LMS_Content__c();
    		lms.Description__c = 'Test Description' + i;
    		lms.New_Content__c = false;
    		lms.Miscellaneous__c = 'Test Miscellaneous' + i;
    		lms.Thumbnail_Document_Id__c = '1234' + i;
			lms.Banner_Document_Id__c = '5678' + i;
            lms.Start_Date__c = Date.valueOf(System.now());
            lms.End_Date__c = Date.valueOf(System.now() + 5);
            lms.External_Media_URL__c = 'https://www.biogen.com';
			if(i < 6){
				lms.Hot_Topic__c = true;
			}else{
				lms.Hot_Topic__c = false;
			}
			lms.Title__c = 'Test Title' + i;
			if(i < 5){
				lms.Category__c = 'Biogen Core Curriculum';
			}else{
				lms.Category__c = 'Functional Learning';
			}
    		tmpList.add(lms);
    	}
    	insert tmpList;
    	return tmpList;
    }
    
    public static List<Learning_Content_Access__c> createLMSLCAccessData(List<LMS_Content__c> lmsList) {
    	
    	List<Learning_Content_Access__c> tmpList = new List<Learning_Content_Access__c>();
    	for(LMS_Content__c lms : lmsList)
    	{
    		Learning_Content_Access__c lca = new Learning_Content_Access__c();
    		lca.Business_Unit__c = '000 MA Talent Acquisition';
    		lca.Division__c = '000 MA Talent Acquisition';
    		lca.Learning_Content__c = lms.Id;
    		lca.Location__c = 'Test Campus';
    		tmpList.add(lca);
    	}
    	insert tmpList;
    	return tmpList;
    }
    
    
	private class PreviewWrapper {
        public Long effectiveDate {get; set;}
        public String businessUnit {get; set;}
        public String division {get; set;} 
        public String location {get; set;} 
        public PreviewWrapper(Long dateInMilliseconds, String businessUnit, String division, String location) {
            this.effectiveDate = dateInMilliseconds;
            this.businessUnit = String.isNotBlank(businessUnit) ? businessUnit : '';
            this.division = String.isNotBlank(division) ? division : '';
            this.location = String.isNotBlank(location) ? location : '';
        }
    }
}