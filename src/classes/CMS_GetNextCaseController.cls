public with sharing class CMS_GetNextCaseController {

    public String CaseIdAssigned { get; set; }


    public CMS_GetNextCaseController() {}
    
    // Code we will invoke on page load.
    public void forwardToNextCase() {

        //Use the currently running user
        String userId = UserInfo.getUserId();
        String userLanguage = UserInfo.getLanguage();
        system.debug('userLanguage::' + userLanguage);

        
        //find out which queues this user is a member of
        List<Id> listGroupIds = getQueuesForUser(userId);
        
        if(listGroupIds.size()>0) { 
            //Find an open case that is assigned to one of those queues

            List<Case> caseObjList = [select Id,OwnerId,Contact_Language__c from Case where 
                                                        IsClosed=false
                                                        and Contact_Language__c=:userLanguage
                                                        and OwnerId in :listGroupIds 
                                                        ORDER BY PRIORITY, Priority_Date__c ASC limit 1];
                                                
            system.debug('caseObjectList::' + caseObjList);
            if (caseObjList!=null && caseObjList.size() > 0) {        
                // If we found one, assign it to the current user/change the status to assigned 
                // and update the case object
                caseObjList[0].OwnerId = userId;
                caseObjList[0].Status = 'Assigned';
                update caseObjList[0];

		    	//PageReference reference = new PageReference('/'+caseObjList[0].Id);
                //reference.getParameters().put('isdtp','vw');

				//reference.setRedirect(true);
				//return reference;
                CaseIdAssigned = caseObjList[0].Id;                
            }
            else {
	        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CMS_NO_CASE_FOUND));
	        	//return null;   	            	
            }
        }
        else {
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CMS_USER_NOT_ASSIGNED));
        	//return null;
        }  	
        //return null;
    }

    //Returns a list of ids of queues that this user is a member of
    public static List<Id> getQueuesForUser(String userId) 
    {
        List<Id> listGroupIds = new List<Id>();
        List<GroupMember> listGroupMembers = [Select GroupId From GroupMember 
                                                where Group.Type='Queue'
                                                and UserOrGroupId=:userId];
                                                
        if (listGroupMembers!=null && listGroupMembers.size()>0) {      
            for (GroupMember gm:listGroupMembers) {
                listGroupIds.add(gm.GroupId);
            }
        }
        
        return listGroupIds;
    }    

}