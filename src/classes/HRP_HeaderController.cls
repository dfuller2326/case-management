public with sharing class HRP_HeaderController {
	public User currentUser {

		get {
			return [SELECT Id, SmallPhotoUrl FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
		}

		set;

	}

	public String contactUsLink {
		get {
			return HRP_Constants.HR_CONTACT_US_LINK;
		}
		set;
	}

	public String userProfileLink {
		get {
			return HRP_Constants.HR_USER_PROFILE_URL;
		}
		set;
	}

	public HRP_HeaderController() {
		
	}
}