public class SignatureFactory {
    /**
     * Calculate a signature for a release URL
     *
     * @param url             The release URL, including any query strings.
     * @param version         The signature version to use. The first version is 0.
     * @param ipAddress       If the signature is for a specific IP address, it should be included here as a String, e.g.
     *                        "127.0.0.1". If the signature is not for a specific IP address, this value should be null.
     * @param secretName      The name of the secret as stored in the Restriction's keys.
     * @param secretValue     The secret string that will be used to encrypt the signature.
     * @param tokenExpiration The Date when this signature expires.
     * @param signQuery       Whether or not the the URL's query parameters should be included as part of the signature.
     * @param releasePid      The relative URL. It is the PID of this signature's associated Release in legacy.
     * @return The signature string for the Release.
     * @throws SignatureException If the secret value is null.
     *
     */
    public String calculateSignature(String url, Integer version, String ipAddress, String secretName,
        String secretValue, Datetime tokenExpiration, boolean signQuery, String releasePid)
    {
        //Replace underscore and hyphens
        String secret = secretValue.replace('-', '+');
        secret = secretValue.replace('_', '/');
        //Get flags in one byte
        Blob flags = getFlags(version, ipAddress, signQuery);
        //Get expiration time in 4 bytes
        Blob expirationBytes = getExpirationTime(tokenExpiration);
        //Get all items together on which checksum will be generated
        Blob clearTextBytes = getSignatureClearTextBytes(url, ipAddress, releasePid, signQuery, flags, expirationBytes);
        //Encrypt using hmacSHA1 algorithm
        Blob encryptedTextBytes = encryptWithSecret(secret, clearTextBytes);
        //Secretname is the last item in the signature
        Blob secretNameBytes = Blob.valueOf(secretName);
        //Combine all the binary data
        List<Blob> signatureBytesArray = new List<Blob>{flags, expirationBytes, encryptedTextBytes, secretNameBytes};
        Blob signatureBlob = joinBlocks(signatureBytesArray);
        //Generate hex code
        String signature = EncodingUtil.convertToHex(signatureBlob);
        signature = signature.replace('+', '-');
        signature = signature.replace('/', '_');
        return signature;
    }
    /*
     * Encrypts with secret using hmacSHA1
     */
    private Blob encryptWithSecret(String secret, blob clearTextBytes)
    {
        return Crypto.generateMac(LMSPortal_Constants.SIG_ENCRYPT_TYPE, clearTextBytes,  Blob.valueOf(secret));
    }
    /*
     * Creates a unencrypted clear text based on parameters passed
     */
    private Blob getSignatureClearTextBytes(String url, String ipAddress, String releasePid, boolean signQuery, Blob flags, Blob expirationBytes)
    {
        //Add all the blobs together and join them
        List<Blob> clearText = new List<Blob>();
        clearText.add(flags);
        clearText.add(expirationBytes);
        clearText.add(Blob.valueOf(releasePid));
        if (signQuery)
        {
            clearText.add(Blob.valueOf(getQueryString(url)));
        }
        if (ipAddress != null)
        {
            clearText.add(Blob.valueOf(ipAddress));
        }
        return joinBlocks(clearText);
    }
    /*
     * Gets query string from a url
     */
    private String getQueryString(String url)
    {
        Integer quesMark = url.indexOf('?');
        if (quesMark != -1)
            return url.substring(quesMark);
        else
            return '';
    }
    /*
     * Gets 4 byte expiration time
     */
    private Blob getExpirationTime(Datetime tokenExpiration) {
        Long seconds = tokenExpiration.getTime()/1000;
        Integer [] seconds32Bits = new Integer[32];
        Long tempInt = 0;
        for (Integer i = 0; i < 32; ++i) {  // assuming a 32 bit int
            tempInt = seconds & (1 << i);
            seconds32Bits[31 - i] = tempInt > 0? 1: 0;
        }
        return EncodingUtil.base64Decode(getBase64EncodedText(seconds32Bits));    
    }
    
    /*
     * Gets one byte flag - 1st 4 bits contain version, the 4th and 5th bits are masked
     * by 0x10 or 0x20 based on whether querystring or ip address will be encrypted as part 
     * of signature
     */
    private Blob getFlags(Integer version, String ipAddress, boolean signQuery) {
        Integer flags = 0;
        //if version is greater than 1111 throw exception
        if (version > 15) {
            throw new SignatureException('Version '+version+' not supported. It should be less than 16');
        }
        
        //Mask if querystring need to be part of signature
        if (signQuery) {
            flags = version | 16;//hex 0x10
        }
        
        //Mask if ipAddress need to be part of signature
        if (ipAddress != null) {
            flags = version | 32;//hex 0x20
        }
        Integer [] flag8Bits = new Integer[8];
        Integer newFlag = 0;
        for (Integer i = 0; i < 8; ++i) {  // assuming a 32 bit int
            newFlag = flags & (1 << i);
            flag8Bits[7-i] = newFlag > 0? 1: 0;
        }

        return EncodingUtil.base64Decode(getBase64EncodedText(flag8Bits));
        
        /*Blob flags = null;
        if (signQuery) {
            flags = EncodingUtil.convertFromHex('10');
        }
        
        if (ipAddress != null) {
            flags = EncodingUtil.convertFromHex('20');
        }
        
        return flags;*/
    }
    
    /*
     * Gets base 64 encoded text from an array of bits represented as integers
     */
    private String getBase64EncodedText (Integer[] bits) {
        Integer bitsLength = bits.size();
        //Length should be divisible by 8 otherwise we cannot encode
        if (Math.mod(bitsLength,8) != 0) throw new SignatureException('Cannot encode ' + bits + ' to base64');
        List<Integer> newList = bits.clone();

        //Here we need to check if the bits falls short and we need to make them 6-bit blocks.
        Integer remainderForLowerOrderBits = Math.mod(bitsLength,6);

        //If remainder of division is 1 meaning we need 5 zero bits more to have a complete 6-bit block
        if(remainderForLowerOrderBits == 1) {
            newList.addAll(new Integer[] {0,0,0,0,0});
        } 
        //If remainder of division is 2 meaning we need 4 zero bits more to have a complete 6-bit block
        else if(remainderForLowerOrderBits == 2) {
            newList.addAll(new Integer[] {0,0,0,0});
        }
        //If remainder of division is 3 meaning we need 3 zero bits more to have a complete 6-bit block
        else if(remainderForLowerOrderBits == 3) {
            newList.addAll(new Integer[] {0,0,0});
        }
        //If remainder of division is 4 meaning we need 2 zero bits more to have a complete 6-bit block
        else if(remainderForLowerOrderBits == 4) {
            newList.addAll(new Integer[] {0,0});
        }
        //If remainder of division is 5 meaning we need 1 zero bits more to have a complete 6-bit block
        else if(remainderForLowerOrderBits == 5) {
            newList.addAll(new Integer[] {0});
        }
        
        //Take 6 bit-blocks and compute decimal index value based on which appropriate letter will
        //be chosen from base64Map
        Integer i = 5;
        Integer newLength = newList.size();
        String base64EncodedText = '';
        while(i < newLength) {
            Integer decimalIndex = newList[i-5] * 32 + newList[i-4] * 16 + newList[i-3] * 8 + newList[i-2] * 4 + newList[i-1] * 2 + newList[i] * 1;
            base64EncodedText = base64EncodedText + base64Map.get(decimalIndex);
            i = i+6;
        }
        
        //Check if we need single "=" or double "==" paddings if we did not have complete 4 6-bit blocks
        Integer remainderForPadding = Math.mod(newLength,24);
        //Check for single padding (if it was short by 1 6-bit block)
        if(remainderForPadding == 18) {
            base64EncodedText = base64EncodedText + '=';
        }
        //Check for double padding (if it was short by 2 6-bit blocks)
        else if(remainderForPadding == 12) {
            base64EncodedText = base64EncodedText + '==';
        }
        return base64EncodedText;
    }

    /**
     * Joins blobs together by converting to hex and concatenating before recreateing a blob
     */ 
    private Blob joinBlocks(List<Blob> blocks) {
        String hexEncoded = '';
        for (Integer i = 0; i < blocks.size(); i++) {
            hexEncoded += EncodingUtil.convertToHex(blocks[i]);
        }
        return EncodingUtil.convertFromHex(hexEncoded);
    }

    //base64Map to be used in finding code based on index/key
    private static Map<Integer, String> base64Map = new Map<Integer, String>();
    static {
        base64Map.put(0,'A');base64Map.put(26,'a');base64Map.put(52,'0');
        base64Map.put(1,'B');base64Map.put(27,'b');base64Map.put(53,'1');
        base64Map.put(2,'C');base64Map.put(28,'c');base64Map.put(54,'2');
        base64Map.put(3,'D');base64Map.put(29,'d');base64Map.put(55,'3');
        base64Map.put(4,'E');base64Map.put(30,'e');base64Map.put(56,'4');
        base64Map.put(5,'F');base64Map.put(31,'f');base64Map.put(57,'5');
        base64Map.put(6,'G');base64Map.put(32,'g');base64Map.put(58,'6');
        base64Map.put(7,'H');base64Map.put(33,'h');base64Map.put(59,'7');
        base64Map.put(8,'I');base64Map.put(34,'i');base64Map.put(60,'8');
        base64Map.put(9,'J');base64Map.put(35,'j');base64Map.put(61,'9');
        base64Map.put(10,'K');base64Map.put(36,'k');base64Map.put(62,'+');
        base64Map.put(11,'L');base64Map.put(37,'l');base64Map.put(63,'/');
        base64Map.put(12,'M');base64Map.put(38,'m');base64Map.put(-1000,'=');
        base64Map.put(13,'N');base64Map.put(39,'n');
        base64Map.put(14,'O');base64Map.put(40,'o');
        base64Map.put(15,'P');base64Map.put(41,'p');
        base64Map.put(16,'Q');base64Map.put(42,'q');
        base64Map.put(17,'R');base64Map.put(43,'r');
        base64Map.put(18,'S');base64Map.put(44,'s');
        base64Map.put(19,'T');base64Map.put(45,'t');
        base64Map.put(20,'U');base64Map.put(46,'u');
        base64Map.put(21,'V');base64Map.put(47,'v');
        base64Map.put(22,'W');base64Map.put(48,'w');
        base64Map.put(23,'X');base64Map.put(49,'x');
        base64Map.put(24,'Y');base64Map.put(50,'y');
        base64Map.put(25,'Z');base64Map.put(51,'z');

    }
    
    //Exception class to handle unexpected problems
    public class SignatureException extends Exception {
    }
    
}