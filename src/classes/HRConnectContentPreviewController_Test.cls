@isTest
private class HRConnectContentPreviewController_Test{
    static testmethod void testHRConnectContentPreviewController() {
        String contentWorkspace = 'Global'; //One of the libraries in Biogen

        ContentWorkspace cw =[select Id from ContentWorkspace where Name = :contentWorkspace LIMIT 1];
        ContentVersion cv = new ContentVersion(ContentURL = 'http://www.biogenidec.com', title = 'biogen', firstpublishlocationid = cw.Id);
        insert cv;
        cv = [select ContentDocumentId from ContentVersion where Id = :cv.Id];

        System.debug('docid:'+cv.ContentDocumentId);

        PageReference pageRef = Page.HRConnectContentPreview;
        Test.setCurrentPage(pageRef);    
        ApexPages.currentPage().getParameters().put('fileId', cv.ContentDocumentId);  
     
        HRConnectContentPreviewController con = new HRConnectContentPreviewController();
        System.assertNotEquals(con.getContentVersions(),'0');
    
 
    }
}