@isTest
private class LMSPortal_DetailCtrlTest {

    @testSetup static void learningContentSetup(){
        // Create Custom Settings
        List<Learning_Portal_Content_Icon__c> contentIconList = createIconCustomSettings();
        INSERT contentIconList;

        //Create MBX Custom Setting Object
        MPX_Platform_Secret__c mpx = new MPX_Platform_Secret__c();
        mpx.Secret_Name__c = 'HRVideoKey';
        mpx.Secret_Value__c = 'KZPVgxzNb59L';
        mpx.URL_Base_Path__c = 'http://link.theplatform.com/s/';

        INSERT mpx;

        // Set current page reference to Detail page        
        Test.setCurrentPageReference(new PageReference('Page.LMSPortal_Detail'));

        List<LMS_Content__c> learningContentList = new List<LMS_Content__c>();
        for(Integer i=0; i < 200; i++){
            LMS_Content__c lms = new LMS_Content__c();
            lms.Description__c = 'Test Description';
            lms.New_Content__c = false;
            lms.Miscellaneous__c = 'Test Miscellaneous';
            lms.Thumbnail_Document_Id__c = '1234';
            lms.Banner_Document_Id__c = '5678';
            lms.Hot_Topic__c = false;
            lms.Title__c = 'Test Title';
            lms.Category__c = 'Test Category';

            if(i >= 0 && i <= 25){
                lms.Functional_Competency__c = 'Compliance';
                lms.Leadership_Competency__c = 'Define Strategy';
                lms.Target_Window__c = 'iFrame';
                lms.External_Media_URL__c = 'https://player.theplatform.com/p/';
                lms.Content_Type__c = 'Video';
            }
            else if(i > 25 && i <= 50){
                lms.Functional_Competency__c = 'Finance';
                lms.Leadership_Competency__c = 'Drive Results';
                lms.Target_Window__c = 'Modal';
                lms.Content_Type__c = 'eLearning';
                lms.External_Media_URL__c = 'https://www.biogen.com';
            }
            else if(i > 50 && i <= 100){
                lms.Functional_Competency__c = 'Sales';
                lms.Leadership_Competency__c = 'Build for the Future';
                lms.Target_Window__c = 'New Window';
                lms.Content_Type__c = 'Instructor Lead Training';
                lms.External_Media_URL__c = 'http://link.theplatform.com/s/';
            }
            else if(i > 100 && i <= 150){
                lms.Functional_Competency__c = 'Sales';
                lms.Leadership_Competency__c = 'Build for the Future';
                lms.Target_Window__c = 'New Window';
                lms.External_Media_URL__c = 'https://www.biogen.com';
                lms.Content_Type__c = null;
            } else {
                lms.External_Media_URL__c = 'https://www.biogen.com';
            }
            lms.Duration__c = 60;
            lms.Summary__c = 'Test Summary';
            lms.Title__c = 'Test Title';
            learningContentList.add(lms);
        }
        System.debug('LEARNING CONTENT LIST : ' + learningContentList);
        INSERT learningContentList;
    } 


    @isTest static void testDetailConstructorPos() {
        //Get Record Id of Learning Content Record
        Test.startTest();
        List<LMS_Content__c> lmsList = [SELECT Id FROM LMS_Content__c];
        //Create instance of page Standard Controller
        LMS_Content__c lmsObj = new LMS_Content__c();
        ApexPages.StandardController stdController = new ApexPages.StandardController(lmsObj);
        //Loop through Learning Content List > Set Id URL Parameter > and Construct LMSPortal_DetailCtrl class
        for(LMS_Content__c lms: lmsList){
            System.currentPageReference().getParameters().put('id', lms.Id);
            LMSPortal_DetailCtrl detailCtrl = new LMSPortal_DetailCtrl(stdController);
            System.assertEquals(detailCtrl.contentId, System.currentPageReference().getParameters().get('id'), 'URL Parameter Passed Does Not Match ContentId in Constructor');
            System.assertEquals(detailCtrl.contentId, lms.Id, 'Learning Content Record Id Does Not Match ContentId in Constructor');
        }
        Test.stopTest();
    }

    @isTest static void testMPXSig() {
        //Get Record Id of Learning Content Record
        Test.startTest();
        List<LMS_Content__c> ctList = [SELECT Id, External_Media_URL__c FROM LMS_Content__c WHERE External_Media_URL__c != '' OR External_Media_URL__c != null LIMIT 1];
        for(LMS_Content__c ct : ctList){
            String response = LMSPortal_DetailCtrl.getContentDetail(ct.Id);
            LMSPortal_DetailCtrl.DetailResponse respObj = (LMSPortal_DetailCtrl.DetailResponse)JSON.deserialize(response, LMSPortal_DetailCtrl.DetailResponse.class);
            System.assertNotEquals(null, respObj.content.externalMediaURL);
        }
        
        Test.stopTest();
    }

    @isTest static void testDetailConstructorNeg() {
        //Get Record Id of Learning Content Record
        Test.startTest();
        List<LMS_Content__c> lmsList = [SELECT Id FROM LMS_Content__c];
        //Create instance of page Standard Controller
        LMSPortal_DetailCtrl stdController = constructDetailCtrl();

        //Loop through Learning Content List > Set Id URL Parameter > and Construct LMSPortal_DetailCtrl class
        for(LMS_Content__c lms: lmsList){
            System.currentPageReference().getParameters().put('id', null);
            LMSPortal_DetailCtrl detailCtrl = constructDetailCtrl();
            System.assertEquals(detailCtrl.contentId, System.currentPageReference().getParameters().get('id'), 'URL Parameter Passed Does Not Match ContentId in Constructor');
            System.assertNotEquals(detailCtrl.contentId, lms.Id, 'Learning Content Record Matches ContentId in Constructor and should not because ContentId should be null');
        }
        Test.stopTest();
    }

    @isTest static void testGetContentDetail() {

        Test.startTest();

        List<LMS_Content__c> lmsList = [SELECT Id, New_Content__c, Hot_Topic__c, External_Media_URL__c, Description__c, Functional_Competency__c, Sub_Functional_Competencies__c, Miscellaneous__c, Leadership_Competency__c, Duration__c, Thumbnail_Document_Id__c, Related_Content__c, Summary__c, Title__c, Banner_Document_Id__c, Target_Window__c, Content_Type__c
                                        FROM LMS_Content__c WHERE Content_Type__c != null AND External_Media_URL__c != null LIMIT 90];
        System.debug('LMS LIST ELEARNING SIZE:: ' + lmsList.size());
        for(LMS_Content__c lms: lmsList){
            //Set URL Id parameter
            System.currentPageReference().getParameters().put('id', lms.Id);
            //Construct Detail Page controller
            LMSPortal_DetailCtrl detailCtrl = constructDetailCtrl();

            //Call getContentDetail remote action and handle response
            String response = LMSPortal_DetailCtrl.getContentDetail(detailCtrl.contentId);

            LMSPortal_DetailCtrl.DetailResponse respObj = (LMSPortal_DetailCtrl.DetailResponse)JSON.deserialize(response, LMSPortal_DetailCtrl.DetailResponse.class);

            //Assert Statements to Test that the Learning Content Queried Matches the Content in the JSON Response Object that would be displayed on the page
            System.assertEquals(String.valueOf(lms.New_Content__c), respObj.content.newContent);
            System.assertEquals(String.valueOf(lms.Description__c), respObj.content.description);
            System.assertEquals(String.valueOf(lms.Functional_Competency__c), respObj.content.functionalCompetency);
            System.assertEquals(String.valueOf(lms.Miscellaneous__c), respObj.content.miscellaenous);
            System.assertEquals(String.valueOf(lms.Leadership_Competency__c), respObj.content.leaderShipCompetency);
            System.assertEquals(String.valueOf(lms.Duration__c), respObj.content.duration);
            System.assertEquals(String.valueOf(lms.Thumbnail_Document_Id__c), respObj.content.thumbNailDocumentId);
            System.assertEquals(String.valueOf(lms.Summary__c), respObj.content.summary);
            System.assertEquals(String.valueOf(lms.Title__c), respObj.content.title);
            System.assertEquals(String.valueOf(lms.Hot_Topic__c), respObj.content.hotTopic);
            System.assertEquals(String.valueOf(lms.Summary__c), respObj.content.summary);
            System.assertEquals(String.valueOf(lms.Banner_Document_Id__c), respObj.content.bannerDocumentId);
            System.assertEquals(String.valueOf(lms.Target_Window__c), respObj.content.targetWindow);
            System.assertEquals(String.valueOf(lms.Content_Type__c), respObj.content.contentType);

            //Testing when value is null - the value should be given an empty string            
            System.assertEquals(respObj.content.subFunctionalCompetencies, '');
            System.assertEquals(respObj.content.relatedContent, '');
        }
        Test.stopTest();
    }
    // Test Null Content Type
    @isTest static void testGetContentDetailNeg() {

        Test.startTest();

        List<LMS_Content__c> lmsList = [SELECT Id, New_Content__c, Hot_Topic__c, External_Media_URL__c, Description__c, Functional_Competency__c, Sub_Functional_Competencies__c, Miscellaneous__c, Leadership_Competency__c, Duration__c, Thumbnail_Document_Id__c, Related_Content__c, Summary__c, Title__c, Banner_Document_Id__c, Target_Window__c, Content_Type__c
                                        FROM LMS_Content__c WHERE Content_Type__c = null AND External_Media_URL__c = null LIMIT 50];

        for(LMS_Content__c lms: lmsList){
            //Set URL Id parameter
            System.currentPageReference().getParameters().put('id', lms.Id);
            //Construct Detail Page controller
            LMSPortal_DetailCtrl detailCtrl = constructDetailCtrl();

            //Call getContentDetail remote action and handle response
            String response = LMSPortal_DetailCtrl.getContentDetail(detailCtrl.contentId);

            LMSPortal_DetailCtrl.DetailResponse respObj = (LMSPortal_DetailCtrl.DetailResponse)JSON.deserialize(response, LMSPortal_DetailCtrl.DetailResponse.class);

            //Assert Statements
            System.assert(respObj.content.iconClassName == '');
            System.assert(respObj.content.contentType == '');
            System.assertNotEquals(null, respObj.content.externalMediaURL);
        }                                       

        Test.stopTest();
    
    }

    // Test Null Record Id
    @isTest static void testGetContentDetailNullId() {

        Test.startTest();

        List<LMS_Content__c> lmsList = [SELECT Id, New_Content__c, Hot_Topic__c, External_Media_URL__c, Description__c, Functional_Competency__c, Sub_Functional_Competencies__c, Miscellaneous__c, Leadership_Competency__c, Duration__c, Thumbnail_Document_Id__c, Related_Content__c, Summary__c, Title__c, Banner_Document_Id__c, Target_Window__c, Content_Type__c
                                        FROM LMS_Content__c WHERE Content_Type__c != null LIMIT 50];

        for(LMS_Content__c lms: lmsList){
            //Construct Detail Page controller
            LMSPortal_DetailCtrl detailCtrl = constructDetailCtrl();

            //Call getContentDetail remote action and handle response
            String response = LMSPortal_DetailCtrl.getContentDetail(detailCtrl.contentId);

            LMSPortal_DetailCtrl.DetailResponse respObj = (LMSPortal_DetailCtrl.DetailResponse)JSON.deserialize(response, LMSPortal_DetailCtrl.DetailResponse.class);

            //Assert Statements - Confirm the error object in the response object contains error information when no Id is provided
            System.assert(respObj.error != null);
        }                                       

        Test.stopTest();
    
    }

    private static LMSPortal_DetailCtrl constructDetailCtrl(){
        //Create instance of page Standard Controller
        LMS_Content__c lmsObj = new LMS_Content__c();
        ApexPages.StandardController stdController = new ApexPages.StandardController(lmsObj);
        LMSPortal_DetailCtrl detailCtrl = new LMSPortal_DetailCtrl(stdController);
        return detailCtrl;
    }

    private static List<Learning_Portal_Content_Icon__c> createIconCustomSettings(){
        List<Learning_Portal_Content_Icon__c> contentIconList = new List<Learning_Portal_Content_Icon__c>();
        for(Integer i=0; i < 7; i++){
            Learning_Portal_Content_Icon__c contentIconCS = new Learning_Portal_Content_Icon__c();
            if(i == 0){
                contentIconCS.Name = 'Articles & Books';
                contentIconCS.css_class_name__c = 'articles-books-icon';
            }
            else if(i == 1){
                contentIconCS.Name = 'BioTalk';
                contentIconCS.css_class_name__c = ' biotalk-icon';
            }
            else if(i == 2){
                contentIconCS.Name = 'eLearning';
                contentIconCS.css_class_name__c = 'elearning-icon';
            }
            else if(i == 3){
                contentIconCS.Name = 'Instructor Lead Training';
                contentIconCS.css_class_name__c = 'instr-training-icon';
            }
            else if(i == 4){
                contentIconCS.Name = 'Other';
                contentIconCS.css_class_name__c = 'other-icon';
            }
            else if(i == 5){
                contentIconCS.Name = 'Podcast';
                contentIconCS.css_class_name__c = 'podcast-icon';
            }
            else if(i == 6){
                contentIconCS.Name = 'Video';
                contentIconCS.css_class_name__c = 'video-icon';
            }
            contentIconList.add(contentIconCS);
        }
        return contentIconList;
    }
}