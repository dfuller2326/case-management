public with sharing class HRP_ArticleUtils {
	public static SObject retrieveArticleTypeVersion (String lang, Id kaId) {
    if(String.isBlank(kaId)) throw new HRP_ArticleException('Knowledge Article Version Id is Required');
    String sObjectName = kaId.getSobjectType().getDescribe().getName();
    if(!sObjectName.endsWith('__ka')) throw new HRP_ArticleException('Article Id is not a valid Article type');
    sObjectName = sObjectName.replace('__ka', '__kav');
    System.debug('sObjectName===='+sObjectName);
    String soql = 'SELECT ';
    for(String fieldName : Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().keySet()) {
      soql += fieldName + ', ';
    }
    //Relationship Fields
    soql += 'LastModifiedBy.FirstName, LastModifiedBy.LastName';
    soql += ' FROM ' + sObjectName + ' WHERE Language = \'' + String.escapeSingleQuotes(lang) + '\' AND ' +  'PublishStatus = \'Online\' AND ' +'KnowledgeArticleId = \'' + String.escapeSingleQuotes(kaId) + '\' UPDATE VIEWSTAT';
    System.debug('soql==='+soql);
    List<SObject> sObjectList = (List<SObject>) Database.query(soql);
    if(sObjectList.isEmpty()) throw new HRP_ArticleException('ArticleTypeVersion is not found for the specified KnowledgeArticleVersionId');
    return sObjectList[0];
  }

  public static SObject retrieveArticleStats(Id kaId) {
    return ([SELECT Id, (Select Id, Channel, ParentId, NormalizedScore FROM ViewStats), (Select Id, Channel, ParentId, NormalizedScore FROM VoteStats) FROM KnowledgeArticle WHERE Id = :kaId]);
  }
}