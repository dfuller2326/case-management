public with sharing class LMSPortal_ErrorInfo {
	// error code
	public String errCode {get; set;}
    // error message
    public String errMsg {get; set;}
    
    // constructor
    public LMSPortal_ErrorInfo(String ec, String em){
        errCode = (String.isNotBlank(ec)) ? ec : '';
        errMsg = (String.isNotBlank(em)) ? em : '';
    }
}