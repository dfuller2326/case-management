global with sharing class LMSPortal_DetailCtrl{

	public Id contentId {get; set;}

	// dynamic query string to be used throughout the controller
    public static String queryStr = 'SELECT Id, New_Content__c, Hot_Topic__c, External_Media_URL__c, Description__c, Functional_Competency__c, Sub_Functional_Competencies__c, Miscellaneous__c, Leadership_Competency__c, Duration__c, Duration_Unit__c, Thumbnail_Document_Id__c, Related_Content__c, Summary__c, Title__c, Banner_Document_Id__c, Target_Window__c, Content_Type__c FROM LMS_Content__c';

    /**
	 * Gets the record in the 'id' URL parameter and assigns it to the contentId variable
	 * @param controller     The page's standard controller
 	*/

	public LMSPortal_DetailCtrl(ApexPages.StandardController controller) {
		// get the content id from page url param
		contentId = (Id)(ApexPages.currentPage().getParameters().get('id'));
	}

	/**
	 * Uses contentId variable to get learning content record information to display to the page
	 * @param contentId      The learning content record Id
	 * @return A serialized JSON string of the learning content to display to the page using the Backbone framework and Underscore templating engine
	 * @throws Exception message if the contentId parameter in the URL is null
	 * @throws An exception message if any other system exception occurs when attempting to retrieve the learning content record
 	*/

	@RemoteAction
	global static String getContentDetail(String contentId) {
		DetailResponse respObj = new DetailResponse();
		try {
			if (contentId != null && contentId.length() > 0) {
				respObj.content = getDetail(contentId);
			}
			else {
				respObj.error = new LMSPortal_ErrorInfo(System.Label.Learning_Content_Detail_Error_Heading, '');
			}

		} catch(Exception ex) {
			respObj.error = new LMSPortal_ErrorInfo(System.Label.Learning_Content_Detail_Error_Heading + ': ', ex.getMessage());
		}

		return JSON.serialize(respObj);
	}

	/**
	 * Called by the getContentDetail() method to support in retrieving the learning content record information to display to the page
	 * @param contentId      The learning content record Id
	 * @return ContentDetailWrapper class object containing the learning content record information
 	*/

	public static ContentDetailWrapper getDetail(String contentId) {

		//set the image Mapping from the custom settings
		Map <String, String> localIconClsMap = getIconClassMap();

		if(String.isNotBlank(contentId)) {
			/*
				* Added isRunningTest to allow for replacing where clause in the query for running multiple tests on this method in one transaction and replacing the 
				where clause in the query each time so that the query does not fail
				* Edited By: Daniel Fuller
			*/
			if(Test.isRunningTest()){
				String whereClause = ' Where Id = :contentId';
				if(queryStr.contains(whereClause)){
					System.debug('QUERY STRING BEFORE :: ' + whereClause);
					queryStr.replace(whereClause, whereClause);
					System.debug('QUERY STRING AFTER :: ' + whereClause);
				}
				else{
					queryStr += ' Where Id = :contentId';
				}
			}
			else{
				queryStr += ' Where Id = :contentId';				
			}
			//Perform dynamic query using the queryStr that has been built with all previous logic
			SObject sObj = Database.query(queryStr);

			ContentDetailWrapper wrapper = new ContentDetailWrapper();

			//Add content from SOQL query to ContentDetailWrapper object

			wrapper.newContent =  sObj.get('New_Content__c') != null ? 
				String.valueOf(sObj.get('New_Content__c')) :'';

			wrapper.hotTopic = sObj.get('Hot_Topic__c') != null ? 
				String.valueOf(sObj.get('Hot_Topic__c')) :'';

			wrapper.description = sObj.get('Description__c') != null ? 
				String.valueOf(sObj.get('Description__c')) :'';

			wrapper.functionalCompetency = sObj.get('Functional_Competency__c') != null ? 
				String.valueOf(sObj.get('Functional_Competency__c')) :'';

			wrapper.subFunctionalCompetencies = sObj.get('Sub_Functional_Competencies__c') != null ? 
				String.valueOf(sObj.get('Sub_Functional_Competencies__c')) :'';

			wrapper.miscellaenous = sObj.get('Miscellaneous__c') != null ? 
				String.valueOf(sObj.get('Miscellaneous__c')) :'';

			wrapper.leaderShipCompetency = sObj.get('Leadership_Competency__c') != null ? 
				String.valueOf(sObj.get('Leadership_Competency__c')) :'';

			wrapper.duration = sObj.get('Duration__c') != null ? 
				String.valueOf(sObj.get('Duration__c')) :'';	
				
			wrapper.durationUnit = sObj.get('Duration_Unit__c') != null ? 
				String.valueOf(sObj.get('Duration_Unit__c')) :'';			

			wrapper.thumbNailDocumentId = sObj.get('Thumbnail_Document_Id__c') != null ? 
				String.valueOf(sObj.get('Thumbnail_Document_Id__c')) :'';

			wrapper.relatedContent = sObj.get('Related_Content__c') != null ? 
				String.valueOf(sObj.get('Related_Content__c')) :'';

			wrapper.summary = sObj.get('Summary__c') != null ? 
				String.valueOf(sObj.get('Summary__c')) :'';

			wrapper.title = sObj.get('Title__c') != null ? 
				String.valueOf(sObj.get('Title__c')) :'';

			wrapper.bannerDocumentId = sObj.get('Banner_Document_Id__c') != null ? 
				String.valueOf(sObj.get('Banner_Document_Id__c')) :'';

			wrapper.targetWindow = sObj.get('Target_Window__c') != null ? 
				String.valueOf(sObj.get('Target_Window__c')) :'';

			//set the class of the icon to be used in the UI
			String contentType = String.valueOf(sObj.get('Content_Type__c'));
			String contentClass= localIconClsMap.get(contentType);

			if (contentType != null && contentClass != null) {
				wrapper.iconClassName = contentClass;
				wrapper.contentType = contentType;
			}
			else {
				wrapper.iconClassName = '';
				wrapper.contentType = '';
			}

			wrapper.externalMediaURL = sObj.get('External_Media_URL__c') != null ?
				String.valueOf(sObj.get('External_Media_URL__c')) :'';

            //Get signature if url base path matches External Media URL
            MPX_Platform_Secret__c mps = MPX_Platform_Secret__c.getOrgDefaults();
            String urlBasePath = mps.URL_Base_Path__c;
            System.debug('URL BASE PATH :: ' + urlBasePath);
            if (String.isNotBlank(urlBasePath) && String.isNotBlank(wrapper.externalMediaURL) &&
                wrapper.externalMediaURL.startsWith(urlBasePath)) {
                    Integer version = 0;
                    String ipAddress = null;
                    String secretName = mps.Secret_Name__c;
                    String secretValue = mps.Secret_Value__c;
                    Datetime tokenExpiration = Datetime.now().addSeconds(3600);
                    boolean signQuery = false;
                    String secondHalfUrl = wrapper.externalMediaURL.replace(urlBasePath, '');
                    Integer index = secondHalfUrl.indexOf('?');
                    
                    String relativePath = '';
                    if (index > 0) {
                        relativePath = secondHalfUrl.substring(0, index);
                    } else {
                        relativePath = secondHalfUrl;
                    }
                    
                    System.debug('****relativePath=' + relativePath);
                    String signature = (new SignatureFactory()).calculateSignature(wrapper.externalMediaURL, version, ipAddress, secretName,
                                                                                   secretValue, tokenExpiration, signQuery,  relativePath);
                    if (index > 0) {
                         wrapper.externalMediaURL += LMSPortal_Constants.SIG_AND_STR + signature;
                    } else {
                        wrapper.externalMediaURL += LMSPortal_Constants.SIG_QUERY_STR + signature;
                    }
                    System.debug('****URL with Sign=' + wrapper.externalMediaURL);
              }
			return wrapper;

		} else {
			return new ContentDetailWrapper();
		}
	}

	/**
	 * Uses contentId variable to get learning content record information to display to the page
	 * @return A map of Learning Content Type values and Icon Class Names
	 * @throws System exception message if any error occurs building the icon class map
 	*/

	public static Map<String, String> getIconClassMap() {
		Map <String, String> localIconClsMap = new Map<String, String>();
		try {
			List<Learning_Portal_Content_Icon__c> iconClassList = Learning_Portal_Content_Icon__c.getall().values();

			if (iconClassList !=  null && iconClassList.size() > 0) {
				//iterate over the list and create a map of the content type to the class name to be used
				for (Learning_Portal_Content_Icon__c iconObject: iconClassList) {
	            	localIconClsMap.put(String.valueOf(iconObject.get('Name')), String.valueOf(iconObject.get('css_class_name__c')));
	        	} 
			}
		} catch(Exception e) {

		}
		return localIconClsMap;

	}

	//Wrapper classes

	public class DetailResponse {
		public LMSPortal_ErrorInfo error {get; set;}
        public ContentDetailWrapper content {get; set;}
	}
	public class LMSPortal_ErrorInfo {
		String requestedPage;
		String errMsg;

		public LMSPortal_ErrorInfo(String requestedPage, String errMsg) {
			this.requestedPage = requestedPage;
			this.errMsg = System.Label.Learning_Content_Detail_Error_Msg + ' ' + '<br>' + errMsg;
		}
	}

	public class ContentDetailWrapper {
		/* 
		* Class variables changed to public for access by test class 
		* Edited by: Daniel Fuller
		*/
		public String newContent;
		public String hotTopic;
		public String description;
		public String functionalCompetency;
		public String subFunctionalCompetencies;
		public String miscellaenous;
		public String leaderShipCompetency;
		public String duration;
		public String durationUnit;
		public String thumbNailDocumentId;
		public String relatedContent;
		public String summary;
		public String title;
		public String bannerDocumentId;
		public String targetWindow;
		public String contentType;
		public String externalMediaURL;
		public String iconClassName;

		public ContentDetailWrapper () {

        }
	}
}