public with sharing class PicklistEntryWrapper {
	public String active {get; set;}
    public String defaultValue {get; set;}
    public string label {get; set;}
    public String value {get; set;}
    public String validFor {get; set;}
	public PicklistEntryWrapper() {}
}