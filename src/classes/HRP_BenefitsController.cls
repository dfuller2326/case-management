global with sharing class HRP_BenefitsController {
	public HRP_BenefitsController() {
		
	}

	@RemoteAction
	global static String getPageData() {
		BenefitsResponse respObj = new BenefitsResponse();
		try {
			// Query context User and User Profile
			User u = [SELECT Id, Country, ProfileId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
			System.debug('USER COUNTRY :: ' + u.Country);
			Profile p = [SELECT Id, Name FROM Profile WHERE Id = :u.ProfileId LIMIT 1];

		  	// Retrieve Home Page Top Services Content
		  	respObj.benefitsContent = HRP_Content.retrieveHomePageBenefits(u.Country, p.Name);

		} catch(Exception ex) {
		  respObj.error = new HRP_ErrorInfo(System.Label.Home_Page_Error_Heading, ex.getMessage());
		  System.debug(LoggingLevel.ERROR, 'ERROR RETRIEVING BENEFITS PAGE DATA === ' + ex.getMessage());
		}
		System.debug('RESPONSE :: ' + JSON.serialize(respObj));
		return JSON.serialize(respObj);
	}

	public class BenefitsResponse {
		public List<HRP_ContentWrapper> benefitsContent {get;set;}
		public HRP_ErrorInfo error {get;set;}
		public BenefitsResponse() {
		}
	}
}