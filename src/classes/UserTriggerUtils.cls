/**
 * ClassName: UserTriggerUtils
 * Purpose: To create functions to handle special actions when a user is created or updates
 */
public with sharing class UserTriggerUtils {
    /**
     * Assign the appropriate profiles to users on create or update
     */
    public static void performUserUpdates(List<User> affectedUsers,Map<id,User> oldVersion){
        //variable to store the profile id from utility function
        String profileId='';

        Map<String,String> profileSFIdMap=HRPortalUtils.getProfilesofInterest();

        Set<String> interestedProfileIds=new Set<String>();
        interestedProfileIds.addAll(profileSFIdMap.values());
        
        for(User userInstance:affectedUsers){
         
        //make sure the current user profile id is in the list of interested profiles
            if(  profileSFIdMap!=null && interestedProfileIds.contains(userInstance.ProfileId)
              && (oldVersion==null ||
                  oldVersion!=null && UserTriggerUtils.isUserProfileAffected(userInstance,oldVersion))
              )
            {
                  userInstance.ProfileId=((profileId=profileSFIdMap.get(determineProfileName(userInstance)))!=null)? profileId : userInstance.ProfileId;
                  System.debug('====userInstance.ProfileId=====' + userInstance.ProfileId);
            }
        }
    }

    public static boolean isUserProfileAffected(User userInstance,Map<Id,User> oldVersion){
         Set<String> integProfileIds=HRPortalUtils.getInstance().getIntegrationProfileIds();
         
        return (   (userInstance.Hire_Date__c!=oldVersion.get(userInstance.Id).Hire_Date__c) 
               ||  (userInstance.Grade__c!=oldVersion.get(userInstance.Id).Grade__c) 
               ||  (userInstance.Cost_Center__c!=oldVersion.get(userInstance.Id).Cost_Center__c) 
               ||  (userInstance.Division__c!=oldVersion.get(userInstance.Id).Division__c) 
               ||  (userInstance.Is_Manager__c!=oldVersion.get(userInstance.Id).Is_Manager__c) 
               ||  (userInstance.ProfileId!=oldVersion.get(userInstance.Id).ProfileId) 
               ||  (integProfileIds!=null && integProfileIds.contains(userInstance.ProfileId))
               ||  (userInstance.Job_Title__c!=oldVersion.get(userInstance.Id).Job_Title__c) 
               ||  (userInstance.IsRegularEmployee__c!=oldVersion.get(userInstance.Id).IsRegularEmployee__c)
              );
    }

     
    
    public static void performPermSetUpdates(Map<Id,User> triggerUserMap,Map<Id,User> oldUserMap){
         
          //construct affected user map
          Map<Id,User> affectedUserMap=new Map<Id,User>();
          
           //get interested profiles
        Map<String,String> profileSFIdMap=HRPortalUtils.getProfilesofInterest();

         //get interested permissions
        Map<String,String> permSetSFIdMap=HRPortalUtils.getPermSetsMap();
        
         //build a set of permsets to be deletedPermSets
        List<PermissionSetAssignment> toBedeletedPermSets=new List<PermissionSetAssignment>();
        
        List<User> chatterDigests=new List<User>();
        
        
        
        List<PermissionSetAssignment> permSetAssignments;
        
          Set<String> interestedProfileIds=new Set<String>();
        interestedProfileIds.addAll(profileSFIdMap.values());
        
        
        for(User userInstance:[select  Id
                                     , UserPreferencesDisableAllFeedsEmail
                                     , UserPreferencesContentNoEmail
                                     , UserPreferencesContentEmailAsAndWhen
                                from  User
                                where Id in :triggerUserMap.keySet()
                                and UserPreferencesContentNoEmail=false
                                and UserPreferencesContentEmailAsAndWhen=false
                                     ]) {
           userInstance.UserPreferencesDisableAllFeedsEmail=true;
           userInstance.UserPreferencesContentNoEmail=true;
           userInstance.UserPreferencesContentEmailAsAndWhen=true;
           
           chatterDigests.add(userInstance);                          
       }
          
          
          for(User userInstance:triggerUserMap.values()){
            
               
           
           if(   interestedProfileIds.contains(userInstance.ProfileId)
              && (userInstance.Id!=null && userInstance.isActive)
              
              ){
                 affectedUserMap.put(userInstance.Id, userInstance);
              }
          }
          
          //variable to store the corresponding permission set id
        String permSetId=null;
        
         //list of perm setnames that are retaining in the db and need not be created
         
        Set<String> retainedPermSets=new Set<String>();
          
        Map<String,Set<PermissionSetAssignment>> existingPermSetMap=HRPortalUtils.getCurrentPermSetAssignments(triggerUserMap.keySet());
          
        //reinitialize the list to create a new list
        permSetAssignments=new List<PermissionSetAssignment>();
        
        for(User userInstance:affectedUserMap.values()){
          
          retainedPermSets=new Set<String>();
             
           if(   interestedProfileIds.contains(userInstance.ProfileId)
              && (userInstance.Id!=null && userInstance.isActive)
              ){
                
                Set<PermissionSetAssignment> currentPermSetAssignments=existingPermSetMap.get(userInstance.id);
                Set<String> newPermSetNames=HRPortalUtils.constructPermSetNames(userInstance);
                
                //get the perm set names for the old values of the user records and clear them as we are going to reassign them anyways.
                if(oldUserMap!=null){
                    Set<String> existingPermSetNames=HRPortalUtils.constructPermSetNames(oldUserMap.get(userInstance.id));
                    retainedPermSets=existingPermSetNames.clone();
                    retainedPermSets.retainAll(newPermSetNames);
                    
                    System.debug('===retainedPermSets==' + retainedPermSets);
                    System.debug('===newPermSetNames==' + newPermSetNames);
                    
                    System.debug('===existingPermSetNames==' + existingPermSetNames);
                    
                    //permission sets to be removed .
                    existingPermSetNames.removeAll(retainedPermSets);
                    
                    System.debug('===existingPermSetNames==' + existingPermSetNames);
                    
                    System.debug('===retainedPermSets after removeAll==' + retainedPermSets);
                    
                    
                    if(existingPermSetNames!=null){
                      for(String permSetName:existingPermSetNames){   
                     PermissionSetAssignment existingPermSet=HRPortalUtils.isPermSetExisting(currentPermSetAssignments, userInstance.id, permSetSFIdMap.get(permSetName)); 
                     if(existingPermSet!=null){
                       System.debug('===toBedeletedPermSets==' + toBedeletedPermSets);
                           toBedeletedPermSets.add(existingPermSet);
                     }
                  } 
                    }
                }
                
                for(String permSetName:newPermSetNames){   
                 //find if there are any perm sets that should not be deleted
                 PermissionSetAssignment existingPermSet=HRPortalUtils.isPermSetExisting(currentPermSetAssignments, userInstance.id, permSetSFIdMap.get(permSetName)); 
                 System.debug('====perm set name ===' + permSetName);
                 System.debug('====permSetAssignments ===' + permSetAssignments);
                 
                   if(    (permSetId=permSetSFIdMap.get(permSetName))!=null 
                       && (existingPermSet==null)
                       ){
                         PermissionSetAssignment permissionSetAssign=new PermissionSetAssignment();
                         permissionSetAssign.PermissionSetId= permSetId;
                         permissionSetAssign.AssigneeId=userInstance.id;
                         permSetAssignments.add(permissionSetAssign);
                         System.debug('====adding perm set ==== ' + permSetAssignments);
                         System.debug('====retainedPermSets ==== ' + retainedPermSets);
                         System.debug('====permSetName ==== ' + permSetName);
                     }
                }
               
                
            }
            
            
         }
         
         System.debug('===permSetAssignments insert =====' + permSetAssignments);
        
        Database.SaveResult[] sr=Database.insert(permSetAssignments); 
          
        String errorString='';

           for (Integer i=0;i<sr.size();i++) {

                if (!sr[i].isSuccess()) {   

                    errorString+=HRPortalUtils.constructErrorString(null,sr[i],null,permSetAssignments[i]);  

                }  

            }

            if(errorString!=null && errorString.length()>0){

              HRPortalUtils.emailError('Permission Set Assignment Failed', errorString); 

            }
            
            
            sr=Database.update(chatterDigests); 
          
            errorString='';

            for (Integer i=0;i<sr.size();i++) {

                if (!sr[i].isSuccess()) {   

                    errorString+=HRPortalUtils.constructErrorString(null,sr[i],null,chatterDigests[i]);  

                }  

            }

            if(errorString!=null && errorString.length()>0){

              HRPortalUtils.emailError('Chatter Digest settings Failed', errorString); 

            }
            
          //delete unwanted perm sets
           
          Database.Deleteresult[] dr=Database.delete(toBedeletedPermSets);  
          
           errorString='';

           for (Integer i=0;i<dr.size();i++) {

                if (!dr[i].isSuccess()) {   

                    errorString+=HRPortalUtils.constructErrorString(null,null,dr[i], permSetAssignments[i]);  

                }  

            }

            if(errorString!=null && errorString.length()>0){

              HRPortalUtils.emailError('Permission Set Assignment Delete Failed', errorString); 

            }
            
      
    }

    private static string determineProfileName(User userInstance){
        //variable to store the needed profile name
        String neededProfileName='';

        if(isNewHire(userInstance)){
            neededProfileName+=BiogenConstants.NEWLY_HIRED;
        }

        /*
         * Check the eligibility for profiles in a hierarchical fashion
         * 1. HR for HR
         * 2. Executive
         * 3. Manager
         * 4. Employee
         */
     
     
        if(isHRforHR(userInstance)){
            neededProfileName+=BiogenConstants.HR4HR_PROFILE;
            //set the feature perms for HR for HR profile
            updateFeaturePerms(userInstance,true);
            
        }

        else if(isExecutive(userInstance)){
            neededProfileName+=BiogenConstants.EXECUTIVE_PROFILE;
            //reset the feature perms for other profiles. It is cheaper to just reset as opposed to checking
            //if the old profile was HR for HR only.
            updateFeaturePerms(userInstance,false);
        }

        else if(isManager(userInstance)){
            neededProfileName+=BiogenConstants.MANAGER_PROFILE;
            updateFeaturePerms(userInstance,false);
        }
        else{
            neededProfileName+=BiogenConstants.EMPLOYEE_PROFILE;
            updateFeaturePerms(userInstance,false);
        }
        System.debug('====neededProfileName===' + neededProfileName);

        //get the needed profile name
        return neededProfileName; 

    }

    /**
     * Method to update the permissions on user record as needed. Will be invoked
     * only by the profile method in this class.
     *
     * No method for employee is needed as it is an employee, if it is not a HR for HR, executive or manager.
     */
    private static void updateFeaturePerms(User userInstance,boolean isTurnedOn){
            userInstance.UserPermissionsKnowledgeUser=isTurnedOn;
            userInstance.UserPermissionsSFContentUser=isTurnedOn;
            userInstance.UserPermissionsSupportUser=isTurnedOn;
    }


    private static boolean isManager(User userInstance){
        return (userInstance!=null && userInstance.Is_Manager__c 
                                   && !isHRforHR(userInstance)
               );
    }

    private static boolean isHRforHR(User userInstance){
        /**
        Update: 8/18/14. Manish. isHRforHR method is modified to accomodate check for case management user 
        Requirement Doc: Salesforce – License Issue Requirements
        **/
        if(userInstance!=null && (userInstance.Cost_Center__c == null || (userInstance.Cost_Center__c != null && userInstance.Cost_Center__c.length()<13)))
            return (userInstance!=null && userInstance.Division__c!=null 
                                   && userInstance.Division__c.contains(BiogenConstants.HUMAN_RESOURCES));
        //System.debug('isHRforHR: '+new Set<String>(HRPortalUtils.getPortalPropertyValues(BiogenConstants.NON_CASE_MGMT_USER_COST_CENTER))+':'+userInstance.Cost_Center__c.substring(0,13));
        return (userInstance!=null && userInstance.Division__c!=null 
                                   && userInstance.Division__c.contains(BiogenConstants.HUMAN_RESOURCES)
                                   && !((new Set<String>(HRPortalUtils.getPortalPropertyValues(BiogenConstants.NON_CASE_MGMT_USER_COST_CENTER))).contains(userInstance.Cost_Center__c.substring(0,13)))
               );
    }
    private static boolean isExecutive(User userInstance){
        //System.debug('usertrigger: '+new Set<String>(HRPortalUtils.getPortalPropertyValues(BiogenConstants.JOB_TITLE_FOR_EXEC)));
        return (userInstance!=null && !isHRforHR(userInstance) 
                                   && (( userInstance.Grade__c!=null 
                                       && userInstance.Grade__c>=HRPortalUtils.getInstance().getLowestExecutiveGrade()
                                       )
                                       || 
                                       (new Set<String>(HRPortalUtils.getPortalPropertyValues(BiogenConstants.JOB_TITLE_FOR_EXEC))).contains(userInstance.Job_Title__c)
                                       ||
                                       (userInstance.Division__c!=null 
                                        && userInstance.Division__c.contains(BiogenConstants.HUMAN_RESOURCES)
                                        && (new Set<String>(HRPortalUtils.getPortalPropertyValues(BiogenConstants.NON_CASE_MGMT_USER_COST_CENTER))).contains(userInstance.Cost_Center__c.substring(0,13))
                                       )      
                                   ) 
               );
    }

    private static boolean isNewHire(User userInstance){
        System.debug('new hire: '+userInstance.Hire_Date__c+ ' : '+HRPortalUtils.getInstance().getNewHireDays());
        
        //if the hire date is empty, we consider them to be a regular employee.
        if(userInstance.Hire_Date__c==null || userInstance.IsRegularEmployee__c){
            return false;
        }
        
        //check the hire date condition or the field set by workflow to be true. 
        //If the hire date condition is satisfied, the record might be pushed by workday after 30 days 
        //from hire date. 

        return (userInstance!=null && 
                 (userInstance.Hire_Date__c.daysBetween(System.today())<= HRPortalUtils.getInstance().getNewHireDays())
               );
    }

    private static Contact buildContactInstance(User userInstance, String accountId){
         
         return new Contact(                       AccountId=accountId
                                                  , FirstName=userInstance.FirstName
                                                  , LastName=userInstance.LastName  
                                                  , MailingStreet=userInstance.Street
                                                  , MailingCity=userInstance.City
                                                  , MailingState=userInstance.State
                                                  , MailingPostalCode=userInstance.PostalCode
                                                  , MailingCountry=userInstance.Country
                                                  , MailingStateCode=userInstance.StateCode
                                                  , MailingCountryCode=userInstance.CountryCode
                                                  , MailingLatitude=userInstance.Latitude
                                                  , MailingLongitude=userInstance.Longitude
                                                  , MobilePhone=userInstance.MobilePhone
                                                  , Email=userInstance.Email
                                                  , Phone=userInstance.Phone
                                                  , Title=userInstance.Title
                                                  , Employee_ID__c=userInstance.Employee_ID__c
                                                  , Cost_Center__c=userInstance.Cost_Center__c
                                                  , Division__c=userInstance.Division__c
                                                  , Grade__c=userInstance.Grade__c
                                                  , Hire_Date__c=userInstance.Hire_Date__c
                                                  , HR_Business_Partner__c=userInstance.HR_Business_Partner__c
                                                  , Is_Manager__c=userInstance.Is_Manager__c
                                                  , Job_Title__c=userInstance.Job_Title__c
                                                  , Location__c=userInstance.Location__c
                                                  , Middle_Initial__c=userInstance.Middle_Initial__c
                                                  , Not_Available__c= userInstance.Not_Available__c
                                                  , Supervisory_Org__c= userInstance.Supervisory_Org__c
                                                  , Termination_Date__c=userInstance.Termination_Date__c
                                                  , Worker_Type__c=userInstance.Worker_Type__c
                                                  , Business_Unit__c= userInstance.Business_Unit__c
                                                  , Profile_Name__c=userInstance.Profile.Name
                                                  , On_Leave__c=userInstance.On_Leave__c
                                                  , Active__c=userInstance.isActive
                                                 );
          
    }
    private static List<User> queryUser(Set<Id> userIds){
         Map<String,User> userMap=new Map<String,User>();
         
       for(User userInstance: [select  FirstName
                                , LastName
                                , Employee_ID__c
                                , Cost_Center__c
                                , Division__c
                                , Grade__c
                                , Hire_Date__c
                                , HR_Business_Partner__c
                                , Is_Manager__c
                                , Job_Title__c
                                , Location__c
                                , Middle_Initial__c
                                , Not_Available__c
                                , Supervisory_Org__c
                                , Termination_Date__c
                                , Worker_Type__c
                                , On_Leave__c
                                , ProfileId
                                , Profile.Name
                                , Business_Unit__c
                                , Street
                                , City
                                , State
                                , PostalCode
                                , Country
                                , StateCode
                                , CountryCode
                                , Latitude
                                , Longitude
                                , MobilePhone
                                , Email
                                , Phone
                                , Title
                                , IsActive

                            from User 
                            where Id in :userIds
                           ]){
           userMap.put(userInstance.Employee_ID__c,userInstance);                 
       }
       
       return userMap.values();
    }
 
   @future
   public static void performContactUpserts(Set<Id> affectedUserIds){
    List<Contact> contactsToBeUpserted=new List<Contact>();
    //query the account with name biogen
    Account accountInstance=[select Id from Account where Name=:BiogenConstants.BIOGEN_ACCOUNT limit 1];
     //query user records
    for(User userInstance : queryUser(affectedUserIds)){
         contactsToBeUpserted.add(buildContactInstance(userInstance,accountInstance.id));
     }
     if(Test.isRunningTest()){
         System.runAs(new User(Id = Userinfo.getUserId())) {
            upsert contactsToBeUpserted Employee_ID__c;
          }
      }
      else{
          Database.Upsertresult[] ur=Database.upsert(contactsToBeUpserted,Contact.Employee_ID__c);
          
          String errorString='';
          
          for (Integer i=0;i<ur.size();i++) {

                if (!ur[i].isSuccess()) {   

                    errorString+=HRPortalUtils.constructErrorString(ur[i],null,null, contactsToBeUpserted[i]);  

                }  

            }

            if(errorString!=null && errorString.length()>0){

              HRPortalUtils.emailError('Concact Creation Failed', errorString); 

            }
      }
   }
   
   private static void removeFromPublicGroup(String groupId, Set<id> affectedUserIds){
     List<GroupMember> groupMembers=[select Id from GroupMember where GroupId=:groupId and UserOrGroupId in :affectedUserIds];
     if(groupMembers!=null && groupMembers.size()>0){
     
     //delete unwanted perm sets
           
          Database.Deleteresult[] dr=Database.delete(groupMembers);  
          
           String errorString='';

           for (Integer i=0;i<dr.size();i++) {

                if (!dr[i].isSuccess()) {   

                    errorString+=HRPortalUtils.constructErrorString(null,null,dr[i], groupMembers[i]);  

                }  

            }

            if(errorString!=null && errorString.length()>0){

              HRPortalUtils.emailError('Permission Set Assignment Delete Failed', errorString); 

            } 
      }
   }
   
   public static void managerhr4hrpublicGrp(List<User> userList){
      Set<Id> tobeaddedUsers=new Set<Id>();
      Set<Id> toberemovedUsers=new Set<Id>();
      
      Group groupInstance=[select Id from Group where DeveloperName=:BiogenConstants.BIOGEN_HR4HR_PUBLIC_GROUP limit 1];
      Set<Id> existingGrpMembers=new Set<Id>();
      //query existing group members
      for(GroupMember grpMemberInstance:[select UserOrGroupId from GroupMember where GroupId=:groupInstance.id]){
        existingGrpMembers.add(grpMemberInstance.UserOrGroupId);
      }
      
      for(User userInstance:userList){
        //the current user is HR 
         if(isHRforHR(userInstance) && !existingGrpMembers.contains(userInstance.Id)
            ){
            tobeaddedUsers.add(userInstance.Id);
         }
         else{
            toberemovedUsers.add(userInstance.Id);
         }
      }
      
      //call the method to add the respective users
      addandRemoveHRforHRPublicGroup(tobeaddedUsers,toberemovedUsers);
       
      
         
   }
   
   private static void addToPublicGroup(String groupId, Set<id> affectedUserIds){
      List<GroupMember> grpMembers=new List<GroupMember>();
     //query user records
     for(User userInstance : queryUser(affectedUserIds)){
        GroupMember grpMemberInstance=new GroupMember(GroupId=groupId,UserOrGroupId=userInstance.id);
        grpMembers.add(grpMemberInstance);
     }

     if(Test.isRunningTest()){
         System.runAs(new User(Id = Userinfo.getUserId())) {
            insert grpMembers;
         }
     }
     else{
        
        Database.SaveResult[] sr=Database.insert(grpMembers,false); 
          
          String errorString='';

           for (Integer i=0;i<sr.size();i++) {

                if (!sr[i].isSuccess()) {   

                    errorString+=HRPortalUtils.constructErrorString(null,sr[i],null, grpMembers[i]);  

                }  

            }

            if(errorString!=null && errorString.length()>0){

              HRPortalUtils.emailError('Public Group Assignment Failed', errorString); 

            }
     }
   }
   
   @future
   public static void addandRemoveHRforHRPublicGroup(Set<Id> tobeAdded,Set<Id> tobeRemoved){

    Group groupInstance=[select Id from Group where DeveloperName=:BiogenConstants.BIOGEN_HR4HR_PUBLIC_GROUP limit 1];
    if(groupInstance!=null){
        addToPublicGroup(groupInstance.Id,tobeAdded);
        removeFromPublicGroup(groupInstance.Id,tobeRemoved);
    }
   }
   
   
   @future
   public static void addToAllBiogenPublicGroup(Set<Id> affectedUserIds){

    Group groupInstance=[select Id from Group where DeveloperName=:BiogenConstants.BIOGEN_PUBLIC_GROUP limit 1];
    if(groupInstance!=null){
        addToPublicGroup(groupInstance.Id,affectedUserIds);
    }
     
  }
}