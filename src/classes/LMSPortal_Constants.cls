public with sharing class LMSPortal_Constants {
	public static String CUSTOM_PERMISSION = 'CustomPermission';
	public static String CUSTOM_PERMISSION_STR = 'Learning Portal Admin App';
	public static String LEARNING_PORTAL = 'Learning Portal';
	public static String LEARNING_PORTAL_ADMINISTRATION = 'Learning Portal Administration';
	public static String LMSPORTAL_HOME = 'LMSPortal_Home';
	public static String LMSPORTAL_ALLLEARNINGCONTENT = 'LMSPortal_AllLearningContent';
	public static final String ASKHR_LINK = 'https://biibhrconnect.force.com/apex/HRConnectCase';

	//Signature Constants
	public static final String SIG_AND_STR = '&sig=';
	public static final String SIG_QUERY_STR = '?sig=';
	public static final String SIG_ENCRYPT_TYPE = 'hmacSHA1';

	// Biogen Course
	public static final String BIOGEN_CORE_STR = 'Biogen Core Learning';
	public static final String FUNCTIONAL_LEARNING_STR = 'Functional Learning';
	public static final String ELECTIVE_OFFERINGS_STR = 'Elective Learning';
	public static final String HOT_TOPICS_STR = 'Hot Topics';
	public static final String POPULAR_STR = 'Popular';
	public static final String NEW_RELEASES_STR = 'New Releases';
	public static final String QUICK_LEARNING_STR = 'Quick Learning';
	public static final String DOCUMENT_URL = '/servlet/servlet.FileDownload?file=';
	public static final String ADDITIONAL_FILTERS = 'Additional Filters';

	//Profiles
	public static final String BIOGEN_ADMIN_PROFILE = 'Biogen System Administrator';
	public static final String BIOGEN_EMPLOYEE_PROFILE = 'Employee';

	//Permission Sets - API Names
	public static final String LEARNING_PORTAL_ADMIN_PS = 'Learning_Portal_Administration';
	public static final String ADDITIONAL_FILTERS_API = 'Additional_Filters__c';
	public static final String FUNCTIONAL_COMPETENCY_API = 'Functional_Competency__c';
	public static final String LEADERSHIP_COMPETENCY_API = 'Leadership_Competency__c';
	public static final String CONTENT_TYPE_API = 'Content_Type__c';
	public static final String CATEGORY_API = 'Category__c';

	//Document Folder DeveloperName
	public static final String LMSPORTAL_DOC_FOLDER = 'LMS_Portal';

	//Error messages
	public static final String VIEWALL_EX_MSG = 'Exception in getting View All data ';
	public static final String BU_DIV_EX_MSG = 'Business Unit, Division and Location are missing';

	//Misc Debug Messages
	public static final String VIEWALL_RESOBJ_DEBUG_HEAD = 'View All Response Object :: ';
	public static final String GETVIEWALL_RETURN_MSG = 'LMSPortal_AllLearningContentController::getViewAllData() returning';
	public static final String GETVIEWALL_ENTER_MSG = 'LMSPortal_AllLearningContentController::getViewAllData() entered';
	public static final String GETLEARNINGCONTENT_ENTER_MSG = 'LMSPortal_AllLearningContentController::getLearningContent() entered';
	public static final String GETLEARNINGCONTENT_RETURN_MSG = 'LMSPortal_AllLearningContentController::getLearningContent() return';
	public static final String GETFILTERS_ENTER_MSG = 'LMSPortal_AllLearningContentController::getFilters() entered';

	//Home Controller Constants
	public static final String LMS_LINK_TYPE_LEARNING = 'My Learning';
	public static final String LMS_LINK_TYPE_QUICKLNK = 'Quick Link';
	public static final String LMS_LINK_STATUS_ACTV = 'Active';
	public static final Integer LMS_COURSE_TITLE_LENGTH = 60;
}