@isTest
public with sharing class LMSPortal_FooterControllerTest {
	
	static testMethod void myUnitTest() {
        Datetime dt = Datetime.now();
    	Long l = dt.getTime();
    	PreviewWrapper preW = new PreviewWrapper(l, '000 MA Talent Acquisition', '000 MA Talent Acquisition', 'Test Campus');
    	String JSONString = JSON.serialize(preW);
    	
    	setUp();
        test.StartTest();
        LMSPortal_FooterController controller = new LMSPortal_FooterController();
        controller.getHelpfulLinks(JSONString);
        system.assertNotEquals(controller.helpfulLinks, null);
        system.assertEquals(controller.listSize, 2);
    }
    
    static void setUp() 
	{
		List<LMS_Content__c> lmsList = LMSPortal_HomeControllerTest.createLMSContentData(8);
		List<Learning_Content_Access__c> lcList = LMSPortal_HomeControllerTest.createLMSLCAccessData(lmsList);
		List<LMS_Link__c> llLink = LMSPortal_HomeControllerTest.createLinks(3);
        List<Learning_Content_Access__c> lcLinkList = LMSPortal_HomeControllerTest.createLMSLCAccessDataLinks(llLink);
	}
	
	private class PreviewWrapper {
        public Long effectiveDate {get; set;}
        public String businessUnit {get; set;}
        public String division {get; set;}
        public String location {get; set;} 
        public PreviewWrapper(Long dateInMilliseconds, String businessUnit, String division, String location) {
            this.effectiveDate = dateInMilliseconds;
            this.businessUnit = String.isNotBlank(businessUnit) ? businessUnit : '';
            this.division = String.isNotBlank(division) ? division : '';
            this.location = String.isNotBlank(location) ? location : '';
        }
    }

}