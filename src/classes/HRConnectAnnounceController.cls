public class HRConnectAnnounceController {
	Public String Selpagenum {get; set;}
	Public Integer size{get;set;}
	Public Integer noOfRecords{get; set;}

	Public String Selpagenuml {get; set;}
	Public Integer sizel{get;set;}
	Public Integer noOfRecordsl{get; set;}

	string userprofile = HRConnectCommon.userprofilename;
	string usercountry = HRConnectCommon.usercountry;
	date todaysdate = system.today();
	datetime now = system.now();

	Public String idparam {get; set;}

	public HRConnectAnnounceController()
	{
		if (userprofile.contains('System Administrator'))
			userprofile = 'Employee';

		idparam = ApexPages.currentPage().getParameters().get('ai');
	}

	Public class annwrapper {
		//Public String createdate {get;set;}
		Public String moddate {get;set;}
		Public String dow {get;set;}
		Public String id {get;set;}
		Public Announcement__c ann {get;set;}
		public annwrapper(Announcement__c a){
			if (system.now().format('MM/dd/yy') == a.Publish_Date__c.format('MM/dd/yy'))
				dow = 'Today';
			else if (system.now().addDays(-1).format('MM/dd/yy') == a.Publish_Date__c.format('MM/dd/yy'))
				dow = 'Yesterday';
			else
				dow = a.Publish_Date__c.format('EEEE');

    		moddate=a.Publish_Date__c.format('MM/dd/yy hh:mma z').replace('AM','am');
    		moddate = moddate.replace('PM','pm');
    		this.id = String.valueOf(a.id).substring(0,15);
    		this.ann = a;
		}
	}
    public ApexPages.StandardSetController setCon
    {
        get{
            if(setCon == null){
            if (Selpagenum == null)
                Selpagenum = '10';
            size = integer.valueof(Selpagenum);
            string queryString = 'Select a.Name, a.publish_date__c, a.LastModifiedDate, a.Id, a.Expiration_Date__c, a.CreatedDate, a.Body__c, a.Audience_Role__c, a.Audience_Location__c'
                               + '  From Announcement__c a '
                               + ' where Audience_Location__c includes(\'Global\') '
                               + '   and (Audience_Role__c includes(\'All\') or Audience_Role__c includes(\''+userprofile+'\')) '
                               + '   and ( Publish_Date__c <= :now or Publish_Date__c = null ) '
                               + '   and Expiration_Date__c >= :todaysdate '
                               + '   and Home_Page_Announcement__c = false '
                               + ' order by Publish_Date__c desc';
			system.debug(queryString);
            setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
            setCon.setPageSize(size);
            noOfRecords = setCon.getResultSize();             }
            return setCon;         }
        set;
    }
    public ApexPages.StandardSetController setConLocal
    {
        get{
            if(setConLocal == null){
            if (Selpagenuml == null)
                Selpagenuml = '10';
            sizel = integer.valueof(Selpagenuml);
            string queryString = 'Select a.Name, a.publish_date__c, a.LastModifiedDate, a.Inet_Announcement__c, a.Id, a.Expiration_Date__c, a.CreatedDate, a.Body__c, a.Audience_Role__c, a.Audience_Location__c '
                               + '  From Announcement__c a '
                               + ' where Audience_Location__c includes(\''+usercountry+'\') '
                               + '   and (Audience_Role__c includes(\'All\') or Audience_Role__c includes(\''+userprofile+'\')) '
                               + '   and ( Publish_Date__c <= :now or Publish_Date__c = null ) '
                               + '   and Expiration_Date__c >= :todaysdate '
                               + '   and Home_Page_Announcement__c = false '                               
                               + ' order by Publish_Date__c desc';
            system.debug(queryString);
            setConLocal = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
            setConLocal.setPageSize(sizel);
            noOfRecordsl = setConLocal.getResultSize();             }
            return setConLocal;         }
        set;
    }

	Public List<annwrapper> getAnnouncements()
    {
    List<annwrapper> annList = new List<annwrapper>();
    for(Announcement__c a : (List<Announcement__c>)setCon.getRecords())
        annList.add(new annwrapper(a));
    return annList;
    }

	Public List<annwrapper> getLocalAnnouncements()
    {
    List<annwrapper> annList = new List<annwrapper>();
    for(Announcement__c a : (List<Announcement__c>)setConLocal.getRecords())
        annList.add(new annwrapper(a));
    return annList;
    }
}