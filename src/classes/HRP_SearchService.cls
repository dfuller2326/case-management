public with sharing class HRP_SearchService {

  // Retrieve primary knowledge articles based on URL search parameter
  public static List<SObject> searchPrimaryArticles(String queryString, String lang, Integer recordLimit) {
    System.debug('QUERY STRING :: ' + queryString);
    List<SObject> sObjectList = null;
    String articleTypes = getSearchArticleTypes('PrimarySearch');

    if (String.isBlank(lang)) return sObjectList;

    // Search All the article of the language if queryString is empty
    if (String.isBlank(queryString)) return retrieveArticleTypes(queryAllArticles(lang, articleTypes));

    System.debug('INSIDE QUERY ARTICLES');
    // Query KnowledgeArticleVersions based on the querySting and langauge
    sObjectList = retrieveArticleTypes(queryArticles(queryString, lang, articleTypes, recordLimit));
    return sObjectList;
  }

  // Retrieve seondary knowledge articles based on URL search parameter
  public static List<SObject> searchSecondaryArticles(String queryString, String lang) {
    System.debug('QUERY STRING :: ' + queryString);
    List<SObject> sObjectList = null;
    String articleTypes = getSearchArticleTypes('SecondarySearch');

    if (String.isBlank(lang)) return sObjectList;

    // Search All the article of the language if queryString is empty
    if (String.isBlank(queryString)) return retrieveArticleTypes(queryAllArticles(lang, articleTypes));

    System.debug('INSIDE QUERY ARTICLES');
    // Query KnowledgeArticleVersions based on the querySting and langauge
    sObjectList = retrieveArticleTypes(queryArticles(queryString, lang, articleTypes, null));
    return sObjectList;
  }

  // Retrieve the article based on the queryString and the language
  private static List<SObject> queryArticles(String queryString, String lang, String articleTypes, Integer recordLimit){
    lang = String.escapeSingleQuotes( lang );
    queryString = String.escapeSingleQuotes(
      EncodingUtil.urlDecode(queryString, 'UTF-8') );
    String sObjectName = 'KnowledgeArticleVersion';
    String sFields = 'Id, MasterVersionId, Title, ArticleNumber, Language, ArticleType';
    String sWhere = ' WHERE publishStatus=\'Online\' AND language=\'' + lang + '\'';
    sWhere += String.isBlank(articleTypes) ? '' : ' AND ArticleType IN (' + articleTypes + ')';
    String sLimit = (recordLimit == null || recordLimit == 0 || recordLimit >= 2000) ? '' :
      ( ' LIMIT ' + String.valueOf(recordLimit));

    String qStr = 'FIND {' + queryString + '*} IN ALL FIELDS' + ' RETURNING ' + sObjectName + '( ' + sFields + sWhere + ' ) ' + sLimit + ' UPDATE TRACKING';
    System.debug('***** Query String *****' + qStr);

    try {
      List<List<SObject>> results = Search.query(qStr);
      System.debug('results =='+results.size());
      System.debug('results 0 =='+results[0].size());
      return results[0];
    } catch(Exception e) {
      System.debug(e.getMessage());
      return new List<SObject>();
    }
  }

  // Query all the article based on the language
  private static List<SObject> queryAllArticles(String lang, String articletypes) {
    lang = String.escapeSingleQuotes( lang );
    String sObjectName = 'KnowledgeArticleVersion';
    String sFields = 'Id, MasterVersionId, Title, ArticleNumber, Language, ArticleType';
   String sWhere = ' WHERE publishStatus=\'Online\' AND language=\'' + lang + '\'';
    sWhere += String.isBlank(articleTypes) ? '' : ' AND ArticleType IN (' + articleTypes + ') ';


    String qStr = 'SELECT ' + sFields + ' FROM ' + sObjectName + sWhere;
    System.debug('***** Query String *****' + qStr);
    List<SObject> sObjList = new List<SObject>();
    try {
      sObjList = Database.query(qStr);
    } catch(Exception e) {
      System.debug(e.getMessage());
      //QueryException: Invalid language code. Check that the language is included in your Knowledge language settings.
    }
    return sObjList;
  }

  private static String getSearchArticleTypes(String type){
    String articleTypes = '';
    Map<String, List<HRP_Settings__c>> articleTypeMap = HRP_Settings.getSettingsByType(type, 'Text1__c');
    if(articleTypeMap.isEmpty()) return articleTypes;
    for(String str: articleTypeMap.keySet()) {
      articleTypes += '\'' + String.escapeSingleQuotes(str) + '\', ';
    }
    articleTypes = String.isNotBlank(articleTypes) ?
      articleTypes.left(articleTypes.lastIndexOf(',')) : '';
    System.debug('ARTICLE TYPES STRING :: ' + articleTypes);
    return articleTypes;
  }

  private static List<SObject> retrieveArticleTypes(List<SObject> allArticles) {
    List<SObject> articles = new List<SObject>();
    Map<Id, SObject> articleMap = retrieveArticleTypesMap(allArticles);
    for(SObject sObj : allArticles){
      if(articleMap.isEmpty()) break;
      if(!articleMap.containsKey(sObj.Id)) continue;

      articles.add(articleMap.get(sObj.Id));
    }
    return articles;
  }

  // Based on the KnowledgeArticleVersion Retrieve the Article Types and build the Map of Id, SObject
  private static Map<Id, SObject> retrieveArticleTypesMap(List<SObject> allArticles) {
    Map<Id, SObject> articleTypeMap = new Map<Id, SObject>();
    if(allArticles == null || allArticles.isEmpty()) return articleTypeMap;

    // Build and Retrieve the Article Type Records for the masterVersionId List
    Map<String, List<Id>> masterVersionSObjectMap = getArticleTypeMap(allArticles, 'MasterVersionId');
    List<sObject> masterVersionList = new List<SObject>();
    for(String articleType : masterVersionSObjectMap.keySet()) {
      masterVersionList.addAll(retrieveArticleTypeVersionRecords(
        masterVersionSObjectMap.get(articleType), true).values());
    }
    Map<Id, SObject> masterVersionMap = new Map<Id, SObject>(masterVersionList);

    // Build and retrieve the Article Type Wrappers based on the KnowledgeArticleVersions
    Map<String, List<Id>> sObjectMap = getArticleTypeMap(allArticles, 'Id');
    for(String articleType : sObjectMap.keySet()) {
      articleTypeMap.putAll(retrieveMasterArticles(
        retrieveArticleTypeVersionRecords(sObjectMap.get(articleType), false).values(), masterVersionMap));
    }
    return articleTypeMap;
  }

  // Build and add the Article Type Wrapper to the list of Article Wrappers.
  private static Map<Id, SObject> retrieveMasterArticles(List<SObject> articleTypes, Map<Id, SObject> masterVersionMap){
    Map<Id, SObject> articleTypeMap = new Map<Id, SObject>();
    for(SObject sObj : articleTypes) {
      Id masterId = (Id) sObj.get('MasterVersionId');
      SObject newSObj = sObj;
      if(!masterVersionMap.isEmpty() && masterVersionMap.containsKey(masterId)){
        newSObj = masterVersionMap.get(masterId);
      }
      articleTypeMap.put(sObj.Id, newSObj);
    }
    return articleTypeMap;
  }

  // Retrieve the Article Type records based  on the list of ids.
  private static Map<Id, SObject> retrieveArticleTypeVersionRecords(List<Id> articleIds, Boolean withDataCategories){
    Map<Id, SObject> sObjectMap = new Map<Id, SObject>();
    if(articleIds == null || articleIds.isEmpty()) return sObjectMap;
    String sObjectName = articleIds[0].getSobjectType().getDescribe().getName();
    String soql = 'SELECT Id, Title, KnowledgeArticleId, ArticleNumber, MasterVersionId, LastPublishedDate, Summary, Language, ArticleType';
    if(withDataCategories != null && withDataCategories)
        System.debug('DATA CATEGORIES FOUND');
       soql += ', (SELECT Id, DataCategoryName, DataCategoryGroupName FROM DataCategorySelections)';
    soql += ' FROM ' + sObjectName + ' WHERE Id IN :articleIds';
    System.debug('soql==='+soql);
    try {
      sObjectMap = new Map<Id, SObject>((List<SObject>) Database.query(soql));
      System.debug('MAP RETURNED BY ARTICLE TYPE VERSIONS :: ' + sObjectMap);
      return sObjectMap;
    } catch(Exception e) {
      System.debug(e.getMessage());
      return sObjectMap;
    }
  }

  // Build a Map<Key, List<Id>> from the List of articles, field name
  private static Map<String, List<Id>> getArticleTypeMap(List<SObject> articles, String key){
    Map<String, List<Id>> articleTypeMap = new Map<String, List<Id>>();
    if(articles == null || articles.isEmpty()) return articleTypeMap;

    String objName = null;
    Id keyId = null;

    // Build the map with the key as field name passed and the value to List of Id's
    for(SObject sObj : articles){
      keyId = (Id) sObj.get(key);
      objName = keyId.getSObjectType().getDescribe().getName();
      if(articleTypeMap.containsKey(objName)){
        articleTypeMap.get(objName).add(keyId);
      } else {
        articleTypeMap.put(objName, new List<Id>{keyId});
      }
    }
    return articleTypeMap;
  }
}