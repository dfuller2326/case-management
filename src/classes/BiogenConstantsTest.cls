@isTest
private class BiogenConstantsTest {
	
	@isTest static void testBiogenConstants() {
		// Implement test code
	 System.assertEquals(BiogenConstants.EMPLOYEE_PROFILE ,'Employee');
     System.assertEquals(BiogenConstants.EXECUTIVE_PROFILE ,'Exec');
     System.assertEquals(BiogenConstants.MANAGER_PROFILE ,'Manager');
     System.assertEquals(BiogenConstants.HR4HR_PROFILE ,'HR for HR');
     
     System.assertEquals(BiogenConstants.NEWLY_HIRED ,'New Hire ');
     System.assertEquals(BiogenConstants.HUMAN_RESOURCES,'Human Resources'); 
     
     /**
      * HR portal properties
      * @type {Custom Settings}
      */
     System.assertEquals(BiogenConstants.INTERESTED_PROFILE,'INTERESTED_PROFILE');
     System.assertEquals(BiogenConstants.NEW_HIRE_CONDITION,'NEW_HIRE_CONDITION');
     System.assertEquals(BiogenConstants.LOWEST_EXECUTIVE_GRADE,'LOWEST_EXECUTIVE_GRADE');
     System.assertEquals(BiogenConstants.BIOGEN_BUSINESS_COUNTRY,'BUSINESS_COUNTRY');
     System.assertEquals(BiogenConstants.PROFILE_ENTITY_TYPE,'Profile');
     System.assertEquals(BiogenConstants.PERMSET_ENTITY_TYPE,'PermissionSet');
          
     System.assertEquals(BiogenConstants.BIOGEN_ACCOUNT,'Biogen');
     System.assertEquals(BiogenConstants.DEFAULT_COUNTRY,'United States');
     System.assertEquals(BiogenConstants.INTEGRATION_PROFILE_COMMUNITY,'Workday Integ Profile');
     
     System.assertEquals(BiogenConstants.INTEGRATION_PROFILE,'INTEGRATION_PROFILE');
     System.assertEquals(BiogenConstants.PERMSET_SUFFIX,'Article_Visibility');
     System.assertEquals(BiogenConstants.BIOGEN_PUBLIC_GROUP,'All_Biogen_Users');
     System.assertEquals(BiogenConstants.BIOGEN_GLOBAL,'Global');
	}
	
	 
	
}