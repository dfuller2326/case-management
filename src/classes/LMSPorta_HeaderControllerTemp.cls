public with sharing class LMSPorta_HeaderControllerTemp {

	public String userName {get;set;}
	public String displayDate {get;set;}
	public String previewDate {get;set;}
	public String previewButton {get; set;}
	public String previewButtonCls {get; set;}
	public static Boolean isPopUpDisplayed {get;set;}
	public String cmpToRender {get;set;}
	public string testVal {get;set;}

	/* <><><> Variables for the Custom Renderer <><><> */
	public Boolean renderWhatsNewViewAll {get;set;}
	public Boolean renderTrainingRequests {get;set;}
	public Boolean renderLearningCoursesContainer {get;set;}
	public Boolean renderPortalHome {get;set;}
	/* <><><> -------------------------------- <><><> */

	public LMSPorta_HeaderControllerTemp() {
		testVal = 'old value';
		isPopUpDisplayed = false;
		previewButton = 'Preview';
		previewButtonCls = 'btn-primary';
		displayDate = 'Today: ' + String.valueOf(Date.today());
		userName = UserInfo.getName();// getUserNameFromId(String userId);
		previewDate = 'Previewing of Biometrics Division as of 09/30/2015';

		/* <><><> Variables for the Custom Renderer <><><> */
		renderWhatsNewViewAll = false;
		renderLearningCoursesContainer = false;
		renderPortalHome = true;
		renderTrainingRequests = false;
		System.debug('RENDER WHATS NEW VIEW ALL IN CONSTRUCTOR :: ' + renderWhatsNewViewAll);
		/* <><><> -------------------------------- <><><> */
	}
	
	public void showPopup(){
		system.debug('in my show method $$');
		testVal = 'new value';
		isPopUpDisplayed = true;
	}
	
	public void submitPopup(){
		isPopUpDisplayed = false;
		previewButton = 'Stop Preview';
		previewButtonCls = 'btn-warning';
	}

	public void setRenderValues(){
		/* @ Purpose: Renders Main Components to the page body based on the attribute passed to the cmpRender variable
		   @ Author: Daniel Fuller 
		*/
		if(!String.isBlank(cmpToRender) && (cmpToRender == 'LMSPortal_WhatsNewViewAll' || cmpToRender == 'LMSPortal_AllVideosViewAll')){
			renderLearningCoursesContainer = true;
			renderTrainingRequests = false;
			renderPortalHome = false;
		}
		else if(!String.isBlank(cmpToRender) && cmpToRender == 'LMSPortal_Home'){
			renderLearningCoursesContainer = false;
			renderTrainingRequests = false;
			renderPortalHome = true;
		}
		else if(!String.isBlank(cmpToRender) && cmpToRender == 'LMSPortal_TrainingRequests'){
			renderLearningCoursesContainer = false;
			renderPortalHome = false;
			renderTrainingRequests = true;
		}
	}
}