public with sharing class LMSPortal_AllVideosController {
	public Integer allVideosCount {get;set;}

	public LMSPortal_AllVideosController() {
		allVideosCount = 0;
	}

	public List<BannerWrapperClass> getLearningResourcesWrapperForAllVideos(){
		List<BannerWrapperClass> wrapperList = new List<BannerWrapperClass>();
		Map<Id, LMS_Content__c> lmsMap = new Map<Id, LMS_Content__c>();
		Set<Id> documentIdSet = new Set<Id>();
		List<LMS_Content__c> lmsContentList = LMSPortal_ContentDataAccess.getLMSContent(false, null, null, null);

		//Create Map of Banner Document Id to LMS Content Record
		for(LMS_Content__c content : lmsContentList){
			documentIdSet.add(content.Banner_Document_Id__c);
			if(!lmsMap.containsKey(content.Banner_Document_Id__c)){
				lmsMap.put(content.Banner_Document_Id__c, content);
			}
		}
		System.debug('FINAL DOCUMENT ID SET :: ' + documentIdSet);

		//Use Map to get Document Record
		List<Document> docList = [SELECT Id, Body, ContentType, Url FROM Document WHERE Id IN :documentIdSet];
		for(Document d : docList){
			allVideosCount++;
			System.debug('DOCUMENT BODY :: ' + d.Body);
			wrapperList.add(new BannerWrapperClass(allVideosCount, d.Id, lmsMap.get(d.Id)));
		}
		System.debug('DOCUMENT LIST FOUND :: ' + docList);
		System.debug('FINAL WRAPPER LIST :: ' + wrapperList);
		return wrapperList;
	}
}