public with sharing class HRConnectHomeController {
	public String marginclass {get;set;}
	public String homepageann {get
		{
			List<Announcement__c> ann = [Select a.Body__c
									From Announcement__c a
									where Home_Page_Announcement__c = true
									and Publish_Date__c <= :system.now()
									and Expiration_Date__c >= :system.today() limit 1];
			if (ann.size() == 0)
				return 'Your One-Stop for all your HR related information!<br>Click <a href="/apex/HRConnectNew">here</a> to learn more about HRConnect';
			else
				return ann[0].Body__c;
		}
		set;}
	//inner class to hold navigation items
	public class NavigationItems
	{
		public String imagename {get;set;}
		public String coltitle {get;set;}
		public String coltext {get;set;}
		public String bsclass {get;set;}
		public String loc {get;set;}
		
		NavigationItems(String img, String title, String txt, String bsc, String loc)
		{
			imagename = img;
			coltitle = title;
			coltext = txt;
			bsclass = bsc;
			this.loc = loc;
		}		
	}
	List<NavigationItems> navitems;

	public List<NavigationItems> getNavitems()
	{
		
		string userprofile = HRConnectCommon.userprofilename;
		Integer bscols = 3;
		String bsclass;
		String bsclassos = '';
		String baseurl = 'https://'+ApexPages.currentPage().getHeaders().get('Host')+'/apex/';
		String welcomeurl = '#';

		HRConnect_Portal_Config__c cs = HRConnect_Portal_Config__c.getValues('PORTAL_CONFIG_LIST');
		if (cs.New_Hire_Welcome_Link__c != null && cs.New_Hire_Welcome_Link__c != '')
			welcomeurl = cs.New_Hire_Welcome_Link__c;
					
		if (userprofile.contains('New'))
				bscols++;	
						
		//if (userprofile.contains('HR for HR'))
		//		bscols++;
		
		if (bscols == 5)
		{
			bsclass = 'col-md-2';
			bsclassos = 'col-md-offset-1';
		}
		else if (bscols == 4)
		{
			bsclass = 'col-md-3';
		}
		else
		{
			bsclass = 'col-md-4';
		}
		
		marginclass = 'bgcust-nav1-'+bscols;
		
		if(navitems == null) {
			
            navitems = new List<NavigationItems>();
            
            //system.debug(ApexPages.currentPage().getHeaders().get('Host'));
            
            if (userprofile.contains('New'))
            	navitems.add(new NavigationItems('main_nav_welcome.png','Welcome','Get all new hire information in one place',bsclass+' '+bsclassos,welcomeurl));
            
            navitems.add(new NavigationItems('whats_new_whats_new.png','What\'s New?','Get the latest HR news and information'
            	,bsclass,baseurl+'HRConnectNew'));            
            navitems.add(new NavigationItems('main_nav_find_search_hr.png','Search HR','Get answers to your most frequently asked HR questions',bsclass,baseurl+'HRConnectAnswers'));
            navitems.add(new NavigationItems('main_nav_ask_HR.png','Ask HR','Get in contact with HR to receive further support'
            	,bsclass,baseurl+'HRConnectCase'));
            	
			//if (userprofile.contains('HR for HR'))
            //	navitems.add(new NavigationItems('main_nav_HR_4_HR.png','HR4HR','Get in touch with your other HR colleagues and lets collaborate!',bsclass,'#'));
        }
        return navitems;
	}

	//public HRConnectHomeController()
	//{
	//}
}