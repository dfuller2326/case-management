@isTest
private class LMSPortal_AllLearningContentCtrlTest {
    
    @testSetup static void learningContentSetup(){

        //Create Content Filter Custom Settings List
        List<Learning_Content_Filters__c> contentFilterList = LMSPortal_TestHelper.createContentFilterCS();
        INSERT contentFilterList;

        // create custom settings record for Functional Competency
        FunctionalCompetencyConfig__c fc = LMSPortal_TestHelper.createFunctionalCompetencies();
        Database.SaveResult sr = Database.insert(fc);
        System.assertNotEquals(null, sr.getId());

        // Set current page reference to Detail page        
        Test.setCurrentPageReference(new PageReference('Page.LMSPortal_AllLearningContent'));

        List<LMS_Content__c> learningContentList = new List<LMS_Content__c>();
        for(Integer i=0; i < 200; i++){
            LMS_Content__c lms = new LMS_Content__c();
            lms.Category__c = 'Values in Action';
            lms.Description__c = 'Test Description';
            lms.New_Content__c = true;
            lms.Miscellaneous__c = 'Test Miscellaneous';
            lms.Thumbnail_Document_Id__c = '1234';
            lms.Banner_Document_Id__c = '5678';
            lms.Hot_Topic__c = true;
            lms.Title__c = 'Test Title';

            if(i >= 0 && i <= 25){
                lms.Functional_Competency__c = 'Compliance';
                lms.Leadership_Competency__c = 'Define Strategy';
                lms.Target_Window__c = 'iFrame';
                lms.External_Media_URL__c = 'https://www.biogen.com';
                lms.Content_Type__c = 'Video';
            }
            else if(i > 25 && i <= 50){
                lms.Functional_Competency__c = 'Finance';
                lms.Leadership_Competency__c = 'Drive Results';
                lms.Target_Window__c = 'Modal';
                lms.Content_Type__c = 'eLearning';
                lms.External_Media_URL__c = 'https://www.biogen.com';
            }
            else if(i > 50 && i <= 100){
                lms.Functional_Competency__c = 'Sales';
                lms.Leadership_Competency__c = 'Build for the Future';
                lms.Target_Window__c = 'New Window';
                lms.Content_Type__c = 'Instructor Lead Training';
                lms.External_Media_URL__c = 'https://www.biogen.com';
            }
            else if(i > 100 && i <= 150){
                lms.Functional_Competency__c = 'Sales';
                lms.Sub_Functional_Competencies__c = 'Business Knowledge';
                lms.Leadership_Competency__c = 'Build for the Future';
                lms.Target_Window__c = 'New Window';
                lms.External_Media_URL__c = 'https://www.biogen.com';
                lms.Content_Type__c = null;
            } else {
                lms.External_Media_URL__c = 'https://www.biogen.com';
            }
            
            lms.Duration__c = 10;
            lms.Summary__c = 'Test Summary';
            lms.Title__c = 'Test Title';
            learningContentList.add(lms);
        }

        System.debug('LEARNING CONTENT LIST : ' + learningContentList);
        INSERT learningContentList;

        List<LMS_Content__c> lmsList = [SELECT Id FROM LMS_Content__c];
        List<Learning_Content_Access__c> caList = new List<Learning_Content_Access__c>();
        for(Integer i=0; i < lmsList.size(); i++){
            //Create Learning Content Access Records
            Learning_Content_Access__c ca = new Learning_Content_Access__c();
            if(i >= 0 && i <= 25){
                    ca.Business_Unit__c = '000 MA Talent Acquisition';
                    ca.Division__c = '000 MA Talent Acquisition';
                    ca.Learning_Content__c = lmsList.get(i).Id;
                    ca.Location__c = 'Campus-Cambridge';
            }
            else if(i > 25 && i <= 50){
                    ca.Business_Unit__c = '1000-0180 Research & Development';
                    ca.Division__c = '1111-0135 Clinical Development';
                    ca.Learning_Content__c = lmsList.get(i).Id;
                    ca.Location__c = 'Campus-Canada';
            }
            else if(i > 50 && i <= 100){
                    ca.Business_Unit__c = '1000-0180 Research & Development';
                    ca.Division__c = '1111-0130 Biometrics';
                    ca.Learning_Content__c = lmsList.get(i).Id;
                    ca.Location__c = 'Campus-Canada';
            }
            else if(i > 100 && i <= 200){
                    ca.Business_Unit__c = '1000-0180 Research & Development';
                    ca.Division__c = '1111-0110 Biometrics';
                    ca.Learning_Content__c = lmsList.get(i).Id;
                    ca.Location__c = 'Campus-Cambridge';
            }
            caList.add(ca);
        }
        INSERT caList;
        
        Additional_Filters__c af = new Additional_Filters__c();
        af.Name = 'Additional Filters';
        af.Filter_Values__c = 'Functional Learning,Popular,New Releases,Quick Learning';
        INSERT af; 
    }

    @isTest static void testConstructor(){
        Test.startTest();

        LMSPortal_AllLearningContentController allContentCtrl = new LMSPortal_AllLearningContentController();
        System.assert(allContentCtrl.recordLimit != null);

        Test.stopTest();
    }

    @isTest static void testGetViewAllData(){

        Test.startTest();
        //Generate Filter Params for View All Method
        String filterParamResponse = generatefilterParamJson();

        //Generate Preview Params for View All Method
        String previewParamResponse = generatepreviewParamJson();

        String response = LMSPortal_AllLearningContentController.getViewAllData(12, 12, filterParamResponse, '', '', previewParamResponse, '', true, true, 'New Content');
        LMSPortal_AllLearningContentController.ViewAllResponse wrapper = (LMSPortal_AllLearningContentController.ViewAllResponse) JSON.deserialize(response, LMSPortal_AllLearningContentController.ViewAllResponse.class);

        Test.stopTest();

        //Assert Statements
        System.assert(wrapper != null);
        System.assert(wrapper.error == null);
        System.assert(wrapper.content.records != null);
        System.assert(wrapper.content.totalRecords != null);
    }

    @isTest static void testGetViewAllDataWithCategory(){

        //Scenario when parameters are blank but category and subcategory are passed

        Test.startTest();

        String response = LMSPortal_AllLearningContentController.getViewAllData(12, 12, '', '', '', '', 'Values in Action', false, false, '');
        LMSPortal_AllLearningContentController.ViewAllResponse wrapper = (LMSPortal_AllLearningContentController.ViewAllResponse) JSON.deserialize(response, LMSPortal_AllLearningContentController.ViewAllResponse.class);

        //Assert Statements
        for(LMSPortal_AllLearningContentController.ContentFilterWrapper wrap : wrapper.filters){
            for(LMSPortal_AllLearningContentController.FilterValue filterWrap : wrap.filterValues){
                if(filterWrap.value == 'Values in Action'){
                    System.assert(filterWrap.isChecked == true);
                }
            }
        }

        Test.stopTest();
    }
    
    @isTest static void testGetViewAllDataWithSearch(){
        Test.startTest();
		
		//Generate Preview Params for View All Method
        String previewParamResponse = generatepreviewParamJson();
        String response = LMSPortal_AllLearningContentController.getViewAllData(12, 12, '', '', 'test', previewParamResponse, 'Values in Action', false, false, 'New Content');
        System.assertNotEquals(null, (LMSPortal_AllLearningContentController.ViewAllResponse) JSON.deserialize(response, LMSPortal_AllLearningContentController.ViewAllResponse.class));

        Test.stopTest();
    }

    @isTest static void testGetViewAllDataWithFunctionalLearning(){

        Test.startTest();

        String response = LMSPortal_AllLearningContentController.getViewAllData(12, 12, '', '', 'val', '', 'Values in Action', false, false, 'Functional Learning');
        System.assertNotEquals(null, (LMSPortal_AllLearningContentController.ViewAllResponse) JSON.deserialize(response, LMSPortal_AllLearningContentController.ViewAllResponse.class));

        Test.stopTest();
    }
    
    @isTest static void testGetViewAllDataWithPopular(){

        Test.startTest();

        String response = LMSPortal_AllLearningContentController.getViewAllData(12, 12, '', '', 'val', '', 'Values in Action', false, false, 'Popular');
        System.assertNotEquals(null, (LMSPortal_AllLearningContentController.ViewAllResponse) JSON.deserialize(response, LMSPortal_AllLearningContentController.ViewAllResponse.class));
        Test.stopTest();
    }
    
    @isTest static void testGetViewAllDataWithNewReleases(){

        Test.startTest();

        String response = LMSPortal_AllLearningContentController.getViewAllData(12, 12, '', '', 'val', '', 'Values in Action', false, false, 'New Releases');
        System.assertNotEquals(null, (LMSPortal_AllLearningContentController.ViewAllResponse) JSON.deserialize(response, LMSPortal_AllLearningContentController.ViewAllResponse.class));
        Test.stopTest();
    }
    
    @isTest static void testGetViewAllDataWithQuickLearning(){

        Test.startTest();

        String response = LMSPortal_AllLearningContentController.getViewAllData(12, 12, '', '', 'val', '', 'Values in Action', false, false, 'Quick Learning');
        System.assertNotEquals(null, (LMSPortal_AllLearningContentController.ViewAllResponse) JSON.deserialize(response, LMSPortal_AllLearningContentController.ViewAllResponse.class));
        Test.stopTest();
    }

    @isTest static void testGetViewAllDataWithJsonError(){

        //Generate Filter Params for View All Method
        String filterParamResponse = generatefilterParamJsonError();

        Test.startTest();

        String response = LMSPortal_AllLearningContentController.getViewAllData(12, 12, filterParamResponse, '', '', '', '', true, false, '');
        LMSPortal_AllLearningContentController.ViewAllResponse wrapper = (LMSPortal_AllLearningContentController.ViewAllResponse) JSON.deserialize(response, LMSPortal_AllLearningContentController.ViewAllResponse.class);
        System.debug('TEST GET VIEW ALL DATA WITH JSON WRAPPER ERROR :: ' + wrapper.error);
        //System.assertNotEquals(wrapper.error, null);
        
        Test.stopTest();
    }

    @isTest static String generatefilterParamJsonError(){
        JSONGenerator gen = JSON.createGenerator(true);
        //Beginning of Main array - left out to create error

            //Beginning of main json object
            gen.writeStartObject();

                gen.writeFieldName('filters');

                gen.writeStartObject();

                    //Field name for field category
                    gen.writeStringField('fieldCategory', 'Category');
                    gen.writeStringField('fieldApiName', 'Category__c');

                    //Field name for filter values json object
                    gen.writeFieldName('filterValues');
                        //Beginning of inner array
                        gen.writeStartArray();

                            //Beginning of Filter Value Object
                            gen.writeStartObject();
                                gen.writeStringField('label', 'Values in Action');
                                gen.writeStringField('value', 'Values in Action');
                                gen.writeBooleanField('isChecked', true);
                            //End of Filter Value object
                            gen.writeEndObject();

                        //End of Filter Values Array
                        gen.writeEndArray();

                gen.writeEndObject();

            gen.writeEndObject();

        return gen.getAsString();
    }

    @isTest static String generatefilterParamJson(){
        JSONGenerator gen = JSON.createGenerator(true);
        //Beginning of Main array
        gen.writeStartArray();

            //Beginning of main json object
            gen.writeStartObject();

                //Field name for field category
                gen.writeStringField('fieldCategory', 'Category');
                gen.writeStringField('fieldApiName', 'Category__c');

                //Field name for filter values json object
                gen.writeFieldName('filterValues');
                    //Beginning of inner array
                    gen.writeStartArray();

                        //Beginning of Filter Value Object
                        gen.writeStartObject();
                            gen.writeStringField('label', 'Values in Action');
                            gen.writeStringField('value', 'Values in Action');
                            gen.writeBooleanField('isChecked', true);
                        //End of Filter Value object
                        gen.writeEndObject();

                    //End of Filter Values Array
                    gen.writeEndArray();

            gen.writeEndObject();

            //Beginning of json object
            gen.writeStartObject();

                    //Field name for field category
                gen.writeStringField('fieldCategory', 'Content Type');
                gen.writeStringField('fieldApiName', 'Content_Type__c');

                //Field name for filter values json object
                gen.writeFieldName('filterValues');
                    //Beginning of inner array
                    gen.writeStartArray();

                        //Beginning of Filter Value Object
                        gen.writeStartObject();
                            gen.writeStringField('label', 'Video');
                            gen.writeStringField('value', 'Video');
                            gen.writeBooleanField('isChecked', true);
                        //End of Filter Value object
                        gen.writeEndObject();

                    //End of Filter Values Array
                    gen.writeEndArray();
            //End of json object
            gen.writeEndObject();
            
            //Beginning of json object
            gen.writeStartObject();

                    //Field name for field category
                gen.writeStringField('fieldCategory', 'Additional Filter');
                gen.writeStringField('fieldApiName', 'Filter_Values__c');

                //Field name for filter values json object
                gen.writeFieldName('filterValues');
                    //Beginning of inner array
                    gen.writeStartArray();

                        //Beginning of Filter Value Object
                        gen.writeStartObject();
                            gen.writeStringField('label', 'New Content');
                            gen.writeStringField('value', 'New Content');
                            gen.writeBooleanField('isChecked', true);
                        //End of Filter Value object
                        gen.writeEndObject();

                    //End of Filter Values Array
                    gen.writeEndArray();
            //End of json object
            gen.writeEndObject();
            
            //Beginning of json object
            gen.writeStartObject();

                    //Field name for field category
                gen.writeStringField('fieldCategory', 'Additional Filter');
                gen.writeStringField('fieldApiName', 'Filter_Values__c');

                //Field name for filter values json object
                gen.writeFieldName('filterValues');
                    //Beginning of inner array
                    gen.writeStartArray();

                        //Beginning of Filter Value Object
                        gen.writeStartObject();
                            gen.writeStringField('label', 'Popular');
                            gen.writeStringField('value', 'Popular');
                            gen.writeBooleanField('isChecked', true);
                        //End of Filter Value object
                        gen.writeEndObject();

                    //End of Filter Values Array
                    gen.writeEndArray();
            //End of json object
            gen.writeEndObject();
            
            //Beginning of json object
            gen.writeStartObject();

                    //Field name for field category
                gen.writeStringField('fieldCategory', 'Additional Filter');
                gen.writeStringField('fieldApiName', 'Filter_Values__c');

                //Field name for filter values json object
                gen.writeFieldName('filterValues');
                    //Beginning of inner array
                    gen.writeStartArray();

                        //Beginning of Filter Value Object
                        gen.writeStartObject();
                            gen.writeStringField('label', 'New Releases');
                            gen.writeStringField('value', 'New Releases');
                            gen.writeBooleanField('isChecked', true);
                        //End of Filter Value object
                        gen.writeEndObject();

                    //End of Filter Values Array
                    gen.writeEndArray();
            //End of json object
            gen.writeEndObject();
            
            //Beginning of json object
            gen.writeStartObject();

                    //Field name for field category
                gen.writeStringField('fieldCategory', 'Additional Filter');
                gen.writeStringField('fieldApiName', 'Filter_Values__c');

                //Field name for filter values json object
                gen.writeFieldName('filterValues');
                    //Beginning of inner array
                    gen.writeStartArray();

                        //Beginning of Filter Value Object
                        gen.writeStartObject();
                            gen.writeStringField('label', 'Quick Learning');
                            gen.writeStringField('value', 'Quick Learning');
                            gen.writeBooleanField('isChecked', true);
                        //End of Filter Value object
                        gen.writeEndObject();

                    //End of Filter Values Array
                    gen.writeEndArray();
            //End of json object
            gen.writeEndObject();

            //Beginning of json object
            gen.writeStartObject();

                    //Field name for field category
                gen.writeStringField('fieldCategory', 'Leadership Competency');
                gen.writeStringField('fieldApiName', 'Leadership_Competency__c');

                //Field name for filter values json object
                gen.writeFieldName('filterValues');
                    //Beginning of inner array
                    gen.writeStartArray();

                        //Beginning of Filter Value Object
                        gen.writeStartObject();
                            gen.writeStringField('label', 'Define Strategy');
                            gen.writeStringField('value', 'Define Strategy');
                            gen.writeBooleanField('isChecked', true);
                        //End of Filter Value object
                        gen.writeEndObject();

                    //End of Filter Values Array
                    gen.writeEndArray();
            //End of json object
            gen.writeEndObject();

        //End of main array
        gen.writeEndArray();
            
        System.debug('JSON GENERATER OUTPUT :: ' + gen.getAsString());
        return gen.getAsString();
    }

    @isTest static String generatepreviewParamJson(){
        JSONGenerator gen = JSON.createGenerator(true);
        //Beginning of Preview Param Object
        gen.writeStartObject();
            gen.writeNumberField('effectiveDate', 1443585600000L);
            gen.writeStringField('division', '000 MA Talent Acquisition');
            gen.writeStringField('businessUnit', '000 MA Talent Acquisition');
            gen.writeStringField('location', 'Campus-Cambridge');
        //End of Preview Param Object
        gen.writeEndObject();
            
        return gen.getAsString();
    }
}