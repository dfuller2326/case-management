global class HRConnectRemoter {

    //edit these to change rating scale.  Must be a stringified integer, 1-5.
    @TestVisible private static string HELPFUL_RATING = '5';
    @TestVisible private static string UNHELPFUL_RATING = '3';

    public HRConnectRemoter(ApexPages.StandardController stdController) {}
    public HRConnectRemoter(HRConnectAnswerController controller) {}
    public HRConnectRemoter(HRConnectTrackController controller) {}
    public HRConnectRemoter(HRConnectHomeController controller) {}
    public HRConnectRemoter(HRConnectFootController controller) {}
    public HRConnectRemoter() {}
    //public HRConnectRemoter(user standardcontroller) {}

/*  global class ArticleDetails
    {
        public string body {get;set;}
        public integer isFavorite {get;set;}

        ArticleDetails(string body,integer fav)
        {
            this.body = body;
            isFavorite = fav;
        }
    }


@RemoteAction
//global static Policies_Guideline__kav getArticleDetail(string id,string arttype) {
global static ArticleDetails getArticleDetail(string id,string arttype) {

        system.debug('Inside HRConnectRemoter->getArticleDetail');
        system.debug('param id: '+id);

        KnowledgeArticleVersion articleqry = [SELECT Id, Summary, Title
            FROM KnowledgeArticleVersion
            WHERE PublishStatus = 'online'
            and Language = 'en_US'
            and knowledgearticleid=:id UPDATE VIEWSTAT][0];

        system.debug('summary: '+articleqry.id);

        List<Favorite_Articles__c> fa = [select id from Favorite_Articles__c where name=:UserInfo.getUserId() and article_id__c = :id limit 1];

        //system.debug('favorite id: '+fa.id);

        //make this @future
        Recently_Viewed__c rv = new Recently_Viewed__c();
        if (articleqry.Title.length() > 40)     //limit search string, replace with Id based search if possible
            rv.Site_URL__c = '/apex/HRConnectAnswers?ss="'+articleqry.Title.substring(0, 40)+'"';
        else
            rv.Site_URL__c = '/apex/HRConnectAnswers?ss="'+articleqry.Title+'"';

        rv.User_ID__c = UserInfo.getUserId();

        if (articleqry.Title.length() > 80)
            rv.name = ('Article: '+articleqry.Title).substring(0, 80);
        else
            rv.name = ('Article: '+articleqry.Title);

        insert rv;

        sObject articletype = Database.query('SELECT Body__c FROM '+arttype+' WHERE PublishStatus = \'online\' and Language = \'en_US\' and id=\''+articleqry.id+'\'');

        Policies_Guideline__kav articlebody1 = new Policies_Guideline__kav();
        FAQ__kav articlebody2 = new FAQ__kav();
        Inet_Announcement__kav articlebody3 = new Inet_Announcement__kav();
        Leave_Administration__kav articlebody4 = new Leave_Administration__kav();
        Newsletter__kav articlebody5 = new Newsletter__kav();
        Procedure__kav articlebody6 = new Procedure__kav();
        Training__kav articlebody7 = new Training__kav();

        ArticleDetails ad;

        if (arttype == 'Policies_Guideline__kav')
        {
            articlebody1 = (Policies_Guideline__kav)articletype;
            ad = new ArticleDetails(articlebody1.Body__c,fa.size());
        }
        else if (arttype == 'FAQ__kav')
        {
            articlebody2 = (FAQ__kav)articletype;
            ad = new ArticleDetails(articlebody2.Body__c,fa.size());
        }
        else if (arttype == 'Inet_Announcement__kav')
        {
            articlebody3 = (Inet_Announcement__kav)articletype;
            ad = new ArticleDetails(articlebody3.Body__c,fa.size());
        }
        else if (arttype == 'Leave_Administration__kav')
        {
            articlebody4 = (Leave_Administration__kav)articletype;
            ad = new ArticleDetails(articlebody4.Body__c,fa.size());
        }
        else if (arttype == 'Newsletter__kav')
        {
            articlebody5 = (Newsletter__kav)articletype;
            ad = new ArticleDetails(articlebody5.Body__c,fa.size());
        }
        else if (arttype == 'Procedure__kav')
        {
            articlebody6 = (Procedure__kav)articletype;
            ad = new ArticleDetails(articlebody6.Body__c,fa.size());
        }
        else if (arttype == 'Training__kav')
        {
            articlebody7 = (Training__kav)articletype;
            ad = new ArticleDetails(articlebody7.Body__c,fa.size());
        }

        return ad;
    }
*/

@RemoteAction
global static List<DispalyData> getCaseDetails(String id) {

        system.debug('Inside HRConnectRemoter->getCaseDetails');
        system.debug('param id: '+id);
        //List <CaseComment> cc = [Select c.CommentBody,c.LastModifiedDate,c.LastModifiedById From CaseComment c where c.ParentId=:id order by c.LastModifiedDate desc];
        //system.debug('Comment count: '+cc.size());
        //return cc;
   List<DispalyData> lst=new List<DispalyData>();
    
    List<CaseComment> cscmnts=HRConnectTrackController.getCaseDetails(id);
    List<Attachment> csAttachmnts=getUploadFileDetails(id);   
    for(CaseComment cs:cscmnts)
    {
     DispalyData disdata=new DispalyData();
        disdata.dataType='case';
        disdata.name=cs.CommentBody;
        disdata.lastmaodifieddate=cs.LastModifiedDate;
        disdata.userid=cs.LastModifiedById;
        
       lst.add(disdata); 
    }
   
    for(Attachment cs:csAttachmnts)
    {
     DispalyData disdata=new DispalyData();
        disdata.dataType='Attachment';
        disdata.name=cs.Name;
        disdata.lastmaodifieddate=cs.LastModifiedDate;
        disdata.userid=cs.LastModifiedById;
        
       lst.add(disdata); 
    }
       
        return lst;
    }
    

global static List<Attachment> getUploadFileDetails(String id) {

        system.debug('Inside HRConnectRemoter->getUploadFileDetails');
        system.debug('param id: '+id);
        List<Attachment> attlst=[SELECT Id,Name,ParentId,LastModifiedById,LastModifiedDate  FROM Attachment where ParentId =:id order by LastModifiedDate desc];
        //List <CaseComment> cc = [Select c.CommentBody,c.LastModifiedDate,c.LastModifiedById From CaseComment c where c.ParentId=:id order by c.LastModifiedDate desc];
        //system.debug('Comment count: '+cc.size());
        //return cc;
        return attlst;
    }

@RemoteAction
global static Case getCaseStatus(String id) {
	
		system.debug('Inside HRConnectRemoter->getCaseStatus');
        system.debug('param id: '+id);
        List<Case> caselst=[SELECT Id,Case_Status__c  FROM Case where Id =:id ];
        return caselst[0];
}    
    
    
    
@RemoteAction
global static void setCaseComment(String id,String cc) {

        system.debug('Inside HRConnectRemoter->setCaseComment');
        system.debug('param id: '+id);
        system.debug('param cc: '+cc);

        /*CaseComment newcomment = new CaseComment();
        newcomment.ParentId = id;
        newcomment.CommentBody = cc;
        insert newcomment;*/
        HRConnectTrackController.setCaseComment(id,cc);
    }
    @RemoteAction
global static void setUploadfile(String id,String fileName,String fileContent) {
    
        system.debug('case id:'+id+'filename-'+fileName);
         Attachment applicationpdf = new Attachment();
          //applicationpdf.Body =blob.valueOf(val);    applicationpdf.Body =EncodingUtil.Base64Decode(val);
         if(fileContent!=null)
         {     
           applicationpdf.Body =EncodingUtil.Base64Decode(fileContent);
           //applicationpdf.Body =blob.valueOf('File Missing');
         }
             else
          applicationpdf.Body =blob.valueOf('File Missing');
          
          applicationpdf.Name = fileName;
       
        applicationpdf.ParentId = id;
        insert applicationpdf; 
           }    
    
@RemoteAction
global static void saveRecentlyviewed(String loc,String name) {

        system.debug('Inside HRConnectRemoter->saveRecentlyviewed');
        system.debug('param loc: '+loc);
        system.debug('param name: '+name);

        Recently_Viewed__c rv = new Recently_Viewed__c();
        if (loc.length() > 255)
            rv.Site_URL__c = loc.substring(0, 255);
        else
            rv.Site_URL__c = loc;

        rv.User_ID__c = UserInfo.getUserId();

        if (name.length() > 80)
            rv.name = name.substring(0, 80);
        else
            rv.name = name;

        Recently_Viewed__c rv2 = recentdupexists(rv.name);
        if (rv2.id != null) {
            rv2.Site_URL__c = rv.Site_URL__c;
            update rv2;
            system.debug('Recently_Viewed__c.id: '+rv2.id);
            }
        else
            insert rv;
    }

@RemoteAction
global static void toggleFavorite(String aid) {

        system.debug('Inside HRConnectRemoter->toggleFavorite');
        system.debug('param aid: '+aid);

        List<Favorite_Articles__c> fa = [select name from Favorite_Articles__c where name=:UserInfo.getUserId() and article_id__c = :aid limit 1];

        if (fa.size() == 0) {
            fa.add( new Favorite_Articles__c(name=UserInfo.getUserId(),article_id__c=aid));
            //fa[0].name = UserInfo.getUserId();
            //fa[0].article_id__c = aid;
            insert fa;
            system.debug('inserted favorite');
            }
        else
            {
            delete fa;
            system.debug('deleted favorite');
            }
    }

/*
@RemoteAction
global static Favorite_Articles__c isFavorite(String aid) {

        system.debug('Inside HRConnectRemoter->isFavorite');
        system.debug('param aid: '+aid);

        Favorite_Articles__c fa = [select name from Favorite_Articles__c where name=:UserInfo.getUserId() and article_id__c = :aid limit 1];
        return fa;
    }
*/

    @RemoteAction
    global static void updateViewstat(String id) {
        system.debug('Inside HRConnectAnswerController->updateViewstat');
        system.debug('param id: '+id);

        KnowledgeArticleVersion articleqry;
        try{
            articleqry = [SELECT Id, Summary, Title, ArticleType
                FROM KnowledgeArticleVersion
                WHERE PublishStatus = 'online'
                and Language = :HRConnectCommon.userlang
                and knowledgearticleid=:id UPDATE VIEWSTAT][0];
        }
        catch(Exception e)
        {
            return;
        }

        Recently_Viewed__c rv = new Recently_Viewed__c();

        /*if (articleqry.Title.length() > 40)       //limit search string, replace with Id based search if possible
            rv.Site_URL__c = '/apex/HRConnectAnswers?ss="'+articleqry.Title.substring(0, 40)+'"';
        else
            rv.Site_URL__c = '/apex/HRConnectAnswers?ss="'+articleqry.Title+'"';*/


        rv.Site_URL__c = '/apex/HRConnectAnswers?id='+id;

        rv.User_ID__c = UserInfo.getUserId();

        if (articleqry.Title.length() > 71)
            rv.name = ('Article: '+articleqry.Title).substring(0, 71);
        else
            rv.name = ('Article: '+articleqry.Title);

        Recently_Viewed__c rv2 = recentdupexists(rv.name);
        if (rv2.id != null) {
            rv2.Site_URL__c = rv.Site_URL__c;
            system.debug('Recently_Viewed__c.id: '+rv2.id);
            update rv2;
            }
        else
            insert rv;
  }

  Static Recently_Viewed__c recentdupexists(String n)
  {
        Recently_Viewed__c rv = new Recently_Viewed__c();

        try {
            rv = [select id from Recently_Viewed__c where User_ID__c = :UserInfo.getUserId()
                    and name = :n limit 1];
            return rv;
        }
        catch (Exception e)
        {
            return rv;
        }
  }

@RemoteAction
global static void saveUserProfile(String lang, String locale, String tz) {

        system.debug('Inside HRConnectRemoter->saveUserProfile');
        //system.debug('param aid: '+aid);

        user u = new user();

        u.id = UserInfo.getUserId();
        u.TimeZoneSidKey = tz;
        u.LocaleSidKey = locale;
        u.LanguageLocaleKey = lang;

        update u;
    }

    @RemoteAction
    global static void saveArticleRating(String aid,String rating) {

        //HRConnectCommon.saveArticleRating(aid, (rating == 'Y' ? HELPFUL_RATING : UNHELPFUL_RATING));
        HRConnectCommon.saveArticleRating(aid, rating);


    }
    
   global  Class DispalyData
     {
         global String dataType;
        global String name;
         global Datetime lastmaodifieddate;
       global string userid;
         
     }      

    @RemoteAction
    global static String getArticleRating(String aid,String userId) {

        return HRConnectCommon.getArticleRating(aid, userId);

    }

    

}