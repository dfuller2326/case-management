@isTest
public with sharing class CMS_GetNextCaseControllerTest {

	private static Group createTestGroup() {

        Group g = new Group(Type='Queue',Name='testForwardToNextCase');
        insert g;
        
        //Make this queue assignable to Cases
        List<QueueSobject> qs = new List<QueueSobject>();
        qs.add(new QueueSobject(QueueId=g.Id,SObjectType='Case'));
        insert qs;
        
        return g;
    }

    private static User createTestUser() {
        User user = new User();
        user.Username = 'test'+System.currentTimeMillis()+'@RetrieveNextUtils.com';
        user.LastName = 'LastTestName';
        user.Email = 'test@biogen.com';
        user.alias = 'testAl';
        user.TimeZoneSidKey = 'America/New_York';
        user.LocaleSidKey = 'es_MX';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.ProfileId = [select id from Profile where Name='System Administrator'].Id;
        user.LanguageLocaleKey = 'es_MX';
        insert user;
        
        return user;
     } 
    
    @isTest(SeeAllData=true)
    static void testForwardToNextCase()
    {    
      	//User u = createTestUser();
      	User u = new User(Id=UserInfo.getUserId());
      	CMS_GetNextCaseController ctrl = new CMS_GetNextCaseController();

       
        Group g = createTestGroup();
        
        GroupMember gm = new GroupMember(UserOrGroupId=u.Id,GroupId=g.Id);
        insert gm;
        
        Test.startTest();  
        System.runAs(u) {
          Case c = new Case(Subject='Test',Contact_Language__c='en_US', OwnerId=g.Id);
          insert c;
          
          ctrl.forwardToNextCase();
          System.assertNotEquals(ctrl.CaseIdAssigned,null);
        }
    }
        
    @isTest(SeeAllData=true)
    static void testNegativeForwardToNextCase()
    {    
      	User u = createTestUser();
               
        Group g = createTestGroup();
        CMS_GetNextCaseController ctrl = new CMS_GetNextCaseController();
        
        Test.startTest();  
        
        //We have to runAs so that we don't get a MIXED_DML_EXCEPTION
        System.runAs(u) {
          
          //Do not insert this user in the queue -- he should not get the case
          Case c = new Case(Subject='Test',Contact_Language__c='en_US', OwnerId=g.Id);
          insert c;
          
          ctrl.forwardToNextCase();
          System.assertEquals(ctrl.CaseIdAssigned,null);

        }
    }       

}