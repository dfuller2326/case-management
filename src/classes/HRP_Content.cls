public with sharing class HRP_Content {
	// Return the list of HRP_ContentWrapper tiles based on the country and userProfie
  public static List<HRP_ContentWrapper> retrieveHomePageTiles(String country, String userProfile){
    List<HRP_Content__c> hrpContentList = retrieveContent(country, userProfile, HRP_Constants.HR_CONTENT_TILE, false);

    // Build the HRP_ContectWrapper based on the HRP_Content
    List<HRP_ContentWrapper> hrpContentWrapperList = buildHRPContentWrapperList( hrpContentList, null);

    return hrpContentWrapperList;
  }

  // Return the list of HRP_ContentWrapper carousel based on the country and userProfie
  public static List<HRP_ContentWrapper> retrieveHomePageCarousel(String country, String userProfile){
    List<HRP_Content__c> hrpContentList = retrieveContent(country, userProfile, HRP_Constants.HR_CONTENT_CAROUSEL, true);

    // Build the HRP_ContectWrapper based on the HRP_Content
    List<HRP_ContentWrapper> hrpContentWrapperList = buildHRPContentWrapperList( hrpContentList, null);

    return hrpContentWrapperList;
  }

  // Return the list of HRP_ContentWrapper benefits tabs based on the country and userProfie
  public static List<HRP_ContentWrapper> retrieveHomePageBenefits(String country, String userProfile){

    // Retrieve HRP Content Carousel Data
    List<HRP_Content__c> hrpContentList = retrieveContent(country, userProfile, HRP_Constants.HR_CONTENT_BENEFIT, false);

    // Retrieve the Article Data associated with the HRP Content Data
    List<SObject> articleList = retrieveArticles(
      buildArticleNumberString(hrpContentList), HRP_Constants.ARTICLE_TYPE_PROCEDURE);

    // Build a Map of Article Number to List<Article> map
    Map<String, List<SObject>> articleNumToSObjMap = buildMapWithKey(articleList, 'ArticleNumber');

    // Build the HRP_ContectWrapper based on the HRP_Content and Article Map
    List<HRP_ContentWrapper> hrpContentWrapperList = buildHRPContentWrapperList( hrpContentList, articleNumToSObjMap);

    return hrpContentWrapperList;
  }

  // Return the list of HRP_ContentWrapper job on boarding based on the country and userProfie
  public static List<HRP_ContentWrapper> retrieveHomePageJobOnboarding(String country, String userProfile){
    List<HRP_Content__c> hrpContentList = retrieveContent(country, userProfile, HRP_Constants.HR_CONTENT_JOB_ONBOARDING, false);

    // Retrieve the Article Data associated with the HRP Content Data
    List<SObject> articleList = retrieveArticles(
      buildArticleNumberString(hrpContentList), HRP_Constants.ARTICLE_TYPE_JOB_ONBOARDING);

    // Build a Map of Article Number to List<Article> map
    Map<String, List<SObject>> articleNumToSObjMap = buildMapWithKey(articleList, 'ArticleNumber');

    // Build the HRP_ContectWrapper based on the HRP_Content and Article Map
    List<HRP_ContentWrapper> hrpContentWrapperList = buildHRPContentWrapperList( hrpContentList, articleNumToSObjMap);

    return hrpContentWrapperList;
  }

  // Retrieve the HRP Content Data for the specified country, userProfie else return the default HRP Content for default counrty and profile
  @TestVisible
  private static List<HRP_Content__c> retrieveContent(String country, String userProfile, String recordType, Boolean includeAttachments){

     List<HRP_Content__c> hrpContentList = retrieveHRPContentData(country, userProfile, recordType, includeAttachments);

    if(hrpContentList.isEmpty()) hrpContentList = retrieveHRPContentData(HRP_Constants.HR_DEFAULT_COUNTRY, userProfile, recordType, includeAttachments);

    if(hrpContentList.isEmpty()) hrpContentList = retrieveHRPContentData(country, HRP_Constants.HR_DEFAULT_PROFILE, recordType, includeAttachments);

    if(hrpContentList.isEmpty()) hrpContentList = retrieveHRPContentData(HRP_Constants.HR_DEFAULT_COUNTRY, HRP_Constants.HR_DEFAULT_PROFILE, recordType, includeAttachments);

    return hrpContentList;
  }

  // Build the soql query and return the List of HRP Content records
  @TestVisible
  private static List<HRP_Content__c> retrieveHRPContentData(String country, String userProfile, String recordType, Boolean includeAttachments){
    String now = DateTime.Now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
    String soql = 'SELECT ';
    for(String fieldName : Schema.getGlobalDescribe().get('HRP_Content__c').getDescribe().fields.getMap().keySet()) {
      soql += fieldName + ', ';
    }
    soql = includeAttachments ? soql + ' (SELECT Id, Name FROM Attachments ORDER BY LastModifiedDate)' : soql.left(soql.lastIndexOf(','));
    soql += ' FROM ' + 'HRP_Content__c' + ' WHERE Countries__c includes (\'' + country + '\')' + ' AND User_Profile__c includes (\'' + userProfile + '\')' + ' AND RecordType.DeveloperName = \'' + recordType + '\'' + ' AND Start_Date__c <= ' + now + ' AND End_Date__c > ' + now + ' ORDER BY Order__c ASC';

    System.debug('soql==='+soql);

    return (List<HRP_Content__c>) Database.query(soql);
  }

  // Build the soql query based on the articletype, user's language and article numbers and return the article records
  @TestVisible
  private static List<SObject> retrieveArticles(String articleNums, String articleType){
    List<SObject> articleList = new List<SObject>();
    if(String.isBlank(articleNums)) return articleList;

    String soql = 'SELECT ';
    for(String fieldName : Schema.getGlobalDescribe().get(articleType).getDescribe().fields.getMap().keySet()) {
      soql += fieldName + ', ';
    }
    soql = soql.left(soql.lastIndexOf(','));
    soql += ' FROM ' + articleType + ' WHERE publishStatus=\'Online\' AND language=\'' + UserInfo.getLanguage() + '\'' + ' AND ArticleNumber IN (' + articleNums + ') ';

    System.debug('soql==='+soql);

    articleList = (List<SObject>) Database.query(soql);
    return articleList;
  }

  // Build a comma separated string of article number based on the HRP Content records
  @TestVisible
  private static String buildArticleNumberString(List<HRP_Content__c> hrpContentList){
    String articleNums = '';
    if(hrpContentList.isEmpty()) return articleNums;
    for(HRP_Content__c hrpc : hrpContentList) {
      if (String.isBlank(hrpc.Article_Numbers__c)) continue;
      for(String articleNum : hrpc.Article_Numbers__c.split(';')){
        articleNum = articleNum.trim();
        articleNums += '\'' + articleNum + '\', ';
      }
    }
    articleNums = String.isNotBlank(articleNums) ?
      articleNums.left(articleNums.lastIndexOf(',')) : '';
    System.debug('ARTICLE NUMS STRING :: ' + articleNums);
    return articleNums;
  }

  // Build a map from the list of SObjects and use the field name as the key to the map
  @TestVisible
  private static Map<String, List<SObject>> buildMapWithKey(List<SObject> sObjList, String fieldName){
    Map<String, List<SObject>> sObjMap = new Map<String, List<SObject>>();
    if(sObjList.isEmpty()) return sObjMap;

    for(SObject sObj : sObjList){
      String key = String.valueOf(sObj.get(fieldName));
      if(String.isBlank(key)) continue;

      if(sObjMap.containsKey(key)) {
        sObjMap.get(key).add(sObj);
      } else {
        sObjMap.put(key, new List<SObject> {sObj});
      }
    }
    return sObjMap;
  }

  // Build a List of HRP_ContentWrapper based on the List of HRP_Content Records and a Map of Article Number to Article Map
  @TestVisible
  private static List<HRP_ContentWrapper> buildHRPContentWrapperList(List<HRP_Content__c> hrpContentList, Map<String, List<SObject>> articleNumToSObjMap){
    List<HRP_ContentWrapper> hrpContentWrapperList = new List<HRP_ContentWrapper>();
    if(hrpContentList.isEmpty()) return hrpContentWrapperList;

    for(HRP_Content__c hrpc : hrpContentList){
      HRP_ContentWrapper hrcWrapper;
      List<SObject> articleList = new List<SObject>();
      if(String.isNotBlank(hrpc.Article_Numbers__c) && articleNumToSObjMap != null && !articleNumToSObjMap.isEmpty()){
        for(String articleNum : hrpc.Article_Numbers__c.split(';')){
          articleNum = articleNum.trim();
          if(articleNumToSObjMap.containsKey(articleNum)){
            articleList.addAll(articleNumToSObjMap.get(articleNum));
          }
        }
      }
      hrpContentWrapperList.add(new HRP_ContentWrapper(articleList, hrpc));
    }
    return hrpContentWrapperList;
  }

}