@isTest
private class BannerWrapperClassTest {

	@isTest static void testConstructor(){
		LMS_Content__c ct = new LMS_Content__c();
		ct.Title__c = 'Test Title';
		ct.Summary__c = 'Test Summary';
		ct.Category__c = 'Test Category';
		ct.External_Media_URL__c = 'https://www.biogen.com';
		INSERT ct;
		BannerWrapperClass cstr = new BannerWrapperClass(5, '1234', ct);

		//Assert Statements
		System.assertEquals(cstr.imageUrlConst, LMSPortal_Constants.DOCUMENT_URL);
		System.assertEquals(cstr.contentRecord, ct);
		System.assertEquals(cstr.contentBannerUrl, cstr.imageUrlConst + '1234');
		System.assertEquals(cstr.contentTitle, ct.Title__c);
		System.assertEquals(cstr.contentSummary, ct.Summary__c);
		System.assertEquals(cstr.count, 5);
	}
}