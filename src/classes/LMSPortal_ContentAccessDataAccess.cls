public with sharing class LMSPortal_ContentAccessDataAccess
{
	public static List<Learning_Content_Access__c> getAccessesToContent(Set<String> learningContentIds)
	{
		List<Learning_Content_Access__c> lstContentAccess = new List<Learning_Content_Access__c>();
		if(learningContentIds != null && !learningContentIds.isEmpty())
		{
			lstContentAccess = [Select Business_Unit__c, Division__c, Learning_Content__c, Learning_Link__c 
								FROM Learning_Content_Access__c WHERE Learning_Content__c IN :learningContentIds];
		}
		return lstContentAccess;
 	}
}