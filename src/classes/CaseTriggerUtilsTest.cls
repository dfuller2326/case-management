@isTest
private class CaseTriggerUtilsTest {

	@isTest static void createPortalNoContactNoOnBehalfOf() {

		 List<Case> generalCases;

		 Profile profileInstance=[select Id from Profile where Name='System Administrator'];

		 User userInstance=new User();
       	 userInstance.FirstName='Test Class';
       	 userInstance.LastName='Biogen - ' ;
       	 userInstance.Email='testclass@biogenidec.com';
       	 userInstance.userName='testclass@biogen.dev';
       	 userInstance.Alias='dumy';
         userInstance.CommunityNickname='dumy';
         userInstance.Employee_ID__c= '1000000i' ;
         userInstance.ProfileId=profileInstance.Id;

       	 userInstance.TimeZoneSidKey = 'America/New_York';
         userInstance.LocaleSidKey = 'en_US';
         userInstance.EmailEncodingKey = 'ISO-8859-1';
         userInstance.LanguageLocaleKey = 'en_US';
         insert userInstance;

		PrepTestDataUtils.initialize();

		System.runAs(userInstance){
			//create contact
			Test.startTest();
			generalCases=PrepTestDataUtils.createBulkCases(true, false, null,null ,10, 'General');
		    Test.stopTest();
	    }

	    Contact contactInstance=[select Id from Contact where Employee_ID__c='1000000i' limit 1];
	    //query test class cases to fetch the updated values
		generalCases=PrepTestDataUtils.queryTestClassCases();
		update generalCases;

		generalCases=PrepTestDataUtils.queryTestClassCases();

		System.debug('=====generalCases==========' + generalCases);

		for(Case caseInstance:generalCases){
			System.assertEquals(caseInstance.ContactId,contactInstance.Id);
			System.assertEquals(caseInstance.On_Behalf_Of2__c,null);
			System.assertEquals(caseInstance.Initial_Case_Creator__c,null);
		}

	}

	@isTest static void createPortalNoOnBehalfOf() {
		PrepTestDataUtils.initialize();

		//create contact
		Contact contactInstance=new Contact(FirstName='Test Class', LastName='Case Contact');
		insert contactInstance;

		List<String> employeeIds=new List<String>();

	    Test.startTest();
		//run the code as the test user just created.
		List<Case> generalCases=PrepTestDataUtils.createBulkCases(true, false, contactInstance.Id,null,10, 'General');
		Test.stopTest();

		//query test class cases to fetch the updated values
		generalCases=PrepTestDataUtils.queryTestClassCases();
		System.debug('===createPortalNoOnBehalfOf in generalCases====' + generalCases);


		for(Case caseInstance:generalCases){
			System.assertEquals(caseInstance.ContactId,contactInstance.Id);
			System.assertEquals(caseInstance.On_Behalf_Of2__c,null);
			System.assertEquals(caseInstance.Initial_Case_Creator__c,null);
		}


	}

	@isTest static void createPortalNoContact() {
		PrepTestDataUtils.initialize();
		//create contact
		Contact OnBehalfOf=new Contact(FirstName='Test Class', LastName='OnBehalfOf Contact');
		insert OnBehalfOf;

		Test.startTest();
		List<Case> generalCases=PrepTestDataUtils.createBulkCases(true, false, null,onBehalfOf,200, 'General');
		Test.stopTest();

		generalCases=PrepTestDataUtils.queryTestClassCases();
		System.debug('===createPortalNoOnBehalfOf in generalCases====' + generalCases);


		for(Case caseInstance:generalCases){
			System.assertEquals(caseInstance.On_Behalf_Of2__c,OnBehalfOf.id);
			System.assertEquals(caseInstance.Initial_Case_Creator__c,null);
			System.assertEquals(caseInstance.ContactId,OnBehalfOf.id);
		}
	}
	@isTest static void createPortalWithContactWithOnBehalfOf() {
		PrepTestDataUtils.initialize();
		//create contact
		Contact contactInstance=new Contact(FirstName='Test Class', Employee_ID__c='123453' , LastName='Case Contact');
		insert contactInstance;

		Contact OnBehalfOf=new Contact(FirstName='Test Class', Employee_ID__c='456743' , LastName='OnBehalfOf Test Class Contact');
		insert OnBehalfOf;

		Test.startTest();
		List<Case> generalCases=PrepTestDataUtils.createBulkCases(true, false, contactInstance.Id,onBehalfOf,10, 'General');
	    Test.stopTest();

	    generalCases=PrepTestDataUtils.queryTestClassCases();
		System.debug('===createPortalNoOnBehalfOf in generalCases====' + generalCases);


		for(Case caseInstance:generalCases){
			System.assertEquals(caseInstance.On_Behalf_Of2__c,OnBehalfOf.id);
			System.assertEquals(caseInstance.Initial_Case_Creator__c,contactInstance.id);
			System.assertEquals(caseInstance.ContactId,OnBehalfOf.id);
		}


	}
	@isTest static void createNonPortalCaseWithContactOnBehalfOf() {
		PrepTestDataUtils.initialize();
		//create contact
		Contact contactInstance=new Contact(FirstName='Test Class', Employee_ID__c='345678' , LastName='Case Contact');
		insert contactInstance;

		Contact OnBehalfOf=new Contact(FirstName='Test Class', Employee_ID__c='123453' , LastName='OnBehalfOf Test Class 123 Contact');
		insert OnBehalfOf;

		Test.startTest();
		List<Case> generalCases=PrepTestDataUtils.createBulkCases(false, false, contactInstance.Id,onBehalfOf,10, 'General');
	    Test.stopTest();

	    generalCases=PrepTestDataUtils.queryTestClassCases();
		System.debug('===createPortalNoOnBehalfOf in generalCases====' + generalCases);


		for(Case caseInstance:generalCases){
			System.assertEquals(caseInstance.On_Behalf_Of2__c,OnBehalfOf.id);
			System.assertEquals(caseInstance.Initial_Case_Creator__c,contactInstance.id);
			System.assertEquals(caseInstance.ContactId,OnBehalfOf.id);
		}
	}

	@isTest static void createNonPortalCaseWithOnBehalfOf() {
		PrepTestDataUtils.initialize();
		//create contact

		Contact OnBehalfOf=new Contact(FirstName='Test Class', Employee_ID__c='123453' , LastName='OnBehalfOf Contact Test Class 456');
		insert OnBehalfOf;

		Test.startTest();
		List<Case> generalCases=PrepTestDataUtils.createBulkCases(false, false, null,onBehalfOf,10, 'General');
	    Test.stopTest();

	    generalCases=PrepTestDataUtils.queryTestClassCases();
		System.debug('===createPortalNoOnBehalfOf in generalCases====' + generalCases);


		for(Case caseInstance:generalCases){
			System.assertEquals(caseInstance.On_Behalf_Of2__c,OnBehalfOf.id);
			System.assertEquals(caseInstance.Initial_Case_Creator__c,null);
			System.assertEquals(caseInstance.ContactId,null);
		}
	}

	@isTest static void createPortalPrivateCase() {
		//variable to store the created case ids.
		List<String> caseIds=new List<String>();
		//create a set that collects the unique members for the cases
		Set<String> uniqueMembers=new Set<String>();
		//create a set with owner id and contact id
		Set<String> expectedMembers=new Set<String>();

		PrepTestDataUtils.initialize();
		//create contact
		Contact contactInstance=new Contact(FirstName='Test Class', Employee_ID__c='123453' , LastName='Case Contact');
		insert contactInstance;

		Contact OnBehalfOf=new Contact(FirstName='Test Class', Employee_ID__c='456745' , LastName='OnBehalfOf Contact Test Class 675');
		insert OnBehalfOf;

		Test.startTest();
		List<Case> generalCases=PrepTestDataUtils.createBulkCases(true, true, contactInstance.Id,onBehalfOf,10, 'General');
	    Test.stopTest();

	    generalCases=PrepTestDataUtils.queryTestClassCases();
		System.debug('===createPortalNoOnBehalfOf in generalCases====' + generalCases);


		for(Case caseInstance:generalCases){
			System.assertEquals(caseInstance.On_Behalf_Of2__c,OnBehalfOf.id);
			System.assertEquals(caseInstance.Initial_Case_Creator__c,contactInstance.id);
			System.assertEquals(caseInstance.ContactId,OnBehalfOf.id);
			caseIds.add(caseInstance.Id);
			expectedMembers.add(caseInstance.ContactId);
			expectedMembers.add(caseInstance.OwnerId);
		}



		//check the case team because it is a private case

		for(CaseTeamMember caseteam:[select ParentId,MemberId from CaseTeamMember where ParentId in :caseIds]){
		     uniqueMembers.add(caseteam.memberId);
		}

		//assert that unique members and expected members are same
		System.assertEquals(true,uniqueMembers.containsAll(expectedMembers));


	}

	@isTest static void createNonPortalPrivateCaseWithContact() {
		//variable to store the created case ids.
		List<String> caseIds=new List<String>();
		//create a set that collects the unique members for the cases
		Set<String> uniqueMembers=new Set<String>();
		//create a set with owner id and contact id
		Set<String> expectedMembers=new Set<String>();

		PrepTestDataUtils.initialize();
		//create contact
		Contact contactInstance=new Contact(FirstName='Test Class', LastName='Case Contact');
		insert contactInstance;

        Test.startTest();
		List<Case> generalCases=PrepTestDataUtils.createBulkCases(false, true, contactInstance.Id,null,10, 'General');
	    Test.stopTest();

	    generalCases=PrepTestDataUtils.queryTestClassCases();
		System.debug('===createPortalNoOnBehalfOf in generalCases====' + generalCases);


		for(Case caseInstance:generalCases){
			System.assertEquals(caseInstance.On_Behalf_Of2__c,null);
			System.assertEquals(caseInstance.Initial_Case_Creator__c,null);
			System.assertEquals(caseInstance.ContactId,contactInstance.Id);
			caseIds.add(caseInstance.Id);
			expectedMembers.add(caseInstance.ContactId);
			expectedMembers.add(caseInstance.OwnerId);
		}



		//check the case team because it is a private case

		for(CaseTeamMember caseteam:[select ParentId,MemberId from CaseTeamMember where ParentId in :caseIds]){
		     uniqueMembers.add(caseteam.memberId);
		}
		system.debug('uniqueMembers::' + uniqueMembers);
		system.debug('expectedMembers::' + expectedMembers);

		//assert that unique members and expected members are same
		System.assertEquals(true,uniqueMembers.containsAll(expectedMembers));



	}

	@isTest static void createExceptionScenario(){
		//Do not populate account or business hours on a case
		Test.startTest();
		Group g1;
		QueuesObject q1;

		System.runAs(new User(Id=UserInfo.getUserId())){
		   g1 = new Group(Name='Test Class Group', type='Queue');
           insert g1;

           q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
           insert q1;
		}

		Case caseInstance=new Case(  Origin='Test Class'
                                    , Status= 'Open'
                                    , Portal_Case__c=true
                                    , ContactId=null
                                    , On_Behalf_Of2__c=null
                                    , Private__c=true
                                    , OwnerId=g1.Id
                                   );
        insert caseInstance;
        Test.stopTest();

	}

	@isTest static void caseUpdateScenario() {
		Test.startTest();
		Group g1, g2;
		QueuesObject q1, q2;

		User u = new User(Id=UserInfo.getUserId());

		System.runAs(u){
		   g1 = new Group(Name='Test Class Group', type='Queue');
           insert g1;

           q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
           insert q1;
		}

		Case caseInstance=new Case(  Origin='Test Class'
                                    , Status= 'Open'
                                    , Portal_Case__c=true
                                    , ContactId=null
                                    , Group__c='HR Connect'
                                    , On_Behalf_Of2__c=null
                                    , Private__c=true
                                    , OwnerId=g1.Id
                                   );
        insert caseInstance;

		System.runAs(u){
		   g2 = new Group(Name='Test Class Group2', type='Queue');
           insert g2;

           q2 = new QueueSObject(QueueID = g2.id, SobjectType = 'Case');
           insert q2;
		}

		caseInstance.Group__c = 'HR Connect - Americas'; 
		caseInstance.OwnerId = g2.Id;

		update caseInstance;

		caseInstance.OwnerId = UserInfo.getUserId();

		update caseInstance;

        Test.stopTest();		
	}


}