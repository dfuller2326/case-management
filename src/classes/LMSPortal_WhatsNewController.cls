public with sharing class LMSPortal_WhatsNewController {

	public Integer whatsNewCount {get;set;}
	public BannerWrapperClass activeItem {get;set;}
	public List<BannerWrapperClass> learningResourcesWrapperForWhatsNew {get;set;}
	public String selectedDateIs {
		get; 
		set {
			if(value != null)
			{
				selectedDateIs = value;
				
			}
		}
	}
	public String businessUnitVal {
		get; 
		set {
			if(value != null)
			{
				businessUnitVal = value;
				
			}
		}
	}
	public String divisionVal {
		get; 
		set {
			if(value != null)
			{
				divisionVal = value;
				
			}
		}
	}

	public LMSPortal_WhatsNewController() {
		whatsNewCount = 0;
		activeItem = new BannerWrapperClass();
		learningResourcesWrapperForWhatsNew = new List<BannerWrapperClass>();
		getWrapperForWhatsNew();
	}

	public void getWrapperForWhatsNew(){
		Map<Id, LMS_Content__c> lmsMap = new Map<Id, LMS_Content__c>();
		Set<Id> documentIdSet = new Set<Id>();
		List<LMS_Content__c> lmsContentList = LMSPortal_ContentDataAccess.getLMSContent(true, selectedDateIs, businessUnitVal, divisionVal);

		//Create Map of Banner Document Id to LMS Content Record
		for(LMS_Content__c content : lmsContentList){
			documentIdSet.add(content.Banner_Document_Id__c);
			if(!lmsMap.containsKey(content.Banner_Document_Id__c)){
				lmsMap.put(content.Banner_Document_Id__c, content);
			}
		}

		//Use Map to get Document Record
		List<Document> docList = [SELECT Id, Body, ContentType, Url FROM Document WHERE Id IN :documentIdSet];
		for(Document d : docList){
			whatsNewCount++;
			if(whatsNewCount == 1){
				activeItem = new BannerWrapperClass(whatsNewCount, d.Id, lmsMap.get(d.Id));
			}
			else{
				learningResourcesWrapperForWhatsNew.add(new BannerWrapperClass(whatsNewCount, d.Id, lmsMap.get(d.Id)));
			}
		}
	}
}