public with sharing class BiogenConstants {
     /**
      * Profile Names
      * @type {Profile Names}
      */
     public final static string EMPLOYEE_PROFILE ='Employee';
     public final static string EXECUTIVE_PROFILE ='Exec';
     public final static string MANAGER_PROFILE ='Manager';
     public final static string HR4HR_PROFILE ='HR for HR';
     
     public final static string NEWLY_HIRED ='New Hire ';
     public final static string HUMAN_RESOURCES='Human Resources'; 
     
     /**
      * HR portal properties
      * @type {Custom Settings}
      */
     public final static string INTERESTED_PROFILE='INTERESTED_PROFILE';
     public final static string NEW_HIRE_CONDITION='NEW_HIRE_CONDITION';
     public final static string LOWEST_EXECUTIVE_GRADE='LOWEST_EXECUTIVE_GRADE';
     public final static string BIOGEN_BUSINESS_COUNTRY='BUSINESS_COUNTRY';
     public final static string PROFILE_ENTITY_TYPE='Profile';
     public final static string PERMSET_ENTITY_TYPE='PermissionSet';
          
     public final static string BIOGEN_ACCOUNT='Biogen';
     public final static string DEFAULT_COUNTRY='United States';
     
      public final static string INTEGRATION_PROFILE='INTEGRATION_PROFILE';
     
     public final static string INTEGRATION_PROFILE_COMMUNITY='Workday Integ Profile';
     public final static string INTEGRATION_PROFILE_PLATFORM='SFWorkday Integ Profile';
     
     public final static string PERMSET_SUFFIX='Article_Visibility';
     public final static string BIOGEN_PUBLIC_GROUP='All_Biogen_Users';
     public final static string BIOGEN_HR4HR_PUBLIC_GROUP='HR_for_HR_Users';
     public final static string BIOGEN_GLOBAL='Global';
     public final static string ERROR_EMAIL='ERROR_EMAIL_ADDRESS';
     
     public final static string JOB_TITLE_FOR_EXEC='JOB_TITLE_FOR_EXEC';
     public final static string NON_CASE_MGMT_USER_COST_CENTER = 'NON_CASE_MGMT_USER_COST_CENTER';
     public final static string ARTICLE_AUTO_PUBLISH_ARCHIVE='ARTICLE_AUTO_PUBLISH_ARCHIVE';
 
}