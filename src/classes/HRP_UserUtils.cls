public with sharing class HRP_UserUtils {

	public HRP_UserUtils() {
		
	}

	/**
	* @param: none
	* @return: single User object record
	* @description: returns any needed information for the currently logged in user
	*/
	public static User getCurrentUser() {
		return [SELECT Id, Country, LanguageLocaleKey, Profile_Name__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
	}
}