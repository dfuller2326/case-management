public with sharing class LMSPortal_LearningCoursesController {

	public Boolean showWhatsNew {get;set;}
	public Boolean showAllVideos {get;set;}
	public Boolean showSearchResult {get;set;}
	public Boolean showLearningCoursesSelected {get;set;}

	public LMSPortal_LearningCoursesController() {
		showWhatsNew = false;
		showAllVideos = false;
		showSearchResult = false;
		showLearningCoursesSelected = false;
	}

	public void showCourseComponent(String cmpId){

	}
}