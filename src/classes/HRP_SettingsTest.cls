@isTest(SeeAllData=false)
private class HRP_SettingsTest {
  private static testMethod void Test_getSettingByType_SingleRecords() {
    List<HRP_Settings__c> hrpSettings = HRP_TestDataFactory.buildHRPSettings();
    insert hrpSettings;

    Test.startTest();
      Map<String, List<HRP_Settings__c>> primarySearchSettingsMap =
        HRP_Settings.getSettingsByType('PrimarySearch', 'Text1__c');

      Map<String, List<HRP_Settings__c>> secondarySearchSettingsMap =
        HRP_Settings.getSettingsByType('SecondarySearch', null);
    Test.stopTest();

    System.assert(primarySearchSettingsMap.containsKey('Procedure__kav'));
    System.assertEquals(1, primarySearchSettingsMap.get('Procedure__kav').size());
    System.assert(secondarySearchSettingsMap.containsKey('SecondarySearch1'));
    System.assertEquals(1, secondarySearchSettingsMap.get('SecondarySearch1').size());
  }

  private static testMethod void Test_getSettingByType_MultipleRecords() {
    List<HRP_Settings__c> hrpSettings = HRP_TestDataFactory.buildHRPSettings();
    hrpSettings.add(new HRP_Settings__c( Name = 'PrimarySearch2'
        , Type__c = 'PrimarySearch'
        , Text1__c = 'Procedure__kav'));
    insert hrpSettings;

    Test.startTest();
      Map<String, List<HRP_Settings__c>> primarySearchSettingsMap =
        HRP_Settings.getSettingsByType('PrimarySearch', 'Text1__c');

      Map<String, List<HRP_Settings__c>> secondarySearchSettingsMap =
        HRP_Settings.getSettingsByType('SecondarySearch', null);
    Test.stopTest();

    System.assert(primarySearchSettingsMap.containsKey('Procedure__kav'));
    System.assertEquals(2, primarySearchSettingsMap.get('Procedure__kav').size());
    System.assert(secondarySearchSettingsMap.containsKey('SecondarySearch1'));
    System.assertEquals(1, secondarySearchSettingsMap.get('SecondarySearch1').size());
  }
}