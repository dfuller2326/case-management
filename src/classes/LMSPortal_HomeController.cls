/**
* @description controller class for Home page
*
*/
global with sharing class LMSPortal_HomeController {
    public User currentUser {get; set;}
    public String userFunctionalArea {get; set;}
    
    public List<LMSPortal_Util.UrlEncodedCategories> catList {
        get {
            return LMSPortal_Util.getCategoryPicklistValues();
        } set;
    }
    public String askHRLink {
        get {
            return LMSPortal_Constants.ASKHR_LINK;
        } set;
    }

    // query string
    private static String queryStr = 'SELECT Id, Description__c, Summary__c, Title__c, Content_Type__c, Banner_Document_Id__c, Thumbnail_Document_Id__c, Category__c, Hot_Topic__c, Duration__c, Calculated_Duration__c, Duration_Unit__c FROM LMS_Content__c';

    //Constructor
    public LMSPortal_HomeController() {
        try{
            currentUser = [SELECT Id, Division__c FROM User WHERE Id = :UserInfo.getUserId()];
            System.debug('User Division :: ' + currentUser.Division__c);
            // Create Pattern Object
            // Pattern fAreaPattern = Pattern.compile('([^\\s]+)');

            // Create Matcher Object
            if(currentUser != null && String.isNotBlank(currentUser.Division__c)) {
                List<BU_Division__c> buDivList = [Select Division__c, Division_Name__c 
                                                    From BU_Division__c 
                                                    Where Division__c = :currentUser.Division__c];
                if(buDivList != null && !buDivList.isEmpty()) {
                    userFunctionalArea = (String.isNotBlank(buDivList[0].Division_Name__c)) ? buDivList[0].Division_Name__c : currentUser.Division__c;
                } 
            }
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR, 'ERROR RETRIEVING CURRENT USER :: ' + ex.getMessage());
        }
    }
    
    /**
    * @description remote action method to get the Learning Content and Learning Links for Home page
    * @param String: previewParamJson
    * @return ContentWrapper
    */
    @RemoteAction
    global static String getHomeData(String previewParamJson){
        HomeResponse respObj = new HomeResponse();
        try {
            // get preview params
            PreviewWrapper previewParam = getPreviewParam(previewParamJson);                               
            // get categorycourse info
            respObj.catCourseObj = getCategoryCourses(previewParam, queryStr);
            //get my learning list
            respObj.myLearningObj = getMyLearning(previewParamJson);
            //get helpful links
            respObj.helpfulLinksObj = getHelpfulLinks(previewParamJson);
            //get banner images
            respObj.whatsNewObj = getWhatsNew(previewParam, queryStr);

        } catch(Exception ex) {
            respObj.error = new LMSPortal_ErrorInfo(System.Label.Home_Page_Error_Heading, ex.getMessage());
        }
        
        return JSON.serialize(respObj);
    }
    
    /**
    * @description method to get the banner images of learning contents 
    * @param PreviewWrapper: previewParam
    * @param String: qStr
    * @return List<LMS_Content__c>: lmsList
    */
    private static List<LMS_Content__c> getWhatsNew(PreviewWrapper previewParam, String qStr) {
        List<LMS_Content__c> lmsList = new List<LMS_Content__c>();
        if(previewParam != null) {
            String strQuery = qStr + ' WHERE New_Content__c = true';
            Date effectiveDate = normalizeDate(previewParam.effectiveDate);
            Set<Id> learningContentIds = new Set<Id>();
            if(String.isNotBlank(previewParam.businessUnit) && String.isNotBlank(previewParam.division) && String.isNotBlank(previewParam.location)) {
                List<String> locList = new List<String>();
                locList.add(previewParam.location);
                for(Learning_Content_Access__c lca: [SELECT Learning_Content__c FROM Learning_Content_Access__c 
                                                     WHERE Business_Unit__c = :previewParam.businessUnit 
                                                     AND Division__c = :previewParam.division
                                                     AND Location__c INCLUDES (:locList[0]) ]) {
                    if(String.isNotBlank(lca.Learning_Content__c)) {
                        learningContentIds.add(lca.Learning_Content__c);
                    }
                }
            }

            if(!learningContentIds.isEmpty() && effectiveDate != null) {
                strQuery += ' AND Id IN : learningContentIds AND Start_Date__c <= :effectiveDate and End_Date__c >= :effectiveDate';
                
                strQuery += ' ORDER BY LastModifiedDate DESC LIMIT 6';

                lmsList = Database.query(strQuery);
            }
        } 

        return lmsList;
    }
    
    /**
    * @description method to get the list of courses associated to category 
    * @param PreviewWrapper: previewParam
    * @param String: qStr
    * @return List<LMSPortal_CategoryCourse>: catCourseWrapList
    */
    private static List<LMSPortal_CategoryCourse> getCategoryCourses(PreviewWrapper previewParam, String qStr){
        List<LMSPortal_CategoryCourse> catCourseWrapList = new List<LMSPortal_CategoryCourse>();
        List<LMS_Content__c> courseList;

        String strQuery = qStr + ' WHERE';

        if(previewParam != null) {
            System.debug('PREVIEW PARAM NOT NULL');
            Date effectiveDate = normalizeDate(previewParam.effectiveDate);
            System.debug('EFFECTIVE DATE :: ' + effectiveDate);
            Set<Id> learningContentIds = new Set<Id>();
            if(String.isNotBlank(previewParam.businessUnit) && String.isNotBlank(previewParam.division) && String.isNotBlank(previewParam.location)) {
                List<String> locList = new List<String>();
                locList.add(previewParam.location);
                for(Learning_Content_Access__c lca: [SELECT Learning_Content__c FROM Learning_Content_Access__c 
                                                     WHERE Business_Unit__c = :previewParam.businessUnit 
                                                     AND Division__c = :previewParam.division
                                                     AND Location__c INCLUDES (:locList[0]) ]) {
                    if(String.isNotBlank(lca.Learning_Content__c)) {
                        learningContentIds.add(lca.Learning_Content__c);
                    }
                }
            }
            system.debug('LEARNING CONTENT IDS :: ' + learningContentIds);

            if(!learningContentIds.isEmpty() && effectiveDate != null) {
                strQuery += ' Id IN : learningContentIds AND Start_Date__c <= :effectiveDate and End_Date__c >= :effectiveDate AND ';
                    
                List<String> categoryList = new List<String>();
                Schema.DescribeFieldResult fieldResult = LMS_Content__c.Category__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for(Schema.PicklistEntry f : ple){
                    categoryList.add(f.getValue());
                }
                system.debug('CATEGORY LIST :: ' + categoryList);
                
                //Popular course list
                courseList = Database.query(strQuery + ' Hot_Topic__c = true ORDER BY LastModifiedDate DESC');
                if(!courseList.isEmpty()){
                    // Calculate length of the title and truncate if greater than length specified in Title Length Constant
                    for(LMS_Content__c lms: courseList) {
                        if(String.isNotBlank(lms.Title__c) && lms.Title__c != null) {
                            if(lms.Title__c.length() > LMSPortal_Constants.LMS_COURSE_TITLE_LENGTH) {
                                String subStr = lms.Title__c.substring(0, lms.Title__c.length() - (lms.Title__c.length() - (LMSPortal_Constants.LMS_COURSE_TITLE_LENGTH)));
                                String newTitle = subStr.replaceAll('...$', '...');
                                lms.Title__c = newTitle;
                            }
                        }
                    }
                    catCourseWrapList.add(new LMSPortal_CategoryCourse(LMSPortal_Constants.POPULAR_STR, courseList));
                }
                
                //New Releases course list
                Date newReldate = effectiveDate.addDays(-30);
                courseList = Database.query(strQuery + ' Start_date__c >= :newReldate ORDER BY LastModifiedDate DESC');
                if(!courseList.isEmpty()){
                    // Calculate length of the title and truncate if greater than length specified in Title Length Constant
                    for(LMS_Content__c lms: courseList) {
                        if(String.isNotBlank(lms.Title__c) && lms.Title__c != null) {
                            if(lms.Title__c.length() > LMSPortal_Constants.LMS_COURSE_TITLE_LENGTH) {
                                String subStr = lms.Title__c.substring(0, lms.Title__c.length() - (lms.Title__c.length() - (LMSPortal_Constants.LMS_COURSE_TITLE_LENGTH)));
                                String newTitle = subStr.replaceAll('...$', '...');
                                lms.Title__c = newTitle;
                            }
                        }
                    }
                    catCourseWrapList.add(new LMSPortal_CategoryCourse(LMSPortal_Constants.NEW_RELEASES_STR, courseList));
                }
                
                //Quick Learning course list
                courseList = Database.query(strQuery + ' Calculated_Duration__c < 10 ORDER BY LastModifiedDate DESC');
                if(!courseList.isEmpty()){
                    // Calculate length of the title and truncate if greater than length specified in Title Length Constant
                    for(LMS_Content__c lms: courseList) {
                        if(String.isNotBlank(lms.Title__c) && lms.Title__c != null) {
                            if(lms.Title__c.length() > LMSPortal_Constants.LMS_COURSE_TITLE_LENGTH) {
                                String subStr = lms.Title__c.substring(0, lms.Title__c.length() - (lms.Title__c.length() - (LMSPortal_Constants.LMS_COURSE_TITLE_LENGTH)));
                                String newTitle = subStr.replaceAll('...$', '...');
                                lms.Title__c = newTitle;
                            }
                        }
                    }
                    catCourseWrapList.add(new LMSPortal_CategoryCourse(LMSPortal_Constants.QUICK_LEARNING_STR, courseList));
                }
                        
            }
        }               
        return catCourseWrapList;       
    }
    
    /**
    * @description method to get the list of My Learning Links
    * @return List<LMSPortal_Links>: linksList
    */
    private static List<LMSPortal_Links> getMyLearning( String previewParamJson){//Add logic for getting helpful links based on assigned BU and DIV and User BU and DIv
        List<LMSPortal_Links> linksList = new List<LMSPortal_Links>();
        String blankstr = '';
        if(Test.isRunningTest()) {
            blankstr = previewParamJson;
        }
        PreviewWrapper pWrapper = getPreviewParam(blankstr);
        System.debug('PREVIEW WRAPPER OUTPUT :: ' + pWrapper);

        if(String.isNotBlank(pWrapper.businessUnit) && String.isNotBlank(pWrapper.division) && String.isNotBlank(pWrapper.location)) {

            //Query Learning Content Access Object with BU and DIV from Preview Wrapper
            Set<Id> learningLinkIds = new Set<Id>();
            List<String> locList = new List<String>();
            locList.add(pWrapper.location);
            for(Learning_Content_Access__c lca: [SELECT Learning_Link__c FROM Learning_Content_Access__c 
                                                 WHERE Business_Unit__c = :pWrapper.businessUnit 
                                                 AND Division__c = :pWrapper.division 
                                                 AND Location__c INCLUDES (:locList[0]) limit 5]) {
                if(String.isNotBlank(lca.Learning_Link__c)) {
                    learningLinkIds.add(lca.Learning_Link__c);
                }
            }

            if(!learningLinkIds.isEmpty()) {
                List<LMS_Link__c> tempList = [SELECT External_Media_URL__c,Label__c,Status__c,Target_Window__c,Type__c FROM LMS_Link__c WHERE Type__c =: LMSPortal_Constants.LMS_LINK_TYPE_LEARNING AND Status__c =: LMSPortal_Constants.LMS_LINK_STATUS_ACTV AND Id IN :learningLinkIds ORDER By Rank__c];
                for(LMS_Link__c link : tempList){
                    linksList.add(new LMSPortal_Links(link.External_Media_URL__c, link.Target_Window__c, link.Label__c));
                }
            }
        }

        return linksList;
    }
    
    /**
    * @description method to get the list of Helpful Links
    * @return List<LMSPortal_Links>: linksList
    */
    private static List<LMSPortal_Links> getHelpfulLinks(String previewParamJson){
        //Add logic for getting helpful links based on assigned BU and DIV and User BU and DIv
        List<LMSPortal_Links> linksList = new List<LMSPortal_Links>();

        String blankstr = '';
        if(Test.isRunningTest()) {
            blankstr = previewParamJson;
        }
        PreviewWrapper pWrapper = getPreviewParam(blankstr);
        System.debug('PREVIEW WRAPPER OUTPUT :: ' + pWrapper);

        if(String.isNotBlank(pWrapper.businessUnit) && String.isNotBlank(pWrapper.division) && String.isNotBlank(pWrapper.location)) {

            //Query Learning Content Access Object with BU and DIV from Preview Wrapper
            Set<Id> learningLinkIds = new Set<Id>();
            List<String> locList = new List<String>();
            locList.add(pWrapper.location);
            for(Learning_Content_Access__c lca: [SELECT Learning_Link__c FROM Learning_Content_Access__c 
                                                 WHERE Business_Unit__c = :pWrapper.businessUnit 
                                                 AND Division__c = :pWrapper.division
                                                 AND Location__c INCLUDES (:locList[0]) ]) {
                if(String.isNotBlank(lca.Learning_Link__c)) {
                    learningLinkIds.add(lca.Learning_Link__c);
                }
            }

            if(!learningLinkIds.isEmpty()) {
                List<LMS_Link__c> tempList = [SELECT External_Media_URL__c,Label__c,Status__c,Target_Window__c,Type__c FROM LMS_Link__c WHERE Type__c =: LMSPortal_Constants.LMS_LINK_TYPE_QUICKLNK AND Status__c =: LMSPortal_Constants.LMS_LINK_STATUS_ACTV AND Id IN :learningLinkIds ORDER By Rank__c limit 6];
                for(LMS_Link__c link : tempList){
                    linksList.add(new LMSPortal_Links(link.External_Media_URL__c, link.Target_Window__c, link.Label__c));
                }
            }
        }

        return linksList;
    }

    /**
    * @description method to get the preview options 
    * @param String: previewParamJson
    * @return PreviewWrapper: previewParams
    */
    private static PreviewWrapper getPreviewParam(String previewParamJson) {
        PreviewWrapper previewParams = null;
        
        if(String.isNotBlank(previewParamJson)) {
            previewParams = (PreviewWrapper) JSON.deserialize(previewParamJson, PreviewWrapper.class);
        } else {
            User usr = [Select Id, Business_Unit__c, Division__c, Location__c From User Where Id = :UserInfo.getUserId()];
            if(usr != null) {
                previewParams = new PreviewWrapper(DateTime.now().getTime(), usr.Business_Unit__c, usr.Division__c, usr.Location__c);
            }
        }

        return previewParams;
    }

    /**
    * @description method to convert the Javascript date string to Salesforce Datetime
    * @param String: Javascript date string
    * @return Datetime: value
    */
    private static Date normalizeDate(Long dateInMilliseconds) {
        Datetime dt = DateTime.newInstance(dateInMilliseconds);
        return dt.date();
    }
    
    // Wrapper class for Home Response
    private class HomeResponse {
        public LMSPortal_ErrorInfo error {get; set;}
        public List<LMSPortal_CategoryCourse> catCourseObj {get;set;}
        public List<LMSPortal_Links> myLearningObj {get;set;}
        public List<LMSPortal_Links> helpfulLinksObj {get;set;}
        public List<LMS_Content__c> whatsNewObj {get;set;}
        public Integer coreCount {get; set;}
        public Integer electiveCount {get; set;}
        public Integer functionalCount {get; set;}
        public Integer hotTopicsCount {get; set;}
        public HomeResponse() {
            this.coreCount = 0;
            this.electiveCount = 0;
            this.functionalCount = 0;
            this.hotTopicsCount = 0;
        }
    }
    
    // Wrapper class for Category Course
    public class LMSPortal_CategoryCourse implements Comparable {
        public String categoryName {get;set;}
        public List<LMS_Content__c> courseList {get;set;}
        
        public LMSPortal_CategoryCourse(String categoryName, List<LMS_Content__c> courseList){
            this.categoryName = (String.isNotBlank(categoryName)) ? categoryName : '';
            this.courseList = courseList;
        }
        
        // Compare objects based on category name
        public Integer compareTo(Object compareTo) {
            // Cast argument to ContentFilterWrapper
            LMSPortal_CategoryCourse compareToObj = (LMSPortal_CategoryCourse)compareTo;
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (this.categoryName > compareToObj.categoryName) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (this.categoryName < compareToObj.categoryName) {
                // Set return value to a negative value.
                returnValue = -1;
            }
            
            return returnValue;       
        }
    }
    
    // Wrapper class for Links
    public class LMSPortal_Links{
        public String externalMediaUrl {get;set;}
        public String targetWindow {get;set;}
        public String label {get;set;}
        
        public LMSPortal_Links(String externalMediaUrl, String targetWindow, String label){
            this.externalMediaUrl = externalMediaUrl;
            this.targetWindow = targetWindow;
            this.label = label;
        }
    }

    // Wrapper class for Error Info
    public class LMSPortal_ErrorInfo {
        String requestedPage;
        String errMsg;

        public LMSPortal_ErrorInfo(String requestedPage, String errMsg) {
            this.requestedPage = requestedPage;
            this.errMsg = System.Label.Learning_Content_Detail_Error_Msg + ' ' + '<br>' + errMsg;
        }
    }    

    // Wrapper class for Preview Options
    private class PreviewWrapper {
        public Long effectiveDate {get; set;}
        public String businessUnit {get; set;}
        public String division {get; set;} 
        public String location {get; set;}
        public PreviewWrapper(Long dateInMilliseconds, String businessUnit, String division, String location) {
            this.effectiveDate = dateInMilliseconds;
            this.businessUnit = String.isNotBlank(businessUnit) ? businessUnit : '';
            this.division = String.isNotBlank(division) ? division : '';
            this.location = String.isNotBlank(location) ? location : '';
        }
    }
}