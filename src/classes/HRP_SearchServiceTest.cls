@isTest(SeeAllData=false)
private class HRP_SearchServiceTest {
  private static List<Procedure__kav> procArticles = new List<Procedure__kav>();
  private static List<Id> procArticleIds = new List<Id>();
  private static List<FAQ__kav> faqArticles = new List<FAQ__kav>();
  private static List<Id> faqArticleIds = new List<Id>();
  private static List<HRP_Settings__c> hrpSettings = new List<HRP_Settings__c>();

  private static void setupProcedureArticles(){
    for(Integer i = 0; i < 5; i++){
      Procedure__kav article = HRP_TestDataFactory.buildProcedureArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      procArticles.add(article);
    }
    insert procArticles;

    procArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId FROM Procedure__kav WHERE Language = 'en_US'
      AND PublishStatus = 'Draft'];

    for(Procedure__kav article : procArticles){
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }

    List<Procedure__DataCategorySelection> dcsList = new List<Procedure__DataCategorySelection>();

    for(Integer i = 0; i < 5; i++){
      Procedure__DataCategorySelection dcs = HRP_TestDataFactory.buildDataCategorySelectionForProcedure();
      dcs.ParentId = procArticles[i].MasterVersionId;
      dcs.DataCategoryName = i > 0 && i < 3 ? 'My_Team' : 'My_Actions';
      dcsList.add(dcs);
      procArticleIds.add(procArticles[i].Id);
    }
    insert dcsList;
  }

  private static void setupFAQArticles(){
    for(Integer i = 0; i < 5; i++){
      FAQ__kav article = HRP_TestDataFactory.buildFAQArticle();
      article.title += ' ' + i;
      article.UrlName += i;
      faqArticles.add(article);
    }
    insert faqArticles;

    faqArticles = [SELECT Id, LastPublishedDate, MasterVersionId, Language,
    PublishStatus, KnowledgeArticleId FROM FAQ__kav WHERE Language = 'en_US'
      AND PublishStatus = 'Draft'];

    for(FAQ__kav article : faqArticles){
      KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
    }

    List<FAQ__DataCategorySelection> dcsList = new List<FAQ__DataCategorySelection>();

    for(Integer i = 0; i < 5; i++){
      FAQ__DataCategorySelection dcs = HRP_TestDataFactory.buildDataCategorySelectionForFAQ();
      dcs.ParentId = faqArticles[i].MasterVersionId;
      dcs.DataCategoryName = i > 0 && i < 3 ? 'My_Team' : 'My_Actions';
      dcsList.add(dcs);
      faqArticleIds.add(faqArticles[i].Id);
    }
    insert dcsList;
  }

  private static void setupHRPSettings(){
    hrpSettings = HRP_TestDataFactory.buildHRPSettings();
    insert hrpSettings;
  }


  private static testMethod void searchPrimaryArticles_Positive_QueryAll() {

    setupHRPSettings();

    setupProcedureArticles();

    List<SObject> resultSet;
    Test.startTest();
    Test.setFixedSearchResults(procArticleIds);
      resultSet = HRP_SearchService.searchPrimaryArticles(null, 'en_US', null);
    Test.stopTest();

    System.assertEquals(5, resultSet.size());
  }

  private static testMethod void searchPrimaryArticles_Positive_QueryLimit() {

    setupHRPSettings();

    setupProcedureArticles();

    List<SObject> resultSet;
    Test.startTest();
    Test.setFixedSearchResults(procArticleIds);
      resultSet = HRP_SearchService.searchPrimaryArticles('Procedure', 'en_US', 3);
    Test.stopTest();

    System.assertEquals(3, resultSet.size());
  }

  private static testMethod void searchSecondaryArticles_Positive() {

    setupHRPSettings();

    setupFAQArticles();

    List<SObject> resultSet;
    Test.startTest();
    Test.setFixedSearchResults(faqArticleIds);
      resultSet = HRP_SearchService.searchSecondaryArticles('Procedure', 'en_US');
    Test.stopTest();

    System.assertEquals(5, resultSet.size());
  }
}