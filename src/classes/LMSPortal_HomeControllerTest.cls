@isTest
public with sharing class LMSPortal_HomeControllerTest {
    static testMethod void myUnitTest() {
        LMSPortal_HomeController controller = new LMSPortal_HomeController();
        Datetime dt = Datetime.now();
        Long l = dt.getTime();
        PreviewWrapper preW = new PreviewWrapper(l, '000 MA Talent Acquisition', '000 MA Talent Acquisition', 'Test Campus');
        String JSONString = JSON.serialize(preW);
        
        setUp();
        test.StartTest();
        String str = LMSPortal_HomeController.getHomeData(null);
        system.assertNotEquals(str, null);
        String str1 = LMSPortal_HomeController.getHomeData(JSONString);
        system.assertNotEquals(str1, null);
        test.stopTest();
    }
    
    static void setUp() 
    {
        List<LMS_Content__c> lmsList = createLMSContentData(8);
        List<Learning_Content_Access__c> lcList = createLMSLCAccessData(lmsList);
        List<LMS_Link__c> llLink = createLinks(3);
        List<Learning_Content_Access__c> lcLinkList = createLMSLCAccessDataLinks(llLink);
    }
    
    public static List<LMS_Link__c> createLinks(Integer recordCount) {
        
        List<LMS_Link__c> tmpList = new List<LMS_Link__c>();
        if(recordCount == null)
        {
            recordCount = 1;
        }
        for(Integer i=1; i <= recordCount; i++)
        {
            LMS_Link__c ll = new LMS_Link__c();
            ll.Target_Window__c = 'New Window';
            ll.Status__c = 'Active';
            ll.Rank__c = i;
            ll.Label__c = 'Test' + i;
            ll.External_Media_URL__c = 'https://www.biogen.com' + i;
            if(i < 2){
                ll.Type__c = 'My Learning';
            }else{
                ll.Type__c = 'Quick Link';
            }
            tmpList.add(ll);
        }
        insert tmpList;
        return tmpList;
    }
    
    public static List<Learning_Content_Access__c> createLMSLCAccessData(List<LMS_Content__c> lmsList) {
        
        List<Learning_Content_Access__c> tmpList = new List<Learning_Content_Access__c>();
        for(LMS_Content__c lms : lmsList)
        {
            Learning_Content_Access__c lca = new Learning_Content_Access__c();
            lca.Business_Unit__c = '000 MA Talent Acquisition';
            lca.Division__c = '000 MA Talent Acquisition';
            lca.Learning_Content__c = lms.Id;
            lca.Location__c = 'Test Campus';
            tmpList.add(lca);
        }
        insert tmpList;
        return tmpList;
    }

    public static List<Learning_Content_Access__c> createLMSLCAccessDataLinks(List<LMS_Link__c> lmsList) {
        
        List<Learning_Content_Access__c> tmpList = new List<Learning_Content_Access__c>();
        for(LMS_Link__c lms : lmsList)
        {
            Learning_Content_Access__c lca = new Learning_Content_Access__c();
            lca.Business_Unit__c = '000 MA Talent Acquisition';
            lca.Division__c = '000 MA Talent Acquisition';
            lca.Learning_Link__c = lms.Id;
            lca.Location__c = 'Test Campus';
            tmpList.add(lca);
        }
        insert tmpList;
        return tmpList;
    }
            
    public static List<LMS_Content__c> createLMSContentData(Integer recordCount) {
        
        List<LMS_Content__c> tmpList = new List<LMS_Content__c>();
        if(recordCount == null)
        {
            recordCount = 1;
        }
        for(Integer i=1; i <= recordCount; i++)
        {
            LMS_Content__c lms = new LMS_Content__c();
            lms.Description__c = 'Test Description' + i;
            lms.New_Content__c = false;
            lms.Miscellaneous__c = 'Test Miscellaneous' + i;
            lms.Thumbnail_Document_Id__c = '1234' + i;
            lms.Banner_Document_Id__c = '5678' + i;
            lms.Start_Date__c = Date.valueOf(System.now());
            lms.End_Date__c = Date.valueOf(System.now() + 5);
            lms.External_Media_URL__c = 'https://www.biogen.com';
            lms.Duration__c = 10;
            if(i < 6){
                lms.Hot_Topic__c = true;
            }else{
                lms.Hot_Topic__c = false;
            }
            lms.Title__c = 'Test Title' + i;
            if(i < 5){
                lms.Category__c = 'Biogen Core Curriculum';
            }else{
                lms.Category__c = 'Functional Learning';
            }
            tmpList.add(lms);
        }
        insert tmpList;
        return tmpList;
    }
    
    private class PreviewWrapper {
        public Long effectiveDate {get; set;}
        public String businessUnit {get; set;}
        public String division {get; set;} 
        public String location {get; set;} 
        public PreviewWrapper(Long dateInMilliseconds, String businessUnit, String division, String location) {
            this.effectiveDate = dateInMilliseconds;
            this.businessUnit = String.isNotBlank(businessUnit) ? businessUnit : '';
            this.division = String.isNotBlank(division) ? division : '';
            this.location = String.isNotBlank(location) ? location : '';
        }
    }
}