@isTest
public with sharing class LMSPortal_ContentInlineControllerTest {
	
	public static List<BU_Division__c> buDivList {get;set;}
	public static List<Document> docList {get;set;}
	public static List<LMS_Content__c> lmsList {get;set;}
	public static List<Learning_Content_Access__c> lcaLink {get;set;}
    public static List<Learning_Content_Access__c> lcaLinkList {get;set;}
		
	static testMethod void myUnitTest() {
    	setUp();
    	Test.setCurrentPageReference(new PageReference('Page.LMSContentInline'));
    	LMS_Content__c lmsObj = new LMS_Content__c();
		ApexPages.StandardController stdController = new ApexPages.StandardController(lmsObj);
        List<LMS_Content__c> tmpList = [Select Id from LMS_Content__c];        
		System.currentPageReference().getParameters().put('id', tmpList[0].Id); 
		
        test.StartTest();
    	LMSPortal_ContentInlineController controller = new LMSPortal_ContentInlineController();
    	controller.showDocument = true;      
        controller.bannerfilename = 'testfilename.jpg';
        controller.thumbfilename = 'testthumbnail.jpg';
        controller.getunSelectedBUValues();
        controller.getSelectedBUValues();
        controller.unselectclickBU();
        PageReference pg1 = controller.selectclickBU();
        
        controller.getunSelectedDivValues();
        controller.getSelectedDivValues();
        controller.selectclickDiv();
        controller.unselectclickDiv();
        
        controller.getunSelectedLocValues();
        controller.getSelectedLocValues();
        controller.selectclickLoc();
        controller.unselectclickLoc();
        controller.selectAllLocations();
                
        controller.saveContentAccess();
        system.assertEquals(pg1, null);

        test.stopTest();
    }
    
    static testMethod void myUnitTest2() {
    	setUp();
    	Test.setCurrentPageReference(new PageReference('Page.LMSContentInline'));
    	LMS_Content__c lmsObj = new LMS_Content__c();
		ApexPages.StandardController stdController = new ApexPages.StandardController(lmsObj);
        List<LMS_Link__c> tmpList = [Select Id from LMS_Link__c];        
		System.currentPageReference().getParameters().put('id', tmpList[0].Id); 
		
        test.StartTest();
    	LMSPortal_ContentInlineController controller = new LMSPortal_ContentInlineController();
    	controller.showDocument = false;      
        controller.bannerfilename = 'testfilename.jpg';
        controller.thumbfilename = 'testthumbnail.jpg';
        controller.getunSelectedBUValues();
        controller.getSelectedBUValues();
        controller.unselectclickBU();
        PageReference pg1 = controller.selectclickBU();
        
        controller.getunSelectedDivValues();
        controller.getSelectedDivValues();
        controller.selectclickDiv();
        controller.unselectclickDiv();
        
        controller.resetLocPicklist();
        controller.selectAllLocations();
        controller.isChecked = true;
        controller.selectAllLocations();
                
        controller.saveContentAccess();
        system.assertEquals(pg1, null);

        test.stopTest();
    }
        
    static void setUp() 
	{
        List<LMS_Link__c> linkObjList = createLinks(3);
		buDivList = createBUDivData();
		docList = createDocumentData();
		lmsList = createLMSContentData(docList);
        lcaLink = createContentAccessData(lmsList, buDivList);
        lcaLinkList = createContentAccessDataLinks(linkObjList, buDivList);
	}
	
	public static List<Learning_Content_Access__c> createContentAccessData(List<LMS_Content__c> lms, List<BU_Division__c> buDiv) {
        List<Learning_Content_Access__c> tmpList = new List<Learning_Content_Access__c>();
        for(LMS_Content__c content : lms) {
            for(BU_Division__c bud : buDiv) {
                Learning_Content_Access__c lca = new Learning_Content_Access__c();
                lca.Business_Unit__c = bud.Business_Unit__c;
                lca.Division__c = bud.Division__c;
                lca.Learning_Content__c = content.Id;
                lca.Location__c = 'Test-Campus';
                tmpList.add(lca); 
                if(content.Category__c == 'Test'){
                	lca.Location__c = 'Test-Campus;Campus-Cambridge';
                }
            }
        }
    	
    	insert tmpList;
    	return tmpList;
    }

    public static List<Learning_Content_Access__c> createContentAccessDataLinks(List<LMS_Link__c> lmsLinks, List<BU_Division__c> buDiv) {
        List<Learning_Content_Access__c> tmpList = new List<Learning_Content_Access__c>();
        for(LMS_Link__c link : lmsLinks) {
            for(BU_Division__c bud : buDiv) {
                Learning_Content_Access__c lca = new Learning_Content_Access__c();
                lca.Business_Unit__c = bud.Business_Unit__c;
                lca.Division__c = bud.Division__c;
                lca.Learning_Link__c = link.Id;
                lca.Location__c = 'Test Campus';
                tmpList.add(lca); 
            }
        }
        
        insert tmpList;
        return tmpList;
    }
    
    public static List<Learning_Content_Access__c> createContentAccessDataLinks2(List<LMS_Link__c> lmsLinks, List<BU_Division__c> buDiv) {
        List<Learning_Content_Access__c> tmpList = new List<Learning_Content_Access__c>();
        for(LMS_Link__c link : lmsLinks) {
            for(BU_Division__c bud : buDiv) {
                Learning_Content_Access__c lca = new Learning_Content_Access__c();
                lca.Business_Unit__c = bud.Business_Unit__c;
                lca.Division__c = bud.Division__c;
                lca.Learning_Link__c = link.Id;
                tmpList.add(lca); 
            }
        }
        
        insert tmpList;
        return tmpList;
    }

    public static List<LMS_Link__c> createLinks(Integer recordCount) {
        
        List<LMS_Link__c> tmpList = new List<LMS_Link__c>();
        if(recordCount == null)
        {
            recordCount = 1;
        }
        for(Integer i=1; i <= recordCount; i++)
        {
            LMS_Link__c ll = new LMS_Link__c();
            ll.Target_Window__c = 'New Window';
            ll.Status__c = 'Active';
            ll.Rank__c = i;
            ll.Label__c = 'Test' + i;
            ll.External_Media_URL__c = 'https://www.biogen.com' + i;
            if(i < 2){
                ll.Type__c = 'My Learning';
            }else{
                ll.Type__c = 'Quick Link';
            }
            tmpList.add(ll);
        }
        insert tmpList;
        return tmpList;
    }
	
	public static List<LMS_Content__c> createLMSContentData(List<Document> doc) {
    	
    	List<LMS_Content__c> tmpList = new List<LMS_Content__c>();
    	
		LMS_Content__c lms = new LMS_Content__c();
		lms.Description__c = 'Test Description';
    	lms.New_Content__c = false;
    	lms.Banner_Document_Id__c = doc[0].Id;
    	lms.Thumbnail_Document_Id__c = doc[0].Id;
    	lms.Category__c = 'Test';
    	lms.External_Media_URL__c = 'https://www.biogen.com';
		tmpList.add(lms);
		
		LMS_Content__c lms2 = new LMS_Content__c();
		lms2.Description__c = 'Test Description 2';
    	lms2.New_Content__c = true;
    	lms2.Banner_Document_Id__c = doc[0].Id;
    	lms2.Thumbnail_Document_Id__c = doc[0].Id;
    	lms2.Category__c = 'Test 2';
    	lms2.External_Media_URL__c = 'https://www.biogen.com';
		tmpList.add(lms2);
		
		LMS_Content__c lms3 = new LMS_Content__c();
		lms3.Description__c = 'Test Description 3';
    	lms3.New_Content__c = true;
    	lms3.Banner_Document_Id__c = doc[0].Id;
    	lms3.Thumbnail_Document_Id__c = doc[0].Id;
    	lms3.Category__c = 'Test 3';
    	lms3.External_Media_URL__c = 'https://www.biogen.com';
		tmpList.add(lms3);
    	
    	insert tmpList;
    	return tmpList;
    }
	
	public static List<BU_Division__c> createBUDivData() {
    	
    	List<BU_Division__c> tmpList = new List<BU_Division__c>();
    	
		BU_Division__c bud = new BU_Division__c();
		bud.Name = '1111-0135 Clinical Development';
		bud.Division__c = '1111-0135 Clinical Development';
		bud.Business_Unit__c = '1000-0180 Research & Development';
		bud.Division_Name__c = 'Research';
		tmpList.add(bud);
		
		BU_Division__c bud1 = new BU_Division__c();
		bud1.Name = '000 MA Talent Acquisition';
		bud1.Division__c = '000 MA Talent Acquisition';
		bud1.Business_Unit__c = '000 MA Talent Acquisition';
		bud1.Division_Name__c = 'Research';
		tmpList.add(bud1);
    	
    	insert tmpList;
    	return tmpList;
    }
    
    public static List<Document> createDocumentData() {
    	
    	List<Document> tmpList = new List<Document>();
    	
		Document doc = new Document();
		doc.folderId = UserInfo.getUserId();
		doc.name = 'name';
		doc.ContentType = 'image/jpeg';
		tmpList.add(doc);
    	
    	insert tmpList;
    	return tmpList;
    }
}