@isTest(SeeAllData=false)
private class HRP_ContentTriggerHandlerTest {
  private static testMethod void updateFieldValueToDefault_Countries_Positive() {
    HRP_Content__c hrp = HRP_TestDataFactory.buildHRPContent(null);
    hrp.Countries__c = 'Australia;Global;United States of America';
    Test.startTest();
      insert hrp;
    Test.stopTest();
    hrp = [Select Id, Countries__c FROM HRP_Content__c WHERE Id = :hrp.Id];
    System.assertEquals(HRP_Constants.HR_DEFAULT_COUNTRY, hrp.Countries__c);
  }

  private static testMethod void updateFieldValueToDefault_Countries_Negative() {
    HRP_Content__c hrp = HRP_TestDataFactory.buildHRPContent(null);
    hrp.Countries__c = 'Global';
    insert hrp;
    Test.startTest();
      hrp.Countries__c = 'Australia;United States of America';
      update hrp;
    Test.stopTest();
    hrp = [Select Id, Countries__c FROM HRP_Content__c WHERE Id = :hrp.Id];
    System.assertEquals('Australia;United States of America', hrp.Countries__c);
  }

  private static testMethod void updateFieldValueToDefault_Profile_Positive() {
    HRP_Content__c hrp = HRP_TestDataFactory.buildHRPContent(null);
    hrp.User_Profile__c = 'New Hire;Employee;All';
    Test.startTest();
      insert hrp;
    Test.stopTest();
    hrp = [Select Id, User_Profile__c FROM HRP_Content__c WHERE Id = :hrp.Id];
    System.assertEquals(HRP_Constants.HR_DEFAULT_PROFILE, hrp.User_Profile__c);
  }

  private static testMethod void updateFieldValueToDefault_Profile_Negative() {
    HRP_Content__c hrp = HRP_TestDataFactory.buildHRPContent(null);
    hrp.User_Profile__c = 'All';
    insert hrp;
    Test.startTest();
      hrp.User_Profile__c = 'Employee;New Hire';
      update hrp;
    Test.stopTest();
    hrp = [Select Id, User_Profile__c FROM HRP_Content__c WHERE Id = :hrp.Id];
    System.assertEquals('Employee;New Hire', hrp.User_Profile__c);
  }
}