public without sharing class HRConnectCommon {

    public static boolean ismanager {get
        {
            if (ismanager == null)
            {
                User u = [SELECT is_manager__c FROM User WHERE Id=:userinfo.getUserId()];
                ismanager = u.is_manager__c;
            }
            return ismanager;
        }
    set;}

    public static string userprofilename {get
        {
            if (userprofilename == null)
            {
                Profile p = [SELECT Name FROM Profile WHERE Id=:userinfo.getProfileId()];

                /*User u = [SELECT Grade__c,Is_Manager__c,Division__c FROM User WHERE Id=:userinfo.getUserId()];

                //exec
                if (p.name.contains('Exec') || u.Grade__c >= 20)
                    userprofilename = 'Exec';   */

                userprofilename = p.name;
            }
            return userprofilename;
        }
    set;}

    public static string usercountry {get
        {
            if (usercountry == null)
            {
                User u = [SELECT Country,is_manager__c FROM User WHERE Id=:userinfo.getUserId()];
                usercountry = u.country;
                ismanager = u.is_manager__c;
            }
            return usercountry;
        }
    set;}

    public static string userlang {get
        {
            if (userlang == null)
            {
                //User u = [SELECT LanguageLocaleKey FROM User WHERE Id=:userinfo.getUserId()];
                //userlang = u.LanguageLocaleKey;
                userlang = UserInfo.getLocale();
            }
            return userlang;
        }
    set;}

    public static void saveArticleRating(String aid,String rating) {

        system.debug('HRConnectCommon->saveArticleRating');
        system.debug('param id: '+aid);
        system.debug('param rating: '+rating);

        try{
            //save a standard Vote, replacing existing vote if found.  can't update a vote so delete if found
            List<Vote> votes = [select id from Vote where parentid = :aid and CreatedById = :UserInfo.getUserId()];
            if(votes.size()>0) {
                delete votes;
            }
            Vote v = new Vote(parentid = aid, Type = rating);
            insert v;
        }
        catch(Exception e) {
            system.debug('error: '+e.getMessage());
        }
    }
    
    public static String getArticleRating(String aid,String userId) {

        system.debug('HRConnectCommon->getArticleRating');
        system.debug('param id: '+aid);
        system.debug('param rating: '+userId);

        try{
            //save a standard Vote, replacing existing vote if found.  can't update a vote so delete if found
            List<Vote> votes = [select Type from Vote where parentid = :aid and CreatedById = :UserInfo.getUserId()];
            if(votes.size()>0) {
                return votes.get(0).Type;
            }
            return '0';
        }
        catch(Exception e) {
            system.debug('error: '+e.getMessage());
            return '0';
        }
    }

}