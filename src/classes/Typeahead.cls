// Controller class for the Force.com Typeahead component
public with sharing class Typeahead {

  @RemoteAction
  public static list<sObject> searchRecords( String prevparams, String queryString, String objectName, 
    list<String> fieldNames, String fieldsToSearch, String filterClause, String orderBy, Integer recordLimit ) {

    if (queryString == null) return null;

    String sQuery = String.escapeSingleQuotes( queryString );
    if (sQuery.length() == 0) return null;
    
    String sFields = (fieldNames == null || fieldNames.isEmpty()) ? 'Id, Name' : 
    	String.escapeSingleQuotes( String.join( fieldNames, ', ' ) );  

    	
	List<LMS_Content__c> lmsList = LMSPortal_Util.searchWithFilters(prevparams, sQuery, sFields, recordLimit);

    return lmsList;
  }  

}