/**
 * Class to create data needed for test classes.
 */
@isTest
public  class PrepTestDataUtils {
	/*
	 * method to insert bulk users for a given user type.
	 */

   public static List<User> createBulkUsers(String userType, Integer noOfUsers) {
		List<User> dummyUsers=new List<User>();
    //query for the integration profile
    Profile defaultIntegProfile=[select id from Profile where Name=:BiogenConstants.INTEGRATION_PROFILE_COMMUNITY limit 1];
		// function to create test users.
		for(Integer cnt=0;cnt<noOfUsers;cnt++){
			dummyUsers.add(createDummyUserInstance(userType,cnt,defaultIntegProfile.id));
		}
		insert dummyUsers;
		return dummyUsers;
	}

  public static List<Case> queryTestClassCases(){
      return new List<Case>([select   Id
                                    , AccountId
                                    , Origin
                                    , OwnerId
                                    , Status
                                    , Portal_Case__c
                                    , ContactId
                                    , On_Behalf_Of2__c
                                    , Initial_Case_Creator__c
                             from Case
                             where Origin='Test Class']);
  }

  /**
   * Method to insert cases based on a set of parameters
   */

  public static List<Case> createBulkCases( Boolean portalCase
                                          , Boolean privateCase
                                          , String caseContactId
                                          , Contact onBehalfOf
                                          , Integer noOfCases
                                          , String recordTypeName){
     System.debug('===caseContactId in createBulkCases ====' + caseContactId);
     List<Case> dummyCases=new List<Case>();

     RecordType recordTypeInstance=[select Id from RecordType where sObjectType='Case' and Name=:recordTypeName limit 1];

     Account accountInstance=[select id from Account where Name=:BiogenConstants.BIOGEN_ACCOUNT limit 1];

     for(Integer index=0;index<noOfCases;index++){
         Case caseInstance=new Case( AccountId=accountInstance.id
                                    , BusinessHours=new BusinessHours(Name ='United States')
                                    , Origin='Test Class'
                                    , Status= 'Open'
                                    , Portal_Case__c=portalCase
                                    , ContactId=caseContactId
                                    , On_Behalf_Of2__c=(onBehalfOf!=null?onBehalfOf.Id:null)
                                    , RecordTypeId=recordTypeInstance.id
                                    , Private__c=privateCase
                                   );
         dummyCases.add(caseInstance);
     }

     insert dummyCases;

     System.debug('====dummycases===' + dummycases);

     return dummyCases;

  }

  public static List<HR_Portal_Property__c> populateInterestedProfiles(){
     List<HR_Portal_Property__c> interestedProfiles=new List<HR_Portal_Property__c>();

     interestedProfiles.add(constructPortalProperty(BiogenConstants.INTEGRATION_PROFILE_COMMUNITY,BiogenConstants.INTEGRATION_PROFILE));
     interestedProfiles.add(constructPortalProperty(BiogenConstants.INTEGRATION_PROFILE_PLATFORM,BiogenConstants.INTEGRATION_PROFILE));

     interestedProfiles.add(constructPortalProperty(BiogenConstants.EMPLOYEE_PROFILE,BiogenConstants.INTERESTED_PROFILE));
     interestedProfiles.add(constructPortalProperty(BiogenConstants.EXECUTIVE_PROFILE,BiogenConstants.INTERESTED_PROFILE));
     interestedProfiles.add(constructPortalProperty(BiogenConstants.MANAGER_PROFILE,BiogenConstants.INTERESTED_PROFILE));
     interestedProfiles.add(constructPortalProperty(BiogenConstants.HR4HR_PROFILE,BiogenConstants.INTERESTED_PROFILE));

     interestedProfiles.add(constructPortalProperty(BiogenConstants.NEWLY_HIRED + BiogenConstants.EMPLOYEE_PROFILE,BiogenConstants.INTERESTED_PROFILE));
     interestedProfiles.add(constructPortalProperty(BiogenConstants.NEWLY_HIRED + BiogenConstants.EXECUTIVE_PROFILE,BiogenConstants.INTERESTED_PROFILE));
     interestedProfiles.add(constructPortalProperty(BiogenConstants.NEWLY_HIRED + BiogenConstants.MANAGER_PROFILE,BiogenConstants.INTERESTED_PROFILE));
     interestedProfiles.add(constructPortalProperty(BiogenConstants.NEWLY_HIRED + BiogenConstants.HR4HR_PROFILE,BiogenConstants.INTERESTED_PROFILE));

     System.runAs(new User(Id = Userinfo.getUserId())) {
        insert interestedProfiles;
     }

     return interestedProfiles;

  }

  public static void initialize(){
    System.runAs(new User(Id = Userinfo.getUserId())) {
         //create an account called with name biogen
          Account accountInstance=new Account(Name=BiogenConstants.BIOGEN_ACCOUNT);
          insert accountInstance;
          //create a public group with name all biogen users
          Group grpInstance=new Group(Name=BiogenConstants.BIOGEN_PUBLIC_GROUP);
          insert grpInstance;

          List<HR_Portal_Property__c> portalProperties=new List<HR_Portal_Property__c>();
          //populate the new hire days and lowest executive grade
          HR_Portal_Property__c portalProperty=new HR_Portal_Property__c(Name='30', Entity_Type__c=BiogenConstants.NEW_HIRE_CONDITION);
          portalProperties.add(portalProperty);

          portalProperty=new HR_Portal_Property__c(Name='20', Entity_Type__c=BiogenConstants.LOWEST_EXECUTIVE_GRADE);
          portalProperties.add(portalProperty);

          insert portalProperties;
    }

  }


  private static HR_Portal_Property__c constructPortalProperty(String name, String entityType){
     return new HR_Portal_Property__c(Name=name,Entity_Type__c=entityType);
  }

  public static List<Contact> queryContact(List<String> employeeIds){
       return new List<Contact>([select  FirstName
                                , LastName
                                , Employee_ID__c
                                , Cost_Center__c
                                , Division__c
                                , Grade__c
                                , Active__c
                                , Hire_Date__c
                                , HR_Business_Partner__c
                                , Is_Manager__c
                                , Job_Title__c
                                , Location__c
                                , Middle_Initial__c
                                , Not_Available__c
                                , Supervisory_Org__c
                                , Termination_Date__c
                                , On_Leave__c
                                , Worker_Type__c
                                , Profile_Name__c
                                , Business_Unit__c
                                , MailingStreet
                                , MailingCity
                                , MailingState
                                , MailingPostalCode
                                , MailingCountry
                                , MailingStateCode
                                , MailingCountryCode
                                , MailingLatitude
                                , MailingLongitude
                                , MobilePhone
                                , Email
                                , Phone
                                , Title
                            from Contact
                            where Employee_ID__c in :employeeIds
                           ]);
  }

  public static Map<Id,User> queryTestClassUsers(){
        return new Map<Id,User>([select  FirstName
                                , LastName
                                , Employee_ID__c
                                , Cost_Center__c
                                , Division__c
                                , Grade__c
                                , Hire_Date__c
                                , HR_Business_Partner__c
                                , Is_Manager__c
                                , Job_Title__c
                                , Location__c
                                , Middle_Initial__c
                                , Not_Available__c
                                , Supervisory_Org__c
                                , Termination_Date__c
                                , Worker_Type__c
                                , ProfileId
                                , Profile.Name
                                , Business_Unit__c
                                , On_Leave__c
                                , Profile_Name__c
                                , Street
                                , City
                                , State
                                , PostalCode
                                , Country
                                , StateCode
                                , CountryCode
                                , Latitude
                                , Longitude
                                , MobilePhone
                                , Email
                                , Phone
                                , Title
                                , IsActive
                            from User
                            where FirstName = 'Test Class'
                           ]);
    }



  public static boolean validateProfileAssignment(List<User> testUsers,String profileName){
     //determine profile Id
     Profile profileInstance=[select Id from Profile where Name=:profileName limit 1];
     Boolean profileValid=true;

     for(User userInstance: testUsers){
        System.assertEquals(profileInstance.Id,userInstance.ProfileId);
        profileValid&=(profileInstance.Id==userInstance.ProfileId);
     }
     return profileValid;
  }

  public static boolean validatePublicGroupAssignment(Set<Id> expectedUserIds){
     //query the group object for the group id
     Group allBiogenGrp=[select Id from Group where Name=:BiogenConstants.BIOGEN_PUBLIC_GROUP limit 1];
     Set<Id> assignedUserIds=new Set<Id>();
     //read all group members for all biogen group
     for(GroupMember grpMemberInstance:[select Id, UserOrGroupId from GroupMember where GroupId=:allBiogenGrp.Id]){
        assignedUserIds.add(grpMemberInstance.UserOrGroupId);
     }
     //make sure the assigned user set contains all user ids in expected list
     Boolean publicGrpsAssigned=expectedUserIds.containsAll(assignedUserIds);
     System.assertEquals(true,publicGrpsAssigned);
     return publicGrpsAssigned;
  }

  public static boolean validatePermSetAssignment(Map<Id,User> testclassUserMap){
    //get the map of perm set names and map from the database. The test class assumes the perm set are being set up.
    Map<String,String> permSetMap=HRPortalUtils.getPermSetsMap();

    //create a map to store the key - assigneeid and value - permissionset id
    Map<String,Set<String>> permSetAssignMap=new Map<String,Set<String>>();

    Boolean allPermSetsAssigned=true;
    //variable to store the expected perm set ids
    Set<String> expectedPermSetIds=new Set<String>();

    for(PermissionSetAssignment permAssign:[select   AssigneeId
                                                   , PermissionSetId
                                            from PermissionSetAssignment
                                            where AssigneeId in :testclassUserMap.keySet()]){
           if(permSetAssignMap.get(permAssign.AssigneeId)!=null){
               permSetAssignMap.get(permAssign.AssigneeId).add(permAssign.PermissionSetId);
           }
           else{
              permSetAssignMap.put(permAssign.AssigneeId,new Set<String> {permAssign.PermissionSetId});
           }
    }

    //iterate through user
    for(User userInstance:testclassUserMap.values()){
       //initalize the expected permset ids set
       expectedPermSetIds=new Set<String>();

       //call the method to find the perm set names that has to be assigned for this user
       for(String permSetName:HRPortalUtils.constructPermSetNames(userInstance)){
          //get the permission set salesforce id from the map for a given permission set name
          //and add to the set of all assigned permission sets for a given user
           expectedPermSetIds.add(permSetMap.get(permSetName));
       }
       //validate that the permission sets assigned are indeed all of expected
       Set<String> assignedPermSetIds=permSetAssignMap.get(userInstance.id);
       allPermSetsAssigned&=(assignedPermSetIds!=null && assignedPermSetIds.containsAll(expectedPermSetIds));
       //assert that the all perm sets assigned value is true
       System.assertEquals(true,allPermSetsAssigned);
    }

    return allPermSetsAssigned;
  }


  public static boolean validateContactCreation(List<User> testclassUsers){
     List<String> employeeIds=new List<String>();
     Map<String,User> employeeIdUserMap=new Map<String,User>();
     Boolean allContactsValidated=true;
     //loop through all users and build the list of employee ids that can be used to query the corresponding contacts
     for(User userInstance:testclassUsers){
        employeeIds.add(userInstance.Employee_ID__c);
        employeeIdUserMap.put(userInstance.Employee_ID__c,userInstance);
     }

     for(Contact contactInstance:queryContact(employeeIds)){
        //validate that the fields are copied over from users to contacts and the corresponding contacts exists
        allContactsValidated&=validateContactInstance(contactInstance,employeeIdUserMap.get(contactInstance.Employee_ID__c));
        //validate that still allContactsValidated flag is true
        System.assertEquals(true,allContactsValidated);
     }
    return allContactsValidated;
  }

  private static boolean validateContactInstance(Contact contactInstance, User userInstance){
    return (contactInstance.FirstName==userInstance.FirstName
         && contactInstance.LastName==userInstance.LastName
         && contactInstance.MailingStreet==userInstance.Street
         && contactInstance.MailingCity==userInstance.City
         && contactInstance.MailingState==userInstance.State
         && contactInstance.MailingPostalCode==userInstance.PostalCode
         && contactInstance.MailingCountry==userInstance.Country
         && contactInstance.MailingStateCode==userInstance.StateCode
         && contactInstance.MailingCountryCode==userInstance.CountryCode
         && contactInstance.MailingLatitude==userInstance.Latitude
         && contactInstance.MailingLongitude==userInstance.Longitude
         && contactInstance.MobilePhone==userInstance.MobilePhone
         && contactInstance.Email==userInstance.Email
         && contactInstance.Phone==userInstance.Phone
         && contactInstance.Title==userInstance.Title
         && contactInstance.Employee_ID__c==userInstance.Employee_ID__c
         && contactInstance.Cost_Center__c==userInstance.Cost_Center__c
         && contactInstance.Division__c==userInstance.Division__c
         && contactInstance.Grade__c==userInstance.Grade__c
         && contactInstance.Hire_Date__c==userInstance.Hire_Date__c
         && contactInstance.HR_Business_Partner__c==userInstance.HR_Business_Partner__c
         && contactInstance.Is_Manager__c==userInstance.Is_Manager__c
         && contactInstance.Job_Title__c==userInstance.Job_Title__c
         && contactInstance.Location__c==userInstance.Location__c
         && contactInstance.Middle_Initial__c==userInstance.Middle_Initial__c
         && contactInstance.Not_Available__c== userInstance.Not_Available__c
         && contactInstance.Supervisory_Org__c== userInstance.Supervisory_Org__c
         && contactInstance.Termination_Date__c==userInstance.Termination_Date__c
         && contactInstance.Worker_Type__c==userInstance.Worker_Type__c
         && contactInstance.Business_Unit__c== userInstance.Business_Unit__c
         && contactInstance.Profile_Name__c==userInstance.Profile.Name
         && contactInstance.On_Leave__c==userInstance.On_Leave__c
         && contactInstance.Active__c==userInstance.IsActive
        );
  }

	private static void populateUserAttributes(String userType, User userInstance){
       if(userType!=null && userType.contains(BiogenConstants.NEWLY_HIRED)){
             userInstance.Hire_Date__c=System.today();
       }
       else{
       	  userInstance.Hire_Date__c=System.today()-31;
       }

       if(userType!=null && userType.contains(BiogenConstants.MANAGER_PROFILE)){
            userInstance.Is_Manager__c=true;
            userInstance.Grade__c=10;
       }

       else if(userType!=null && userType.contains(BiogenConstants.HR4HR_PROFILE)){
            userInstance.Division__c=BiogenConstants.HUMAN_RESOURCES;
       }

       else if(userType!=null && userType.contains(BiogenConstants.EXECUTIVE_PROFILE)){
            userInstance.Grade__c=30;
       }
	}

	private static User createDummyUserInstance(String userType, Integer uniqueIndex, String ProfileId){
		 User userInstance=new User();
       	 userInstance.FirstName='Test Class';
       	 userInstance.LastName='Biogen - ' + uniqueIndex;
       	 userInstance.Email='testclass@biogen.com';
       	 userInstance.userName='testclass'+uniqueIndex+'@biogen.dev';
       	 userInstance.Alias='dumy'+uniqueIndex;
         userInstance.CommunityNickname='dumy'+uniqueIndex;
         userInstance.Employee_ID__c= '1000000i' + uniqueIndex;
         userInstance.ProfileId=profileId;

       	 userInstance.TimeZoneSidKey = 'America/New_York';
         userInstance.LocaleSidKey = 'en_US';
         userInstance.EmailEncodingKey = 'ISO-8859-1';
         userInstance.LanguageLocaleKey = 'en_US';

         //populate attributes specific to the user being created.
         populateUserAttributes(userType,userInstance);

         return userInstance;
	}


}