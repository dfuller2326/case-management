public class HRConnectPositionRequestController{

    public String ifSoWhatMonthAndYear {get;set;}
    public Database.DmlOptions.Assignmentruleheader assignmentRuleHeader {get; set;}
    public String successflag {get;set;}

    public string OnBehalfOfId {get; set;}
    public string OnBehalfOf {get; set;}
    public string ReplacingEmployeeId {get; set;}
    public string ReportsToId {get; set;}
    public string JobTitleId {get; set;}

    public boolean OnBehalfOfError {get; set;}
    public boolean aopmyfError {get; set;}
    public boolean ReplacingEmployeeError {get; set;}
    public boolean ReportsToError {get; set;}
    public boolean JobTitleError {get; set;}

    private ApexPages.StandardController controller;

    public HRConnectPositionRequestController(ApexPages.StandardController stdController) {
        this.controller = stdController;
        successflag = 'N';
    }

    public PageReference doSave() {
        Case hrcase = (Case)controller.getRecord();
        Id userid = UserInfo.getUserId();

        if (hrcase.Position_approved_in_AOP_process__c == 'Yes' ){
            if (ifSoWhatMonthAndYear == null || ifSoWhatMonthAndYear == '') {
                aopmyfError = true;
            }
            else {
                aopmyfError = false;
                hrcase.If_so_for_what_month_and_year__c = ifSoWhatMonthAndYear;
            }
        }
        else {
            aopmyfError = false;
            hrcase.If_so_for_what_month_and_year__c = null;
        }

        if (hrcase.Replacement_term_transferred_employee__c == 'Yes'){
            if (replacingEmployeeId == null || replacingEmployeeId == '') {
                replacingEmployeeError = true;
            }
            else {
                replacingEmployeeError = false;
                hrcase.If_so_what_is_the_employees_name__c = replacingEmployeeId;
            }
        }
        else {
            replacingEmployeeError = false;
            hrcase.If_so_what_is_the_employees_name__c = null;

        }

        if (JobTitleId == null || JobTitleId == '') {
            JobTitleError = true;
        }
        else{
            JobTitleError = false;
            hrcase.What_is_the_Job_Profile_Job_Title__c = JobTitleId;
        }

        if (ReportsToId == null || ReportsToId == '') {
            ReportsToError = true;
        }
        else{
            ReportsToError = false;
            hrcase.Who_will_position_report_to__c = ReportsToId;
        }

        if(String.isBlank(OnBehalfOfId) && !String.isEmpty(OnBehalfOf) && OnBehalfOf != 'On Behalf Of' ) {
            OnBehalfOfError = true;
        }
        else {
            OnBehalfOfError = false;
        }

        if (aopmyfError || ReplacingEmployeeError || JobTitleError || ReportsToError || OnBehalfOfError) {
            return null;
        }

        if (OnBehalfOfId != '' && OnBehalfOfId != null)
        {
            hrcase.On_Behalf_Of__c = OnBehalfOfId;
            userid = OnBehalfOfId;
        }

        //The following code retrieve case assignment rules
        //Create DMLOtions for "Assign using Active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        //dmlOpts.assignmentRuleHeader.assignmentRuleId = AR.Id;
        dmlOpts.assignmentRuleHeader.useDefaultRule=true;
        //Set  the assignment option for our case
        hrcase.setOptions(dmlOpts);

        //Set the subject field to "New Position Request"
        if (!(JobTitleId == null || JobTitleId == '')) {
            Job_Code__c jobcode = [select name from Job_Code__c where id=:JobTitleId];
            hrcase.Subject = jobcode.name;      //'New Position Request';
            hrcase.Description = hrcase.Subject;
        }

        // Set the Record type for the new case to "Position Request Form"
        RecordType PositionRequestFormrecordType = [SELECT id FROM RecordType where SobjectType='Case' and Name='Position Request Form' limit 1];
        hrcase.recordtypeID = PositionRequestFormrecordType.Id;

        //Set Contact Name field to the employee the case is created on behalf of
        User u = [select Employee_ID__c from user where id=:userid];
        List<Contact> c = [select id from contact where Employee_ID__c= :u.Employee_ID__c];
        if(c.size() >0)
            hrcase.ContactId = c[0].id;

        //Set case origin to "Web"
        hrcase.Origin = 'Web';
        //Set cas Portal_web__c flag to true
        hrcase.Portal_Case__c = true;
        insert hrcase;
        system.assertNotEquals(null, hrcase.Id); // BOOM!

        successflag = 'Y';

        return null;
    }


    public List<SelectOption> getMonthYears() {
            Schema.DescribeFieldResult F = Case.If_so_for_what_month_and_year__c.getDescribe();
            List<Schema.PicklistEntry> PicklistValues = F.getPicklistValues();
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('','--None--'));
            for (Schema.PicklistEntry  q : PickListValues){
                options.add(new SelectOption(q.getLabel(),q.getValue()));
            }
            return options;
    }


}