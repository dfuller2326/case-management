global with sharing class HRP_HomeController {
	public User currentUser {get;set;}
	public Boolean mobile {get;set;}
	public HRP_HomeController() {
		
	}

	@RemoteAction
	global static String getHomePageData() {
		HomeResponse respObj = new HomeResponse();
		try {
			// Query context User and User Profile
			User u = [SELECT Id, Country, ProfileId, SmallPhotoUrl FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
			System.debug('USER COUNTRY :: ' + u.Country);
			Profile p = [SELECT Id, Name FROM Profile WHERE Id = :u.ProfileId LIMIT 1];

			// Retrieve Home Page Carousel Content
		  	respObj.carouselContent = HRP_Content.retrieveHomePageCarousel(u.Country, p.Name); 

		  	// Retrieve Home Page Top Services Content
		  	respObj.tileContent = HRP_Content.retrieveHomePageTiles(u.Country, p.Name);
		} catch(Exception ex) {
		  respObj.error = new HRP_ErrorInfo(System.Label.Home_Page_Error_Heading, ex.getMessage());
		  System.debug(LoggingLevel.ERROR, 'ERROR RETRIEVING HOME PAGE DATA === ' + ex.getMessage());
		}
		System.debug('RESPONSE :: ' + JSON.serialize(respObj));
		return JSON.serialize(respObj);
	}

	public class HomeResponse {
		public List<HRP_ContentWrapper> carouselContent {get;set;}
		public List<HRP_ContentWrapper> tileContent {get;set;}
		public HRP_ErrorInfo error {get;set;}
		public HomeResponse() {
		}
	}
}