var Biogen_Home = function($, remotingErrorMsg, remoteActionMethod, mediator) {
    
    // Backbone sync override
    function sync(method, model, options) {
        switch (method) {
            case 'read':
                return getPageData(method, model, options);
            default:
                break;
        }
    }

    // Custom read function
    function getPageData(method, model, options) {
        Visualforce.remoting.Manager.invokeAction(remoteActionMethod,
            Home.previewParam,
            function(result, event) {
                if (event.status) {
                    var response = $.parseJSON(result);
                    if (response.error === null) {
                        options.success(response);
                        if(Home.isPreview) {
                            mediator.publish("previewStarted", {
                                params: Home.previewParam
                            });
                        }
                    } else {
                        Home.displayError(response.error.errMsg);
                    }
                } else {
                    Home.displayError(remotingErrorMsg);
                }
            }, { escape: false }
        );
    }

    // model
    var model = Backbone.Model.extend({
        id: 'home',
        idAttribute: 'Id',
        defaults: {
            catCourseObj: {
            	categoryName: '',
                courseList: {
                    contentBannerUrl:'',
                    contentTitle: '',
                    contentSummary: '',
                    count: ''                    
                }
            },
            myLearningObj: {
                externalMediaUrl: '',
                targetWindow: '',
                label: ''
            },
            helpfulLinksObj: {
                externalMediaUrl: '',
                targetWindow: '',
                label: ''
            },
            whatsNewObj: {
                contentBannerUrl:'',
                contentTitle: '',
                contentSummary: '',
                count: ''
            }
        }
    });

    // collection
    var collection = Backbone.Collection.extend({
        model: model,
        sync: sync,
        initialize: function() {
            this.fetch();
        }
    });

    // Home Sub Category Child Record View
    var subCategoryView = Backbone.View.extend({
        initialize: function(){
            var self = this;
            self.collection.on('add reset orientationchange', self.render, self);
            self.template = _.template($("#home-subcategory-template").html());
        },
        events: {},
        render: function() {
            var subcategoryModel = this.collection.at(0);
            if(subcategoryModel && subcategoryModel.attributes.catCourseObj) {
                this.$el.html(this.template(subcategoryModel.attributes));
            }
            if(!$('#pgBody').is(':visible')) {
                $('#pgBody').fadeIn('slow');
            }

            // Calculate Height of Learning Course Title and Set all Title Heights to Height of Max Height Title
            // var maxHeight = 0;
            // $("div.caption > h5[id*='courseSummaryTitle']").each(function(){
            //     maxHeight = Math.max(maxHeight, $(this).height());
            // }).height(maxHeight);

            // Initalize Swipe Functionality for Subcategory Carousel
            $("div.carousel[id*='carousel-subcategory']").swiperight(function(){
                $(this).carousel('prev');
            });
            $("div.carousel[id*='carousel-subcategory']").swipeleft(function(){
                $(this).carousel('next');
            });
            return this;
        }
    });
    
    // Home My Learning Child Record View
    var myLearningView = Backbone.View.extend({
        initialize: function(){
            var self = this;
            self.collection.on('add reset', self.render, self);
            self.template = _.template($("#home-myLearning-template").html());
        },
        events: {},
        render: function() {
            var myLearningModel = this.collection.at(0);
            if(myLearningModel) {
                this.$el.html(this.template(myLearningModel.attributes));
            }
            return this;
        }
    });
    
    // Home Helpful Links Child Record View
    var helpfulLinksView = Backbone.View.extend({
        initialize: function(){
            var self = this;
            self.collection.on('add reset', self.render, self);
            self.template = _.template($("#home-helpfulLinks-template").html());
        },
        events: {
        },
        render: function() {
            var helpfulLinksModel = this.collection.at(0);
            if(helpfulLinksModel) {
                this.$el.html(this.template(helpfulLinksModel.attributes));
            }
            return this;
        }
    });
    
    // Home What's new Child Record View
    var whatsNewView = Backbone.View.extend({
        initialize: function(){
            var self = this;
            self.collection.on('add reset', self.render, self);
            self.template = _.template($("#home-whatsNew-template").html());
        },
        events: {},
        render: function() {
            var whatsNewModel = this.collection.at(0);
            if(whatsNewModel && whatsNewModel.attributes.whatsNewObj) {
                this.$el.html(this.template(whatsNewModel.attributes));
                
				// auto play carousel
                $('#topCarousel').carousel({
                    interval: 5000
                });

                // Initalize Swipe Functionality for Subcategory Carousel
                $("div[id='topCarousel']").swiperight(function(){
                    $(this).carousel('prev');
                });
                $("div[id='topCarousel']").swipeleft(function(){
                    $(this).carousel('next');
                });
            }

            return this;
        }
    });

    var courseCountView = Backbone.View.extend({
        initialize: function(){
            var self = this;
            self.collection.on('add reset', self.render, self);
            self.template = _.template($("#course-count-template").html());
        },
        events: {
            'click .content-category': 'navigateTo'
        },
        render: function() {
            var mainModel = this.collection.at(0);
            if(mainModel && mainModel.attributes) {
                this.$el.html(this.template(mainModel.attributes));
            }
            return this;
        },
        navigateTo: function(e) {
            e.preventDefault();
            var target = $(e.currentTarget).attr('data-target');
            if($(target) !== undefined) {
                $('html, body').animate({
                    scrollTop: $(target).offset().top - 80
                }, 1100);
            }
        }
    });

    return {
        getCollection: function() {
            return collection;
        },
        getSubCategoryView: function(){
            return subCategoryView;
        },
        getMyLearningView: function(){
            return myLearningView;
        },
        getHelpfulLinksView: function(){
            return helpfulLinksView;
        },
        getWhatsNewView: function(){
            return whatsNewView;
        },
        getCourseCountView: function(){
            return courseCountView;
        },
        spliceArray: function(data, n) {
            var lists = _.chain(data).groupBy(function(element, index){
                return Math.floor(index/n);
            }).toArray()
            .value();

            return lists;
        },
        createCategoryId: function(name) {
            return name.replace(/\s/g, '-').toLowerCase();
        },
        displayError: function(errorMsg){
            $('#errorPanel').find('p').html(errorMsg);

            if($('#pgBody').is(':visible')) {
                $('#pgBody').hide();
            } 
            
            $('#errorPanel').removeClass('hidden').fadeIn(); 
        },
        previewParam: '',
        isPreview: false,
        createPreviewJson: function(dateStr, businessUnit, division, location) {
            var retVal = {};
            if(dateStr && businessUnit && division, location) {
                retVal = {effectiveDate: (new Date(dateStr)).getTime(), division: division, businessUnit: businessUnit, location: location};
            }

            return (retVal) ? JSON.stringify(retVal) : '';
        },
        getContentTypeIcon: function(type) {
            var iconCls = 'core-icon';
            if(type == 'Video') {
                iconCls = 'video-icon';
            } else if(type == 'eLearning') {
                iconCls = 'elearning-icon';
            } else if(type == 'Instructor Led Training') {
                iconCls = 'instr-training-icon';
            } else if(type == 'Article or Book') {
                iconCls = 'articles-books-icon';
            } else if(type == 'BioTalk') {
                iconCls = 'biotalk-icon';
            } else if(type == 'Podcast') {
                iconCls = 'podcast-icon';
            } else if(type == 'Other Content Type') {
                iconCls = 'other-icon';
            } 

            return iconCls;
        }
    }
};
