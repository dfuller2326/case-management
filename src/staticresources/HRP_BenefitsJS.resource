/* Benefits Component Backbone Controller */

var Biogen_Benefits = function($, searchResultsMethod, remotingErrorMsg) {
	// Backbone sync override - A sync event is fired once the "request or GET" event completes successfully with the server.
	function sync(method, model, options) {
		switch (method) {
			case 'read':
				return getPageData(method, model, options);
			default:
				break;
		}
	}

	// Custom read function
	function getPageData(method, model, options) {
		Visualforce.remoting.Manager.invokeAction(searchResultsMethod,
			function(result, event) {
				if (event.status) {
					var response = $.parseJSON(result);
					if (response.error == null) {
						options.success(response);
					} else {
						Benefits.displayError(response.error.errMsg);
					}
				} else {
					Benefits.displayError(remotingErrorMsg);
				}
			}, {
				escape: false
			}
		);
	}

	// model
	var benefitsModel = Backbone.Model.extend({
		id: 'modelId',
		idAttribute: 'Id'
	});

	// collection
	var benefitsCollection = Backbone.Collection.extend({
		model: benefitsModel,
		sync: sync,
		initialize: function() {
			this.fetch();
		}
	});

	// Article Search Results View
	var tabView = Backbone.View.extend({
		initialize: function() {
			// Self variable for holding the context of the parent view
			var self = this;
			self.collection.on('add reset re-render', self.render, self);
			self.template = _.template($("#tabViewTemplate").html());
		},
		events: {
			'click .benefitsExpandIcon': 'showSummary',
			'click .benefitsCollapseIcon': 'collapseSummary',
		},
		showSummary: function(e) {
			var buttonId = $(e.currentTarget).prop("id");
			var currentTarget = $(e.currentTarget);
			if($(currentTarget).hasClass("glyphicon-plus")) {
				$(e.currentTarget).removeClass("glyphicon-plus").addClass("glyphicon-minus");
			} else {
				$(e.currentTarget).removeClass("glyphicon-minus").addClass("glyphicon-plus");
			}
			if(isMobile) {
				$("div[id=" + "'" + buttonId + "'" + "]").slideToggle("slow");
			} else {
				$("div[id=" + "'" + buttonId + "'" + "]").slideToggle("fast");
			}
		},
		render: function() {
			var model = this.collection.at(0);
			if (model && model.attributes.benefitsContent) {
				this.$el.html(this.template(model.attributes));
			}
			return this;
		}
	});

	return {
		getCollection: function() {
			return benefitsCollection;
		},
		getTabView: function() {
			return tabView;
		},
		displayError: function(errorMsg){
		    $('#errorPanel').find('p').html(errorMsg);
		    $('#errorPanel').removeClass('hidden').fadeIn(); 
		}
	}
}