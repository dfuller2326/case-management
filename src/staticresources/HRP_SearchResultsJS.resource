// Backbone controller for search results page
var Biogen_SearchResults = function($, searchResultsMethod, remotingErrorMsg, mediator) {
	// Backbone sync override - A sync event is fired once the "request or GET" event completes successfully with the server.
	function sync(method, model, options) {
		switch (method) {
			case 'read':
				return getPageData(method, model, options);
			default:
				break;
		}
	}

	// Custom read function
	function getPageData(method, model, options) {
		Visualforce.remoting.Manager.invokeAction(searchResultsMethod, SearchResults.searchString,
			function(result, event) {
				if (event.status) {
					var response = $.parseJSON(result);
					if (response.error == null) {
						options.success(response);
						if (response.content && response.content.records &&
							response.content.records.length > 0) {
							// Increase the offset when a successful response is returned
							SearchResults.offset += SearchResults.recordLimit;
						}
						// Function to build comparison JSON object for filters
						buildCompareJSON();
						// Function to build article Ids JSON object for filters
						buildArticleIdObjs();
					} else {
						SearchResults.displayError(response.error.errMsg);
					}
				} else {
					SearchResults.displayError(remotingErrorMsg);
				}
			}, {
				escape: false
			}
		);
	}

	// model
	var model = Backbone.Model.extend({
		id: 'article',
		idAttribute: 'Id',
		primaryResults: {
			ArticleNumber: '',
			ArticleType: '',
			DataCategorySelections: [],
			Id: '',
			KnowledgeArticleId: '',
			Language: '',
			LastPublishedDate: '',
			MasterVersionId: '',
			Summary: '',
			Title: ''
		},
		secondaryResults: {
			ArticleNumber: '',
			ArticleType: '',
			DataCategorySelections: [],
			Id: '',
			KnowledgeArticleId: '',
			Language: '',
			LastPublishedDate: '',
			MasterVersionId: '',
			Summary: '',
			Title: ''
		},
		dataCatMap: []
	});

	// collection
	var collection = Backbone.Collection.extend({
		model: model,
		sync: sync,
		initialize: function() {
			this.fetch();
		}
	});


	// Data Category Filter View
	var filterView = Backbone.View.extend({
		initialize: function() {
			// Self variable for holding the context of the parent view
			var self = this;
			self.collection.on('add reset orientationchange', self.render, self);
			self.template = _.template($("#article-filter-template").html());
		},
		events: {
			'change input[type=checkbox]': 'filter',
		},
		render: function() {
			var filterModel = this.collection.at(0);
			if (filterModel && filterModel.attributes.dataCatMap) {
				this.$el.html(this.template(filterModel.attributes));
			}
			return this;
		},
		// filter: function(e) {
		// 	var isChecked = $(e.currentTarget).prop('checked');
		// 	var resultsModel = this.collection.at(0);
		// 	var checkedList = [];
		// 	var newPrimaryList = [];
		// 	var newSecondaryList = [];
		// 	if(resultsModel === undefined || resultsModel.attributes.primaryResults === undefined || resultsModel.attributes.secondaryResults === undefined) {
		// 		return;
		// 	}
		// 	// Build Array of Strings of All Filter Values that are checked
		// 	$("input[type=checkbox][data-categoryName]").filter(":checked").each(function(index, obj) {
		// 		checkedList.push($(obj).attr("data-categoryname"));
		// 	});
		// 	// Get list of article ids for each filter that is checked
		// 	var articleIdList = findArticleIds(checkedList);

		// 	// Get list of article objects for the array of article ids found
		// 	var articleObjectList = findArticleObjects(articleIdList);

		// 	// Set list of article objects equal to the list displayed on the page
		// 	$.each(articleObjectList, function(index, articleObj) {
		// 		if(articleObj.ArticleType === "FAQ__kav") {
		// 			newSecondaryList.push(articleObj);
		// 		} else {
		// 			newPrimaryList.push(articleObj);
		// 		}
		// 	});

		// 	SearchResults.primaryFilterList = (newPrimaryList.length === 0) ? [] : newPrimaryList;
		// 	SearchResults.secondaryFilterList = (newSecondaryList.length === 0) ? [] : newSecondaryList;

		// 	// Get results view
		// 	if(SearchResults.primaryFilterList && SearchResults.secondaryFilterList) {
		// 		resultsSectionView.render();
		// 		qaSectionView.render();
		// 	}
		// }
	});

	// Article Search Results View
	var resultsView = Backbone.View.extend({
		initialize: function() {
			// Self variable for holding the context of the parent view
			var self = this;
			self.collection.on('add reset orientationchange', self.render, self);
			self.template = _.template($("#article-search-results-template").html());
		},
		events: {
			'click #primaryResultsNextBtn': 'showMore',
			'click #primaryResultsPrevBtn': 'showLess',
		},
		// showMore: function() {
		// 	if((SearchResults.primaryResultsOffset + SearchResults.primaryResultsLimit) > SearchResults.primaryResultsLength) {
		// 		return;
		// 	} else {
		// 		SearchResults.primaryResultsOffset += SearchResults.primaryResultsLimit;
		// 	}
		// 	this.render();
		// },
		// showLess: function() {
		// 	if(SearchResults.primaryResultsOffset === 0) {
		// 		return;
		// 	} else {
		// 		SearchResults.primaryResultsOffset -= SearchResults.primaryResultsLimit;
		// 		this.render();
		// 	}
		// },
		render: function() {
			var primaryResultsModel = this.collection.at(0);
			var tempList = [];
			SearchResults.primaryResultsLength = 0;
			// Reset the offset and empty the filter list if a filter has been applied
			if(SearchResults.filtersApplied) {
				if(SearchResults.primaryFilterList.length > 0) {
					SearchResults.primaryResultsOffset = 0;
					SearchResults.primaryStartShowing = SearchResults.primaryResultsOffset + 1;
					tempList = SearchResults.primaryFilterList;
					SearchResults.primaryResultsLength = tempList.length;
					SearchResults.primaryFilterList = [];

				} else {
					SearchResults.primaryResultsOffset = 0;
					SearchResults.primaryStartShowing = 0;
					SearchResults.primaryEndShowing = 0;
				}
			} else {
				tempList = primaryResultsModel.attributes.primaryResults;
				SearchResults.primaryResultsLength = tempList.length;
				if(tempList.length > 0) {
					SearchResults.primaryStartShowing = SearchResults.primaryResultsOffset + 1;
				} else {
					SearchResults.primaryStartShowing = 0;
				}
			}

			if (primaryResultsModel && SearchResults.primaryFilterList) {
				if(SearchResults.primaryResultsOffset < SearchResults.primaryResultsLength && SearchResults.primaryResultsLength !== 0) {
					SearchResults.primaryEndShowing = (SearchResults.primaryResultsOffset + SearchResults.primaryResultsLimit < SearchResults.primaryResultsLength) ? SearchResults.primaryResultsOffset + SearchResults.primaryResultsLimit : SearchResults.primaryResultsLength;
					SearchResults.primaryResultsList = tempList.slice(SearchResults.primaryResultsOffset, SearchResults.primaryResultsOffset + SearchResults.primaryResultsLimit);
				}
			}

			this.$el.html(this.template(SearchResults.primaryResultsList));
			return this;
		}
	});

	// Article Search Results View
	var qaView = Backbone.View.extend({
		initialize: function() {
			// Self variable for holding the context of the parent view
			var self = this;
			self.collection.on('add reset orientationchange', self.render, self);
			self.template = _.template($("#question-answer-template").html());
		},
		events: {
			'click #secondaryResultsNextBtn': 'showMore',
			'click #secondaryResultsPrevBtn': 'showLess',
			'click .qaTitleIcon': 'showAnswer'
		},
		// showAnswer: function(e) {
		// 	var buttonId = $(e.currentTarget).prop("id");
		// 	var currentTarget = $(e.currentTarget);
		// 	if($(currentTarget).hasClass("glyphicon-plus")) {
		// 		$(e.currentTarget).removeClass("glyphicon-plus").addClass("glyphicon-minus");
		// 	} else {
		// 		$(e.currentTarget).removeClass("glyphicon-minus").addClass("glyphicon-plus");
		// 	}
		// 	if(isMobile) {
		// 		$(".searchResultSummary[id=" + "'" + buttonId + "'" + "]").fadeToggle("slow");
		// 	} else {
		// 		$(".searchResultSummary[id=" + "'" + buttonId + "'" + "]").slideToggle("fast");
		// 	}
		// },
		// showMore: function() {
		// 	if((SearchResults.secondaryResultsOffset + SearchResults.secondaryResultsLimit) > SearchResults.secondaryResultsLength) {
		// 		return;
		// 	} else {
		// 		SearchResults.secondaryResultsOffset += SearchResults.secondaryResultsLimit;
		// 	}
		// 	this.render();
		// },
		// showLess: function() {
		// 	if(SearchResults.secondaryResultsOffset === 0) {
		// 		return;
		// 	} else {
		// 		SearchResults.secondaryResultsOffset -= SearchResults.secondaryResultsLimit;
		// 		this.render();
		// 	}
		// },
		render: function() {
			var secondaryResultsModel = this.collection.at(0);
			var tempList = [];
			SearchResults.secondaryResultsLength = 0;
			// Reset the offset and empty the filter list if a filter has been applied
			if(SearchResults.filtersApplied) {
				if(SearchResults.secondaryFilterList.length > 0) {
					SearchResults.secondaryResultsOffset = 0;
					SearchResults.secondaryStartShowing = SearchResults.secondaryResultsOffset + 1;
					tempList = SearchResults.secondaryFilterList;
					SearchResults.secondaryResultsLength = tempList.length;
					SearchResults.secondaryFilterList = [];

				} else {
					SearchResults.secondaryResultsOffset = 0;
					SearchResults.secondaryStartShowing = 0;
					SearchResults.secondaryEndShowing = 0;
				}

			} else {
				tempList = secondaryResultsModel.attributes.secondaryResults;
				SearchResults.secondaryResultsLength = tempList.length;
				if(tempList.length > 0) {
					SearchResults.secondaryStartShowing = SearchResults.secondaryResultsOffset + 1;
				} else {
					SearchResults.secondaryStartShowing = 0;
				}
			}

			if (secondaryResultsModel && SearchResults.secondaryFilterList) {
				if(SearchResults.secondaryResultsOffset < SearchResults.secondaryResultsLength && SearchResults.secondaryResultsLength !== 0) {
					SearchResults.secondaryEndShowing = (SearchResults.secondaryResultsOffset + SearchResults.secondaryResultsLimit < SearchResults.secondaryResultsLength) ? SearchResults.secondaryResultsOffset + SearchResults.secondaryResultsLimit : SearchResults.secondaryResultsLength;
					SearchResults.secondaryResultsList = tempList.slice(SearchResults.secondaryResultsOffset, SearchResults.secondaryResultsOffset + SearchResults.secondaryResultsLimit);
				}
			}

			this.$el.html(this.template(SearchResults.secondaryResultsList));
			return this;
		}
	});

	function buildCompareJSON() {
		// Build JSON object for Article Filter Comparison when Data is Successfully Returned from the Server
		var responseModel = this.collection.at(0);
		// Check for Content Category filters
		if(responseModel.attributes.dataCatMap.Content_Category === undefined || responseModel.attributes.dataCatMap.Content_Category === null || responseModel.attributes.dataCatMap.Content_Category[0].category.childCategories.length === 0) {
			SearchResults.displayError('Advanced Filters not Found or Filter List is empty. Please create Content_Category group or data categories in Salesforce Environment.');
			return;
		}
		$.each(responseModel.attributes.dataCatMap.Content_Category[0].category.childCategories, function(index, dataGroupObj) {
			// Temporary array for adding each list of article objects for each advanced filter category
			var tempArray = [];
			// Loop through all articles for each category
			$.each(responseModel.attributes.primaryResults, function(index, articleObj) {
				// Loop through Data Category Selections for Each Article if the article has data categories assigned
				if(articleObj.DataCategorySelections === undefined || articleObj.DataCategorySelections === null) {
					return;
				}

				$.each(articleObj.DataCategorySelections.records, function(index, dataCatSelectionObj) {
					if(dataCatSelectionObj.DataCategoryGroupName === 'Content_Category') {
						if(dataCatSelectionObj.DataCategoryName === dataGroupObj.name) {
							tempArray.push({"articleId" : articleObj.Id});
						}
					}
				});
			});
			$.each(responseModel.attributes.secondaryResults, function(index, articleObj) {
				// Loop through Data Category Selections for Each Article if the article has data categories assigned
				if(articleObj.DataCategorySelections === undefined || articleObj.DataCategorySelections === null) {
					return;
				}

				$.each(articleObj.DataCategorySelections.records, function(index, dataCatSelectionObj) {
					if(dataCatSelectionObj.DataCategoryGroupName === 'Content_Category') {
						if(dataCatSelectionObj.DataCategoryName === dataGroupObj.name) {
							tempArray.push({"articleId" : articleObj.Id});
						}
					}
				});
			});
			var dataGroupObjKey = dataGroupObj.name;
			SearchResults.compareToArray.push({dataGroupObjKey : tempArray});
		});
		console.log(SearchResults.compareToArray);
	}

	// function buildArticleIdObjs() {
	// 	// Build map of article Ids to Article Objects
	// 	var responseModel = this.collection.at(0);
	// 	var primaryList = responseModel.attributes.primaryResults;
	// 	var secondaryList = responseModel.attributes.secondaryResults;
	// 	if(primaryList === undefined || primaryList === null) {
	// 		return;
	// 	}
	// 	$.each(primaryList, function(index, articleObj) {
	// 		SearchResults.articleIdsObj.push({[articleObj.Id] : articleObj});
	// 	});
	// 	$.each(secondaryList, function(index, articleObj) {
	// 		SearchResults.articleIdsObj.push({[articleObj.Id] : articleObj});
	// 	});
	// }

	// function findArticleIds(filterList) {
	// 	var articleIdList = [];
	// 	var compareKeyList = [];
	// 	var intersectionList = [];
	// 	var intersectionStr = '';
	// 	if(filterList.length > 0) {
	// 		SearchResults.filtersApplied = true;
	// 	} else {
	// 		SearchResults.filtersApplied = false;
	// 	}
	// 	$.each(filterList, function(filterListIndex, filterName) {
	// 		$.each(SearchResults.compareToArray, function(compareIndex, compareToObj) {
	// 			$.each(Object.keys(compareToObj), function(compareObjIndex, key){
	// 				if(key === filterName) {
	// 					var filterItemList = [];
	// 					$.each(compareToObj[filterName], function(filterNameIndex, article) {
	// 						filterItemList.push(article.articleId);
	// 					});
	// 					intersectionList.push(filterItemList);
	// 				}
	// 			});
	// 		});
	// 	});

	// 	// User Underscore intersection method to only get the article Ids that match all the filters applied
	// 	articleIdList = _.intersection.apply(_, intersectionList);
	// 	// Log matched article ids to the console for testing purposes
	// 	console.log("Matching Articles Found :: " + JSON.stringify(articleIdList));
	// 	return articleIdList;
	// }

	// function findArticleObjects(filterIdList) {
	// 	var articleObjList = [];
	// 	$.each(filterIdList, function(filterIdIndex, filterId) {
	// 		$.each(SearchResults.articleIdsObj, function(idObjIndex, articleObj) {
	// 			$.each(Object.keys(articleObj), function(keyIndex, key) {
	// 				if(filterId === key) {
	// 					articleObjList.push(articleObj[key]);
	// 				}
	// 			});
	// 		});
	// 	});
	// 	return articleObjList;
	// }

	// function getLabel(groupName, catName){
	// 	var dataCatMap = this.collection.at(0).attributes.dataCatMap;
	// 	var objMap = dataCatMap[groupName][0].category.childCategories;
	//     var cat = _.findWhere(objMap, {name: catName}) || _.findWhere(objMap[0].childCategories, {name: catName}) || _.findWhere(_.findWhere(objMap[0].childCategories, {name: 'Denmark'}).childCategories, {name: catName});
	// 	return cat;
	// }

	return {
		getCollection: function() {
			return collection;
		},
		getFilterView: function() {
			return filterView;
		},
		getResultsView: function() {
			return resultsView;
		},
		getQAView: function() {
			return qaView;
		},
		getLabel: function(groupName, catName) {
			return getLabel(groupName, catName);
		},
		searchResultsOffset: 0,
		primaryResultsOffset: 0,
		secondaryResultsOffset: 0,
		primaryResultsLimit: 10,
		secondaryResultsLimit: 5,
		searchString: '',
		primaryResultsList: [],
		secondaryResultsList: [],
		showMore: false,
		primaryStartShowing: 0,
		primaryEndShowing: 0,
		secondaryStartShowing: 0,
		secondaryEndShowing: 0,
		totalShowing: 0,
		primaryResultsLength: 0,
		secondaryResultsLength: 0,
		compareToArray: [],
		articleIdsObj: [],
		primaryFilterList: [],
		secondaryFilterList: [],
		filtersApplied: false,
		displayError: function(errorMsg){
		    $('#errorPanel').find('p').html(errorMsg);

		    if($('#pgBody').is(':visible')) {
		        $('#pgBody').hide();
		    } 
		    
		    $('#errorPanel').removeClass('hidden').fadeIn(); 
		}
	}
}