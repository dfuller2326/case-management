<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Key Milestones and Deliverables</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Actual_Completion_Date__c</fullName>
        <externalId>false</externalId>
        <label>Actual Completion Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Confirmed__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Confirmation by the Delivery Manager that a key deliverable has been completed</description>
        <externalId>false</externalId>
        <inlineHelpText>Confirmed by the SFDC Delivery Manager</inlineHelpText>
        <label>Confirmed</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Date__c</fullName>
        <description>Milestone Date</description>
        <externalId>false</externalId>
        <label>Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>Milestone Description</description>
        <externalId>false</externalId>
        <label>Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Fixed_Fee_Payment__c</fullName>
        <description>Designates payment to be made when milestone is completed in a fixed fee project.</description>
        <externalId>false</externalId>
        <inlineHelpText>Designates payment to be made when milestone is completed in a fixed fee project.</inlineHelpText>
        <label>Fixed Fee Payment</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Key_Deliverable__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Indicates a key deliverable milestone</description>
        <externalId>false</externalId>
        <inlineHelpText>Is this milestone a key deliverable in the project (i.e. a formal signoff is required by the Client)?</inlineHelpText>
        <label>Key Deliverable?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Milestone_Status_Icon__c</fullName>
        <description>Milestone Status Icon</description>
        <externalId>false</externalId>
        <formula>IMAGE( 
CASE(   Status__c  , 
&quot;New&quot;, &quot;/img/samples/stars_100.gif&quot;, 
&quot;In Process&quot;, &quot;/img/samples/stars_300.gif&quot;, 
&quot;On Hold&quot;, &quot;/img/samples/flag_red.gif&quot;, 
&quot;Complete&quot;, &quot;/img/samples/stars_500.gif&quot;, 
&quot;/s.gif&quot;), 
&quot;milestone status icon&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Milestone Status Icon</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Milestone__c</fullName>
        <description>Milestone Name</description>
        <externalId>false</externalId>
        <label>Milestone</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Original_Completion_Date__c</fullName>
        <description>As defined by the original project timeline during the planning stage of the project</description>
        <externalId>false</externalId>
        <inlineHelpText>As defined by the original project timeline during the planning stage of the project</inlineHelpText>
        <label>Original Completion Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Phase__c</fullName>
        <externalId>false</externalId>
        <label>Phase</label>
        <picklist>
            <picklistValues>
                <fullName>1 - Plan</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>2 - Analyze</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>3 - Design</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>4 - Build &amp; Validate</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5 - Deploy</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>6 - Closure</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Progress_Flag__c</fullName>
        <externalId>false</externalId>
        <formula>IMAGE( 
CASE(  Progress__c , 
&quot;Green&quot;, &quot;/img/samples/flag_green.gif&quot;, 
&quot;Yellow&quot;, &quot;/img/samples/flag_yellow.gif&quot;, 
&quot;Red&quot;, &quot;/img/samples/flag_red.gif&quot;, 
&quot;/s.gif&quot;), 
&quot;cust sat indicator&quot;)</formula>
        <label>Progress Flag</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Progress__c</fullName>
        <description>G = on target to meet completion date
Y = delayed, but no impact to overall project schedule
R = delayed, will impact go live date</description>
        <externalId>false</externalId>
        <inlineHelpText>G = on target to meet completion date
Y = delayed, but no impact to overall project schedule
R = delayed, will impact go live date</inlineHelpText>
        <label>Progress</label>
        <picklist>
            <picklistValues>
                <fullName>Green</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Yellow</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Red</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DONE</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Project_Overview__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Related Project</description>
        <externalId>false</externalId>
        <label>Project Overview</label>
        <referenceTo>Project_Overview__c</referenceTo>
        <relationshipLabel>Project Milestones and Deliverables</relationshipLabel>
        <relationshipName>R00N80000002QnvmEAC</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Revised_Estimated_Completion_Date__c</fullName>
        <description>If the estimate changes, enter the new date here</description>
        <externalId>false</externalId>
        <inlineHelpText>If the estimate changes, enter the new date here</inlineHelpText>
        <label>Revised Estimated Completion Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Status</description>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>New</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>On Hold</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>In Process</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Complete</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Project Milestones and Deliverables</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Date__c</columns>
        <columns>Milestone__c</columns>
        <columns>Progress__c</columns>
        <columns>Status__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>{0000000000}</displayFormat>
        <label>Project Milestones and Deliverables</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Project Milestones and Deliverables</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Project_Overview__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Milestone__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Description__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Project_Overview__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Milestone__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Project_Overview__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Milestone__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Description__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Date__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>Project_Overview__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Milestone__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Description__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
