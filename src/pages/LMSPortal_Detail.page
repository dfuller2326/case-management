<apex:page showHeader="false" sidebar="false" standardStylesheets="false" standardController="LMS_Content__c" extensions="LMSPortal_DetailCtrl">
	<head>
	    <meta charset="utf-8"/>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0"/>
	    <title>{!$Label.University_Portal_Title}</title>
	    <apex:stylesheet value="{!URLFOR($Resource.LMSPortalResources, '/css/salesforce1style.css')}"/>
		<apex:stylesheet value="{!URLFOR($Resource.LMSPortalResources, '/css/bootstrap.min.css')}"/>
		<apex:stylesheet value="{!URLFOR($Resource.LMSPortalResources, '/css/bootstrap-datepicker.css')}"/>
		<apex:stylesheet value="{!URLFOR($Resource.LMSPortalResources, '/css/icons_50X50.css')}"/>
		<apex:stylesheet value="{!$Resource.LMSPortalMainCSS}"/>		
	</head>

	<!-- Page Body -->
	<body>
		<!-- Header Component -->
		<c:LMSPortal_Header showAttribute="false"/>
		<!-- Container -->
		<div class="container" id="detail-container">
			<!-- error panel -->
			<div class="alert alert-danger hidden" role="alert" id="errorPanel">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
				<span class="h4">{!$Label.Error_Panel_Head}</span>
				<p></p>
			</div>
			<div id="pgBody">
				<div id="learning-content-container"></div>
				<!-- Scroll to Top icon div -->
                <div id="toTop" class="icon-float">
                    <img src="{!URLFOR($Resource.LMSPortalResources, '/images/BacktoTop.png')}"/>
                </div>
                <!-- To Home icon div -->
                <div id="toHome" class="icon-float icon-float-home">
                    <a href="/apex/LMSPortal_Home" target="_parent">
                        <img src="{!URLFOR($Resource.LMSPortalResources, '/images/Home.png')}"/>
                    </a>
                </div>
			</div>	
		</div>    					
		<!-- Footer --> 
		<footer>  
			<c:LMSPortal_Footer />
		</footer>
	</body>
	<!-- Javascript Libraries -->
	<apex:includeScript value="{!URLFOR($Resource.LMSPortalResources, '/js/jquery.min.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.LMSPortalResources, '/js/bootstrap.min.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.LMSPortalResources, '/js/bootstrap-datepicker.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.LMSPortalResources, '/js/json2.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.LMSPortalResources, '/js/underscore.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.LMSPortalResources, '/js/backbone.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.LMSPortalResources, '/js/mediator.min.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.LMS_PortalDetail)}"/>	

	<script type="text/template" id="learning-content-template">
		<div id="learning-content-detail">
			<div class="panel">
				<div class="panel-heading panel-heading-branding text-center">
					<h3 class="mvn"><%= title %></h3>
				</div>
				<div class="panel-body nhp">
					<!-- back button -->
					<div class="ptxs pbl">
						<img id="backBtn" role="button" class="btn-back" src="{!URLFOR($Resource.LMSPortalResources, '/images/BackButton.png')}"/>
					</div>
					<% if((bannerDocumentId != null && bannerDocumentId != '') && (contentType != "Video")) { %>
						<div class="pvm">
							<a href="<%= externalMediaURL %>" target="_blank" role="button">
								<img class="img-responsive content-detail-banner" src="/servlet/servlet.FileDownload?file=<%= bannerDocumentId %>" alt="No Banner Image Available"/>
							</a>
						</div>
					<% } %>
					<div class="col-xs-12 col-lg-9 nhpl">
						<% if(summary != '' && description != '' && leaderShipCompetency !='') { %>
							<% if((targetWindow === "iFrame" && contentType === "Video" && externalMediaURL.length > 0) && (targetWindow != "New Window"))  { %>
								<div class="col-md-12 mbs nhpl">
									<div class="embed-responsive embed-responsive-16by9">
										<iframe class="embed-responsive-item" 
									          src="<%= externalMediaURL %>"
									          allowfullscreen="true" id="mediaFrame" seamless="seamless"></iframe>
									</div>							
								</div>                
			                <% } %> 
							<div class="col-md-12 mbs nhpl">
								<div class="paragraphs">
							  	  	<span><img class=" detailIcon dim_50X50 <%= iconClassName %>" /></span>
							  	  	<% if(duration && duration.length > 0) { %>
							  	  		<span class="durationDesc"><%= duration %>&nbsp;<%= durationUnit %>.</span>
							  	  	<% } %>
							      	<span class="detailSummary"><%= summary %></span>
								</div>
							</div>
							<% if(description && description.length > 0) { %>
								<br/>
								<div class="col-md-12 nhpl mvs" id="description">
									<div><%= description %></div>
								</div>
							<% } %>
							<% if(functionalCompetency && functionalCompetency.length > 0) { %>
								<div class="col-md-12 nhpl mvs">
									<span class="detailCompetency">{!$Label.Functional_Competency_Label}&nbsp;</span>
										<span class="font-weight-500"><%= functionalCompetency %></span>
										<% if(subFunctionalCompetencies) { %>
											&nbsp;-&nbsp;<span class="font-weight-500"><%= subFunctionalCompetencies %></span>
										<% } %>
								</div> 
							<% } %>
							<% if(leaderShipCompetency && leaderShipCompetency.length > 0) { %>
								<div class="col-md-12 nhpl mvs">
									<span class="detailCompetency">{!$Label.Leadership_Competency_Label}:&nbsp;</span>
									<span class="font-weight-500"><%= leaderShipCompetency%></span>
								</div> 
							<% } %> 
	                        <% if(miscellaenous && miscellaenous.length > 0 && miscellaenous != 'N/A') { %>
								<div class="col-md-12 nhpl mvs">
									<span class="detailCompetency">{!$Label.Miscellaneous_Label}:&nbsp;</span>
									<span class="font-weight-500"><%= miscellaenous %></span>
								</div> 
							<% } %>
							<% if(targetWindow === "New Window") { %>
								<div class="col-md-12 mvs nhpl">
									<a href="<%= externalMediaURL %>" target="_blank">
										<button class="btn" type="button">{!$Label.View_Lbl}&nbsp;<%= contentType %></button>
									</a>
								</div>						
			                <% } %>
						<% } else { %>	
							<h4 class="text-center">{!$Label.No_Detail_Found_Msg}</h4>
						<% } %>
					</div>
					<div class="col-xs-12 col-lg-3 text-right" >						
						<% if(bannerDocumentId != null && bannerDocumentId != '' && contentType != "Video") { } else { %>
							<div class="row">
								<a href="<%= externalMediaURL %>" target="_blank" role="button">
									<img class="img-responsive content-detail-thumbnail" src="/servlet/servlet.FileDownload?file=<%= thumbNailDocumentId %>" alt="No Banner Image Available"/>
								</a>
							</div>
							<br/>
						<% } %>
						<div class="row" id="related-learning-content">
							<div class="pvm">
								<span class="relatedContentTitle">{!$Label.Related_Content_Heading}</span>
							</div>
							<% if(relatedContent != '') { %>
								<div id ="relatedContent"><%= relatedContent %></div>
							<% } else { %>	
								<h4 class="text-right">{!$Label.No_Records_Found}</h4>
							<% } %>
						</div>
					</div>
				</div>
			</div>
		</div>
	</script>

	<script type="text/javascript">
		var $j = jQuery.noConflict();
		$j('#pgBody').hide();

		// mediator instance for publish/subscribe custom event
	   	var mediatorObj = new Mediator();
	    
	    var remotingErrorMsg = '{!$Label.Remoting_Error_Message}';
	    
	    // Backbone setup
	    var ViewDetail = Biogen_Detail($j, '{!$RemoteAction.LMSPortal_DetailCtrl.getContentDetail}', remotingErrorMsg);
	    ViewDetail.contentId = "{!$CurrentPage.Parameters.Id}";
	    var ViewDetail_Collection = ViewDetail.getCollection();
	    var ViewDetail_ContentView = ViewDetail.getContentView();
	    var collection = new ViewDetail_Collection();

		$j(document).ready(function() {
	        /* content detail view */
	        var contentView = new ViewDetail_ContentView({
	            el: '#learning-content-container',
	            collection: collection
	        });

			// event listener for back button click event
			$j('body').on('click', '#backBtn', function() {
			    window.history.back();
			});

	        // listen to scroll event to show toTop icon
	        $j(window).scroll(function() {
		        if($j(this).scrollTop() != 0) {
		            $j('#toTop').fadeIn();    
		        } else {
		            $j('#toTop').fadeOut();
		        }
		    });

	        // event listener for toTop button click event
	        $j('#toTop').click(function() {
	            scrollToTop();
	        });  	        		 
		});
		
		$j( window ).load(function() {
			setTimeout(function(){ 
				$j('#description img').addClass('img-responsive margin0auto');
				$j('#relatedContent img').addClass('img-responsive margin0auto');
			}, 500);
		});

		function scrollToTop() {
            $j("html, body").animate({
                scrollTop: 0
            }, 1000);
        }
	</script>
</apex:page>