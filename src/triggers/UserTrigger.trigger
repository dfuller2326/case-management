trigger UserTrigger on User (before insert,after insert,before update, after update) {
    //The user record updates are done in before insert and before update.
   
    if(Trigger.isBefore){
          if(Trigger.isUpdate){
       
               List< User> userList = new List<User >();
               for (User modUser:Trigger.new){
                            User nonModUser = Trigger.oldMap.get(modUser.id);
                            if(modUser.Username <> nonModUser.Username){
                                  modUser.Username=nonModUser.Username;
                            }
                     }
       }       
        UserTriggerUtils.performUserUpdates(Trigger.new,Trigger.oldMap);
    }
    //the perm set updates are done in after insert and after update process as it is a related entity to the User.
    else{
           
       UserTriggerUtils.performPermSetUpdates(Trigger.newMap , Trigger.oldMap);
       //upsert contacts based on user record updates
       UserTriggerUtils.performContactUpserts(Trigger.newMap.keySet());
       UserTriggerUtils.addToAllBiogenPublicGroup(Trigger.newMap.keySet());
       UserTriggerUtils.managerhr4hrpublicGrp(Trigger.new);
    }
    
}