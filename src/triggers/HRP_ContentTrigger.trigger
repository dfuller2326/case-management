trigger HRP_ContentTrigger on HRP_Content__c (before insert, before update) {
  HRP_ContentTriggerHandler handler = new HRP_ContentTriggerHandler();
  if(Trigger.isInsert && Trigger.isBefore) {
    handler.OnBeforeInsert(Trigger.new);
  } else if(Trigger.isUpdate && Trigger.isBefore) {
    handler.OnBeforeUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
  }
}