trigger CaseTrigger on Case (before insert, before update, after insert, after update) {
	//update case entities in before trigger
	if(Trigger.isBefore){
        CaseTriggerUtils.performCaseUpserts(Trigger.new);
        // Update the Case's contact language and escalations if the case is created by a user
        CaseTriggerUtils.updateCaseDetails(Trigger.new); 

        //After update the records
       if (Trigger.isUpdate) {
        	CaseTriggerUtils.performCaseUpdates(Trigger.newMap, Trigger.oldMap);
    	}
	}
	//update case team entities in after trigger
	else{
       CaseTriggerUtils.performCaseTeamUpserts(Trigger.newMap);
    }
    
}