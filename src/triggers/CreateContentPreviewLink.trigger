trigger CreateContentPreviewLink on ContentVersion (before insert, before update) {

    for (ContentVersion cv : trigger.new) {
        cv.Content_Preview_Link__c = 'http://' + System.URL.getSalesforceBaseUrl().getHost() + '/apex/HRConnectContentPreview?fileId=' + String.ValueOf(cv.ContentDocumentId);

    }
}