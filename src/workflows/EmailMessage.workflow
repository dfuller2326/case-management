<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Email_Message_Count</fullName>
        <description>Increment Email Message sent</description>
        <field>Number_of_Email_Sent__c</field>
        <formula>IF(ISNULL(Parent.Number_of_Email_Sent__c), 1, Parent.Number_of_Email_Sent__c + 1)</formula>
        <name>Email Message Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case Email Count</fullName>
        <actions>
            <name>Email_Message_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Count the Email Messages being sent per case</description>
        <formula>Incoming == False &amp;&amp;
 ( ToAddress ==  Parent.Contact.Email ||
 ToAddress  == Parent.On_Behalf_Of2__r.Email )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
