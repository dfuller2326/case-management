<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Active</fullName>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Update Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Active_to_False</fullName>
        <field>IsActive</field>
        <literalValue>0</literalValue>
        <name>Update Active to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_New_Hire_2_Regular</fullName>
        <description>Update the regular employee field</description>
        <field>IsRegularEmployee__c</field>
        <literalValue>1</literalValue>
        <name>Update New Hire 2 Regular</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Hire Check</fullName>
        <active>true</active>
        <description>Workflow rule that checks that a user is still a new hire based on hire date.</description>
        <formula>AND(  NOT(ISNULL(Hire_Date__c))  ,NOT(ISBLANK(Hire_Date__c))  ,TODAY()- Hire_Date__c &lt;= 90  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_New_Hire_2_Regular</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>User.Hire_Date__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Termination Date Added</fullName>
        <active>true</active>
        <criteriaItems>
            <field>User.Termination_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to update the user from Active to Inactive one day after the Termination Date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Active_to_False</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>User.Termination_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
