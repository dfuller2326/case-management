<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_Case_You_Own_has_Been_Reopened</fullName>
        <description>A Case You Own has Been Reopened</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/Case_Reopened</template>
    </alerts>
    <alerts>
        <fullName>An_ADA_For</fullName>
        <description>An ADA Form is at it&apos;s Due Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/ADA_Due_Date</template>
    </alerts>
    <alerts>
        <fullName>An_ADA_Form_is_at_it_s_2nd_Due_Date</fullName>
        <description>An ADA Form is at it&apos;s 2nd Due Date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/ADA_Due_Date</template>
    </alerts>
    <alerts>
        <fullName>Case_Closed</fullName>
        <description>Case Closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/Case_Has_Been_Closed</template>
    </alerts>
    <alerts>
        <fullName>Case_On_Behalf_of_Closed</fullName>
        <description>Case On Behalf of Closed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/Case_Submitted_Other_User_Closed</template>
    </alerts>
    <alerts>
        <fullName>Contact_Email_when_Comment_Added_to_Case</fullName>
        <description>Contact Email when Comment Added to Case</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/A_Comment_Has_Been_Added_to_Your_Case</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_EU_Case_Holiday_Shutdown_message</fullName>
        <description>Email Alert: EU Case Holiday Shutdown message</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/EU_Case_Holiday_message</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_US_and_Executive_Case_Holiday_Shutdown_message</fullName>
        <description>Email Alert: US and Executive Case Holiday Shutdown message</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/US_and_Executive_Case_Holiday_Shutdown_message</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_notify_COE_Case_Owner_when_added</fullName>
        <description>Email alert to notify COE Case Owner when added</description>
        <protected>false</protected>
        <recipients>
            <field>COE_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/COE_Owner_For_Escalated_Case</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_notify_Case_Escalated_to_COE_IT_Functional</fullName>
        <description>Email alert to notify Case Escalated to COE - IT Functional</description>
        <protected>false</protected>
        <recipients>
            <recipient>COE_IT_Functional</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/Case_Escalated_to_COE</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_notify_Case_Escalated_to_COE_IT_Support</fullName>
        <description>Email alert to notify Case Escalated to COE - IT Support</description>
        <protected>false</protected>
        <recipients>
            <recipient>COE_IT_Support</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/Case_Escalated_to_COE</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_notify_Case_Escalated_to_HRIS_COE</fullName>
        <description>Email alert to notify Case Escalated to HRIS COE</description>
        <protected>false</protected>
        <recipients>
            <recipient>COE_HRIS_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/Case_Escalated_to_COE</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_notify_Case_Owner_and_IT_for_Escalation_Status_Change_to_Transfer</fullName>
        <description>Email alert to notify Case Owner for Escalation Status Change to Transferred to IT</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/Status_change_for_Escalated_Case</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_notify_Case_Owner_for_Case_Re_Escalation</fullName>
        <description>Email alert to notify Case Owner for Case Re-Escalation</description>
        <protected>false</protected>
        <recipients>
            <field>COE_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/COE_Owner_For_Re_Escalated_Case</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_notify_Case_Owner_for_Escalation_Status_Change</fullName>
        <description>Email alert to notify Case Owner for Escalation Status Change</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/Status_change_for_Escalated_Case</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_notify_IT_for_Escalation_Status_Change_to_Transferred_to_IT</fullName>
        <ccEmails>ithr.issues@biogen.com</ccEmails>
        <description>Email alert to notify IT for Escalation Status Change to Transferred to IT</description>
        <protected>false</protected>
        <recipients>
            <recipient>manish.shrestha@biogen.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/Email_to_IT_for_Escalated_Status_Transferred_to_IT</template>
    </alerts>
    <alerts>
        <fullName>Executive_Case_Open_4_hours</fullName>
        <description>Executive Case Open &gt; 4 hours</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/Executive_Case_Open</template>
    </alerts>
    <alerts>
        <fullName>HR_Group_Manager_Case_Open_25_days</fullName>
        <description>HR: Group Manager Case Open &gt; 25 days</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Group_Manager</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/HR_Case_Open_24_Hours</template>
    </alerts>
    <alerts>
        <fullName>HR_Notify_Case_Owner_for_Case_Waiting_for_HRBP</fullName>
        <description>HR: Notify Case Owner for Case Waiting for HRBP</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/HR_Case_Updated_to_Waiting_for_HRBP</template>
    </alerts>
    <alerts>
        <fullName>HR_Notify_Case_Owner_for_Case_Waiting_for_Vendor</fullName>
        <description>HR: Notify Case Owner for Case Waiting for Vendor</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/HR_Case_Updated_to_Waiting_for_Vendor</template>
    </alerts>
    <alerts>
        <fullName>HR_Notify_Group_Manager_Case_Open_10_Days</fullName>
        <description>HR: Notify Group Manager Case Open &gt; 10 Days</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Group_Manager</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/HR_Case_Open_24_Hours</template>
    </alerts>
    <alerts>
        <fullName>HR_Notify_HR_Coordinator_1_for_Open_Case_After_24_hours</fullName>
        <description>HR: Notify HR Coordinator 1 for Open Case After 24 hours</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Coordinator_Tier_1</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/HR_Case_Open_24_Hours</template>
    </alerts>
    <alerts>
        <fullName>HR_Notify_HR_Coordinator_2_for_Open_Case_After_24_hours</fullName>
        <description>HR: Notify HR Coordinator 2 for Open Case After 24 hours</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Coordinator_Tier_2</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/HR_Case_Open_24_Hours</template>
    </alerts>
    <alerts>
        <fullName>HR_Notify_HR_Coordinator_3_for_Open_Case_After_24_hours</fullName>
        <description>HR: Notify HR Coordinator 3 for Open Case After 24 hours</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Coordinator_Tier_3</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/HR_Case_Open_24_Hours</template>
    </alerts>
    <alerts>
        <fullName>HR_Notify_User_for_Waiting_for_Employee_48_hrs</fullName>
        <description>HR: Notify User for Waiting for Employee &gt; 48 hrs</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/Case_Updated_to_Waiting_for_Employee</template>
    </alerts>
    <alerts>
        <fullName>HR_Notify_User_when_Case_is_Resolved</fullName>
        <description>HR: Notify User when Case is Resolved</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/HR_Case_Has_Been_Resolved</template>
    </alerts>
    <alerts>
        <fullName>New_Case_escalation_email</fullName>
        <description>New Case escalation email</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Coordinator_Tier_1</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/HR_Case_Open_24_Hours</template>
    </alerts>
    <alerts>
        <fullName>New_hire_case_escalation_email</fullName>
        <description>New hire case escalation email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/Newly_Case_Open_12</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner_if_a_Comment_is_Added_to_a_Case</fullName>
        <description>Notify Owner if a Comment is Added to a Case</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>hrconnect@biogen.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Biogen_HR_Connect/A_Comment_Has_Been_Added_to_a_Case_You_Own</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Case_Creator_When_Status_is_Changed_to_Waiting_for_Employee</fullName>
        <description>Send Email to Case Creator When Status is Changed to Waiting for Employee</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/Case_Updated_to_Waiting_for_Employee</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Reason_ADA</fullName>
        <field>Reason</field>
        <literalValue>ADA Case</literalValue>
        <name>Case Reason - ADA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Reason_Recruitment</fullName>
        <field>Reason</field>
        <literalValue>Recruitment Case</literalValue>
        <name>Case Reason - Recruitment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Customer_First_Contact_Resolution_Update</fullName>
        <field>Customer_First_Contact_Resolution__c</field>
        <literalValue>1</literalValue>
        <name>Customer First Contact Resolution Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_to_Add_COE_Escalated_Date</fullName>
        <description>Field update to assign current date to the COE Case Escalated Date whenever case gets escalated to COEs</description>
        <field>COE_Escalated_Date__c</field>
        <formula>now()</formula>
        <name>Field Update to Add COE Escalated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_to_Add_COE_Escalation_Statu</fullName>
        <description>Field update that defaults COE Case Escalation Status to &apos;Escalated&apos; whenever case gets escalated to COEs</description>
        <field>COE_Escalation_Status__c</field>
        <literalValue>Escalated</literalValue>
        <name>Field Update to Add COE Escalation Statu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_to_Add_COE_Resolved_Date</fullName>
        <description>Field update to assign current date to the COE Resolved Date whenever case gets resolved by COEs</description>
        <field>COE_Resolved_Date__c</field>
        <formula>now()</formula>
        <name>Field Update to Add COE Resolved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_to_Add_Case_Reescalate_Date</fullName>
        <description>Field update to assign current date to the Re-escalated Date whenever case gets re-escalated by HROps</description>
        <field>COE_Re_escalate_Date__c</field>
        <formula>now()</formula>
        <name>Field Update to Add Case Reescalate Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_to_Add_First_Escalated_Date</fullName>
        <field>First_Escalation_Date_to_COE__c</field>
        <formula>now()</formula>
        <name>Field Update to Add First Escalated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Make_Priority_High</fullName>
        <description>Change Priority to High for Executive Cases</description>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Make Priority High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Process_First_Contact_Resolution</fullName>
        <field>Process_First_Contact_Resolution__c</field>
        <literalValue>1</literalValue>
        <name>Process First Contact Resolution</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Priority_to_High</fullName>
        <description>If the Case is VIP then set it to High Priority</description>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Set Priority to High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Origin</fullName>
        <field>Origin</field>
        <literalValue>Manual</literalValue>
        <name>Update Case Origin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Reason_Mobility</fullName>
        <field>Reason</field>
        <literalValue>Mobility Case</literalValue>
        <name>Update Case Reason - Mobility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_Assigned</fullName>
        <field>Case_Status__c</field>
        <literalValue>Assigned</literalValue>
        <name>Update Case Status: Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_Closed</fullName>
        <field>Case_Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Update Case Status: Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_In_Progress</fullName>
        <field>Case_Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>Update Case Status: In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_Open</fullName>
        <field>Case_Status__c</field>
        <literalValue>Open</literalValue>
        <name>Update Case Status - Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_Reopen</fullName>
        <field>Case_Status__c</field>
        <literalValue>Reopen</literalValue>
        <name>Update Case Status: Reopen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_Resolved</fullName>
        <field>Case_Status__c</field>
        <literalValue>Resolved</literalValue>
        <name>Update Case Status: Resolved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Time_Case_On_Hold</fullName>
        <description>Captures the date and time when status of case changes into On Hold</description>
        <field>Date_Time_On_Hold__c</field>
        <formula>NOW()</formula>
        <name>Update Date Time Case On Hold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Executive_Field</fullName>
        <field>Executive_Case__c</field>
        <literalValue>1</literalValue>
        <name>Update Executive Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Email_Field</fullName>
        <field>Owner_Email__c</field>
        <formula>Owner:User.Email</formula>
        <name>Update Owner Email Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Case_Status_Waiting</fullName>
        <field>Case_Status__c</field>
        <literalValue>Waiting for Employee</literalValue>
        <name>Updated Case Status: Waiting</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>COE Case Rescalate - Notify Owner</fullName>
        <actions>
            <name>Email_alert_to_notify_Case_Owner_for_Case_Re_Escalation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_Update_to_Add_COE_Escalation_Statu</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Field_Update_to_Add_Case_Reescalate_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.COE_Re_escalate__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Case is re-escalated to COE, do following:
1. Notify the Case Owner 
2. Update Re-escalated Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COE Escalation Status Change - Notify Owner</fullName>
        <actions>
            <name>Email_alert_to_notify_Case_Owner_for_Escalation_Status_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.COE_Escalation_Status__c</field>
            <operation>equals</operation>
            <value>More Info Required,Incorrect Escalation</value>
        </criteriaItems>
        <description>Notify the Case Owner when COE Escalation Status changes to one of the following
1. More Info Required
2. Incorrect Escalation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COE Escalation Status Change to Resolved</fullName>
        <actions>
            <name>Email_alert_to_notify_Case_Owner_for_Escalation_Status_Change</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_Update_to_Add_COE_Resolved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.COE_Escalation_Status__c</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <description>When COE Escalation Status changes to &apos;Resolved&apos;
1. Notify Case Owner for Escalation Status Change 
2. Update Field to Add COE Resolved Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>COE Escalation Status Change to Transferred to IT</fullName>
        <actions>
            <name>Email_alert_to_notify_Case_Owner_and_IT_for_Escalation_Status_Change_to_Transfer</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_alert_to_notify_IT_for_Escalation_Status_Change_to_Transferred_to_IT</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_Update_to_Add_COE_Resolved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.COE_Escalation_Status__c</field>
            <operation>equals</operation>
            <value>Transferred to IT</value>
        </criteriaItems>
        <description>When COE Escalation Status changes to &apos;Transferred to IT&apos;
1. Notify Case Owner for Escalation Status Change 
2. Send email to VSM ticket generation email address
2. Update Field to Add COE Resolved Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Closed</fullName>
        <actions>
            <name>Case_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>General,Position Request Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>ONLY FOR GENERAL AND Position Request Form CASES.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Escalated to COE - HRIS</fullName>
        <actions>
            <name>Email_alert_to_notify_Case_Escalated_to_HRIS_COE</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_Update_to_Add_COE_Escalated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Field_Update_to_Add_COE_Escalation_Statu</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Escalated_to_COE__c</field>
            <operation>equals</operation>
            <value>HRIS</value>
        </criteriaItems>
        <description>When case is escalated to HRIS COE:
1. Everyone in HRIS Team public group gets notified via email 
2. COE Escalation Status defaults to &apos;Escalated&apos;
3. COE Escalated Date gets populated to current date/time</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Escalated to COE - IT Functional</fullName>
        <actions>
            <name>Email_alert_to_notify_Case_Escalated_to_COE_IT_Functional</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_Update_to_Add_COE_Escalated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Escalated_to_COE__c</field>
            <operation>equals</operation>
            <value>IT Functional</value>
        </criteriaItems>
        <description>When case is escalated to IT Functional COE:
1. Everyone in COE - IT Functional public group gets notified via email 
2. COE Escalation Status defaults to &apos;Escalated&apos;
3. COE Escalated Date gets populated to current date/time</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Escalated to COE - IT Support</fullName>
        <actions>
            <name>Email_alert_to_notify_Case_Escalated_to_COE_IT_Support</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Field_Update_to_Add_COE_Escalated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Escalated_to_COE__c</field>
            <operation>equals</operation>
            <value>IT Support</value>
        </criteriaItems>
        <description>When case is escalated to IT Support COE:
1. Everyone in COE - IT Support public group gets notified via email 
2. COE Escalation Status defaults to &apos;Escalated&apos;
3. COE Escalated Date gets populated to current date/time</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Escalated to COE - Update First Escalation Date</fullName>
        <actions>
            <name>Field_Update_to_Add_First_Escalated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Escalated_to_COE__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.First_Escalation_Date_to_COE__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When case is escalated to any COE for the first time:
1. COE First Escalated Date gets populated to current date/time</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Reason - ADA</fullName>
        <actions>
            <name>Case_Reason_ADA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>ADA/Leave Administration</value>
        </criteriaItems>
        <description>Workflow to update the case reason on all Global Mobility cases to ADA Case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Reason - Mobility</fullName>
        <actions>
            <name>Update_Case_Reason_Mobility</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Relocation or Assignment Authorization</value>
        </criteriaItems>
        <description>Workflow to update the case reason on all Global Mobility cases to Mobility.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Reason - Recruitment</fullName>
        <actions>
            <name>Case_Reason_Recruitment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Position Request Form,Interview Request Form,Phone Interview Request Form</value>
        </criteriaItems>
        <description>Workflow to update the case reason on all Global Mobility cases to Recruitment.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Resolved</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Status Updated to Waiting for Employee</fullName>
        <actions>
            <name>Send_Email_to_Case_Creator_When_Status_is_Changed_to_Waiting_for_Employee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for Employee</value>
        </criteriaItems>
        <description>Workflow to send an email to the case creator when the Case Status is updated to Waiting for Employee.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Submitted On Behalf of Other User Closed</fullName>
        <actions>
            <name>Case_On_Behalf_of_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow to send email to user who created a case for another user when the case is closed.</description>
        <formula>NOT(ISBLANK( On_Behalf_Of2__c )) &amp;&amp;  (ISPICKVAL(Status, &quot;Closed&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Customer First Contact Resolution</fullName>
        <actions>
            <name>Customer_First_Contact_Resolution_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set as “Yes” when the following conditions are met:

1)        One email exchange with the customer
2)        Case set as resolved</description>
        <formula>Number_of_Email_Sent__c  == 1 &amp;&amp;
ISPICKVAL (Status, &apos;Resolved&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EU Case Holiday Shutdown message</fullName>
        <actions>
            <name>Email_Alert_EU_Case_Holiday_Shutdown_message</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>General</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_Country__c</field>
            <operation>equals</operation>
            <value>Spain,France,Italy,United Kingdom,Ireland,Germany,Denmark,Sweden,Norway,Finland,Switzerland,Zug,Czech Republic,Austria,Hungary,Poland,Slovenia,Slovakia,India,Japan,Australia,New Zealand,Netherlands,Belgium,Portugal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_State__c</field>
            <operation>equals</operation>
            <value>Hillerod,Zug</value>
        </criteriaItems>
        <description>Holiday Shutdown message to go out for the EU General Cases</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Notification - New Hire Case Open %3E 12 Hours</fullName>
        <active>false</active>
        <description>Workflow to send email notification to a user when a new hire case has been open for 12 hours.</description>
        <formula>OR (CONTAINS( CreatedBy.Profile.Name, &quot;New&quot;),
(CONTAINS( Contact.Profile_Name__c , &quot;New&quot;))
&amp;&amp; ISPICKVAL( Status , &quot;Open&quot;))
&amp;&amp; RecordType.Name &lt;&gt; &apos;Employee Relations&apos;
&amp;&amp; RecordType.Name &lt;&gt; &apos;HR Investigations&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>New_hire_case_escalation_email</name>
                <type>Alert</type>
            </actions>
            <timeLength>12</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Executive Case Checkbox</fullName>
        <actions>
            <name>Make_Priority_High</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Executive_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 or 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Contact.Profile_Name__c</field>
            <operation>equals</operation>
            <value>Exec</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Profile_Name__c</field>
            <operation>equals</operation>
            <value>New Hire Exec</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Grade__c</field>
            <operation>greaterOrEqual</operation>
            <value>20</value>
        </criteriaItems>
        <description>Checkbox to automatically update Executive Case Checkbox (used for reporting) when a case is assigned to the Executive Queue.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Executive Case Open %3E 4 Hours</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Executive_Case__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Executive_Case_Open_4_hours</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR%3A Case Resolved Notification</fullName>
        <actions>
            <name>HR_Notify_User_when_Case_is_Resolved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved,Resolved - Customer Referral</value>
        </criteriaItems>
        <description>Case Resolved Notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>HR%3A Case Updated to Waiting for Employee</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for Employee</value>
        </criteriaItems>
        <description>Notify after 48 hrs if the case status is Waiting for Employee</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>HR_Notify_User_for_Waiting_for_Employee_48_hrs</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>48</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR%3A Case Updated to Waiting for HRBP</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for Vendor,Waiting for HRBP</value>
        </criteriaItems>
        <description>Notify Case Owner when the case has been in Wating for HRBP for &gt; 48 hrs</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>HR_Notify_Case_Owner_for_Case_Waiting_for_HRBP</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>48</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR%3A Case Updated to Waiting for Vendor</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for Vendor</value>
        </criteriaItems>
        <description>Notify Case Owner when the case has been in wating for Vendor for &gt; 48 hrs</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>HR_Notify_Case_Owner_for_Case_Waiting_for_Vendor</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.LastModifiedDate</offsetFromField>
            <timeLength>48</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR%3A Tier 1 Case Open Notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>HR Connect - Americas,HR Connect - EU &amp; Asia</value>
        </criteriaItems>
        <description>Tier 1 Case Open Notification:
1. &gt; 24 hrs - HR Coordinator 1 Group</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>HR_Group_Manager_Case_Open_25_days</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>HR_Notify_Group_Manager_Case_Open_10_Days</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>HR_Notify_HR_Coordinator_1_for_Open_Case_After_24_hours</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR%3A Tier 2 Case Open Notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>HR Connect - Americas,HR Connect - EU &amp; Asia,HRIT Support,HR Connect Tier 2</value>
        </criteriaItems>
        <description>Tier 2 Case Open Notification:
1. &gt; 24 hrs - HR Coordinator 2 Group</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>HR_Group_Manager_Case_Open_25_days</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>HR_Notify_Group_Manager_Case_Open_10_Days</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>HR_Notify_HR_Coordinator_2_for_Open_Case_After_24_hours</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR%3A Tier 3 Case Open Notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Group__c</field>
            <operation>equals</operation>
            <value>HR Connect - Americas,HR Connect - EU &amp; Asia,HRIT Support,HR Connect Tier 2,People Systems,People Development,Benefits,Compensation,Global Mobility,HRIT Functional,HR Services,Employee Relations,Leave Management</value>
        </criteriaItems>
        <description>Tier 3 Case Open Notification:
1. &gt; 24 hrs - HR Coordinator 3 Group</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>HR_Group_Manager_Case_Open_25_days</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>HR_Notify_Group_Manager_Case_Open_10_Days</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>HR_Notify_HR_Coordinator_3_for_Open_Case_After_24_hours</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notify COE Case Owner</fullName>
        <actions>
            <name>Email_alert_to_notify_COE_Case_Owner_when_added</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notify the COE Case Owner when COE Case Owner is added</description>
        <formula>ISCHANGED( COE_Owner__c ) &amp;&amp; COE_Owner__c != null &amp;&amp;  $User.Id != COE_Owner__r.Id</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Case Owner%2C Case Reopened</fullName>
        <actions>
            <name>A_Case_You_Own_has_Been_Reopened</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Reopened__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Process First Contact Resolution</fullName>
        <actions>
            <name>Process_First_Contact_Resolution</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Process First Contact Resolution: Set as “Yes” when :
Number of Group Transfers = 0 &amp;&amp; Number of Assignees = 1&amp;&amp;  Time to Response &gt;= 24 hours from the open date/time &amp;&amp; Case status = Resolved &amp;&amp; KB Source = &apos;Knowledge Base&apos;</description>
        <formula>Number_of_Assignee_Change__c  == 1 &amp;&amp;
 Number_of_Escalations__c == 0 &amp;&amp;
 ISPICKVAL (Status, &apos;Resolved&apos;) &amp;&amp;
 ISPICKVAL(Knowledge_Source__c,&apos;Knowledge Base&apos;) &amp;&amp;
 (CreatedDate - NOW()) &gt;= 24</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Public Comment Added - Notify Contact</fullName>
        <actions>
            <name>Contact_Email_when_Comment_Added_to_Case</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Workflow to update a contact when a public comment is added to a case.</description>
        <formula>ISCHANGED( Last_Comment_Date__c ) &amp;&amp;  Last_Comment_Public__c == &apos;true&apos; &amp;&amp;  (  RecordType.Name  &lt;&gt; &apos;ADA&apos; &amp;&amp; RecordType.Name &lt;&gt; &apos;Employee Relations&apos; &amp;&amp; RecordType.Name &lt;&gt; &apos;HR Investigations&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Public Comment Added - Notify Owner</fullName>
        <actions>
            <name>Notify_Owner_if_a_Comment_is_Added_to_a_Case</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Workflow to update a owner when a public comment is added to a case.</description>
        <formula>ISCHANGED( Last_Comment_Date_Owner_Notification__c  ) &amp;&amp;  Last_Comment_Public__c = &apos;true&apos; &amp;&amp; 
(  RecordType.Name  &lt;&gt; &apos;ADA&apos; &amp;&amp; RecordType.Name &lt;&gt; &apos;Employee Relations&apos; &amp;&amp; RecordType.Name &lt;&gt; &apos;HR Investigations&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send 2nd ADA Due Date Notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.ADA_Due_Date_2__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to send notice to ADA Team upon the 2nd ADA Due Date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>An_ADA_Form_is_at_it_s_2nd_Due_Date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.ADA_Due_Date_2__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send ADA Due Date Notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.X1st_ADA_Due_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to send notice to ADA Team upon the 1st ADA Due Date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>An_ADA_For</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.X1st_ADA_Due_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Status - Assigned%3A Update Case Status</fullName>
        <actions>
            <name>Update_Case_Status_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <description>Workflow to update the Case Status (Portal User) field to Assigned when standard Status = Assigned</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status - Closed%3A Update Case Status</fullName>
        <actions>
            <name>Update_Case_Status_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Workflow to update the Case Status (Portal User) field to Closed when standard Status = Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status - In Progress%3A Update Case Status</fullName>
        <actions>
            <name>Update_Case_Status_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress,Waiting for Vendor,Waiting for HRBP,Waiting for COE</value>
        </criteriaItems>
        <description>Workflow to update the Case Status (Portal User) field to In Progress when standard Status = In Progress</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status - Open%3A Update Case Status</fullName>
        <actions>
            <name>Update_Case_Status_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <description>Workflow to update the Case Status (Portal User) field to Open when standard Status = Open</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status - Reopen%3A Update Case Status</fullName>
        <actions>
            <name>Update_Case_Status_Reopen</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Reopen</value>
        </criteriaItems>
        <description>Workflow to update the Case Status (Portal User) field to Closed when standard Status = Reopen</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status - Resolved%3A Update Case Status</fullName>
        <actions>
            <name>Update_Case_Status_Resolved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <description>Workflow to update the Case Status (Portal User) field to Closed when standard Status = Resolved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Status - Waiting%3A Update Case Status</fullName>
        <actions>
            <name>Updated_Case_Status_Waiting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for Employee</value>
        </criteriaItems>
        <description>Workflow to update the Case Status (Portal User) field to Waiting for Employee when standard Status =  Waiting for Employee</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>US and Executive Case Holiday Shutdown message</fullName>
        <actions>
            <name>Email_Alert_US_and_Executive_Case_Holiday_Shutdown_message</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>General</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>equals</operation>
            <value>United States of America,Canada,Argentina,Chile,Uruguay,Brazil</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Executive_Case__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Holiday Shutdown message to go out for the US and Executive General Cases</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Origin - Interview Request</fullName>
        <actions>
            <name>Update_Case_Origin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Position Request Form,Interview Request Form,Phone Interview Request Form,ADA/Leave Administration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Workflow to update the Case Origin to manual if a user creates an Interview Request and doesn’t update the Case Origin field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Owner Email</fullName>
        <actions>
            <name>Update_Owner_Email_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to get the case owner email and use in the workflow sent to case owner when a portal user adds a comment.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Date Time Case On Hold</fullName>
        <actions>
            <name>Update_Date_Time_Case_On_Hold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <description>Captures the date and time when status of case changes into On Hold</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VIP Case</fullName>
        <actions>
            <name>Set_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.VIP_Case__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If Case is VIP then set it to high Priority</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
