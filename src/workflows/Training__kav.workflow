<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_Message_for_Training_Article</fullName>
        <ccEmails>articlearchiveemailservice@1zocvol2dwgtm7eoe7rt59xysvsatv5zhoettkd74808dzx2ik.g-iu14mae.na11.apex.salesforce.com</ccEmails>
        <description>Send Email Message for Training Article</description>
        <protected>false</protected>
        <recipients>
            <recipient>manish.shrestha@biogen.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/Archive_Training_Article_Email</template>
    </alerts>
    <alerts>
        <fullName>Training_Article_Has_Been_Approved</fullName>
        <description>Training Article Has Been Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/Training_Article_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Validation_Status</fullName>
        <field>ValidationStatus</field>
        <literalValue>Under Review</literalValue>
        <name>Update Validation Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Validation_Status_Published</fullName>
        <field>ValidationStatus</field>
        <literalValue>Published</literalValue>
        <name>Update Validation Status - Published</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Validation_Status_Rejected</fullName>
        <field>ValidationStatus</field>
        <literalValue>Rejected</literalValue>
        <name>Update Validation Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validation_Status_Archived</fullName>
        <field>ValidationStatus</field>
        <literalValue>Archived</literalValue>
        <name>Validation Status - Archived</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validation_Status_Published</fullName>
        <field>ValidationStatus</field>
        <literalValue>Published</literalValue>
        <name>Validation Status - Published</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish_Article</fullName>
        <action>Publish</action>
        <label>Publish Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>Archive Training Article</fullName>
        <actions>
            <name>Send_Email_Message_for_Training_Article</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Training__kav.Archive_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to send email to Email Service when archive date is added for the article of type &apos;Training&apos;. Email service will then schedule the archiving of article.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Train - Pub Status - Published</fullName>
        <actions>
            <name>Validation_Status_Published</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training__kav.PublishStatus</field>
            <operation>equals</operation>
            <value>Published</value>
        </criteriaItems>
        <description>Workflow to keep the publication status and the validation status fields in sync.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Train - Pub Status -Archived</fullName>
        <actions>
            <name>Validation_Status_Archived</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Training__kav.PublishStatus</field>
            <operation>equals</operation>
            <value>Archived</value>
        </criteriaItems>
        <description>Workflow to keep the publication status and the validation status fields in sync.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
