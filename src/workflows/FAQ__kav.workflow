<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FAQ_has_been_approved</fullName>
        <description>FAQ has been approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/FAQ_Article_Approved</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_Message_for_FAQ_Article</fullName>
        <ccEmails>articlearchiveemailservice@1zocvol2dwgtm7eoe7rt59xysvsatv5zhoettkd74808dzx2ik.g-iu14mae.na11.apex.salesforce.com</ccEmails>
        <description>Send Email Message for FAQ Article</description>
        <protected>false</protected>
        <recipients>
            <recipient>manish.shrestha@biogen.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Biogen_HR_Connect/Archive_FAQ_Article_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>FAQ_Pub_Status_Archived</fullName>
        <field>ValidationStatus</field>
        <literalValue>Archived</literalValue>
        <name>FAQ - Pub Status Archived</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FAQ_Update_Validation_Status_Publish</fullName>
        <field>ValidationStatus</field>
        <literalValue>Published</literalValue>
        <name>FAQ - Update Validation Status - Publish</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Validation_Status</fullName>
        <field>ValidationStatus</field>
        <literalValue>Under Review</literalValue>
        <name>Update Validation Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Validation_Status_Published</fullName>
        <description>Update the validation status to Published when an FAQ article is Approved.</description>
        <field>ValidationStatus</field>
        <literalValue>Published</literalValue>
        <name>Update Validation Status - Published</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Validation_Status_Rejected</fullName>
        <description>Update the validation status to rejected if an article has been submitted and needs to be revised.</description>
        <field>ValidationStatus</field>
        <literalValue>Rejected</literalValue>
        <name>Update Validation Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish_Article</fullName>
        <action>Publish</action>
        <description>This step publishes the article as a new article.</description>
        <label>Publish Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>Archive_FAQ_Article</fullName>
        <actions>
            <name>Send_Email_Message_for_FAQ_Article</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>FAQ__kav.Archive_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to send email to Email Service when archive date is added for the article of type &apos;FAQ&apos;. Email service will then schedule the archiving of article.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FAQ - Pub Status %3D Validation Status</fullName>
        <actions>
            <name>FAQ_Update_Validation_Status_Publish</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>FAQ__kav.PublishStatus</field>
            <operation>equals</operation>
            <value>Published</value>
        </criteriaItems>
        <description>Workflow to keep the publication status and the validation status fields in sync.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FAQ - Pub Status %3D Validation Status Archived</fullName>
        <actions>
            <name>FAQ_Pub_Status_Archived</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>FAQ__kav.PublishStatus</field>
            <operation>equals</operation>
            <value>Archived</value>
        </criteriaItems>
        <description>Workflow to keep the publication status and the validation status fields in sync.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
